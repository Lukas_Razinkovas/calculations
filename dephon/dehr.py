import os
import re
import pickle
import numpy as np
import h5py
from scipy.constants import physical_constants, angstrom
from scipy.constants import hbar as hbar_J_s
from vasp import Poscar


hbar_in_ev_s = physical_constants["Planck constant in eV s"][0]/(2*np.pi)
Ha_in_ev = physical_constants["Hartree energy in eV"][0]
bohr_in_angstr = physical_constants["Bohr radius"][0]*1e10
mev_to_rad_per_second = 1e-3/hbar_in_ev_s
atomic_angular_freq_unit = Ha_in_ev/hbar_in_ev_s


def remove_pb_artifacts(atoms1, atoms2):
    """
    Removes periodic boundary condition artifacts.
    Changes only atoms2.
    """
    positions1 = atoms1.get_positions()
    positions2 = atoms2.get_positions()

    new_positions2 = []
    for nr, (pos1, pos2) in enumerate(zip(positions1, positions2)):
        v, d = atoms1.get_separation_vector(pos1, pos2)
        v_, d_ = atoms1.get_separation_vector(pos1, pos2, direct=True)
        if d != d_:
            new_positions2.append(pos1-v)
        else:
            new_positions2.append(pos2)

    atoms2.set_positions(new_positions2)


def calculate_displacements(atoms1, atoms2):

    positions1 = atoms1.get_positions()
    positions2 = atoms2.get_positions()

    displ = np.zeros(positions1.shape, dtype=float)
    for nr, (pos1, pos2) in enumerate(zip(positions1, positions2)):
        v, d = atoms1.get_separation_vector(pos2, pos1)
        displ[nr] = v

    distances = np.apply_along_axis(np.linalg.norm, 1, displ)

    return displ, distances


def read_displacements(poscar1, poscar2):
    if isinstance(poscar1, str):
        atoms1 = Poscar(poscar1).create_atoms()
        atoms2 = Poscar(poscar2).create_atoms()
    else:
        atoms1 = poscar1
        atoms2 = poscar2

    remove_pb_artifacts(atoms1, atoms2)

    ##############################
    #         Check
    ##############################

    for pos1, pos2 in zip(atoms1.get_positions(), atoms2.get_positions()):
        assert np.allclose(pos1, pos2, atol=0.2, rtol=0), pos1 - pos2

    mass_center1 = atoms1.get_mass_center()
    mass_center2 = atoms2.get_mass_center()

    atoms2.set_positions(
        atoms2.get_positions() - (mass_center2 - mass_center1))

    displ, distances = calculate_displacements(atoms1, atoms2)

    return displ, distances, atoms1, atoms2


def read_embeded_atoms(filepath):
    with open(filepath, "rb") as f:
        atoms = pickle.load(f)

    return atoms


def iter_modes(hdf5dir, sym=None, units="au"):
    """
    Iterates calculated modes and returns angular frequencie and displacements
    """

    # Yields freq in (2 pi Hz) and displ in dimensionless unxits
    hdf5file = os.path.join(hdf5dir, "modes.hdf5")
    f = h5py.File(hdf5file, "r")

    # Frequency is read in meV
    if units.lower() == "au":
        freq_conversion = mev_to_rad_per_second/atomic_angular_freq_unit
        dist_conversion = 1/bohr_in_angstr
    elif units.lower() == "si":
        freq_conversion = mev_to_rad_per_second
        dist_conversion = angstrom

    print("Distance_conversion: {:.4}".format(dist_conversion))
    print("Freq_conversion: {:.4}".format(freq_conversion))

    freq = f["frequencies"][:].T[0]*freq_conversion

    ##############################
    #          Iteration
    ##############################

    if sym is not None:
        hdf5file2 = os.path.join(hdf5dir, "mode_analysis.hdf5")
        f2 = h5py.File(hdf5file2, "r")
        iterator = np.where(b'Exy' == f2["irreps"][:])[0]
        f2.close()
    else:
        iterator = list(range(len(freq)))

    for cnt, nr in enumerate(iterator):
        if cnt % 100 == 0:
            print("{}/{}".format(cnt, len(iterator)), end="\r")

        if nr < 3:
            continue

        displ = f["displacements"][nr]*dist_conversion

        yield freq[nr], displ

    f.close()


def fc_displ_from_relaxation(eatoms, bulk_atoms, displ, hdf5dir,
                             sym=None,
                             units="au", weight=False):

    if units.lower() == "au":
        dist_conversion = 1/bohr_in_angstr
        masses = eatoms.get_masses(units="me").repeat(3)

    elif units.lower() == "si":
        dist_conversion = angstrom
        masses = eatoms.get_masses(units="kg").repeat(3)

    print("dist_conversion:", dist_conversion)
    print("masses:", set(masses))

    masses_squared = np.sqrt(masses)

    # --------------------------- #
    #          Embeding           #
    # --------------------------- #

    eatoms.sort_positions()
    displacements = np.zeros(len(eatoms)*3).reshape(len(eatoms), 3)
    for nr, (pos, d) in enumerate(zip(bulk_atoms.get_positions(), displ)):
        indx = eatoms.find_by_position(pos, pbc=True, method=3)
        displacements[indx] = np.array(d)*dist_conversion

    print("NORM:", np.apply_along_axis(np.linalg.norm, 1, displacements).sum())

    displacements = displacements.flatten()
    weighted_displ = displacements*masses_squared
    print("Weighted_displ:", max(weighted_displ))

    fc_displ = []
    frequencies = []
    print("Weighted:", weight)
    for nr, (freq, mode_displ) in enumerate(
            iter_modes(hdf5dir, sym=sym, units=units)):
        if weight:
            mode_displ *= masses_squared
        mode_displ = mode_displ/np.linalg.norm(mode_displ)

        frequencies.append(freq)
        fc_displ.append(weighted_displ.dot(mode_displ))

    return np.array(frequencies), np.array(fc_displ)


def calculate_fc_displacements(poscar1, poscar2, bulkg_poscar,
                               hdf5dir, from_forces=False, units="si",
                               sym=None, weight=False):

    if from_forces:
        raise NotImplemented("!!!")
    else:
        print("Calculating fc displacements from displacements")
        displ, distances, atoms1, atoms2 = read_displacements(poscar1, poscar2)

    # Read corresponding bulk positions
    bulk_atoms = Poscar(bulkg_poscar).create_atoms()

    # Read atoms of embeded cell
    eatoms = read_embeded_atoms(os.path.join(hdf5dir, "atoms.p"))

    #####################################################
    #  Equalise lattice constants for bulk and embeded
    #####################################################

    embeded_size = int(re.search("x(.*?)x", hdf5dir).group(1))
    bulk_size = int(re.search("x(.*?)x", bulkg_poscar).group(1))

    elc = eatoms.get_cell()[0, 0]/embeded_size
    blc = bulk_atoms.get_cell()[0, 0]/bulk_size
    eatoms.set_cell(eatoms.get_cell()*blc/elc)

    ##############################
    #       Error checking
    ##############################

    tmp = bulk_atoms.copy()
    tmp.set_cell(atoms1.get_cell())
    remove_pb_artifacts(bulk_atoms, atoms1)
    remove_pb_artifacts(bulk_atoms, atoms2)

    for indx, (pos1, pos2) in enumerate(
            zip(tmp.get_positions(), atoms1.get_positions())):
        assert np.allclose(pos1, pos2, atol=0.2, rtol=0), (
            "Positions strongly differ from bulk geometry:",
            indx, pos1, pos2)

    for indx, (pos1, pos2) in enumerate(
            zip(atoms2.get_positions(), atoms1.get_positions())):
        assert np.allclose(pos1, pos2, atol=0.2, rtol=0), (
            "Positions strongly differ from each other:",
            indx, pos1, pos2)

    if from_forces:
        raise NotImplemented("!!!")
    else:
        freq, fc_displ = fc_displ_from_relaxation(
            eatoms, bulk_atoms, displ, hdf5dir=hdf5dir, units=units,
            sym=sym, weight=weight)

    return freq, fc_displ


def gaussian_smoothing(x, y, sig=6, n=1000):
    def gaussian(x, mu, sig=sig):
        return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
            2 * np.power(sig, 2.)))

    x_new = np.linspace(0, 200, n)
    y_new = np.zeros(len(x_new))

    for _x, _y in zip(x, y):
        y_new += _y*gaussian(x_new, _x)

    return x_new, y_new


def calculate_hr_factors(poscar1, poscar2, bulkg_poscar,
                         hdf5dir, from_forces=False, units="au",
                         sym=None, weight=False):

    freq, fc_displ = calculate_fc_displacements(
        poscar1, poscar2, bulkg_poscar,
        hdf5dir, from_forces=from_forces, units=units, sym=sym,
        weight=weight)

    if units.lower() == "au":
        hbar = 1
    elif units.lower() == "si":
        hbar = hbar_J_s

    hr_factors = (np.power(fc_displ, 2)*freq)/(2*hbar)

    freq *= atomic_angular_freq_unit/mev_to_rad_per_second
    data = np.array([freq, hr_factors]).T

    return data


def read_most_displaced(poscar1, poscar2, min_displ=0.014):
    atoms1 = Poscar(poscar1).create_atoms()
    atoms2 = Poscar(poscar2).create_atoms()
    displacements, distances = read_displacements(atoms1, atoms2)
    indx = np.where(distances > min_displ)[0]

    lattice_constant = atoms1.get_cell()[0, 0]

    rel_displacements = []
    numbers = []
    for nr in indx:
        rel_displacements.append(displacements[nr]/lattice_constant)
        numbers.append(nr)

    rel_displacements = np.array(rel_displacements)
    numbers = np.array([[i] for i in numbers])

    data = np.hstack((numbers, rel_displacements))

    print(data[0], numbers[0], rel_displacements[0])

    return data


if __name__ == "__main__":
    from deutils import ensure_dir

    output_dir1 = "../diamond/vasp/embeded/hr_factors/HSEdispnew/"
    output_dir12 = "../diamond/vasp/embeded/hr_factors/HSEdispnew-jt/"
    output_dir2 = "../diamond/vasp/embeded/hr_factors/HSEdispneww/"
    output_dir22 = "../diamond/vasp/embeded/hr_factors/HSEdispneww-jt/"

    for i in [output_dir1, output_dir12, output_dir2, output_dir22]:
        ensure_dir(i)

    for i in range(15, 16, 1):

        # ##############################
        # #        Not Weighted
        # ##############################

        data = calculate_hr_factors(
            poscar1="../diamond/vasp_hybrid/nv4x4x4/relaxed/POSCAR",
            poscar2="../diamond/vasp_hybrid/nve4x4x4/relaxed/POSCAR",
            bulkg_poscar="../diamond/vasp_hybrid/nv4x4x4/bulk_geometry/POSCAR",
            hdf5dir="../diamond/vasp/embeded/nv{0}x{0}x{0}".format(i))
        output_file = os.path.join(
            output_dir1,
            "nv{0}x{0}x{0}_hr_spectrum.dat".format(i))
        np.savetxt(
            output_file, data,
            header='frequencies, HR_factors', fmt=['%16.12f', '%18.12f'])

        # jt
        data = calculate_hr_factors(
            poscar1="../diamond/vasp_hybrid/nv4x4x4/relaxed/POSCAR",
            poscar2="../diamond/vasp_hybrid/nve4x4x4jt/relax/CONTCAR",
            bulkg_poscar="../diamond/vasp_hybrid/nv4x4x4/bulk_geometry/POSCAR",
            hdf5dir="../diamond/vasp/embeded/nv{0}x{0}x{0}".format(i))
        output_file = os.path.join(
            output_dir12, "nv{0}x{0}x{0}_hr_spectrum.dat".format(i))
        np.savetxt(
            output_file, data,
            header='frequencies, HR_factors', fmt=['%16.12f', '%18.12f'])

        ##############################
        #         Weighted
        ##############################

        data = calculate_hr_factors(
            poscar1="../diamond/vasp_hybrid/nv4x4x4/relaxed/POSCAR",
            poscar2="../diamond/vasp_hybrid/nve4x4x4/relaxed/POSCAR",
            bulkg_poscar="../diamond/vasp_hybrid/nv4x4x4/bulk_geometry/POSCAR",
            hdf5dir="../diamond/vasp/embeded/nv{0}x{0}x{0}".format(i),
            weight=True)
        output_file = os.path.join(
            output_dir2,
            "nv{0}x{0}x{0}_hr_spectrum.dat".format(i))
        np.savetxt(
            output_file, data,
            header='frequencies, HR_factors', fmt=['%16.12f', '%18.12f'])

        # jt
        data = calculate_hr_factors(
            poscar1="../diamond/vasp_hybrid/nv4x4x4/relaxed/POSCAR",
            poscar2="../diamond/vasp_hybrid/nve4x4x4jt/relax/CONTCAR",
            bulkg_poscar="../diamond/vasp_hybrid/nv4x4x4/bulk_geometry/POSCAR",
            hdf5dir="../diamond/vasp/embeded/nv{0}x{0}x{0}".format(i),
            weight=True)
        output_file = os.path.join(
            output_dir22, "nv{0}x{0}x{0}_hr_spectrum.dat".format(i))
        np.savetxt(
            output_file, data,
            header='frequencies, HR_factors', fmt=['%16.12f', '%18.12f'])
