"""
This module contains routines for:

 * displacement analysis
 * force difference analysis
 * Frank-Condon factor calculations
 * Vibrational and vibronic coupling strenght calculations

"""
import os
import re
import pickle
import numpy as np
import h5py
from scipy.constants import physical_constants, angstrom
from scipy.constants import hbar as hbar_J_s
from vasp import Poscar, Outcar, VaspXML
from deutils import gaussian_smoothing
from defc import ForceConstants
import logging

logger = logging.getLogger(__name__)

hbar_in_ev_s = physical_constants["Planck constant in eV s"][0]/(2*np.pi)
Ha_in_ev = physical_constants["Hartree energy in eV"][0]
bohr_in_angstr = physical_constants["Bohr radius"][0]*1e10
mev_to_rad_per_second = 1e-3/hbar_in_ev_s
atomic_angular_freq_unit = Ha_in_ev/hbar_in_ev_s


def remove_pb_artifacts(atoms1, atoms2, test=True):
    r"""
    Removes discrepancies because of the periodic boundary condition
    between simmilar supercells appearing during relaxation process.
    For example cyclic displacement:

    .. math::
        0 - \Delta \quad \to\quad \mbox{lattice constant} - \Delta.

    Changes only ``atoms2`` supercell.

    Args:
       atoms1 (deatoms.Atoms): reference supercell geometry
       atoms2 (deatoms.Atoms): geometry to be changed

    """
    positions1 = atoms1.get_positions()
    positions2 = atoms2.get_positions()

    new_positions2 = []
    seps = []
    for nr, (pos1, pos2) in enumerate(zip(positions1, positions2)):
        v, d = atoms1.get_separation_vector(pos1, pos2)
        seps.append(v)
        v_, d_ = atoms1.get_separation_vector(pos1, pos2, direct=True)
        if d != d_:
            logger.debug("Changing position of atom {}:\n {} -> {}".format(
                nr, pos2, pos1 + v))
            new_positions2.append(pos1 + v)
        else:
            new_positions2.append(pos2)

    atoms2.set_positions(new_positions2)


def align_mass_center(atoms1, atoms2):
    """
    Aligns mass center of second supercell with first one.

    Args:
       atoms1 (deatoms.Atoms): reference supercell geometry
       atoms2 (deatoms.Atoms): geometry to be alligned
    """
    mass_center1 = atoms1.get_mass_center()
    mass_center2 = atoms2.get_mass_center()
    logger.debug("Mass_center1: {}\nMass_center2: {}".format(
        mass_center1, mass_center2))

    atoms2.set_positions(
        atoms2.get_positions() - (mass_center2 - mass_center1))


def calculate_displacements(atoms1, atoms2, correct_discrepancies=True,
                            units="au"):
    """
    Calculates displacements of atoms assuming that the initial geometry
    was the same. Displacements are calculated substracting
    ``atoms2 - atoms1``.

    Args:
        atoms1 (Atoms): geometry
        atoms2 (Atoms): geometry
        units (str): units of displacements:

           * ``a`` or ``angstrom`` returns displacements in Angstroms
           * ``au`` or ``bohr`` returns displacements in Bohrs
           * ``si`` or ``m`` returns displacments in meters

        correct_discrepancies (bool): if True removes discrepancies comming
             from periodic boundary conditions and equalizes mass centers.
    Returns:
        displacements (``np.array``) with shape ``(nr_of_atoms, 3)``
    """
    if correct_discrepancies:
        logger.info("Removing pbc artifacts")
        remove_pb_artifacts(atoms1, atoms2)
        logger.info("Aligning mass centers")
        align_mass_center(atoms1, atoms2)

    positions1 = atoms1.get_positions()
    positions2 = atoms2.get_positions()

    displ = np.zeros(positions1.shape, dtype=float)
    max_displ = 0
    for nr, (pos1, pos2) in enumerate(zip(positions1, positions2)):
        v, d = atoms1.get_separation_vector(pos1, pos2)
        max_displ = d if d > max_displ else max_displ
        displ[nr] = v

    logging.info("Largest displacment: {} A".format(max_displ))
    if units.lower() in {"a", "angstrom", "angstroms"}:
        conversion_factor = 1
    elif units.lower() in {'au', "bohr", "bohr radius"}:
        conversion_factor = 1/bohr_in_angstr
    elif units.lower() in {'si', 'm'}:
        conversion_factor = angstrom
    else:
        raise NotImplementedError("Units {} not implemented!".format(units))

    return displ*conversion_factor


def read_displacements(
        poscar1, poscar2, units="au", correct_discrepancies=True):
    """
    Reads ``POSCAR``'s, removes pbc discrepancies, aligns mass centers
    and calculates displacements.
    """

    if isinstance(poscar1, str):
        atoms1 = Poscar(poscar1).create_atoms()
        atoms2 = Poscar(poscar2).create_atoms()
    else:
        atoms1 = poscar1
        atoms2 = poscar2

    displ = calculate_displacements(
        atoms1, atoms2, units=units,
        correct_discrepancies=correct_discrepancies)

    return displ, atoms1, atoms2


def read_force_differences(input1, input2, units="au"):
    """Reads force differences. If ``units='au'`` returns forces in
    ``[Ha/bohr]``. Substracts forces in manner
    ``forces(input2) - forces(input1)``

    Args:
      input1 (str): ``vasprun.xml`` or ``OUTCAR`` initial file
      input2 (str): ``vasprun.xml`` or ``OUTCAR`` final file
      units (str): force units:

        * ``au`` returns in ``[Ha/Bohr]``
        * ``vasp`` returns in ``[eV/Angstroms]``
        * ``si`` return in ``[J/m]``
    """
    forces = []
    max_force = []
    for inp in [input1, input2]:
        if "OUTCAR" in inp:
            _inp = Outcar(inp)
            forces.append(_inp.get_forces(units=units))
        else:
            _inp = VaspXML(inp)
            forces.append(_inp.get_forces(units=units))

        max_force = np.apply_along_axis(
            np.linalg.norm, 0, forces[-1]).max()
        logger.info("Max force in {}: {:.6f} {}".format(
            inp, max_force, units))

    return forces[1] - forces[0]


def read_embeded_atoms(filepath):
    with open(filepath, "rb") as f:
        atoms = pickle.load(f)

    return atoms


def iter_modes_from_hdf5dir(
        hdf5dir, atoms, sym=None, units="au", weighted=True):
    """
    Iterates modes and returns angular frequencies and displacements

    hdf5dir must contain ``modes.hdf5`` and ``mode_analysis.hdf5``.
    File ``modes.hdf5`` should contain direct mode displacements in Angstroms.
    Due to diagonalisation weighted displacements :math:`w_i = \sqrt{m_i}u_i`
    should be normalised.

    Args:
       hdf5dir (str): directory path where ``modes.hdf5`` and
          ``mode_analysis.hdf5`` files are saved.
       atoms (deatoms.Atoms): geometry of cell whose modes are stored
          in ``modes.hdf5`` file
       sym (str, None): if ``None`` all symmetry types will be iterated,
          otherwise iterates symmetry types given by ``sym``
       units (str): unit system for returned values:

          * ``au`` returns frequency in units ``[Ha/hbar]``
          * ``si`` returns frequency in units ``[2Pi Hz]``

       weighted (bool): it ``True`` returns weighted mode displacements
          :math:`w_i = \sqrt{m_i}u_i` else return direct ones.

    Yields:
       angular frequency, displacement, irreducible representation
    """

    # Yields freq in (2 pi Hz) and displ in dimensionless unxits
    hdf5file = os.path.join(hdf5dir, "modes.hdf5")
    assert os.path.exists(hdf5file), "modes.hdf5 is missing inside {}".format(
        hdf5dir)

    f = h5py.File(hdf5file, "r")

    ##############################
    #     Unit Conversion
    ##############################

    # frequency is read in meV displ in A
    if units.lower() == "au":
        freq_conversion = mev_to_rad_per_second/atomic_angular_freq_unit
        dist_conversion = 1/bohr_in_angstr
        masses = atoms.get_masses(units="me").repeat(3)
        dist_conversion *= np.sqrt(masses)
    elif units.lower() == "si":
        freq_conversion = mev_to_rad_per_second
        dist_conversion = angstrom
        masses = atoms.get_masses(units="kg").repeat(3)
        dist_conversion *= np.sqrt(masses)
    else:
        raise NotImplementedError("Unknown units system: {}".format(units))

    # Read frequencies
    freq = f["frequencies"][:].T[0]*freq_conversion

    # Read symmetry description
    mode_analysis = os.path.join(hdf5dir, "mode_analysis.hdf5")
    if os.path.exists(mode_analysis):
        f2 = h5py.File(mode_analysis, "r")
        irreps = np.array(f2["irreps"][:], str)
        f2.close()
        _indx = np.where((irreps == "Ex") | (irreps == "Ey"))
        irreps[_indx] = "Exy"
        if sym is not None:
            iterator = np.where(sym == irreps)[0]
            assert len(iterator) > 0
        else:
            iterator = list(range(len(freq)))
    else:
        if sym is not None:
            raise RuntimeError(
                "Symmetry filtering is imposible, because " +
                "'mode_analysis.hdf5' file is missing inside dir:" +
                "\n{}".format(hdf5dir))
        irreps = np.array(["Unknown"]*len(freq), dtype=str)
        iterator = list(range(len(freq)))

    for cnt, nr in enumerate(iterator):
        if cnt % 100 == 0 and len(iterator) > 100:
            print("{}/{}".format(cnt, len(iterator)), end="\r")

        if nr < 3:
            continue

        displ = f["displacements"][nr]*dist_conversion
        displ = displ/np.linalg.norm(displ)

        if not weighted:
            displ /= np.sqrt(masses)

        yield freq[nr], displ, irreps[nr]

    f.close()


def iter_modes_from_phonopy(phonopy_dir, atoms, sym=None, units="au",
                            weighted=True):

    # ------------------------------------------- #
    #    Calculate modes from force constants     #
    # ------------------------------------------- #

    fc = ForceConstants("vasp", phonopydir=phonopy_dir)
    fc.perform_cutoff()
    fc.symmetrize_fc(2)
    modes = fc.calculate_modes()
    modes.analyse()

    if units.lower() == "au":
        freq_conversion = mev_to_rad_per_second/atomic_angular_freq_unit
        masses = atoms.get_masses(units="me").repeat(3)
    elif units.lower() == "si":
        freq_conversion = mev_to_rad_per_second
        masses = atoms.get_masses(units="kg").repeat(3)
    else:
        raise NotImplementedError("Unknown units system: {}".format(units))

    if weighted is True:
        displacements = fc._evects
    else:
        displacements = fc.get_displacements(units=units)

    freq = modes.get_frequencies("meV")*freq_conversion
    irreps = np.array(modes.sym_types, str)

    _indx = np.where((irreps == "Ex") | (irreps == "Ey"))
    irreps[_indx] = "Exy"

    # Filted by symmetry
    if sym is not None:
        iterator = np.where(sym == irreps)[0]
        assert len(iterator) > 0
    else:
        iterator = list(range(len(freq)))

    for cnt, nr in enumerate(iterator):
        if cnt % 100 == 0 and len(iterator) > 100:
            print("{}/{}".format(cnt, len(iterator)), end="\r")

        if nr < 3:
            continue

        displ = displacements[nr]
        if weighted:
            displ = displ/np.linalg.norm(displ)

        yield freq[nr], displ, irreps[nr]


def too_far_from_defect(bulk_atoms, pos, defect_center):
    # ignore nonphysical displacements further than 0.95*0.5*lat_const
    if defect_center is not None:
        vec, dist = bulk_atoms.get_separation_vector(defect_center, pos)
        if dist > (bulk_atoms.get_cell()[0][0]/2)*0.95:
            return True

    return False


def fc_displ_from_relaxation(eatoms, bulk_atoms, displ,
                             hdf5dir=None, phonopy_dir=None,
                             sym=None, units="au", defect_center=None):
    r"""
    Relation used for FC displacement calculations:

    .. math::
        \Delta Q_k = \sum_{n}\Delta w_{n} w_{n}^{k}

    where :math:`w_n^k=u_n^k \sqrt{m_n}` is three dimensional vector describing
    weighted displacements of atom :math:`n` in mode :math:`k` and
    :math:`\Delta w_{n}` is three dimensional weighted relaxation of atom
    :math:`n`.
    """

    if units.lower() == "au":
        masses = eatoms.get_masses(units="me").repeat(3)
    elif units.lower() == "si":
        masses = eatoms.get_masses(units="kg").repeat(3)

    # --------------------------- #
    #    Embeding Displacements   #
    # --------------------------- #

    eatoms.sort_positions()
    displacements = np.zeros(len(eatoms)*3).reshape(len(eatoms), 3)
    for nr, (pos, d) in enumerate(zip(bulk_atoms.get_positions(), displ)):

        # Ignore forces outside physical radius of 0.5*0.95*lattice_comnstant
        if too_far_from_defect(bulk_atoms, pos, defect_center):
            continue

        indx = eatoms.find_by_position(pos, pbc=True, method=3)
        displacements[indx] = np.array(d)

    # Displacements should be weighted for projection <displ|mode_displ>
    weighted_displ = displacements.flatten()*np.sqrt(masses)

    # --------------------------------- #
    #    Calculating FC displacements   #
    # --------------------------------- #

    if hdf5dir is not None:
        iterator = iter_modes_from_hdf5dir(
            hdf5dir, eatoms, sym=sym, units=units, weighted=True)
    else:
        iterator = iter_modes_from_phonopy(
            phonopy_dir, eatoms, sym=sym, units=units, weighted=True)

    fc_displ = []
    frequencies = []
    irreps = []
    for nr, (freq, weighted_mode_displ, irrep) in enumerate(iterator):
        frequencies.append(freq)
        fc_displ.append(-weighted_displ.dot(weighted_mode_displ))
        irreps.append(irrep)

    return np.array(frequencies), np.array(fc_displ), np.array(
        irreps, dtype="U6")


def fc_displ_from_forces(eatoms, bulk_atoms, forces,
                         hdf5dir=None, phonopy_dir=None,
                         sym=None,
                         units="au", defect_center=None):
    r"""
    Relation used for FC displacement calculations:

    .. math::
        \Delta Q_k = \frac{1}{\omega^2_k}\sum_{n}\Delta F_{n}u_{n}^{k}

    where :math:`u_n^k` is three dimensional vector describing real
    displacement of atom :math:`n` in mode :math:`k` and
    :math:`\Delta F` is three dimensional force difference between excited
    and ground states.
    """

    eatoms.sort_positions()
    embeded_forces = np.zeros(len(eatoms)*3).reshape(len(eatoms), 3)
    for nr, (pos, d) in enumerate(zip(bulk_atoms.get_positions(), forces)):

        # Embeding to smaller cell
        if eatoms.get_cell().max() < bulk_atoms.get_cell().max():
            if np.any(pos > eatoms.get_cell().diagonal()):
                continue

        # Ignore forces outside physical radius of 0.5*0.95*lattice_comnstant
        if too_far_from_defect(bulk_atoms, pos, defect_center):
            continue

        indx = eatoms.find_by_position(pos, pbc=True, method=3)
        embeded_forces[indx] = np.array(d)

    embeded_forces = embeded_forces.flatten()

    # --------------------------------- #
    #    Calculating FC displacements   #
    # --------------------------------- #

    if hdf5dir is not None:
        iterator = iter_modes_from_hdf5dir(
            hdf5dir, eatoms, sym=sym, units=units, weighted=False)
    else:
        iterator = iter_modes_from_phonopy(
            phonopy_dir, eatoms, sym=sym, units=units, weighted=False)

    fc_displ = []
    frequencies = []
    normal_forces = []
    irreps = []
    for nr, (freq, mode_displ, irrep) in enumerate(iterator):
        frequencies.append(freq)
        force = embeded_forces.dot(mode_displ)
        normal_forces.append(force)
        fc_displ.append(force/np.power(freq, 2))
        irreps.append(irrep)

    return np.array(frequencies), np.array(fc_displ), np.array(
        irreps, dtype="U6"), np.array(normal_forces)


def read_and_allign_embeded_cell(bulkg_poscar, hdf5dir=None,
                                 phonopy_bulk_poscar=None):
    # Read corresponding bulk positions
    bulk_atoms = Poscar(bulkg_poscar).create_atoms()

    if hdf5dir is not None:
        eatoms = read_embeded_atoms(os.path.join(hdf5dir, "atoms.p"))
        embeded_size = int(
            re.search("x(.*?)x", os.path.abspath(hdf5dir)).group(1))
    else:
        assert phonopy_bulk_poscar is not None, (
            "phonopy_bulk_poscar should be specified to read phonopy"
            " atoms in bulk geometry!")
        eatoms = Poscar(phonopy_bulk_poscar).create_atoms()
        embeded_size = int(
            re.search("x(.*?)x", os.path.abspath(
                phonopy_bulk_poscar)).group(1))

    # ------------------------------------------------- #
    #  Equalise lattice constants for bulk and embeded
    # ------------------------------------------------- #

    bulk_size = int(
        re.search("x(.*?)x", os.path.abspath(bulkg_poscar)).group(1))

    elc = eatoms.get_cell()[0, 0]/embeded_size
    blc = bulk_atoms.get_cell()[0, 0]/bulk_size
    eatoms.set_cell(eatoms.get_cell()*blc/elc)

    return eatoms, bulk_atoms


def _error_checking(bulk_atoms, atoms1, atoms2):
    tmp = bulk_atoms.copy()
    tmp.set_cell(atoms1.get_cell())
    remove_pb_artifacts(bulk_atoms, atoms1)
    remove_pb_artifacts(bulk_atoms, atoms2)

    for indx, (pos1, pos2) in enumerate(
            zip(tmp.get_positions(), atoms1.get_positions())):
        assert np.allclose(pos1, pos2, atol=0.2, rtol=0), (
            "Atoms1 positions strongly differ from bulk geometry:" +
            "indx: {}\nbulk_pos:{}\natm2_pos:{}\nbcell:{}\nacell:{}".format(
                indx, pos1, pos2, bulk_atoms.get_cell(), atoms1.get_cell()))

    for indx, (pos1, pos2) in enumerate(
            zip(atoms2.get_positions(), atoms1.get_positions())):
        assert np.allclose(pos1, pos2, atol=0.2, rtol=0), (
            "Atoms2 positions strongly differ from bulk geometry:" +
            "indx: {}\nbulk_pos:{}\natm2_pos:{}\nbcell:{}\nacell:{}".format(
                indx, pos1, pos2, bulk_atoms.get_cell(), atoms2.get_cell()))


def calculate_fc_displacements(
        poscar1, poscar2, bulkg_poscar,
        hdf5dir=None, phonopy_dir=None,
        phonopy_bulk_poscar=None,
        forces_output1=None, forces_output2=None,
        from_forces=False, units="au",
        sym=None, defect_center="N"):
    r"""
    Concept of Franck--Condon displacment comes from solid state
    approximation. In this approximation electronic excitation modifies
    normal modes potentials by linear term:

    .. math:: \frac{1}{2} \omega^2 Q^2 + V Q

    Where :math:`Q` is mass weighted normal modes :math:`\sim \sqrt{M}u`.
    Such modification doesn't change frequency -- it only displaces
    oscilator equilibrium positions by:

    .. math:: \Delta Q = -\frac{V}{\mu\omega^2}.

    where :math:`\mu` is mode effective mass. In our calculations always
    :math:`\mu = 1`. This :math:`\Delta Q` displacement is called
    Franck--Condon displacement.

    Args:
        poscar1 (str): path of ground state ``POSCAR`` or ``CONTCAR``
        poscar2 (str): path of excited state ``POSCAR`` or ``CONTCAR``
        bulkg_poscar (str): path of ``POSCAR`` or ``CONTCAR`` where defect
           supercell positions are in bulk geometry.
        hdf5dir (str, optional): directory path where ``modes.hdf5``,
          ``mode_analysis.hdf5`` and ``atoms.p`` files are saved.
          From this directory all information about ground state vibrational
          structure will be read.
        phonopy_dir (str, optional): directory path where ``disp.yaml`` and
          ``FORCE_CONSTANTS`` files are saved. From this directory all
          information about ground state vibrational structure will be read.
        forces_output1 (str): path to ``vasprun.xml`` or ``OUTCAR`` file.
            Must be given if ``from_forces`` is ``True``
        forces_output2 (str): path to ``vasprun.xml`` or ``OUTCAR`` file.
            Must be given if ``from_forces`` is ``True``
        from_forces (bool): if True Franck--Condon displacements will be read
            from force differences
        units (str): units for Frank--Condon factors:

            * ``au`` returns displacements in :math:`\mbox{Bohrs}\sqrt{m_e}`
            * ``si`` returns displacments in :math:`\mbox{meters}\sqrt{m_e}`

        defect_center (None, str, array): for cutoff procedure
    """

    # ----------------------------------------------- #
    #   Calculate displacements or force differences  #
    # ----------------------------------------------- #

    if from_forces:
        assert (forces_output1 is not None) and (forces_output2 is not None), (
            "To read forces vasprun.xml or OUTCAR should be specified")
        atoms1 = Poscar(poscar1).create_atoms()
        atoms2 = Poscar(poscar2).create_atoms()
        forces = read_force_differences(
            forces_output1, forces_output2, units=units)
    else:
        displ, atoms1, atoms2 = read_displacements(
            poscar1, poscar2, units=units)

    # ----------------------------------------------- #
    #          Read bulk and embeded atoms            #
    # ----------------------------------------------- #

    if hdf5dir is not None:
        assert phonopy_dir is None, (
            "Phonopy output dir should be None if hdf5dir is specified")
    else:
        assert phonopy_dir is not None, (
            "Phonopy output dir should be specified hdf5dir is None")

    eatoms, bulk_atoms = read_and_allign_embeded_cell(
        bulkg_poscar, hdf5dir=hdf5dir, phonopy_bulk_poscar=phonopy_bulk_poscar)

    _error_checking(bulk_atoms, atoms1, atoms2)

    # ----------------------------------------------- #
    #               Find defect center                #
    # ----------------------------------------------- #

    if isinstance(defect_center, str):
        symbol = defect_center
        defect_center = bulk_atoms.get_positions()[
            list(bulk_atoms.symbols).index(defect_center)]
        defect_center_in_embeded_cell = eatoms.get_positions()[
            list(eatoms.symbols).index(symbol)]
        assert np.allclose(defect_center_in_embeded_cell, defect_center), (
            "Defect placement is different in embeded and direct supercells:" +
            "\n -direct: {}\n -embeded:{}".format(
                defect_center, defect_center_in_embeded_cell))

    # -------------------------- #
    #    Find FC displacements   #
    # -------------------------- #

    if from_forces:
        freq, fc_displ, irreps, forces = fc_displ_from_forces(
            eatoms, bulk_atoms, forces,
            hdf5dir=hdf5dir, phonopy_dir=phonopy_dir,
            units=units, sym=sym, defect_center=defect_center)
    else:
        freq, fc_displ, irreps = fc_displ_from_relaxation(
            eatoms, bulk_atoms, displ,
            hdf5dir=hdf5dir, phonopy_dir=phonopy_dir,
            units=units, sym=sym, defect_center=defect_center)

    data = {
        "freq": freq,
        "fc_displ": fc_displ,
        "irreps": irreps,
    }

    if from_forces:
        data["forces"] = forces

    return data


def calculate_couplings(poscar1, poscar2, bulkg_poscar,
                        hdf5dir=None, phonopy_dir=None,
                        phonopy_bulk_poscar=None,
                        forces_output1=None, forces_output2=None,
                        from_forces=False, units="au",
                        sym=None, vibronic=True):
    r"""
    Calculates different constants from Franck--Condon displacements.

    Args:
       poscar1 (str): path to ``POSCAR`` or ``CONTCAR`` file
           (usually ground state)
       poscar2 (str): path to ``POSCAR`` or ``CONTCAR`` file
           (usually excited state)
       bulkg_poscar (str): path to ``POSCAR`` or ``OUTCAR`` file with
           bulk geometry configuration. Used to map atoms between relaxed
           and embeded cells.
       hdf5dir (str): directory where atomic structure and vibrational data
           of embeded cell is saved
       forces_output1 (str): path to ``vasprun.xml`` or ``OUTCAR`` file.
           Must be given if ``from_forces`` is ``True``
       forces_output2 (str): path to ``vasprun.xml`` or ``OUTCAR`` file.
           Must be given if ``from_forces`` is ``True``
       from_forces (bool): if ``True`` calculates Frank-Condon displacements
           from forces


    Main equations:

    * Relation betweem Franck-Condon displacement and vibronic constant v
      $$ \\Delta Q = -\\frac{v}{m\\omega^2}, \\qquad
      v = - \\Delta Q m \\omega^2 $$
    * Dimensionles coupling constant
      $$ k = \\frac{v}{\\sqrt{2\\hbar\\omega^3}} $$
    * Huang--Rhys factor:
      $$ S = \\frac{a^2}{2m\\hbar \\omega^2} = E_R/\\hbar\\omega $$
    * Oscilator relaxation energy:
      $$ E_r = \\frac{a^2}{2m\\omega^2} =
      \\frac{\\Delta Q^2 m \\omega^2}{2} $$
    """
    data = calculate_fc_displacements(
        poscar1, poscar2, bulkg_poscar,
        hdf5dir=hdf5dir, phonopy_dir=phonopy_dir,
        phonopy_bulk_poscar=phonopy_bulk_poscar,
        forces_output1=forces_output1,
        forces_output2=forces_output2,
        from_forces=from_forces,
        units=units, sym=sym)

    freq, fc_displ, irreps = data["freq"], data["fc_displ"], data["irreps"]

    if units.lower() == "au":
        hbar = 1
    elif units.lower() == "si":
        hbar = hbar_J_s

    coupling_constants = -fc_displ*np.power(freq, 2)
    hr_factors = (np.power(fc_displ, 2)*freq)/(2*hbar)

    data["freq_mev"] = freq * atomic_angular_freq_unit/mev_to_rad_per_second
    data["hr_factors"] = hr_factors
    data["coupling_constants"] = coupling_constants
    data["hr_tot"] = sum(hr_factors)

    if not vibronic:
        return data

    e_data = calculate_nondimensional_vibronic_couplings(
        freq, fc_displ, irreps)

    return data, e_data

#################################
#  Dynamic Jahn--Teller theory
#################################


def calculate_nondimensional_vibronic_couplings(freq, fc_displ, irreps):
    r"""
    Adiabatic potential of E doublet is:

    .. math::
        U(\rho) = \frac{1}{2}\omega^2\rho^2 - \vert V\vert\rho

    Minimum of this potential is:

    .. math::
        \rho_0 = \frac{\vert V \vert}{\omega^2}

    This minimum is connected to Franck--Condon displacments by:

    .. math::
       \rho^2_0 = \Delta Q_x^2 + \Delta Q_y^2

    """

    freq_mev = freq * atomic_angular_freq_unit/mev_to_rad_per_second
    indx = np.where(irreps == "Exy")[0]

    return_data = dict(
        k=[],
        v=[],
        ejt=[],
        freq_mev=[],
        freq=[])

    for i in range(0, len(indx), 2):
        exindx = indx[i]
        eyindx = indx[i + 1]

        assert np.isclose(
            freq[exindx], freq[eyindx], rtol=0), (
                "{} != {}".format(freq[exindx], freq[eyindx]))

        rho = np.sqrt(fc_displ[exindx]**2 + fc_displ[eyindx]**2)
        f = freq[exindx]
        return_data["freq"].append(f)

        k = rho * np.sqrt(f/2)
        v = rho * np.power(f, 2)
        e = -0.5 * np.power(v/f, 2)

        assert np.isclose(v, k*np.sqrt(2*np.power(f, 3)))

        return_data["k"].append(k)
        return_data["v"].append(v)
        return_data["ejt"].append(e)
        return_data["freq_mev"].append(freq_mev[exindx])

    for i in return_data:
        if isinstance(return_data[i], list):
            return_data[i] = np.array(return_data[i])

    ##############################
    #    Derived quantities
    ##############################

    keff = np.sqrt(sum(return_data["k"]**2))
    return_data["keff"] = keff
    efffreqsq = sum(
        (return_data["k"]**2) * (return_data["freq_mev"]**2) /
        (return_data["keff"]**2))
    return_data["effreq_mev"] = np.sqrt(efffreqsq)
    efffreqsq = sum(
        (return_data["k"]**2) * (return_data["freq"]**2) /
        (return_data["keff"]**2))

    return_data["effreq"] = np.sqrt(efffreqsq)
    return_data["freq_conversion"] = (
        atomic_angular_freq_unit/mev_to_rad_per_second)

    return return_data


if __name__ == "__main__":
    phonopy_dir = "../diamond/vasp/nv3x3x3/phonopy_phonons/"
    fc = ForceConstants("vasp", phonopydir=phonopy_dir)
    fc.perform_cutoff()
    fc.symmetrize_fc(2)
    modes = fc.calculate_modes()
    modes.analyse()
