"""Calculates displacements and phonons using phonopy.
Also postproceses results. Simply calls phonopy routines
with few small default changes usefull for defect calculations."""

import subprocess
import os
import logging
import pprint
import shutil
import yaml
import re
import numpy as np

from deutils import ensure_dir, config, Units
from espresso import EspressoInput
from declusters import Cluster
from deatoms import Atoms


logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()
pp = pprint.PrettyPrinter()


PHONOPY = config["DEFAULT"]["PHONOPY"]
espresso_path = config["DEFAULT"]["QEBINS"]
PWX = os.path.join(espresso_path, "pw.x")

##############################
#  DISPLACEMENT CREATION
##############################


def create_pwscf_displacements(workdir, options, dim, log_file, **kwargs):
    assert "filepath" in kwargs, "For pwscf calculations filepath mus be given"
    filepath = kwargs["filepath"]
    newpath = shutil.copy(filepath, workdir)
    i = EspressoInput(newpath)
    with open(newpath, "wt") as f:
        f.write(i.create_input())

    call_line = '{0} --pwscf -d --dim="{1} {1} {1}" -c {2}'.format(
        PHONOPY, dim, filepath) + options

    with open(log_file, "wt") as f:
        subprocess.call(
            call_line,
            cwd=workdir,
            shell=True,
            stdout=f)

    with open(log_file, "at") as f:
        f.write("\nCalled phonopy with args:\n {}\n".format(call_line))

    supercell = EspressoInput(newpath)
    files = [os.path.join(workdir, i) for i in os.listdir(workdir)
             if i.startswith("supercell") and i.endswith(".in")]

    for fpath in files:
        with open(fpath, "rt") as f:
            content = f.read()

        nat = re.search("nat *= *([0-9]+)", content).group(1)

        a = re.split(
            "(CELL_PARAMETERS bohr|ATOMIC_SPECIES|ATOMIC_POSITIONS crystal)",
            content)

        try:
            atoms_text = "ATOMIC_POSITIONS crystal" + a[-1] + "\n"
            cell_text = "CELL_PARAMETERS bohr" + a[2]
        except:
            print(content)
            raise

        cont = supercell.create_input(
            atoms_text=atoms_text, cell_text=cell_text, nat=nat,
            prefix=os.path.splitext(os.path.basename(fpath))[0],
            newpsps="../../psps")

        with open(fpath, "wt") as f:
            f.write(cont)


def create_vasp_displacements(workdir, options, dim, log_file, **kwargs):
    basedir = kwargs.get("basedir", ".")

    for _file in os.listdir(basedir):
        if _file in {"INCAR", "POSCAR", "KPOINTS", "POTCAR"}:
            shutil.copy(os.path.join(basedir, _file), workdir)

    call_line = '{0} -d --dim="{1} {1} {1}"'.format(PHONOPY, dim) + options
    with open(log_file, "wt") as f:
        subprocess.call(
            call_line,
            cwd=workdir,
            shell=True,
            stdout=f)

    with open(log_file, "at") as f:
        f.write("\nCalled phonopy with args:\n {}\n".format(call_line))

    for _file in os.listdir(workdir):
        if _file.startswith("POSCAR-"):
            dirname = os.path.join(workdir, _file.replace("POSCAR", "disp"))
            ensure_dir(dirname)
            shutil.move(os.path.join(workdir, _file),
                        os.path.join(dirname, "POSCAR"))

            for i in {"INCAR", "KPOINTS", "POTCAR"}:
                shutil.copy(os.path.join(workdir, i), dirname)

        if _file == "SPOSCAR":
            shutil.move(os.path.join(workdir, _file),
                        os.path.join(workdir, "POSCAR"))


def create_displacements(calc, dim, tolerance=None, **kwargs):
    """Uses phonopy to generate displacements

    Args:
      workdir(str): working directory (all files will be copied)
      filepath(str): pwscf required
      basedir(str): vasp required

    """
    workdir = kwargs.get("workdir", "phonopy_phonons")

    ensure_dir(workdir)
    log_file = os.path.join(workdir, "phonopy.log")
    log.info("Generating displacement files")

    options = ""
    if "amplitude" in kwargs:
        options += " --amplitude={}".format(kwargs["amplitude"])
    if kwargs.get("pm", False):
        options += " --pm"

    if tolerance is not None:
        options += " --tolerance={}".format(tolerance)

    if calc == "pwscf":
        create_pwscf_displacements(workdir, options, dim, log_file, **kwargs)
    elif calc == "vasp":
        create_vasp_displacements(workdir, options, dim, log_file, **kwargs)

    log.info("Finished")

##############################
#  Job file generation
##############################


def _divide_jobs(calcs, jobs, workdir, cluster, jobparams):
    total_nr_of_calcs = len(calcs)
    per_job = int(total_nr_of_calcs/jobs)

    job_files = []
    for nr in range(jobs - 1):
        name = "phonons{}".format(nr+1)
        jobparams["output"] = name + ".out"
        content = cluster.generate_job_file(
            jobparams, calcs[nr*per_job: nr*per_job + per_job])
        fname = name + ".job"
        job_files.append(fname)
        with open(os.path.join(workdir, fname), "wt") as f:
            f.write(content)

    name = "phonons{}".format(jobs)
    fname = name + ".job"
    jobparams["output"] = name + ".out"
    content = cluster.generate_job_file(
        jobparams, calcs[(jobs - 1)*per_job:])

    job_files.append(fname)
    with open(os.path.join(workdir, fname), "wt") as f:
        f.write(content)

    #####################################
    #  Python script to submit all jobs
    #####################################

    with open(os.path.join(workdir, "run_jobs.py"), "wt") as f:
        content = "import subprocess\n"

        for i in job_files:
            content += ('subprocess.call("{} {}", shell=True)\n'.format(
                cluster.submit_cmd, i))

        f.write(content)

    with open(os.path.join(workdir, "run_jobs_debug.py"), "wt") as f:
        content = "import subprocess\nimport time\n\njobs = [\n"

        for i in job_files:
            content += '    "{}",\n'.format(i)

        content += "]\n\n"

        content += (
            "while jobs:\n    "
            "proc = subprocess.Popen(['sqs', '-w'], stdout=subprocess.PIPE)\n"
            "    lines = proc.communicate()[0].split()\n"
            "    print(lines)\n"
            "    time.sleep(2)"
            )

        f.write(content)


def generate_vasp_job_files(cluster, name, nodes, np, plan, jobs, time, qos,
                            **kwargs):
    workdir = kwargs.get("workdir", "phonopy_phonons")
    binary = kwargs.get("binary", False)
    execpath = kwargs.get("execpath", False)
    assert binary, "VASP executable must be given by param binary='vasp_std'"

    calcs = []
    for _dir in os.listdir(workdir):
        if (os.path.isdir(os.path.join(workdir, _dir)) and
                _dir.startswith("disp-")):
            calcs.append(_dir)
    calcs.append("")

    calcs = [{"exec": binary, "np": np, "cwd": "./{}".format(i)}
             for i in sorted(calcs)]

    if execpath:
        for i in calcs:
            i["execpath"] = execpath

    jobparams = {
        "nodes": nodes,
        "name": name,
        "plan": plan,
        "time": time,
        "qos": qos,
    }

    cluster = Cluster(cluster, "vasp")

    ##############################
    #    Division Into Jobs
    ##############################
    _divide_jobs(calcs, jobs, workdir, cluster, jobparams)


def generate_pwscf_job_files(cluster, name, nodes, np, plan, jobs, time,
                             qos, **kwargs):
    workdir = kwargs.get("workdir", "phonopy_phonons")

    calcs = []
    for _file in os.listdir(workdir):
        if (_file.startswith("supercell") and _file.endswith(".in")):
            calcs.append(_file)

    calcs = [{"np": np, "cwd": ".", "fname": os.path.splitext(i)[0]}
             for i in sorted(calcs)]

    jobparams = {
        "nodes": nodes,
        "name": name,
        "plan": plan,
        "time": time,
        "qos": qos
    }

    cluster = Cluster(cluster, "pwscf")

    ##############################
    #    Division Into Jobs
    ##############################

    _divide_jobs(calcs, jobs, workdir, cluster, jobparams)


def generate_job_files(calc, cluster, name, nodes, np, qos="premium",
                       plan="debug", jobs=1, time="30:00", **kwargs):
    if calc == "vasp":
        generate_vasp_job_files(
            cluster, name, nodes, np, plan, jobs, time, qos, **kwargs)
    elif calc == "pwscf":
        generate_pwscf_job_files(
            cluster, name, nodes, np, plan, jobs, time, qos, **kwargs)
    else:
        raise NotImplementedError("!!!")


########################################
#  Postprocess ab-initio calculations
########################################


def create_force_sets(calc, workdir=".", tolerance=None):
    if calc == "pwscf":
        calc_opt = " --pwscf"
        list_of_f = sorted([
            i for i in os.listdir(workdir)
            if i.endswith(".out") and
            i.startswith("supercell") and not i == "supercell.out"])
    elif calc == "vasp":
        calc_opt = ""
        list_of_f = list(sorted([
            os.path.join(i, "vasprun.xml") for i in os.listdir(workdir)
            if i.startswith("disp-")]))
        pp.pprint(list_of_f)
    else:
        raise NotImplementedError(
            "Calculator {} not implemented!".format(calc))

    if tolerance is not None:
        calc_opt += " --tolerance={}".format(tolerance)

    outfiles = " ".join(list_of_f)
    print("Found {} files".format(len(list_of_f)))

    subprocess.call(
        '{0}{1} -f {2}'.format(
            config["DEFAULT"]["PHONOPY"], calc_opt, outfiles),
        cwd=workdir,
        shell=True)


def calculate_fcmatrix(calc, conv_file_name=None, dim=1, fc_symmetry=1,
                       workdir=".", cutoff=None, nac=False,
                       spg_symmetry=2, tolerance=None):
    """ --factor=3634.87468282{3}"""

    if calc == "pwscf":
        calc_opt = " --pwscf"
        assert conv_file_name is not None, (
            "For pwscf conv_file_name is required")
        conv_file_name = " -c {}".format(conv_file_name)
    elif calc == "vasp":
        calc_opt = ""
        conv_file_name = ""
    else:
        raise NotImplementedError(
            "Calculator {} not implemented!".format(calc))

    if fc_symmetry is None:
        fc_symmetry = ""
    else:
        fc_symmetry = " --fc_symmetry={}".format(fc_symmetry)

    if nac:
        fc_symmetry += " --nac"

    if cutoff is not None:
        fc_symmetry += " --cutoff_radius={}".format(cutoff/Units.Bohr)

    if spg_symmetry:
        fc_symmetry += " --fc_spg_symmetry"

    if tolerance is not None:
        calc_opt += " --tolerance={}".format(tolerance)

    call_line = ('{0}{4} --writedm --writefc --qpoints="0 0 0" --dim=' +
                 '"{1} {1} {1}"{2} --eigvecs').format(
                     config["DEFAULT"]["PHONOPY"], dim, conv_file_name,
                     fc_symmetry,
                     calc_opt)
    print(call_line)
    subprocess.call(
        call_line,
        cwd=workdir,
        shell=True)


def create_plot(calc, workdir, conv_file_name=None, dim=1,
                fc_symmetry=None,
                primitive_axis="0.0 0.5 0.5  0.5 0.0 0.5  0.5 0.5 0.0",
                band=("0 0 0 3/8 3/4 3/8, -5/8 -5/8 -1/4"
                      " -0.5 -0.5 0.0  0.0 0.0 0.0  0.5 0.5 0.5"
                      "  0.0 0.5 0.5  0.25 0.75 0.5  0.5 0.5 0.5"),
                band_labels="\Gamma  K X \Gamma L X W L", dos=True,
                nac=False,
                MP=40):

    if calc == "pwscf":
        calc_opt = " --pwscf"
        assert conv_file_name is not None, (
            "For pwscf conv_file_name is required")
        conv_file_name = " -c {}".format(conv_file_name)
    elif calc == "vasp":
        calc_opt = ""
        conv_file_name = ""
    else:
        raise NotImplementedError(
            "Calculator {} not implemented!".format(calc))

    bandconf = os.path.join(workdir, "band.conf")
    with open(bandconf, "wt") as f:
        f.write(
            "DIM = {0} {0} {0}\n".format(dim) +
            "PRIMITIVE_AXIS = {}\n".format(primitive_axis) +
            "BAND = {}\n".format(band) +
            "BAND_LABELS = {}\n".format(band_labels) +
            ("FC_SYMMETRY = {}\n".format(fc_symmetry) if fc_symmetry else "") +
            ("MP = {0} {0} {0}\nDOS = .TRUE.".format(MP) if dos else ""))

    call = ('{}{}{} -p "band.conf" -s'
            ' --factor=3634.87468282{}').format(
                config["DEFAULT"]["PHONOPY"], calc_opt, conv_file_name,
                " --nac" if nac else "")

    subprocess.call(call, cwd=curdir, shell=True)


##############################
#  YAML
##############################


def dispyaml_to_atom(filepath, calc, pwscf=True, calculate_symmetry=False):
    if calc == "pwscf":
        dist_conv = 1/Units.a_to_bohr
    elif calc == "vasp":
        dist_conv = 1

    with open(filepath, "rt") as f:
        content = f.read()

    data = yaml.load(content)
    scaled_positions = []
    symbols = []

    if "points" in data:
        for atom in data["points"]:
            scaled_positions.append(atom["coordinates"])
            symbols.append(atom["symbol"])
    elif "atoms" in data:
        for atom in data["atoms"]:
            scaled_positions.append(atom["position"])
            symbols.append(atom["symbol"])

    cell = np.array(data["lattice"])*dist_conv
    atoms = Atoms(scaled_positions=scaled_positions, symbols=symbols,
                  cell=cell, calculate_symmetry=calculate_symmetry)

    return atoms
