import os
import pprint
import logging
import time
import numpy as np
import csv
import gc
from scipy import linalg
import copy
from itertools import product
from scipy import sparse
import h5py
import pickle
import resource
# from memory_profiler import profile
from dephonopy import dispyaml_to_atom
from deutils import (Units, ensure_dir,
                     remove_lil_col, remove_lil_row)
from demodes import Modes

# Printing
np.set_printoptions(4)
pp = pprint.PrettyPrinter()

# Logging
logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

# For automatic garbage collection
gc.enable()


class ForceConstants():
    """Stores info about lattice atoms and forces.
    FORCE constants are stored in csr format.
    Directly calculates Vibrational modes and displacement vectors
    for relatively small matrices.

    Uses Atomic Hartree Units:
    - lenght: [Bohr Radius]
    - mass:   [m_e]
    - energy: [Ry]
    - angular frequencies: 2*pi*Ha/hbar = 2*pi*4.13e16 s^{-1}

    """

    def __init__(self, calc, atoms=None, phonopydir=None, hdf5dir=None,
                 log_fh=None, calculate_symmetry=False):
        """Can be initialised given:
        - Atoms object: no force constants will be assigned
        - Phonopy Output Directory: force constants and geometry will be
            read from phonopy output
        - Hdf5 saved directory
        """

        if log_fh is not None:
            log.addHandler(log_fh)
            self.log_fh = log_fh

        self.calc = calc
        self.diag_time = None

        # Matrices
        self.hessian = None
        self.inv_square_masses = None
        self.masses = None
        self.whessian = None

        # Results of diagonalization
        self._evals = None
        self._evects = None
        self._recalculate = True

        # -----------------------------
        #        Atoms object
        # -----------------------------
        if atoms is not None:
            self.atoms = atoms
            self.create_empty_hessian()

        # -----------------------------
        #       Phonopy Output
        # -----------------------------
        elif phonopydir is not None:
            self.atoms = dispyaml_to_atom(
                os.path.join(phonopydir, "disp.yaml"), calc,
                calculate_symmetry=calculate_symmetry)
            self.create_empty_hessian()
            self.parse_force_constants(
                calc, os.path.join(phonopydir, "FORCE_CONSTANTS"))

        # -----------------------------
        #       Phonopy Output
        # -----------------------------
        elif hdf5dir is not None:
            self.load_from_hdf5(hdf5dir)
        else:
            raise RuntimeError("ForceConstants missing one of required args!")

        # ----------------------------
        #       Embeding info
        # ----------------------------
        self.embeding_info = {
            "bulk": {},
            "defect": {}
        }
        self.embeded = []
        self.interaction_vectors = {}
        self.interaction_maps = {}
        self.pw_bands = None

    @property
    def shape(self):
        return (len(self.atoms)*3, len(self.atoms)*3)

    def copy(self):
        return copy.deepcopy(self)

    ##############################
    #    Array manipulations
    ##############################

    def _get_used_memory(self, array):
        units = "b"
        try:
            b = array.nbytes
        except AttributeError:
            b = array.data.nbytes

        size = b
        if b > 1e9:
            units = "Gb"
            size = b/1e9
        elif b > 1e6:
            units = "Mb"
            size = b/1e6
        elif b > 1e3:
            units = "Kb"
            size = b/1e3

        return size, units

    def _allocate_matrix(self, name, shape, storage_format):
        assert storage_format in {"dense", "csr", "lil"}, (
            "Wrong {} matrix storage format: {}".format(name, storage_format))

        setattr(self, "{}_storage".format(name), storage_format)

        if storage_format == "dense":
            setattr(self, "{}".format(name),
                    np.zeros(shape, np.dtype('f8')))
        elif storage_format == "lil":
            setattr(self, "{}".format(name),
                    sparse.lil_matrix(shape, dtype=np.dtype('f8')))
        elif storage_format == "csr":
            setattr(self, "{}".format(name),
                    sparse.csr_matrix(shape, dtype=np.dtype('f8')))

        log.info(" - allocated {:.3f} {} for {}x{} {} matrix in {} format"
                 "".format(*self._get_used_memory(getattr(self, name)),
                           *shape, name, storage_format))

    def _change_storage_format(self, name, storage_format):
        assert storage_format in {"dense", "csr", "lil"}, (
            "Wrong {} matrix storage format: {}".format(name, storage_format))

        curr_storage = getattr(self, "{}_storage".format(name))
        if curr_storage == storage_format:
            return

        setattr(self, "{}_storage".format(name), storage_format)

        matrix = getattr(self, name)
        init_size = self._get_used_memory(matrix)

        beg = time.time()
        log.info("* Changing {} storage type from {} to {}".format(
            name, curr_storage, storage_format))

        if storage_format == "dense":
            setattr(self, name, matrix.toarray())
        elif storage_format == "csr":
            setattr(self, name,
                    sparse.csr_matrix(
                        matrix, dtype=np.dtype('f8')))
        elif storage_format == "lil":
            setattr(self, name,
                    sparse.lil_matrix(
                        matrix, dtype=np.dtype('f8')))

        del matrix

        log.info(" - storage changed in {:.3f} s ({}{} -> {}{})".format(
            time.time() - beg,
            *init_size,
            *self._get_used_memory(getattr(self, name))))

        gc.collect()

    ##############################
    #    HESSIAN MANIPULATION
    ##############################

    def create_empty_hessian(self, storage_format="dense"):
        self._allocate_matrix("hessian", self.shape, storage_format)

    def change_hessian_storage(self, storage_format):
        if storage_format == self.hessian_storage:
            return
        self._change_storage_format("hessian", storage_format)

    def get_dense_hessian(self, units=None):
        """Returns force constants matrix given in Ha/bohr^2"""
        if units is None:
            conversion = 1
        elif units.lower() in {"ev/a^2", "a", "ev"}:
            conversion = Units.ha_to_ev/(Units.bohr_to_a**2)
        elif units.lower() in {"ry", "ry/bohr^2"}:
            conversion = Units.ha_to_ry

        if self.hessian_storage == "dense":
            return conversion*self.hessian
        else:
            return conversion*self.hessian.A

    def remove_lil_positions(self, positions):
        positions.sort()
        offset = 0
        positions.sort()
        for i in positions:
            remove_lil_col(self.hessian, i + offset)
            remove_lil_row(self.hessian, i + offset)
            offset -= 1

    def remove_atoms(self, indexes, transform_to_lil=True):
        if isinstance(indexes, int):
            indexes = [indexes]

        print("- Deleting atoms:", indexes)
        print("- Positions:{}".format(
            "".join(["\n  + {}".format(self.atoms.get_positions()[i])
                     for i in indexes])))

        positions = []
        for i in indexes:
            positions += [i*3, i*3+1, i*3+2]

        self.atoms.delete_atoms(indexes)
        oldshape = self.hessian.shape

        if transform_to_lil:
            self.change_hessian_storage("lil")
            self.remove_lil_positions(positions)
            self.change_hessian_storage("dense")
        else:
            self.hessian = np.delete(self.hessian, positions, 0)
            self.hessian = np.delete(self.hessian, positions, 1)

        print("- Matrix reshaped:", oldshape, "-->", self.hessian.shape)

    ##############################
    #     HESSIAN SYMMETRIES
    ##############################

    # @profile
    def ensure_permutation_symmetry(self, indx=1, mytype=2):
        """
        Atomic indexes: a, b
        Cartesian indexes: i, j:

        Phi_ab_ij = Phi_ba_ji
        """
        beg = time.time()
        log.info("- Ensuring permutation symmetry {}".format(indx))

        _max = 0
        if mytype == 1:
            fc_copy = self.copy()
            for i, j in self.atoms.iter_pairs():
                m = self.get_atom_pair_part(i, j)
                m2 = fc_copy.get_atom_pair_part(j, i).T

                newm = (m + m2)/2
                _imax = abs(m - newm).max()
                if _imax > _max:
                    _max = _imax

                self.set_atom_pair_part(i, j, newm)
        else:
            self.hessian = (self.hessian + self.hessian.T)/2

        log.info(
            "  + finished in {:.3f}s".format(time.time()-beg))

    def ensure_third_newtown_law(self, indx=1, which='diagonal'):
        if which != 'diagonal':
            raise NotImplementedError(
                "Accoustic sum rule for non diagonal "
                "elements is not implemented!")
        else:
            log.info("- Ensuring 3rd Newton Law {}".format(indx))

        beg = time.time()

        _max = 0
        for atom, i, in product(range(len(self.atoms)), range(3)):
            m = self.hessian[atom*3 + i, i::3].sum()
            _max = m if m > _max else _max
            self.hessian[atom*3 + i, atom*3 + i] -= m

        log.info(
            "  + finished in {:.3f}s max discrepancy {:.3e} Ry/Bohr**2".format(
                time.time()-beg, _max))

    def symmetrize_fc(self, times=1, mytype=2, phonopy=False):
        """Ensures some force constants matrix symmetries."""
        if phonopy:
            phonon = self._create_phonopy_phonon()
            phonon.symmetrize_force_constants(times)

            _i = 0
            force_constants = phonon.get_force_constants()
            for i, j in self.atoms.iter_pairs():
                if i != _i:
                    print("Gathering symmetrized force constants:",
                          i, end="\r")
                    _i = i
                self.set_atom_pair_part(i, j, force_constants[i][j]/2)
            return

        self.change_hessian_storage("csr")
        for i in range(1, times + 1):
            self.ensure_permutation_symmetry(i, mytype)
            self.ensure_third_newtown_law(i)

    ##############################
    #        MASS MATRIX
    ##############################

    def get_masses(self, units=None):
        if units is None:
            return self.atoms.masses*Units.amu_to_me

        if units.lower() in {"amu"}:
            return self.atoms.masses
        else:
            raise RuntimeError("Unknown mass units: {}".format(units))

    def get_mass_diagonal_matrix(self, inverse_square=True):

        if inverse_square:
            if self.inv_square_masses is None:
                self.inv_square_masses = np.diag(
                    np.repeat(1/np.sqrt(self.get_masses()), 3))
            return self.inv_square_masses

        if self.masses is None:
            self.masses = np.diag(
                    np.repeat(self.get_masses(), 3))

        return self.masses

    def get_mass_weighted_force_constants(self, storage_format="dense"):
        if self.whessian is None:
            self.change_hessian_storage(storage_format)
            log.info(" * Calculating weigted hessian")
            beg = time.time()

            self._allocate_matrix("whessian", self.shape, storage_format)
            log.info(" - allocated matrix in {:.3f} s".format(
                time.time() - beg))

            tic = time.time()
            mass_matrix = self.get_mass_diagonal_matrix(inverse_square=True)
            log.info(" - mass matrix created in {:.3f} s".format(
                time.time() - tic))

            self.whessian = mass_matrix.dot(self.hessian).dot(mass_matrix)
            log.info(" -> whessian calculated in {:.3f} s".format(
                time.time() - beg))

        return self.whessian

    ##############################
    #           FORCES
    ##############################

    def set_forces(self, atom_nr, direction, forces, delta=0.2):
        # Slicing for sparse matrices is very slow
        self.change_hessian_storage("dense")

        self._recalculate = True
        self.delta = delta

        try:
            self.hessian[:, atom_nr*3 + direction] = -1*np.concatenate(
                forces)/delta
        except ValueError:
            self.hessian[:, atom_nr*3 + direction] = -1*forces/delta

    def get_forces(self, atom_nr, direction, delta=0.02):
        if self._hstorage == "dense":
            return -1*self.hessian[:, atom_nr*3 + direction]*delta
        else:
            return -1*self.hessian.A[:, atom_nr*3 + direction]*delta

    def get_force(self, displ_atom, atom, direction, delta=0.02):
        return self.get_forces(displ_atom, direction)[
            (atom + 1)*3: (atom + 1)*3 + 3]

    ######################################################
    #  Force constants for atomic pairs (3 x 3 matrices)
    ######################################################

    def get_atom_pair_part(self, i, j):
        self.change_hessian_storage("dense")
        return self.hessian[i*3:i*3 + 3, j*3:j*3 + 3]

    def set_atom_pair_part(self, i, j, matrix):
        self.change_hessian_storage("dense")
        self._recalculate = True
        self.hessian[i*3:i*3 + 3, j*3:j*3 + 3] = matrix

    def add_to_atom_pair_part(self, i, j, matrix):
        self.change_hessian_storage("dense")
        self._recalculate = True
        self.hessian[i*3:i*3 + 3, j*3:j*3 + 3] += matrix

    ##############################
    #     Modes calculation
    ##############################

    def get_frequencies(self, units="cm^{-1}"):
        """Returns calculated frequencies in requested units.
        Default units are given in cm-1"""
        if self._recalculate:
            self.calculate_modes()

        to_hz = Units.ha_to_j/Units.hbar_si/(2*np.pi)

        if units.lower() in {"hz", "s^{-1}", "s", "s^-1", "s-1"}:
            conversion_factor = to_hz

        elif units.lower() in {"thz"}:
            conversion_factor = to_hz/1e12

        elif units.lower() in {
                "cm", "cm^{-1}", "cm-1", "cm^-1"}:
            conversion_factor = to_hz/Units.c_si/100

        elif units.lower() in {"mev"}:
            conversion_factor = to_hz/Units.c_si/100*Units.cm_to_mev
            print(conversion_factor)
        else:
            raise NotImplementedError(
                "Units {} are not implemented".format(units))

        return self._evals * conversion_factor

    def get_displacements(self, units="A"):
        """Returns displacement vectors"""

        if self._recalculate:
            self.calculate_modes()

        in_bohrs = self.get_mass_diagonal_matrix(inverse_square=True).dot(
            self._evects)

        if units.lower() in {"a", "angstroms"}:
            return in_bohrs * Units.bohr_to_a

        elif units.lower() in {"bohr", "bohr radius", "bohrs", "au"}:
            return in_bohrs

        else:
            raise NotImplementedError(
                "Units {} are not implemented!".format(units))

    def calculate_modes(self):
        """Calculates vibrational modes. Diagonalizes mass weighted
        Hessian (force constants) matrix and obtains
        eigenvalues and eigendisplacements

        Calculated angular frequencies are given in Atomic Units::

            omega = 2*pi*E_h/hbar

        """
        eigh = linalg.eigh

        log.debug("Start of diagonalization of {}x{} matrix".format(
            *self.shape))

        beg = time.time()

        evals, evects = eigh(
            self.get_mass_weighted_force_constants("dense"))

        evects = evects.T
        self.diag_time = time.time() - beg
        log.info("Diagonalization finished in {:.3f} s".format(
            self.diag_time))

        # Dealing with squares of negative frequencies of transl. motion
        negative = evals < 0
        evals[negative] *= -1
        evals = np.sqrt(evals)
        evals[negative] *= -1

        self._evals = evals
        self._evects = evects
        self._recalculate = False
        log.info("Found {} vibrational modes".format(len(evals)))

        modes = Modes(fc=self)
        return modes

    ##############################
    #          CUTOFF
    ##############################

    def max_cutoff(self):
        return self.atoms.get_cell().max()/2

    def get_shells(self):
        distances = set()
        for i, j in self.atoms.iter_pairs():
            vec, d = self.atoms.get_separation_vector(i, j)
            distances.add(d)

        return sorted(list(distances))

    def perform_cutoff(self, cutoff=None):
        """Performs cutoff given in angstroms!"""
        log.info("Performing cuttoff {} Angstrom".format(cutoff))

        if cutoff is None:
            cutoff = self.max_cutoff()

        distances = set()
        for i, j in self.atoms.iter_pairs():
            vec, d = self.atoms.get_separation_vector(i, j)
            distances.add(d)
            if d > cutoff:
                self.set_atom_pair_part(i, j, np.zeros([3, 3]))

        return len({i for i in distances if i < cutoff}) - 1

    ##############################
    #         PHONOPY
    ##############################

    def parse_force_constants(self, calc, filepath):
        """Reads force constants from phonopy FORCE_CONSTANTS file"""
        log.debug(
            "Reading force constants from phonopy output ({}):\n{}".format(
                calc, filepath))

        a, b = None, None

        if calc == "pwscf":
            conversion_factor = 0.5
        elif calc == "vasp":
            conversion_factor = Units.ev_to_ha/(Units.a_to_bohr**2)
        else:
            raise NotImplementedError(
                "Calculators {} not implemented!".format(calc))

        with open(filepath, "rt") as f:
            lines = f.readlines()

        matrix = []
        for nr, i in enumerate(lines[1:]):

            splited = i.split()
            if len(splited) == 1 and splited[0].endswith('1000'):
                splited = [splited[0][:-4], splited[0][-4:]]

            if len(splited) == 2:
                if a is not None:
                    try:
                        self.set_atom_pair_part(
                            a-1, b-1, np.array(matrix)*conversion_factor)
                    except:
                        print(splited)
                        print(nr, a, b)
                        print(np.array(matrix))
                        input()
                        raise

                a = int(splited[0])
                b = int(splited[1])
                matrix = []
            else:
                matrix.append(np.array(splited, float))

        self.set_atom_pair_part(a-1, b-1, np.array(matrix)*conversion_factor)

    ######################################
    #  Saving data in different formats
    ######################################

    def save_to_hdf5(self, directory, store_inverse_square_mass_matrix=True,
                     delete_after_saving=False):
        ensure_dir(directory)

        to_hz = Units.ha_to_j/Units.hbar_si/(2*np.pi)
        to_mev = to_hz/Units.c_si/100*Units.cm_to_mev

        if self.shape[1] < 64:
            chunks = (4, 4)
        else:
            chunks = (64, 64)

        log.info("Saving ForceConstants Object:")

        # ----------------------------
        #        Save Hessian
        # ----------------------------

        filepath = os.path.join(directory, "hessian.hdf5")
        log.info("* Writing hessian to: {}".format(filepath))
        fl = h5py.File(filepath, 'w')
        dataset = fl.create_dataset("hessian", self.shape, dtype='d',
                                    chunks=chunks, compression='szip',
                                    data=self.get_dense_hessian())
        dataset.attrs["units"] = "Ha/bohr^2"
        dataset.attrs['factor'] = Units.ha_to_ev/(Units.bohr_to_a**2)
        dataset.attrs['factor_units'] = "eV/A^2"
        dataset.attrs['freq_factor'] = to_mev
        dataset.attrs['freq_factor_units'] = "meV"

        if delete_after_saving:
            del self.hessian
            gc.collect()

        # -----------------------------------------------
        #     Save mass matrix used for eigenproblem
        # -----------------------------------------------
        if store_inverse_square_mass_matrix:
            dataset = fl.create_dataset("inverse_sqr_mass_matrix",
                                        self.shape, dtype='d',
                                        chunks=chunks, compression='szip',
                                        data=self.get_mass_diagonal_matrix(
                                            inverse_square=True))
            if delete_after_saving:
                del self.inv_square_masses
                gc.collect()

        fl.close()

        # -------------------------------------------
        #    Save atoms object with geometry info
        # -------------------------------------------

        with open(os.path.join(directory, "atoms.p"), "wb") as f:
            pickle.dump(self.atoms, f, pickle.HIGHEST_PROTOCOL)

    def load_from_hdf5(self, directory):
        log.info("Loading force constants object from dir: {}".format(
            directory))

        with open(os.path.join(directory, "atoms.p"), "rb") as f:
            self.atoms = pickle.load(f)

        filepath = os.path.join(directory, "hessian.hdf5")
        fl = h5py.File(filepath, 'r')
        self.hessian = fl["hessian"][:]
        self.hessian_storage = "dense"
        fl.close()

    def create_xyz_content(self, iterator=None,
                           embeding_pattern=False,
                           interaction_pattern=False):

        nr = 0
        content = ""
        if iterator is None:
            iterator = self.atoms.iterate()

        for atom in iterator:
            name = atom["symbol"]
            pos = atom["position"]

            if embeding_pattern and atom["index"] in self.embeded:
                if name != "N":
                    name = "Fe"

            atom_line = name + ' '.join('%15.9f' % F for F in pos)

            if (interaction_pattern and atom["index"]
                    in self.interaction_vectors):
                atom_line = name + ' '.join('%15.9f' % F for F in pos)
                for vec in self.interaction_vectors[atom["index"]]:
                    nr += 1
                    interact_line = atom_line
                    interact_line += "      "
                    interact_line += ' '.join(
                        ['{:15.9f}'.format(i) for i in vec]) + "\n"
                    content += interact_line

            else:
                nr += 1
                content += atom_line + "\n"

        head = "	{}\n\"{}, cubic\"\n".format(
            nr, "Unknown")

        return head + content

    def create_xsf_content(self):
        text = "CRYSTAL\n\n"
        text += "PRIMVEC\n"
        for i in self.atoms.get_cell():
            text += "   {:.10f}    {:.10f}    {:.10f}\n".format(*i)

        text += "\nPRIMCOORD\n{} 1\n".format(len(self.atoms))
        for atom in self.atoms.iterate():
            text += "{}      {:.10f}     {:.10f}      {:.10f}\n".format(
                atom["number"], *atom["position"]
            )

        return text

    def _create_phonopy_phonon(self, dim=1):
        from phonopy.structure.atoms import PhonopyAtoms
        from phonopy import Phonopy

        cell = self.atoms.get_cell("bohr")
        factor = 3634.87468282

        unitcell = PhonopyAtoms(
            symbols=np.array(self.atoms.symbols).tolist(),
            cell=cell,
            scaled_positions=self.atoms.get_scaled_positions())

        if self.atoms.symmetry is None:
            self.atoms.calculate_symmetry()

        prim = self.atoms.symmetry.prim_lattice/self.atoms.cell.max()
        phonon = Phonopy(
            unitcell,
            [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
            primitive_matrix=prim,
            factor=factor)

        force_constants = np.zeros((len(self.atoms), len(self.atoms), 3, 3))
        _i = 0
        for i, j in self.atoms.iter_pairs():
            if i != _i:
                print("Uploading to phonopy:", i, end="\r")
                _i = i
            force_constants[i][j] = self.get_atom_pair_part(i, j)*2

        phonon.set_force_constants(force_constants)
        return phonon

    def calculate_pw_dos(self):
        phonon = self._create_phonopy_phonon()
        phonon.set_mesh([100, 100, 100])
        phonon.set_total_DOS()

        return phonon.get_total_DOS()

    def calculate_bulk_band_structure(self, paths=None, labels=None,
                                      points=200, show=False, ax=None,
                                      color="red", label=None,
                                      marker='.',
                                      markersize=1,
                                      markevery=1,
                                      linestyle='-',
                                      dashes=(5, 5),
                                      linewidth=1.5,
                                      alpha=0.7,
                                      just_line=False):
        plot_kwargs = {}
        if not just_line:
            for nm, val in [
                    ["marker", marker],
                    ["markersize", markersize],
                    ["markevery", markevery],
                    ["dashes", dashes]]:
                if val is not None:
                    plot_kwargs[nm] = val

        if paths is None:
            paths = [
                [[0, 0, 0], [-0.5, -0.5, 0.0]],
                [[-0.5, -0.5, 0.0], [-5/8, -5/8, -1/4]],
                [[3/8, 3/4, 3/8], [0, 0, 0]],
                [[0, 0, 0], [0.5, 0.5, 0.5]],
                [[0.5, 0.5, 0.5], [0.0, 0.5, 0.5]],
                [[0.0, 0.5, 0.5], [0.25, 0.75, 0.5]],
                [[0.25, 0.75, 0.5], [0.5, 0.5, 0.5]]]

            labels = [
                r"$\Gamma$", "X", "K", r"$\Gamma$", "L", "X", "W", "L"]

        if self.pw_bands is None:
            if self.atoms.symmetry is None:
                self.atoms.calculate_symmetry()

            phonon = self._create_phonopy_phonon()
            bands = []
            for q_start, q_end in paths:
                q_start = np.array(q_start)
                q_end = np.array(q_end)
                band = []
                for i in range(points + 1):
                    band.append(q_start + (q_end - q_start) / points * i)

                bands.append(band)

            phonon.set_band_structure(bands)
            q_points, distances, frequencies, _ = phonon.get_band_structure()
            self.pw_bands = q_points, distances, frequencies
        else:
            q_points, distances, frequencies = self.pw_bands

        ticks = [d[0] for d in distances]
        ticks.append(distances[-1][-1])

        if show:
            import matplotlib.pyplot as plt
            f, ax = plt.subplots(1)

        if self.calc == "vasp":
            conv = Units.cm_to_mev
        elif self.calc == "pwscf":
            conv = Units.cm_to_mev

        if ax is not None:
            for distances, frequencies in zip(distances, frequencies):
                for freqs in frequencies.T:
                    ax.plot(distances, freqs*conv,
                            linestyle=linestyle, color=color,
                            linewidth=linewidth, alpha=alpha,
                            **plot_kwargs)

            if label is not None:
                ax.plot(distances, freqs*conv, label=label,
                        linestyle=linestyle, color=color,
                        linewidth=linewidth, alpha=alpha,
                        **plot_kwargs)

            ax.set_xticks(ticks)
            ax.set_xlim(left=0, right=np.array(distances).max())
            maxy = (np.array(frequencies)*conv).max()
            ax.set_ylim(bottom=0, top=maxy + maxy*0.1)
            if labels is not None:
                ax.set_xticklabels(labels)

            ax.grid(True, alpha=0.5, color="gray")

        if show:
            plt.show()

        return {
            "q_points": np.array(q_points),
            "distances": np.array(distances),
            "frequencies": np.array(frequencies),
            "ticks": ticks,
            "labels": labels,
        }

    def plot_experimental_data(self, directory, ax, ignore=None):
        if ignore is None:
            ignore = []

        files = os.listdir(directory)

        files = [i for i in files if i not in ignore]

        def scatter_plot(name):
            with open(os.path.join(directory, name),
                      newline='') as csvfile:
                x = []
                y = []
                spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
                for row in spamreader:
                    x.append(float(row[0]))
                    y.append(float(row[1]))

            if "line" in name:
                marker = "*"
                color = "red"
            elif "square" in name:
                marker = "D"
                color = "blue"
            else:
                marker = "o"
                color = "green"

            if "black_squares" in name:
                marker = "o"
                color = "black"

            ax.scatter(x, y, color=color, marker=marker)

        for nr, i in enumerate(files):
            scatter_plot(i)
