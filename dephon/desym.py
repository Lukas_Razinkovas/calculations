import numpy as np
import spglib
import pprint
import logging

log = logging.getLogger(__name__)
pp = pprint.PrettyPrinter()


operations = {
    "e":
    [[1, 0, 0],
     [0, 1, 0],
     [0, 0, 1]],
    "C3+":
    [[0, 0, 1],
     [1, 0, 0],
     [0, 1, 0]],
    "C3-":
    [[0, 1, 0],
     [0, 0, 1],
     [1, 0, 0]],
    "sig2":
    [[0, 1, 0],
     [1, 0, 0],
     [0, 0, 1]],
    "sig1":
    [[1, 0, 0],
     [0, 0, 1],
     [0, 1, 0]],
    "sig3":
    [[0, 0, 1],
     [0, 1, 0],
     [1, 0, 0]]
}


class Symmetry():
    """Class for symmetry analysis and operations."""

    def __init__(self, atoms, symprec=0.01):

        self.atoms = atoms
        self.dataset = None
        self._symprec = symprec
        self._maps = None
        self._transforms = None
        self._inverse_maps = None
        self.prim_lattice = None
        self.prim_scaled_positions = None
        self.prim_numbers = None
        self.report = None
        self.rotation_names = None
        self.calculate_symmetry()

    def calculate_symmetry(self):
        """Obtains information about supercell symmetry
        and stores it into dataset atribute."""

        self.dataset = spglib.get_symmetry_dataset(self.atoms,
                                                   symprec=self._symprec)
        self.find_primitive()

        log.info("Calculated symmetry for suppercell with {} atoms!".format(
            len(self.atoms)))

        self.report = (
            "- found pointgroup {} with {} rotations and {} translations"
            "".format(self.dataset["pointgroup"],
                      len(self.rotations),
                      len(self.translations)))

        self.rotation_names = []
        nr = 0
        for tr in self.dataset["rotations"]:
            found = False
            for name in operations:
                if np.allclose(tr, operations[name]):
                    self.rotation_names.append(name)
                    found = True

            if not found:
                self.rotation_names.append("tr{}".format(nr))
                nr += 1

        log.info(self.report)

    def find_primitive(self):
        prim = spglib.find_primitive(self.atoms)
        if prim is None:
            return

        lattice, scaled_positions, numbers = prim
        self.prim_lattice = lattice
        self.prim_scaled_positions = scaled_positions
        self.prim_numbers = numbers

    @property
    def rotations(self):
        return self.dataset["rotations"]

    @property
    def translations(self):
        return self.dataset["rotations"]

    def get_site_symmetry(self, atom_number, symprec=None):
        pos = self.atoms.scaled_positios[atom_number]

        if symprec is None:
            symprec = self._symprec

        site_symmetries = []
        for r, t in zip(self.rotations, self.translations):
            rot_pos = np.dot(pos, r.T) + t
            diff = pos - rot_pos
            if (abs(diff - np.rint(diff)) < symprec).all():
                site_symmetries.append(r)

        return np.array(site_symmetries, dtype='intc')

    def get_equivalent_atoms(self):
        if self.dataset is None:
            self.calculate_symmetry()

        return self.dataset["equivalent_atoms"]

    def get_independent_atoms(self):
        """Return list of independent atoms, which are unique
        with respect to symmetry transformations."""

        return np.unique(self.equivalent_atoms)

    def get_dublicated_atoms(self):
        """Returns indexes of atoms that can be generatet from
        independent atoms."""

        return np.array([i for i in range(len(self)) if i not in
                         self.get_independent_atoms()])

    def calculate_transformation_maps(self, transforms="rotation"):
        """Calculates symmetry mappings betweem atoms."""

        if transforms != "rotation":
            raise NotImplementedError("Implemented only for rotations!")

        self._maps = {}
        self._inverse_maps = {}
        self._transforms = {}

        for fr, i in enumerate(self.atoms.scaled_positions):
            self._maps[fr] = {}

            for tnr, trans in enumerate(self.rotations):
                transformed = trans.dot(i)

                try:
                    to = [nr for nr, i
                          in enumerate(
                              np.isclose(
                                  self.atoms.scaled_positions, transformed))
                          if i.all()][0]
                except:
                    print(trans)
                    print(transformed)
                    print(self.atoms.scaled_positions)
                    input()
                    raise

                t = self._maps[fr].get(to, [])

                t.append(tnr)
                self._maps[fr][to] = t

                t = self._transforms.get(tnr, {})
                t[fr] = to
                self._transforms[tnr] = t

                a = self._inverse_maps.get(to, {})
                b = a.get(fr, [])
                b.append(tnr)
                a[fr] = b
                self._inverse_maps[to] = a

    def get_atoms_transformations(self, transform="rotation"):
        if transform != "rotation":
            raise NotImplementedError("Implemented only for rotations!")

        if self._transforms is None:
            self.calculate_transformation_maps()

        return self._transforms


if __name__ == "__main__":
    from force_constants import read_fc
    log.setLevel(logging.INFO)

    diamond = read_fc(
        "test_data/diamond/2x2x2/phonopy_displacements/")

    dataset = diamond.atoms.calculate_symmetry()
    sym = diamond.atoms.symmetry
    pp.pprint(diamond.atoms.symmetry.prim_lattice)
