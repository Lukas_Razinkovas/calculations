import os
import numpy as np
import time
import logging
import pprint
from scipy.stats import gaussian_kde
import scipy.integrate as integrate
from scipy import sparse
import h5py
import pickle

from deutils import normalize_result

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

log = logging.getLogger(__name__)
pp = pprint.PrettyPrinter()
cm_to_mev = 1.23981e-1


class Modes():
    def __init__(self, fc=None, hdf5dir=None, log_fh=None):
        """Initialize modes object.

        Args:
            atoms(Atoms): atoms object with lattice and atoms info
            frequencies(numpy.array): frequencies given in cm^-1
            displacements(numpy.array): displacements given in Angstroms
            evects(numpy.array): eigenvectors of
        """

        self.init_len = None
        self.iprs = None
        if fc is not None:
            self.freq = fc.get_frequencies("mev")
            self.displ = fc.get_displacements("bohr radius")
            self.atoms = fc.atoms
            self.fc = fc
            self.hdf5 = False
        elif hdf5dir is not None:
            self.read_from_hdf5(hdf5dir)
            self.fc = None
            self.hdf5 = True
        else:
            raise RuntimeError("One of required arguments must be given!")

        self._normalized = None
        self._kde = None
        self.dos = None
        self.tr_names = []
        if self.iprs is None:
            self.sym_types = np.array(len(self.atoms)*3*['??'], dtype="S3")
            self.iprs = np.zeros(len(self.atoms)*3)
        self.transform_projections = {}
        self.tr_names = []
        self.write_to_file = None
        self._tmp = []

        self.log_fh = None
        if log_fh:
            self.set_log_handler(log_fh)

    def save_to_hdf5(self, directory):
        f = h5py.File(os.path.join(directory, "mode_analysis.hdf5"), "w")
        f['frequencies'] = self.freq
        f['iprs'] = self.iprs
        if self.sym_types is not None:
            f['irreps'] = self.sym_types
        f.close()

    def read_from_hdf5(self, directory, fl="modes.hdf5",
                       results="mode_analysis.hdf5"):
        self.hdf5file = os.path.join(directory, fl)
        f = h5py.File(self.hdf5file, "r")
        self.freq = f['frequencies'][:].flatten()
        f.close()

        with open(os.path.join(directory, "atoms.p"), "rb") as f:
            self.atoms = pickle.load(f)

        results = os.path.join(directory, results)
        if os.path.exists(results):
            f = h5py.File(results, "r")
            self.freq = f['frequencies'][:]
            self.iprs = f['iprs'][:]
            self.sym_types = f['irreps'][:]
            f.close()
        else:
            print("NO FILE", results)

    def get_frequencies(self, units="cm^-1"):
        if units.lower() in {"mev"}:
            return self.freq

        return self.freq/cm_to_mev

    def set_log_handler(self, hdlr):
        self.log_fh = hdlr
        log.addHandler(hdlr)

    def remove_log_handler(self):
        if self.log_fh is not None:
            log.removeHandler(self.log_fh)

    @property
    def symmetry(self):
        if self.atoms.symmetry is None:
            self.atoms.calculate_symmetry()

        return self.atoms.symmetry

    def get_normalised_displacements(self):
        """Retruns normalized to 1 displacement vectors."""
        if self._normalized is None:
            norms = np.apply_along_axis(np.linalg.norm, 1, self.displ)
            self._normalized = self.displ/norms[:, np.newaxis]

        return self._normalized

    #####################################
    #  Find equivalent transformations
    #####################################
    def find_equivalent_transformation(self, atoms):
        """Given other lattice with same geometry, but different
        atoms order finds transformation matrix"""
        atoms_transform = sparse.lil_matrix(
            (len(self.atoms)*3, len(self.atoms)*3),
            dtype=np.dtype('f8'))

        for indx_fr, pos in enumerate(self.atoms.get_positions()):
            indx_to = atoms.find_by_position(pos, pbc=False, method=3)
            for i in range(3):
                atoms_transform[indx_to*3 + i, indx_fr*3 + i] = 1

            # atoms_transform[
            #     indx_to*3: indx_to*3+3, indx_fr*3: indx_fr*3+3
            # ] = np.eye(3)

        return atoms_transform.tocsr()

    ##############################
    #  IPRS
    ##############################

    def get_displ(self, nr):
        m = 1
        if self.init_len is not None:
            if nr + 1 > self.init_len:
                nr = ((nr + 1) % self.init_len) - 1
            m = self._mult

        if self.hdf5:
            f = h5py.File(self.hdf5file, "r")
            displ = np.tile(f['displacements'][nr][:], m)
            f.close()
        else:
            displ = self.displ[nr]

        return displ

    def get_next_displ(self):
        for i in range(len(self.atoms)*3):
            yield self.f['displacements'][i]

    def calculate_iprs(self):
        if self.hdf5:
            for nr, disp in enumerate(self.get_next_displ()):
                self.iprs[nr] = self.calculate_ipr(disp)
        else:
            self.calculate_all_iprs()

    def calculate_ipr(self, displ):
        displ /= np.linalg.norm(displ)
        atoms = displ.shape[0]
        grouped_by_atom = displ.reshape(int(atoms/3), 3)
        p = np.sum(grouped_by_atom**2, axis=1)
        ipr = 1/np.sum(p**2)
        return ipr

    def calculate_all_iprs(self):
        beg = time.time()

        norm = self.get_normalised_displacements()
        rows, cols = norm.shape
        atoms = int(cols/3)
        grouped_by_atom = norm.reshape(rows, atoms, 3)
        self.iprs = 1/np.sum(np.sum(grouped_by_atom**2, axis=2)**2, 1)

        log.info("- iprs calculated in {:.3f} s".format(time.time()-beg))
        return self.iprs

    # def calculate_symmetry_transform(self, trans):
    #     atoms_transform = np.zeros(
    #         [len(self.atoms)*3, len(self.atoms)*3])

    #     for indx_fr, pos in enumerate(self.atoms.get_positions()):
    #         new_pos = trans.dot(pos)
    #         indx_to = self.atoms.find_by_position(new_pos, pbc=False,
    #                                               method=3)
    #         atoms_transform[
    #             indx_to*3: indx_to*3+3, indx_fr*3: indx_fr*3+3] = trans

    #     return atoms_transform

    def calculate_position_transformation_matrices(self):
        """Calculates matrix for mode position tranformations."""
        beg = time.time()
        self.mode_transforms = {}
        print("Sorting")
        self.atoms.sort_positions()

        for tnr, trans in enumerate(self.symmetry.rotations):

            atoms_transform = sparse.lil_matrix(
                (len(self.atoms)*3, len(self.atoms)*3),
                dtype=np.dtype('f8'))

            print("Transformation matrices")
            for indx_fr, pos in enumerate(self.atoms.get_positions()):
                print("{}/{}".format(indx_fr, len(self.atoms)), end="\r")
                new_pos = trans.dot(pos)
                indx_to = self.atoms.find_by_position(new_pos, pbc=False,
                                                      method=3)

                rows, cols = np.where(trans != 0)
                for i, j in zip(rows, cols):
                    atoms_transform[indx_to*3 + i, indx_fr*3 + j] = trans[i, j]
                # try:
                #     atoms_transform[
                #         indx_to*3: indx_to*3+3, indx_fr*3: indx_fr*3+3
                #     ] = trans
                # except ValueError:
                #     print(trans)
                #     print(indx_to*3, indx_fr*3, atoms_transform.shape)
                #     print(pos, "->", new_pos)
                #     raise

            self.mode_transforms[tnr] = atoms_transform.tocsr()

        log.info("- mode tranform matrices calculated in {:.3f} s".format(
            time.time()-beg))
        return self.mode_transforms

    ##############################
    #        SYMMETRIES
    ##############################

    def prep_for_irrep_calculations(self, rtol=1e-1, atol=1e-1):
        if self.symmetry.dataset["pointgroup"] != "3m":
            log.info("- pointgroup {} analysis is not implemented".format(
                self.symmetry.dataset["pointgroup"]
            ))
            self.sym_types = None
            return False

        log.info("- found pointgroup: {} (c3v) ".format(
            self.symmetry.dataset["pointgroup"]))

        def isclose(x, y):
            return np.isclose(x, y, rtol=rtol, atol=atol)

        self.classifiers = {
            "A1": lambda x: np.where(
                isclose(x["C3+"], 1) & isclose(x["sig1"], 1)),
            "A2": lambda x: np.where(
                isclose(x["C3+"], 1) & isclose(x["sig1"], -1)),
            "Exy": lambda x: np.where(
                isclose(x["C3+"], -0.5) & isclose(x["C3-"], -0.5)),
            "Ex": lambda x: np.where(
                isclose(x["C3+"], -0.5) & isclose(x["sig1"], 1)),
            "Ey": lambda x: np.where(
                isclose(x["C3+"], -0.5) & isclose(x["sig1"], -1))
        }

        self.classifiers_order = ["A1", "A2", "Exy", "Ex", "Ey"]
        self.calculate_position_transformation_matrices()

        for nm in self.symmetry.rotation_names:
            self.tr_names.append(nm)
            if self.hdf5:
                self.transform_projections[nm] = np.zeros(len(self.atoms)*3)

        return True

    def calculate_mode_symmetry(self, displ, nr):
        displ /= np.linalg.norm(displ)
        self.tr_names = []
        transform_projection = {}
        for name, trnr in zip(
                self.symmetry.rotation_names, self.mode_transforms):
            new_trans = self.mode_transforms[trnr]
            self.tr_names.append(name)
            pr = new_trans.dot(displ).dot(displ)
            self.transform_projections[name][nr] = pr
            transform_projection[name] = np.array([pr])

        for i in self.classifiers_order:
            if len(self.classifiers[i](transform_projection)[0]) > 0:
                return i

    def calculate_sphere_weight(self, displ, point, radius=4):
        weight = 0
        displ /= np.linalg.norm(displ)
        for nr, i in enumerate(displ.reshape((len(self.atoms), 3))):
            pos = self.atoms.get_positions()[nr]
            if np.linalg.norm(pos - np.array(point)) < radius:
                weight += sum(i**2)

        return weight

    def calculate_all_irreps(self):
        # Finds irreducible representations with single shot
        beg = time.time()
        norm = self.get_normalised_displacements()

        self.tr_names = []
        for name, trnr in zip(
                self.symmetry.rotation_names, self.mode_transforms):
            new_trans = self.mode_transforms[trnr]

            self.tr_names.append(name)
            self.transform_projections[name] = np.apply_along_axis(
                lambda x: new_trans.dot(x).dot(x), 1, norm)

        for i in self.classifiers_order:
            self.sym_types[self.classifiers[i](self.transform_projections)] = i

        log.info("- irreducible reps determined in {:.3f} s".format(
            time.time()-beg))

    ##############################
    #          Analyse
    ##############################

    def analyse(self, logtillipr=None, write_to_file=None):
        log.info("\n" + "-"*70 +
                 "\nAnalysing obtained phonon modes\n" +
                 "-"*70)
        if logtillipr is None:
            logtillipr = len(self.atoms)*0.15

        self.weights = np.zeros(len(self.freq))
        if self.hdf5:
            self.analyse_mode_by_mode()
        else:
            self.calculate_iprs()
            self.calculate_dos()
            if self.prep_for_irrep_calculations():
                self.calculate_all_irreps()
                self.print_unknown_symmetry()

            self.print_small_ipr_info(logtillipr=logtillipr)

    def open_file(self):
        self.f = h5py.File(self.hdf5file, "r")

    def close_file(self):
        self.f.close()

    def analyse_weights_and_save_to_hdf5(self, directory, logfile=None):
        self.f = h5py.File(self.hdf5file, "r")
        beg = time.time()
        self.weights = np.zeros(len(self.freq))
        for nr, displ in enumerate(self.get_next_displ()):
            if nr % 10 == 0:
                info = "{}/{} ({:.3f} \% {:.1f} s)".format(
                    nr, len(self.atoms)*3, (100/(len(self.atoms)*3))*nr,
                    time.time() - beg)
                print(info, end="\r")
                if logfile is not None:
                    with open(logfile, "wt") as f:
                        f.write(info)

            self.weights[nr] = self.calculate_sphere_weight(
                    displ, [4, 4, 4], radius=4)
        self.f.close()

        f = h5py.File(os.path.join(directory, "mode_analysis.hdf5"), "r+")
        try:
            f['weights'] = self.weights
        except:
            f['weights'][...] = self.weights

        f.close()

    def analyse_mode_by_mode(self, write_to_file=None):
        print("Analyse mode by mode")

        if write_to_file is not None:
            with open(write_to_file, "wt") as f:
                f.write("{:>6}  {:<9}  {:>12}  {:<3}\n".format(
                    "NR", "Fr(meV)", "IPR", "Sym"))

        print("Reading HDF5 file")
        self.f = h5py.File(self.hdf5file, "r")
        print("Preparing for calculations")
        sym = self.prep_for_irrep_calculations()
        self.weights = np.zeros(len(self.freq))

        print("Iterating")
        for nr, displ in enumerate(self.get_next_displ()):
            print("{}/{}".format(nr, len(self.atoms)*3), end="\r")

            self.iprs[nr] = self.calculate_ipr(displ)
            if sym:
                self.sym_types[nr] = self.calculate_mode_symmetry(displ, nr)
                self.weights[nr] = self.calculate_sphere_weight(
                    displ, [4, 4, 4], radius=4)
                if nr == 260:
                    h = self._mode_header()
                    print("-"*(len(h) + 2))
                    print(h)
                    print("-"*(len(h) + 2))
                if 260 <= nr <= 280:
                    print(self._mode_info(nr))
            if write_to_file is None:
                continue

            s = "--"
            if sym:
                s = self.sym_types[nr]

            with open(write_to_file, "at") as f:
                try:
                    np.char.decode(s)
                except:
                    pass

                f.write("{:>6}  {:>9.5f}  {:>12.3f}  {:>3}\n".format(
                    nr, self.freq[nr], self.iprs[nr], str(s)))

        self.f.close()

    def get_kde(self, bw=0.055, ignore_trans=True):
        if self._kde is None:
            vals = self.freq
            if ignore_trans:
                vals = self.freq[3:]
            self._kde = gaussian_kde(vals)
            self._kde.set_bandwidth(bw)

        return self._kde

    def calculate_dos(self, bw=0.055, points=2000, ignore_trans=True):
        beg = time.time()

        vals = self.freq
        if ignore_trans:
            vals = self.freq[3:]

        kde = self.get_kde(bw, ignore_trans)

        xlim = np.array([0, vals.max() + 100])
        x = np.linspace(*xlim, 1000)
        y = kde(x)

        self.dos = {
            "freq": x,
            "dos": normalize_result(x, y)
        }

        log.info("- dos calculated in {:.3f} s".format(
            time.time()-beg))

        return self.dos

    ##################################
    #  Printing, saving and loading
    ##################################

    def _mode_info(self, nr, just_data=False):
        data = [nr,
                self.get_frequencies("meV")[nr],
                self.iprs[nr]]
        st = "{:>9} {:>12.3f} {:>10.3f}"

        if self.sym_types is not None:
            st += " {:>9.4f}"
            st += " {:>6}"
            try:
                data.append(self.weights[nr])
                data.append(str(np.char.decode(self.sym_types[nr])))
            except AttributeError:
                data.append(self.weights[nr])
                data.append(self.sym_types[nr])

        for i in self.tr_names:
            st += " {:7.3}".format(self.transform_projections[i][nr])

        if just_data:
            return data

        return st.format(*data)

    def _mode_header(self):
        header = "{:>9} {:>12} {:>10}".format(
            "Mode Nr", "Freq(meV)", "IPR")

        if self.sym_types is not None:
            header += " {:>9}".format("Weight")
            header += " {:>6}".format("IRREP")

        for i in self.tr_names:
            header += " {:>7}".format(i)

        return header

    def print_unknown_symmetry(self, logtillipr=35):
        if self.sym_types is None:
            return

        indx = np.where(self.sym_types == b'??')[0]

        header = self._mode_header()
        if len(indx):
            log.info(
                "\n     All modes with undetermined symmetry ({}):".format(
                    len(indx)))
            log.info(" "*5 + "."*(len(header)-5))
            log.info(header)
            log.info(" "*5 + "."*(len(header)-5))

        for i in indx:
            log.info(self._mode_info(i))

        if len(indx):
            log.info(" "*5 + "."*(len(header)-5))

    def print_small_ipr_info(self, logtillipr=35):
        indx = np.where(self.iprs < logtillipr)[0]

        header = self._mode_header()
        if len(indx):
            log.info("\n     All modes with ipr < {}:".format(logtillipr))
            log.info(" "*5 + "."*(len(header)-5))
            log.info(header)
            log.info(" "*5 + "."*(len(header)-5))

        for i in indx:
            log.info(self._mode_info(i))

        if len(indx):
            log.info(" "*5 + "."*(len(header)-5))

    def write_mode_frequencies_to_file(self, filepath):
        log.info("* writting freq. (ipr) to file: {}".format(
            filepath))

        types = [
            ("nr", np.int),
            ("freq", np.float),
            ("freqmev", np.float),
            ("ipr", np.float)]

        fmt = ["%d", "%.6f", "%.6f", "%.6f"]
        if self.sym_types is not None:
            types.append(("irrep", "<U10"))
            fmt.append("%3s")
            for nm in self.tr_names:
                types.append((nm, np.float))
                fmt.append("%.6f")

        data = np.zeros(self.freq.size, dtype=types)

        data['nr'] = np.arange(len(self.freq))
        data['freq'] = self.freq
        data['freqmev'] = self.get_frequencies("meV")
        data['ipr'] = self.iprs

        if self.sym_types is not None:
            data['irrep'] = self.sym_types
            for nm in self.tr_names:
                data[nm] = self.transform_projections[nm]

        fmt = ",".join(fmt)
        np.savetxt(filepath, data.T, fmt=fmt, delimiter=",",
                   header=self._mode_header().replace(" ", ""))

    def write_mode_displacements_to_file(self, filepath):
        log.info("* writting displ. info to file: {}".format(
            filepath))
        np.savetxt(filepath, self.displ, "%.6e", delimiter=",",
                   header="Mode Displacement Vectors (Angstroms)")

    def write_dos_to_file(self, filepath):
        log.info("* writting phonon dos to file: {}".format(filepath))
        if self.dos is None:
            self.calculate_dos()

        np.savetxt(
            filepath, np.array([self.dos["freq"], self.dos["dos"]]).T,
            delimiter=",",
            header="Normalised dos plot (1/cm)")

    def write_all_info(self, directory):
        self.write_mode_frequencies_to_file(os.path.join(
            directory, "{}.csv".format("freq")))
        self.write_mode_displacements_to_file(os.path.join(
            directory, "{}.csv".format("displ")))
        self.write_dos_to_file(os.path.join(
            directory, "{}.csv".format("dos")))

    def read_modes_info(self, directory):
        """Reads saved data about modes."""

        ##############################
        #    FREQ IPRS AND IRREPS
        ##############################

        freqpath = os.path.join(directory, "{}.csv".format("freq"))
        with open(freqpath, 'r') as f:
            first_line = f.readline()
            assert first_line.startswith(
                "#"), "Wrong freq.csv header: {}".format(first_line)

        vals = [i.lstrip().rstrip()
                for i in first_line[1:].rstrip().split(",")]

        assert len(vals) >= 4, "Wrong freq.csv header: {}".format(first_line)
        print([repr(i) for i in vals])

        types = [
            ("nr", np.int),
            ("freq", np.float),
            ("freqmev", np.float),
            ("ipr", np.float)]

        if len(vals) > 4:
            types.append(("irrep", "<U10"))
            for nm in vals[5:]:
                types.append((nm, np.float))

        data = np.genfromtxt(freqpath,
                             dtype=types,
                             delimiter=',', unpack=True)

        self.freq = data["freq"]
        self.iprs = data["ipr"]
        if len(vals) > 4:
            self.sym_types = data["irrep"]
            for nm in vals[5:]:
                if nm == "C3":
                    nm = "C3+"
                elif nm == "C3_1":
                    nm = "C3-"
                self.transform_projections[nm] = data[nm]

        ##############################
        #      DISPLACEMENTS
        ##############################

        displpath = os.path.join(directory, "{}.csv".format("displ"))
        self.displ = np.loadtxt(displpath, delimiter=",")

        #########
        #  DOS
        #########

        dospath = os.path.join(directory, "{}.csv".format("dos"))
        if os.path.exists(dospath):
            data = np.loadtxt(dospath, delimiter=",", unpack=True)
            self.dos = {
                "freq": data[0],
                "dos": data[1]
            }

    ##############################
    #  PLOTING
    ##############################

    def _add_mev_axis(self, ax):
        mn, mx = ax.get_xlim()
        cmax = ax.twiny()
        cmax.set_xlim(mn*cm_to_mev, mx*cm_to_mev)
        cmax.set_xlabel("meV")
        lim = cmax.get_xlim()
        cmax.set_xticks(np.arange(*lim, 20))

    def plot_dos(self, ax, bw=0.055, title=None, mev=True, **kwargs):
        if self.dos is None:
            self.calculate_dos(bw=bw)

        ax.plot(self.dos["freq"], self.dos["dos"],
                **kwargs)

        if title is not None:
            ax.set_title(title, y=1.2)

        ax.set_xlabel("cm$^{-1}$")
        ax.set_ylabel("DOS")
        ax.grid(True)
        ax.legend(loc=2)

    def _plot_diff_of_dos(self, ax, modes, name1, name2,
                          integration_points=400):
        x = np.linspace(
            min([min(self.dos["freq"]), min(modes.dos["freq"])]),
            max([max(self.dos["freq"]), max(modes.dos["freq"])]) + 200,
            integration_points)

        def diff(x):
            return (self.get_kde()(x)*len(self.dos["dos"]) -
                    modes.get_kde()(x)*len(modes.dos["dos"]))

        ax.fill_between(x, 0, diff(x), color="maroon",
                        label=name2 + " $-$ " + name1)

        ax.set_xlabel("cm$^{-1}$")
        ax.set_ylabel(
            "$\mathrm{{DOS}}_{{\mathrm{{{}}}}}(\epsilon)-".format(name1) +
            "\mathrm{{DOS}}_{{\mathrm{{{}}}}}(\epsilon)$".format(name2))
        ax.grid(True)
        ax.legend()
        return diff, x

    def _plot_integral(self, ax, x, diff, name1, name2):
        integral = []
        for nr, till in enumerate(x, 1):
            print("Integrating:{}/{}".format(nr, len(x)), end="\r")
            integral.append(integrate.quad(diff, min(x), till)[0])

        ax.fill_between(
            x, 0, integral, color="maroon")

        ax.set_xlabel("cm$^{-1}$")
        ax.set_ylabel(
            r"$\int_0^{{\epsilon}} \mathrm{{d}}\epsilon'\,\mathrm{{DOS}}" +
            "_{{\mathrm{{{}}}}}(\epsilon')-".format(name1) +
            r"\mathrm{{DOS}}" +
            "_{{\mathrm{{{}}}}}(\epsilon')$".format(name2))

        ax.grid(True)

    def compare_freq_displacements(self, ax, modes, small_ipr=False):
        energies = []
        diffs = []
        dots = []
        displ1 = self.get_normalised_displacements()
        displ2 = modes.get_normalised_displacements()

        if small_ipr:
            till_ipr = 0.15*len(self.atoms)
            iterator = zip(
                np.where(self.iprs < till_ipr)[0],
                displ1[np.where(self.iprs < till_ipr)[0]])
        else:
            iterator = enumerate(displ1)

        tr = self.find_equivalent_transformation(modes.atoms)
        for i, vec1 in iterator:
            vec1 = tr.dot(vec1)
            dot = np.around(abs(np.apply_along_axis(vec1.dot, 1, displ2)), 4)
            dots.append(dot.max())
            energies.append(self.freq[i])
            d = self.freq[i] - modes.freq[dot.argmax()]
            if d*cm_to_mev > 100:
                print("-"*70)
                print("Indexes:", i, dot.argmax())
                print("Projection:", dot.max())
                print("Diff:", d*cm_to_mev)

            diffs.append(d)

        energies = np.array(energies[3:])
        diffs = np.array(diffs[3:])*cm_to_mev
        dots = abs(np.array(dots[3:]) - 1)

        if not small_ipr:
            ax.plot(energies, diffs, ".", color="red", markersize=7,
                    alpha=0.5)
        else:
            ax.plot(energies, diffs, ".", color="blue", markersize=9,
                    alpha=1, label="small ipr")
            ax.set_xlabel("cm$^{-1}$")
            ax.set_ylabel("$\\Delta E$ (meV)")
            ax.legend()
            ax.grid(True)

    def compare_small_ipr_freq(self, ax, modes):
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
        colLabels = ("nr", "freq (cm$^{-1}$)", "freq (meV)", "IPR", "IRREP")

        data = []

        for i in np.where(self.iprs < 35)[0]:
            line = []
            for i in self._mode_info(i, just_data=True):
                if isinstance(i, float):
                    line.append("{:.3f}".format(i))
                else:
                    line.append(i)

            data.append(line)

        data.append(["---"]*5)

        for i in np.where(modes.iprs < 35)[0]:
            line = []
            for i in modes._mode_info(i, just_data=True):
                if isinstance(i, float):
                    line.append("{:.3f}".format(i))
                else:
                    line.append(i)

            data.append(line)

        the_table = ax.table(cellText=data,
                             colLabels=colLabels,
                             loc='center')
        ax.axis('off')
        the_table.scale(1, 2)
        # the_table.scale(1.5, 1.5)

    def compare_phonon_dispersion(self, modes, ax, differences=None, lw=1.5):
        colors = ["darkmagenta", "darkcyan"]

        assert self.fc is not None, "Force Constants object must be given"
        assert modes.fc is not None, "Force Constants object must be given"

        if not hasattr(self, 'band_struc'):
            self.band_struc = self.fc.calculate_bulk_band_structure()
            self.band_struc2 = modes.fc.calculate_bulk_band_structure()

        band_struc = self.band_struc
        band_struc2 = self.band_struc2

        lbl_set = False
        intervals = []

        for d1, d2, f1, f2 in zip(
                band_struc["distances"],
                band_struc2["distances"],
                band_struc["frequencies"]*cm_to_mev,
                band_struc2["frequencies"]*cm_to_mev):
            intervals.append([d1.min(), d1.max()])

            for freqs1, freqs2 in zip(f1.T, f2.T):
                if not lbl_set:
                    ax.plot(d1, freqs1, colors[0], linewidth=lw,
                            label="all int.")
                    ax.plot(d1, freqs2, colors[1], linewidth=lw,
                            label="cropped int.")
                    lbl_set = True
                else:
                    ax.plot(d1, freqs1, colors[0], linewidth=lw)
                    ax.plot(d1, freqs2, colors[1], linewidth=lw)

                if differences is None:
                    continue

                diff = (freqs1 - freqs2)
                indx = diff.argmax()
                if abs(diff[indx]) > differences:
                    c = "gray"
                    if abs(diff[indx]) >= 1:
                        c = "yellow"
                    if abs(diff[indx]) >= 2:
                        c = "red"

                    ax.annotate(
                        "{:.1f} meV".format(diff[indx]),
                        xy=(d1[indx], freqs1[indx]),
                        # xytext=(d1[indx], freqs1[indx] + 15),
                        arrowprops=dict(
                            facecolor=c,
                            shrink=0.002, width=0.2)
                    )

        ax.set_xticks(band_struc["ticks"])
        ax.set_xlim(left=0, right=band_struc["distances"].max())
        ax.grid(True, alpha=0.5, color="gray")
        maxy = (band_struc["frequencies"]*cm_to_mev).max()

        ax.set_ylim(bottom=0, top=int((maxy + 20)/10)*10)

        if band_struc["labels"] is not None:
            ax.set_xticklabels(band_struc["labels"])

        ax.set_ylabel('meV')
        ax.set_xlabel('Wave vector')
        return intervals

    def compare_dos(self, axes, modes, name1, name2,
                    integration_points=400,
                    plots="difference", title=None,
                    axes_map=None,
                    **kwargs):

        """Compares dos with other modes object"""
        colors = ["darkmagenta", "darkcyan"]

        if self.iprs is None:
            self.analyse()
        if modes.iprs is None:
            modes.analyse()

        if plots in {"difference"}:
            assert len(axes) == 3
        elif plots in {"freq_displ"}:
            assert len(axes) == 5

        ##############################
        #  DOS PLOT
        ##############################

        self.plot_dos(axes[0], label=name1, title=title,
                      color=colors[0], mev=False, **kwargs)
        modes.plot_dos(axes[0], label=name2, color=colors[1], **kwargs)

        if plots in {"dos", "only_dos"}:
            axes[0].legend()
            return

        diff, x = self._plot_diff_of_dos(axes[1], modes, name1, name2,
                                         integration_points=integration_points)

        self._plot_integral(axes[2], x, diff, name1, name2)

        xlim = axes[0].get_xlim()
        for ax in axes[:3]:
            ax.set_xlim(xlim)
            self._add_mev_axis(ax)

        if plots in {"freq_displ"}:
            self.compare_freq_displacements(axes[3], modes)
            self.compare_freq_displacements(axes[3], modes, small_ipr=True)
            axes[3].set_xlim(xlim)
            self._add_mev_axis(axes[3])
            self.compare_small_ipr_freq(axes[4], modes)
        elif plots in {"difference"}:
            return
        else:
            raise NotImplementedError("Plots type {} not implemented".format(
                plots))

    def multiply(self, nr):
        from demegabulk import multiply_atoms_object
        self.init_len = len(self.atoms)
        self._mult = nr*nr*nr
        self.atoms = multiply_atoms_object(self.atoms, [nr, nr, nr])

    def defect_plane_iterator(self, defect, dist1=4, dist2=2):
        ones = np.ones(3)
        ones = ones/np.linalg.norm(ones)
        # print("DEFECT:", defect)

        for a in self.atoms.iterate():
            fr_defect = []
            for def_pos in defect:
                v, _ = self.atoms.get_separation_vector(def_pos, a["position"])
                fr_defect.append(v)
            d = np.apply_along_axis(ones.dot, 1, np.array(fr_defect))
            # print("Pos:", a["position"])
            # print(fr_defect)
            # print(d)
            # input()

            if -dist2 < d[0] < dist1:
                vec, _ = self.atoms.get_separation_vector(
                    defect[0], a["position"])
                a["position"] = defect[0] + vec
                yield a

            if -dist2 < d[1] < dist1:
                vec, _ = self.atoms.get_separation_vector(
                    defect[1], a["position"])
                a["position"] = defect[1] + vec
                yield a

    def create_mode_xyz_content(self, mode_nr, defect, dist=3, multiply=2):
        from deutils import get_transformation

        tr = get_transformation([0, 0, 0], [1, 1, 1], [0, 0, -1])

        content = ""
        displ = self.get_displ(mode_nr)
        displ *= 2000
        iterator = self.defect_plane_iterator(defect)
        nr = 0
        for atom in iterator:
            nr += 1
            name = atom["symbol"]
            pos = atom["position"]
            indx = atom["index"]
            astr = name + ' '.join('%15.9f' % F for F in tr(pos))

            try:
                d = tr(displ[indx*3: indx*3 + 3])
            except:
                print(len(displ))
                print(indx*3)
                input()
                raise

            astr += "      "
            astr += ' '.join(['{:15.9f}'.format(i) for i in d])
            content += astr + "\n"

        content = "	{}\n\"{}, cubic\"\n".format(
            nr, "Unknown") + content

        return content

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from defc import ForceConstants

    # fc = ForceConstants("vasp", phonopydir="/home/lukas/super/calculations/diamond/vasp/nv2x2x2/phonopy_phonons")
    # modes = fc.calculate_modes()
    # modes.analyse()

    cell = 4
    modes = Modes(
        hdf5dir="../diamond/vasp/embeded/nv{0}x{0}x{0}/".format(cell))
    # modes.multiply(2)

    defects = [[3.5737, 3.5737, 3.5737], [4.4671, 4.4671, 4.4671]]

    # nr = 3
    iprsort = np.argsort(modes.iprs)

    # modes.analyse()
    # plt.scatter(modes.freq, modes.weights, color="red", label="weights")
    # # plt.scatter(modes.freq, modes.iprs, color="red", label="weights")
    # plt.savefig("weights.pdf")

    for nr, i in enumerate(iprsort[:10]):
        print("{}example{}{}.xyz".format(
            cell, nr, modes.sym_types[i].decode()))
        d = modes.get_displ(nr)
        modes.calculate_sphere_weight(d, [4, 4, 4])
        text = modes.create_mode_xyz_content(i, defects)
        with open("/home/lukas/programming/blender/{}example{}{}.xyz".format(
                cell, nr, modes.sym_types[i].decode()),
                  "wt") as f:
            f.write(text)

    # modes.multiply(3)

    # defects = [[3.5737, 3.5737, 3.5737], [4.4671, 4.4671, 4.4671]]

    # nr = 3
    # iprsort = np.argsort(modes.iprs)

    # for nr, i in enumerate(iprsort[:10]):
    #     text = modes.create_mode_xyz_content(i, defects)
    #     with open("/home/lukas/programming/blender/2example{}{}.xyz".format(
    #             nr, modes.sym_types[i].decode()),
    #               "wt") as f:
    #         f.write(text)

        # modes.analyse_mode_by_mode("results.dat")


    # from defc import ForceConstants
    # import matplotlib.pyplot as plt
    # from phonopy.harmonic.force_constants import symmetrize_force_constants

    # def symmetrize_fc(fc):
    #     """
    #     Phi_ij_ab = Phi_ji_ba

    #     i, j: atom index
    #     a, b: Cartesian axis index
    #     """

    #     force_constants = np.zeros((len(fc.atoms), len(fc.atoms), 3, 3))
    #     _i = 0
    #     for i, j in fc.atoms.iter_pairs():
    #         if i != _i:
    #             print("Uploading to phonopy:", i, end="\r")
    #             _i = i
    #             force_constants[i][j] = fc.get_atom_pair_part(i, j)*2

    #     symmetrize_force_constants(force_constants)
    #     for i, j in fc.atoms.iter_pairs():
    #         if i != _i:
    #             print("Gathering symmetrized force constants:", i, end="\r")
    #             _i = i

    #     fc.set_atom_pair_part(i, j, force_constants[i][j]/2)

    # ##############################
    # #           Defect
    # ##############################

    # fr = 3
    # calc = "vasp"
    # defect_fc_dir = "../diamond/{1}/nv{0}x{0}x{0}/phonopy_phonons".format(
    #     fr, calc)
    # defect_fc = ForceConstants(calc, outdir=defect_fc_dir)

    # modes = defect_fc.calculate_modes()
    # modes.analyse()

    # defect_fc.symmetrize_fc()
    # modes = defect_fc.calculate_modes()
    # modes.analyse()

    # defect_fc = ForceConstants(calc, outdir=defect_fc_dir)
    # defect_fc.symmetrize_fc(mytype=2)
    # modes = defect_fc.calculate_modes()
    # modes.analyse()
