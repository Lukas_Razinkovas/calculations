"""Module for generation of different job files for different
clusters. Just to make execution simmilar for diferent
supercomputers"""

JOBPY = """import subprocess

files = [
{files}
]

for i in files:
    out = i[:-2] + 'out'
    subprocess.call(
        "mpirun -np {np} {pwx} -inp {workdir}/{{ifile}} "
        "> {workdir}/{{ofile}}".format(
            ifile=i, ofile=out),
        shell=True,
        cwd="..")
"""


JOBPBS = """#!/bin/bash
#PBS -l select={nodes}
#PBS -q apc14j
#PBS -N {name}{number}
cd $PBS_O_WORKDIR
date >&2 ;hostname >&2

module add use.sistema USE.compilers intell_10.0
python3 phononsjob{number}.py >&2

date >&2
"""


clusters = {
    ##############################
    #           RAIJIN
    ##############################
    "raijin": {
        "vasp": {
            "jobfile": "\n".join(
                [
                    "#!/bin/bash",
                    "#PBS -l walltime={time}",
                    "#PBS -l mem={mem}",
                    "#PBS -l ncpus={cpus}",
                    "#PBS -l software=vasp",
                    "#PBS -l wd\n",
                    "module load vasp/5.4.1\n"
                ]),
            "execline": 'mpirun --wd "{cwd}" {exec}\n',
            "jobfields": ["cpus"],
            "execfields": ["exec", "cwd"],
            "execpath": "/global/homes/a/audrius/VASP/",
            "time": "24:00:00",
            "mem": "32gb",
        }
    },
    ##############################
    #           CORI
    ##############################
    "cori": {
        "submit_cmd": "sbatch",
        ############
        #   VASP
        ############
        "vasp": {
            "jobfile": "\n".join(
                [
                    "#!/bin/bash -l",
                    "#SBATCH -p {plan}",
                    "#SBATCH -N {nodes}",
                    "#SBATCH -C haswell",
                    "#SBATCH -t {time}",
                    "#SBATCH -L SCRATCH,project",
                    "#SBATCH -A m892",
                    "#SBATCH --qos=premium",
                    "#SBATCH -o {output}\n",
                    "#SBATCH -J {name}\n\n"
                ]),
            "execline": 'srun -n {np} --chdir="{cwd}"  {execpath}{exec}\n',
            "jobfields": ["nodes", "name"],
            "execfields": ["exec", "np", "cwd"],
            "execpath": "/global/homes/a/audrius/VASP/",
            "plan": "debug",
            "time": "1-00:00"
        },
        ############
        #   PWSCF
        ############
        "pwscf": {
            "jobfile": "\n".join(
                [
                    "#!/bin/bash -l",
                    "#SBATCH -p {plan}",
                    "#SBATCH -N {nodes}",
                    "#SBATCH -C haswell",
                    "#SBATCH -t {time}",
                    "#SBATCH -L SCRATCH,project",
                    "#SBATCH -A m892",
                    "#SBATCH --qos=premium",
                    "#SBATCH -J {name}\n",
                    "#SBATCH -o {output}\n",
                    "module load espresso/6.0\n\n"
                ]),
            "execline": ('srun -n {np} --chdir="{cwd}" pw.x '
                         '-inp {fname}.in > {fname}.out\n'),
            "jobfields": ["nodes", "name"],
            "execfields": ["np", "cwd", "fname"],
            "execpath": None,
            "plan": "debug",
            "time": "1-00:00",
        }
    },
    ##############################
    #       HPC SAULETEKIS
    ##############################
    "hpc-sauletekis": {
        "vasp": {
            "jobfile": "\n".join(
                [
                    "#!/bin/bash",
                    "#PBS -l select={nodes}",
                    "#PBS -q apc14j",
                    "#PBS -N {name}",
                    "cd $PBS_O_WORKDIR",
                    "date >&2 ;hostname >&2\n",
                    "source /opt/intel/Compiler/11.1/073/mkl/tools/" +
                    "environment/mklvarsem64t.sh",
                    "source /opt/intel/impi/4.0.3.008/intel64/bin/mpivars.sh",
                    "source /opt/intel/Compiler/11.1/073/bin/" +
                    "iccvars.sh intel64\n\n",
                ]),
            "execline": 'mpirun -np {np} --wdir="{cwd}"  {execpath}{exec}\n',
            "jobfields": ["nodes", "name"],
            "execfields": ["exec", "np", "cwd"],
            "execpath": "~/src/vasp.5.4.1/bin/"
        }
    },
}


class Cluster():
    def __init__(self, name, calc):
        assert name in clusters, "Unknown cluster {}!".format(name)
        assert calc in clusters[name], "Unknown calculator {}!".format(name)
        self.jobfields = clusters[name][calc]["jobfields"]
        self.execfields = clusters[name][calc]["execfields"]
        self.execline = clusters[name][calc]["execline"]
        self.execpath = clusters[name][calc]["execpath"]
        self.jobfile = clusters[name][calc]["jobfile"]

        if "plan" in clusters[name][calc]:
            self.plan = clusters[name][calc]["plan"]
        else:
            self.plan = None

        if "mem" in clusters[name][calc]:
            self.mem = clusters[name][calc]["mem"]
        else:
            self.mem = None

        # self.submit_cmd = clusters[name]["submit_cmd"]
        self.time = clusters[name][calc]["time"]

    def generate_job_file(self, jobparams, jobs):
        for i in self.jobfields:
            assert i in jobparams, i

        if "time" not in jobparams:
            jobparams["time"] = self.time

        if "plan" not in jobparams:
            jobparams["plan"] = self.plan

        if "mem" not in jobparams:
            jobparams["mem"] = self.mem

        if "output" not in jobparams:
            jobparams["output"] = '"slurm-%A-%a.out"'

        if jobparams["plan"] == "debug":
            jobparams["time"] = "30:00"

        jobfile = self.jobfile.format(**jobparams)
        if jobparams.get("qos") != "premium":
            jobfile = jobfile.replace("\n#SBATCH --qos=premium", "")

        for job in jobs:
            for field in self.execfields:
                assert field in job, "Missing job field {}!".format(field)

            if "execpath" not in job:
                job["execpath"] = self.execpath

            jobfile += self.execline.format(**job)

        return jobfile

if __name__ == "__main__":
    clust = Cluster("raijin", "vasp")
    jobparams = {
        "cpus": 32,
        "name": "test",
    }

    jobs = [{"cwd": ".", "np": 64, "exec": "vasp_std"} for i in range(3)]
    print(clust.generate_job_file(jobparams, jobs))
