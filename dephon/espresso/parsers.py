"""Quantum espresso pw.x input and output parsers.
Obtains atomic positions and forces

Requires ase:
$ sudo pip3 install ase
"""

import os
import pprint
import re
import numpy as np
import logging
from deatoms import Atoms
from deutils import Units
# from utils import ensure_dir
from ase import units


log = logging.getLogger(__name__)
pp = pprint.PrettyPrinter()


def f2f(value):
    """Converts a fortran-formatted double precision number (e.g., 2.323d2)
    to a python float. value should be a string."""
    value = value.replace('d', 'e')
    value = value.replace('D', 'e')
    return float(value)


class SimplifiedOutputParser():
    def __init__(self, filepath):
        self.filepath = filepath

    def get_energy(self):

        with open(self.filepath, "rt") as f:
            content = f.read()

        e = []
        for i in re.finditer("! *total energy *= *(.*?) Ry", content):
            e.append(float(i.group(1)) * Units.ry_to_ev)

        return e

    def get_forces(self):
        with open(self.filepath, "rt") as f:
            content = f.read()

        for st in re.finditer(r" *Forces acting on atoms \(Ry/au\): *\n *\n",
                              content):
            pass

        for en in re.finditer(r" *The non-local contrib.  to forces *\n",
                              content):
            pass

        force_set = []
        for line in content[st.end(): en.start()].split("\n"):
            if "force =" not in line:
                continue

            fc = re.search("force = (.*)", line)
            force_set.append([float(i)*Units.ry_to_ev/(Units.bohr_to_a**2)
                              for i in fc.group(1).split()])

        return np.array(force_set)

    def force_max(self):
        return np.apply_along_axis(np.linalg.norm, 0, self.get_forces()).max()

    def relax_info(self):
        e = self.get_energy()
        return e[0], e[-1], e[0] - e[-1]


class EspressoInput():
    """Quantum Espresso input parser."""
    def __init__(self, filepath):
        self.filepath = filepath
        self.lines = None
        self.data = None
        self.cell_text = None
        self.atoms = None
        self.atoms_text = None
        self.read_espresso_in()
        if self.atoms.numbers is None:
            self.atoms._symbols_to_numbers()
        if self.atoms.masses is None:
            self.atoms._symbols_to_masses()
        log.debug("Input file succesfully loaded:\n{}".format(filepath))

    def filename(self):
        return os.path.split(self.filepath)[-1]

    def basename(self):
        return os.path.splitext(self.filename())[0]

    def read_espresso_in(self):
        """Reads espresso input files."""

        with open(self.filepath, "rt") as f:
            self.lines = f.readlines()

        try:
            self.data, extralines = self.read_fortran_namelist()
        except:
            print("Current file:", self.filepath)
            raise

        self.positions = self.get_atomic_positions(
            extralines,
            n_atoms=self.data['system']['nat'])

        cell = self.get_cell_parameters(extralines, self.data)

        self.atoms = self.build_atoms(self.positions, cell)

    def read_fortran_namelist(self):
        """Takes a fortran-namelist formatted file and returns appropriate
        dictionaries, followed by lines of text that do not fit this pattern.
        """
        data = {}
        extralines = []
        indict = False

        for line in self.lines:
            if indict and line.strip().startswith('/'):
                indict = False

            elif line.strip().startswith('&'):
                indict = True
                dictname = line.strip()[1:].lower()
                data[dictname] = {}

            elif (not indict) and (len(line.strip()) > 0):
                extralines.append(line)

            elif indict:
                try:
                    key, value = line.strip().split('=')
                except:
                    print("Line:", line)
                    raise

                if value.endswith(','):
                    value = value[:-1]
                value = value.strip()
                try:
                    value = eval(value)
                except SyntaxError:
                    value = {'.true.': True, '.false.': False}.get(
                        value, value)

                data[dictname][key.strip()] = value

        return data, extralines

    def get_cell_parameters(self, lines, data):
        """Returns the cell parameters as a matrix."""

        if data["system"]["ibrav"] in {1, 2}:
            self.cell_method = "alat"
            return np.eye(3)

        elif data["system"]["ibrav"] != 0:
            raise NotImplementedError("ibrav={} not implemented!".format(
                data["system"]["ibrav"]))

        cell_parameters = np.zeros((3, 3))
        line = [n for (n, l) in enumerate(lines) if 'CELL_PARAMETERS' in l]

        if len(line) == 0:
            return None

        if len(line) > 1:
            raise RuntimeError('More than one CELL_PARAMETERS section?')

        line_no = line[0]
        self.cell_text = lines[line_no]

        method = re.search(
            r"CELL_PARAMETERS +(\{|\(|)(?P<name>[a-zA-Z]+)(\}|\)|)",
            self.cell_text)

        if method is None:
            self.cell_method = "alat"
        else:
            self.cell_method = method.group("name")

        for vector, line in enumerate(lines[line_no + 1:line_no + 4]):
            self.cell_text += line
            x, y, z = line.split()
            cell_parameters[vector] = (f2f(x), f2f(y), f2f(z))

        return cell_parameters

    def build_atoms(self, positions, cell):
        """Creates the atoms for a quantum espresso in file."""

        if self.cell_method == "alat":
            cell = cell * self.data["system"]["celldm(1)"] * units.Bohr
        elif self.cell_method == "angstrom":
            cell = cell
        elif self.cell_method == "bohr":
            cell = cell * units.Bohr
        else:
            raise NotImplementedError(
                "CELL_PARAMETERS {} method not implemented".format(
                    self.cell_method))

        if self.atoms_method == "angstrom":
            xmul = 1/cell[0][0]
            ymul = 1/cell[1][1]
            zmul = 1/cell[2][2]
        elif self.atoms_method in {'crystal', "alat"}:
            xmul = ymul = zmul = 1
        else:
            raise NotImplementedError(
                'Only supported for crystal method of '
                'ATOMIC_POSITIONS, not %s.' % self.atoms_method)

        symbols = []
        scaled_positions = []
        for el, (x, y, z) in positions:
            symbols.append(el)
            scaled_positions.append([x*xmul, y*ymul, z*zmul])

        atoms = Atoms(
            symbols=symbols,
            scaled_positions=scaled_positions,
            cell=cell)

        return atoms

    def get_atomic_positions(self, lines, n_atoms):
        """Returns the atomic positions of the atoms as an (ordered)
        list from the lines of text of the espresso input file."""
        atomic_positions = []

        line = [n for (n, l) in enumerate(lines) if 'ATOMIC_POSITIONS' in l]

        if len(line) == 0:
            return None

        if len(line) > 1:
            raise RuntimeError('More than one ATOMIC_POSITIONS section?')

        line_no = line[0]
        self.atoms_text = lines[line_no]

        for line in lines[line_no + 1:line_no + n_atoms + 1]:
            self.atoms_text += line
            el, x, y, z = line.split()
            atomic_positions.append([el, (f2f(x), f2f(y), f2f(z))])

        line = lines[line_no]

        method = re.search(
            r"ATOMIC_POSITIONS +(\{|\(|)(?P<name>[a-zA-Z]+)(\}|\)|)", line)

        if method is None:
            raise RuntimeError(
                "None ATOMIC_POSITIONS method found:\m{}".format(line))

        self.atoms_method = method.group("name")

        return atomic_positions

    def create_atoms_text(self):
        text = "ATOMIC_POSITIONS crystal\n"
        for name, pos in zip(self.atoms.get_chemical_symbols(),
                             self.atoms.get_scaled_positions()):
            text += "     {}".format(name)
            for i in pos:
                text += "    {:.9f}".format(i)
            text += "\n"
        return text

    def create_cell_text(self, alat=True):
        text = "CELL_PARAMETERS bohr\n"
        rows = self.atoms.cell/units.Bohr

        for row in rows:
            text += ""
            for i in row:
                text += "    {:.9f}".format(i)
            text += "\n"

        return text

    def create_input(self, atoms_text=None, cell_text=None,
                     ecutwfc=None, kpoints=None, nat=None, newpsps=None,
                     wfcollect=None,
                     prefix=None,
                     primitive=False):
        """Creates new input file."""

        content = "".join(self.lines)

        # ATOMIC_POSITIONS text
        if atoms_text is None:
            atoms_text = self.create_atoms_text()
        if primitive:
            atoms_text = self.atoms_text

        content = content.replace(self.atoms_text, atoms_text)

        # CELL text
        if not primitive:
            if cell_text is None:
                cell_text = self.create_cell_text()
            if self.cell_text is None:
                content = content.replace(
                        "ATOMIC_SPECIES", cell_text + "ATOMIC_SPECIES")
            else:
                content = content.replace(self.cell_text, cell_text)

            content = self.change_variable(content, "ibrav", 0)
            content = re.sub(r"\n +celldm\(1\) *=.*,+ *\n", r"\n", content)

        content = self.change_variable(content, "ecutwfc", ecutwfc)

        if wfcollect is not None:
            content = self.change_variable(content, "wf_collect", ".true.")

        content = self.change_variable(content, "nat", len(self.atoms))
        content = self.change_variable(content, "nat", nat)
        content = self.change_variable(content, "pseudo_dir", newpsps)
        content = self.change_variable(content, "prefix", prefix)

        if kpoints is not None:
            content = re.sub(
                r"(K_POINTS +automatic *\n)(.*)\n",
                r"\1  {k} {k} {k}   0 0 0\n".format(k=kpoints),
                content
            )

        return content

    def change_variable(self, content, var, val):
        if val is None:
            return content

        content = re.sub(
            r"(" + str(var) + " *= *)(.*?)([ ,\n])",
            r"\1" + "<<???>>" + r"\3",
            content)

        replacement = str(val)

        if var in ["pseudo_dir", "prefix"]:
            replacement = "'{}'".format(replacement)

        content = content.replace("<<???>>", replacement)

        return content


class EspressoOutput():

    def __init__(self, filepath):
        self.filepath = filepath
        self.read_espresso_out()

    @property
    def content(self):
        with open(self.filepath, "rt") as f:
            content = f.read()
        return content

    def read_espresso_out(self, index=-1):
        """Reads quantum espresso output text files."""

        lines = self.content.split("\n")

        ################################
        #  Check for supported formats
        ################################

        bl_line = [line for line in lines if 'bravais-lattice index' in line]

        if len(bl_line) != 1:
            raise NotImplementedError('Unsupported: unit cell changing.')

        bl_line = bl_line[0].strip()
        brav_latt_index = bl_line.split('=')[1].strip()

        if brav_latt_index not in {'0', '1'}:
            raise NotImplementedError('Supported only for Bravais-lattice '
                                      'index of 0 (free) and 1 (cubic).')

        lp_line = [line for line in lines if 'lattice parameter (alat)' in
                   line]

        if len(lp_line) != 1:
            raise NotImplementedError('Unsupported: unit cell changing.')

        lp_line = lp_line[0].strip().split('=')[1].strip().split()[0]
        lattice_parameter = float(lp_line) * units.Bohr
        ca_line_no = [number for (number, line) in enumerate(lines) if
                      'crystal axes: (cart. coord. in units of alat)' in line]

        if len(ca_line_no) != 1:
            raise NotImplementedError('Unsupported: unit cell changing.')

        ca_line_no = int(ca_line_no[0])
        self.cell = np.zeros((3, 3))

        for number, line in enumerate(lines[ca_line_no + 1: ca_line_no + 4]):
            line = line.split('=')[1].strip()[1:-1]
            values = [eval(value) for value in line.split()]
            self.cell[number, 0] = values[0]
            self.cell[number, 1] = values[1]
            self.cell[number, 2] = values[2]

        self.cell *= lattice_parameter
        self.create_atoms(self.content, self.cell)

        ##################
        #    Energies
        ##################

        self.energies = []
        for i in re.finditer("! *total energy *= *(.*?) Ry", self.content):
            self.energies.append(float(i.group(1)) * Units.ry_to_ev)

    def create_atoms(self, content, cell):
        """Scan through lines to get the atomic positions."""
        final_coord = re.search(
            r"Begin final coordinates\n *\n(.*)\nEnd final coordinates",
            content, flags=re.DOTALL)

        if final_coord is None:
            raise Exception("Final coordinates not found")

        final_coord = final_coord.group(1)

        header, *lines = final_coord.split("\n")
        symbols = []
        scaled_positions = []

        if 'ATOMIC_POSITIONS (crystal)' in header:
            for line in lines:
                sym, *pos = line.split()
                symbols.append(sym)
                scaled_positions.append([float(i) for i in pos])
        else:
            raise NotImplementedError(
                "Positions format '{}' is not implemented".format(header))

        self.atoms = Atoms(symbols=symbols, scaled_positions=scaled_positions,
                           cell=cell)

    def get_forces(self):
        for st in re.finditer(r" *Forces acting on atoms \(Ry/au\): *\n *\n",
                              self.content):
            pass

        for en in re.finditer(r" *The non-local contrib.  to forces *\n",
                              self.content):
            pass

        force_set = []
        for line in self.content[st.end(): en.start()].split("\n"):
            if "force =" not in line:
                continue

            fc = re.search("force = (.*)", line)
            force_set.append([float(i)*Units.ry_to_ev/(Units.bohr_to_a**2)
                              for i in fc.group(1).split()])

        return np.array(force_set)

    def relax_info(self):
        e = self.energies
        return e[0], e[-1], e[0] - e[-1]

    def force_max(self):
        return np.apply_along_axis(np.linalg.norm, 0, self.get_forces()).max()

if __name__ == "__main__":
    out = SimplifiedOutputParser(
        "/home/lukas/calculations/diamond/pwscf/nv2x2x2/nv.relax.out")
    print(out.get_energy())
    out.get_forces()
