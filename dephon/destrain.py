
"""Module for strain input file creation"""
import os
import numpy as np
from vasp import Poscar, VaspXML
from espresso import EspressoInput
from deutils import ensure_dir
import shutil
import re
import pprint
from scipy.optimize import curve_fit

pp = pprint.PrettyPrinter()


# ORBITALS = {
#     "NV-1": {
#         "3x3x3": {
#             "spin 2": [("a1-1", 429), ("e1", 430), ("e2", 431)],
#             "spin 1": [("a1-2", 429)],
#         },
#         "4x4x4": {
#             "spin 1": [("a1-1", 1021), ("e1", 1022), ("e2", 1023)],
#             "spin 2": [("a1-2", 1021)]
#         }
#     }
# }


ORBITALS = {
    "NV-1": {
        "3x3x3": {
            "spin 1": [("a1", 429), ("e1", 430), ("e2", 431)],
            "spin 2": [("a1", 429), ("e1", 430), ("e2", 431)],
        },
    }
}


##############################
#   Creation of input files
##############################


def create_normal_strain(atoms, strain):
    new_atoms = atoms.copy()
    cell = new_atoms.get_cell()
    for i in range(3):
        cell[i][0] += cell[i][0]*strain

    new_atoms.set_cell(cell)
    return new_atoms


def create_shear_strain(atoms, strain):
    new_atoms = atoms.copy()
    cell = new_atoms.get_cell()
    for i in range(3):
        cell[i] = cell[i] + np.array([0, cell[i][0]*strain, 0])

    new_atoms.set_cell(cell)
    return new_atoms


def create_hydrostatic_strain(atoms, strain):
    new_atoms = atoms.copy()
    cell = new_atoms.get_cell()
    cell += np.array(cell)*strain
    new_atoms.set_cell(cell)
    return new_atoms


def create_strain_inputs(basedir, strains, output_dir, strain="normal",
                         calc="vasp", filename="POSCAR",
                         copy_files=None):
    """Generates input files with induced strain."""

    if copy_files is None:
        copy_files = [["INCAR"]*2, ["KPOINTS"]*2, ["POTCAR"]*2]

    ensure_dir(output_dir)
    base_filepath = os.path.join(basedir, filename)

    if calc == "vasp":
        poscar = Poscar(base_filepath)
        atoms = poscar.create_atoms()
    elif calc == "pwscf":
        qeinput = EspressoInput(base_filepath)
        atoms = qeinput.atoms

    # Iterating through strains
    dirs = []
    for st in strains:
        if strain == "normal":
            new_atoms = create_normal_strain(atoms, st)
        elif strain == "shear":
            new_atoms = create_shear_strain(atoms, st)
        elif strain == "hydrostatic":
            new_atoms = create_hydrostatic_strain(atoms, st)

        name = "{}_strain_{:.3f}".format(strain, st)
        dirs.append(name)
        newdir = os.path.join(output_dir, name)
        ensure_dir(newdir)

        if calc == "vasp":
            poscar.atoms = new_atoms
            new_POSCAR = os.path.join(newdir, "POSCAR")
            with open(new_POSCAR, "wt") as f:
                f.write(poscar.create_input())

            with open(os.path.join(newdir, "geometry.xyz"), "wt") as f:
                f.write(new_atoms.create_xyz_content())

            for f, t in copy_files:
                shutil.copy(os.path.join(basedir, f), os.path.join(newdir, t))

        elif calc == "pwscf":
            qeinput.atoms = atoms
            new_filepath = os.path.join(newdir, filename)
            with open(new_filepath, "wt") as f:
                f.write(qeinput.create_input(prefix="{}strain{:.4}".format(
                    strain[0], st)))

    return dirs


##############################
#      Gathering data
##############################

def gather_strain_data(directory, calc="vasp", defect="NV-1",
                       cell="3x3x3"):
    print()
    print("="*70)
    print("Gathering strain data from dir: {} ({})".format(directory, calc))
    print("="*70)
    print()

    eigenvals = {"shear": {}, "normal": {}}

    for nm in os.listdir(directory):
        _dir = os.path.join(directory, nm)

        if not os.path.isdir(_dir):
            continue

        if nm.startswith("shear_strain"):
            energy = eigenvals["shear"]
        elif nm.startswith("normal_strain"):
            energy = eigenvals["normal"]
        else:
            continue

        if calc == "vasp":
            try:
                xml = VaspXML(os.path.join(_dir, "vasprun.xml"))
                eig = xml.read_eigenvalues()
                occ = xml.occupations
                strain = float(re.search("[0-9\.]+", nm).group(0))

                for chanel in ["spin 1", "spin2"]:
                    for (name, nr) in ORBITALS[defect][cell][chanel]:
                        vals = energy.get(
                            name, {"strain": [], "e": [], "occ": []})
                        vals["strain"].append(strain)
                        vals["occ"].append(occ[chanel][nr])
                        print(directory,
                              chanel, "occ:",
                              occ[chanel][nr],
                              nr, name)
                        vals["e"].append(eig[chanel][nr])
                        energy[name] = vals
            except:
                print(nm, "failed!!!")
                raise
                continue
        else:
            raise NotImplementedError("pwscf is not implemented")

    return eigenvals


def fit_data(strains, delta_data, name, cell, strain_type):
    def delta(x, k, dkp2):
        return 4*k*x - 4*dkp2*np.power(x, 2)

    popt, pcov = curve_fit(
        delta, strains, delta_data)

    text = ("$\\delta_{{xx}} = 4({:.2f}\\pm{:.2f})"
            "\\varepsilon_{{xx}} - "
            "4({:.2f}\\pm {:.2f} )\\varepsilon^2_{{xx}}$")

    text = text.format(
        popt[0], np.sqrt(np.diag(pcov))[0],
        popt[1], np.sqrt(np.diag(pcov))[1])

    if strain_type.startswith("shear"):
        text = text.replace("xx", "xy")

    return text, lambda x: delta(x, popt[0], popt[1])


def _plot_result(directory, axes, name=None):
    plot_map = {
        "e": {
            "normal": (0, 0),
            "shear": (0, 1),
        },
        "a": {
            "normal": (1, 0),
            "shear": (1, 1)
        },
        "delta": {
            "normal": (2, 0),
            "shear": (2, 1)
        }
    }

    name_map = {
        "nv3x3x3pbe": "nv3x3x3 pbe",
        "nv3x3x3relpbe": "nv3x3x3 pbe relaxed",
        "nv3x3x3hse": "nv3x3x3 hse relaxed",
    }

    plot_map["delta"]["normal"] = (1, 0)
    plot_map["delta"]["shear"] = (1, 1)

    alpha = 1 if "rel" in directory else 0.3
    if "hse" in directory:
        alpha = 1

    if name is None:
        name = os.path.split(directory)[-1]

    if name in name_map:
        name = name_map[name]

    colors = {
        "e": None,
        "a": None
    }

    labels = {
        "shear": {
            "a": False,
            "e": False
        },
        "normal": {
            "a": False,
            "e": False
        }
    }

    evals = gather_strain_data(directory)
    for strain_type in evals:
        dat = []
        for orbital in evals[strain_type]:
            if orbital[0] == "a":
                continue
            ax = axes[plot_map[orbital[0]][strain_type]]
            data = evals[strain_type][orbital]
            s = np.array(data["strain"])
            e = np.array(data["e"])
            indx = np.argsort(s)
            s = s[indx]
            e = e[indx]
            if orbital[0] == "e":
                e = e - e[0]

            label = None
            if not labels[strain_type][orbital[0]]:
                label = "{} {} {}".format(orbital[0], name, strain_type)
                labels[strain_type][orbital[0]] = True

            if colors[orbital[0]] is None:
                p = ax.plot(s, e, alpha=alpha, label=label)
                colors[orbital[0]] = p[0].get_color()
            else:
                p = ax.plot(s, e, color=colors[orbital[0]], alpha=alpha,
                            label=label)

            ax.scatter(
                s, e, color=colors[orbital[0]],
                alpha=alpha)

            if orbital.startswith("e"):
                dat.append(e)

        text, foo = fit_data(s, dat[0] - dat[1], name, "3x3x3", strain_type)
        ax = axes[plot_map["delta"][strain_type]]
        ax.scatter(s, dat[0] - dat[1],
                   label=r"$\delta_{{{}}}$ {}".format(
                       "xx" if strain_type == "normal" else "xy", name),
                   color=colors[orbital[0]], alpha=alpha)

        x = np.linspace(s.min(), s.max(), 1000)
        y = foo(x)
        ax.plot(x, y, color=colors[orbital[0]], label=text)

        print(text)


def plot_results(dirs):
    f, axes = plt.subplots(2, 2, figsize=(20, 20))
    for _dir in dirs:
        _plot_result(_dir, axes)

    for i in [axes[0][0], axes[1][0]]:
        i.set_xlabel(r"normal strain $\epsilon_{xx}$")
        i.set_ylabel("eV")

    for i in [axes[0][1], axes[1][1]]:
        i.set_xlabel(r"shear strain $\epsilon_{xy}$")
        i.set_ylabel("eV")

    for ax in axes.flatten():
        ax.legend()
        ax.grid()

    plt.tight_layout()
    plt.savefig("strains2virtual.pdf")
    plt.clf()

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    dirs = ["../diamond/vasp/strains/nv3x3x3pbe",
            "../diamond/vasp/strains/nv3x3x3relpbe",
            "../diamond/vasp/strains/nv3x3x3hse"]

    plot_results(dirs)
    # eigenvals = gather_strain_data(outdir)

    # f, axes = plt.subplots(2, 1)

    # for nr, strain_type in enumerate(eigenvals):
    #     for orbital in eigenvals[strain_type]:
    #         data = eigenvals[strain_type][orbital]
    #         axes[nr].scatter(data["strain"], data["e"], label=orbital)
    #         axes[nr].set_title(strain_type)

    # plt.tight_layout()
    # plt.savefig("test.pdf")
    # plt.clf()


if __name__ == "__main__" and False:
    from declusters import Cluster
    strains = [0, 0.001, 0.002, 0.003, 0.005, 0.007, 0.01, 0.011, 0.012, 0.015]
    # strains = [0.002, 0.005, 0.01, 0.015]
    indir = "../diamond/vasp/nv3x3x3/relax"
    outdir = "../diamond/vasp/strains/nv3x3x3relpbe"
    # indir = "../diamond/vasp_hybrid/nv3x3x3/relax/"
    # outdir = "../diamond/vasp/strains/nv3x3x3hse"
    for strain_type in ["normal", "shear"]:
        dirs = create_strain_inputs(indir, strains,
                                    outdir,
                                    strain=strain_type)

        jobparams = {"cpus": 32, "name": strain_type + "strain216rpbe"}
        jobs = [
            {
                "cwd": i,
                "exec": "vasp_gam"
            } for i in dirs
        ]

        clust = Cluster("raijin", "vasp")
        content = clust.generate_job_file(jobparams, jobs)
        with open(os.path.join(outdir, strain_type + ".job"), "wt") as f:
            f.write(content)
