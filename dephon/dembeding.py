"""Module for defect embeding procedure.
"""

import os
from demegabulk import (calculate_modes_and_write_results)
from defc import ForceConstants
import numpy as np
import pprint
import time
import logging
import copy
from deutils import ensure_dir

logging.basicConfig(format='%(message)s')
log = logging.getLogger(__name__)

pp = pprint.PrettyPrinter()


def debug_pair(fc, i, j):
    """Just for debuging purpose."""
    print("="*70)
    print("Atoms: {}, Cell: {}".format(
        len(fc.atoms), fc.atoms.get_cell().max()))
    spos = fc.atoms.get_scaled_positions()
    pos = fc.atoms.get_positions()
    print("pos1:{}\t{}".format(spos[i], pos[i]))
    print("pos2:{}\t{}".format(spos[j], pos[j]))
    print("diff:{}\t{}".format(spos[j] - spos[i], pos[j] - pos[i]))
    fc.atoms.get_separation_vector(i, j, debug=True)
    print("-"*70)
    print(fc.get_atom_pair_part(i, j))
    print("="*70)


def create_defect_inside_bulk(bulk_fc, defect_fc):
    """Given bulk and defect cells identifies and creates substitutional
    defects/vacancies inside mega bulk (modifies bulk object).

    Args:
        bulk (Atoms): supercell which will be embeded with defect
            supercell
        defect (Atoms): defect supercell which will be emebeded in the
            corner of bulk cell

    Returns:
        pos_of_defects (list): list of positions of defects inside mega bulk.
        bulk_to_defect_cell_maps (dict): dictionary containing index mappings
            from mega bulk cell to defect cell
    """

    log.info("Creating defect inside bulk supercell")

    vacancy_indexes = []
    defect_indexes = []
    defect_pos = []
    bulk_to_defect_cell_maps = {}
    bulk_positions = bulk_fc.atoms.get_positions()
    scaled_pos = bulk_fc.atoms.get_scaled_positions()

    for indx, pos in enumerate(bulk_positions):

        # skip if outside defect cell (cell wil be embeded in x=y=z=0 corner)
        if (pos >= np.array(
                [defect_fc.atoms.cell[i][i]*0.999 for i in range(3)])).any():
            continue

        found = None
        for indx2, dpos in enumerate(defect_fc.atoms.get_positions()):
            if np.allclose(pos, dpos, rtol=0, atol=1e-3):
                found = indx2

        if found is None:
            log.info("- found vacancy at {} A".format(pos))
            log.info("  + in cell dimensions: {}".format(scaled_pos[indx]))
            vacancy_indexes.append(indx)
            defect_pos.append(pos)
            continue

        if bulk_fc.atoms.get_symbol(indx) != defect_fc.atoms.get_symbol(found):
            log.info("- found substitutional defect {} at {} A".format(
                bulk_fc.atoms.get_chemical_symbols()[indx], pos))
            log.info("  + in cell dimensions: {}".format(scaled_pos[indx]))
            defect_pos.append(pos)
            defect_indexes.append(indx)
            bulk_fc.atoms.symbols[indx] = defect_fc.atoms.symbols[found]

        new_indx = indx - len(vacancy_indexes)
        bulk_to_defect_cell_maps[new_indx] = found

    # Removing vacancies
    log.info("- total point defects found: {}".format(len(defect_pos)))
    log.info("- vacancies removed from bulk at positions:")
    for i in vacancy_indexes:
        log.info("  + {} A".format(bulk_fc.atoms.get_positions()[i]))
        log.info("    {} Cryst Coord.".format(
            bulk_fc.atoms.get_scaled_positions()[i]))

    bulk_fc.remove_atoms(vacancy_indexes)
    bulk_fc.atoms._symbols_to_masses()
    log.info("- atoms changed in bulk at positions:")
    for i in defect_indexes:
        log.info("  + {} A".format(bulk_fc.atoms.get_positions()[i]))
        log.info("    {} Cryst Coord.".format(
            bulk_fc.atoms.get_scaled_positions()[i]))

    bulk_fc.embeding_info["defect"]["defect_pos"] = defect_pos

    return defect_pos, bulk_to_defect_cell_maps


def _add_to_dict_list(d, key, val):
    lst = d.get(key, [])
    lst.append(val)
    d[key] = lst


def iter_defect_pairs(defect, defect_positions, dcutoff,
                      bulk=None,
                      temp_log_file=None):
    """Iterates defect pairs which resides in dcutoff region
    around defect atoms.
    """

    def _log_info(message):
        print(message, end="\r")
        if temp_log_file is not None:
            with open(temp_log_file, "wt") as f:
                f.write(message)

    info = {
        "total": 0,
        "embeded": 0,
        "to_far_from_defect": 0,
        "to_far_from_each_other": 0,
        "in_two_different_images": 0,
        "max_distance_from_defect": 0,
    }

    positions = defect.atoms.get_positions()

    def memodict(f):
        """ Memoization decorator for a function taking a single argument """
        class memodict(dict):
            def __missing__(self, key):
                ret = self[key] = f(key)
                return ret
        return memodict().__getitem__

    @memodict
    def distance_to_defect(pos):
        """Finds shortest distance to defect."""

        distances = []
        for def_pos in defect_positions:
            vec, dist = defect.atoms.get_separation_vector(pos, def_pos)
            vecd, distd = defect.atoms.get_separation_vector(
                pos, def_pos, direct=True)
            distances.append([dist, vec, def_pos, distd, vecd])

        return min(distances, key=lambda x: x[0])

    _i_def = 0
    for i_def, j_def in defect.atoms.iter_pairs():
        info["total"] += 1

        if i_def != _i_def:
            _i_def = i_def
            _log_info(
                "Iterrating though defect pairs: {}/{} ({:.3f}\%)".format(
                    i_def, len(defect.atoms), 100/len(defect.atoms)*i_def))

        pos = [positions[i_def], positions[j_def]]

        #####################################################
        #  Embed fc for pairs inside spheres arround defet
        #####################################################

        # Find shortest distance to point defects
        #   ([distance, displacement_vec, defect_position], ...)
        sep_from_def = [
            distance_to_defect(tuple(i)) for i in pos]
        shortest_distances_to_defect = np.array([i[0] for i in sep_from_def])

        for indx, d in zip([i_def, j_def], shortest_distances_to_defect):
            if d > info["max_distance_from_defect"]:
                info["max_distance_from_defect"] = d

        # Filter pairs only in cutoff radius
        if not (shortest_distances_to_defect < dcutoff).all():
            info["to_far_from_defect"] += 1
            continue

        # Get shortest distance and displacement in periodic supercell
        displ_vec, dist = defect.atoms.get_separation_vector(i_def, j_def)

        if dist > dcutoff:
            info["to_far_from_each_other"] += 1
            continue

        # Displacement vector should not cross cutoff boundaries
        # Rule for this:
        #   Vec(at1, def1) + Vec(def1, def2) + Vec(def2, at2) = Vec(at1, at2)
        atdef11 = sep_from_def[0][1]
        def12, _ = defect.atoms.get_separation_vector(
            sep_from_def[0][2], sep_from_def[1][2])
        defat22 = -sep_from_def[1][1]
        displ2 = atdef11 + def12 + defat22

        if not np.allclose(displ2, displ_vec, 0.01):
            # print("="*70)
            # print("Ignoring:\n -{}\n -{}".format(pos[0], pos[1]))
            # print("-"*70)
            # print(" -{}".format(displ_vec))
            # print("-"*70)
            # print("Defects:\n -{}\n -{}".format(sep_from_def[0][2],
            #                                     sep_from_def[1][2]))
            # print("atom1 -> defect1:", atdef11)
            # print("defect1 -> defect2:", def12)
            # print("defect2 -> atom2:", defat22)
            # print("sum:", displ2)
            # input()
            info["in_two_different_images"] += 1
            continue

        ###########################################################
        # Change possition assuming periodic boundary conditions
        ###########################################################

        pos_for_bulk = copy.deepcopy(pos)

        for indx, (_, v, _, _, dir_v) in enumerate(sep_from_def):
            for i in range(3):
                cell_lenght = defect.atoms.get_cell()[i][i]
                if abs(dir_v[i]) - 0.00001 > abs(v[i]):
                    pos_for_bulk[indx][i] = pos_for_bulk[indx][i] - cell_lenght

        # if np.allclose(pos_for_bulk, pos):
        #     continue

        info["embeded"] += 1

        yield i_def, j_def, pos_for_bulk, displ_vec, dist, info


def embed_force_constants(defect, bulk, defect_positions, dcutoff=None,
                          debug=True):
    """Embeds defect interaction force constants into the bulk supercell.
    Interactions are replaced for atom pairs which are separated by
    distance less than dcutoff and which are in the vicinity of any
    point defect by same distance dcutoff.

    PLACES DEFECT CELL IN THE CORNER x=y=z=0
    """
    whole_time_beg = time.time()

    bulk.interaction_vectors = {}
    bulk.interaction_maps = {}
    bulk.distance_to_defect = {}

    log.info("\n" + "-"*70 +
             "\nEmbeding force constants from defect supercell\n" +
             "-"*70)

    # Assign 0.9 of max cutoff if it is not given
    if dcutoff is None:
        dcutoff = defect.max_cutoff()*0.9
        log.info("* Cutoff for defect embeding is not given")

    log.info("- Max cutoff possible: {}\n".format(defect.max_cutoff()) +
             "- Using cutoff: {}".format(dcutoff))

    atom_maps = {}

    log.info("* Sorting positions...")
    sortbeg = time.time()
    bulk.atoms.sort_positions()
    log.info(" - sorting finished in {:.3f} s".format(time.time() - sortbeg))

    ############################################
    #  Iterating through all defect cell pairs
    ############################################
    for i_def, j_def, pos_for_bulk, displ_vec, dist, info in iter_defect_pairs(
            defect, defect_positions, dcutoff, temp_log_file=None):

        ##############################################
        # Find analogous atom pair in bulk supercell
        ##############################################

        if i_def not in atom_maps:
            i_bulk = bulk.atoms.find_by_position(pos_for_bulk[0], method=3)
            atom_maps[i_def] = i_bulk
        else:
            i_bulk = atom_maps[i_def]

        if j_def not in atom_maps:
            j_bulk = bulk.atoms.find_by_position(pos_for_bulk[1], method=3)
            atom_maps[j_def] = j_bulk
        else:
            j_bulk = atom_maps[j_def]

        # i_bulk_pos = bulk.atoms.get_positions()[i_bulk]
        # j_bulk_pos = bulk.atoms.get_positions()[j_bulk]
        # j_bulk_pos = np.around(i_bulk_pos + displ_vec, 4)

        # err_message = (
        #     "While iterating through defect atoms and analysing " +
        #     "atom pair:\n  {} A\n  {} A\n".format(pos[0], pos[1]) +
        #     "I found that displacement vector is:" +
        #     "\n  {} (dist: {})\n".format(displ_vec, dist) +
        #     "In bulk cell i found first one:\n  {}\n".format(
        #         i_bulk_pos) +
        #     "but could not find " +
        #     "the second one at pos=pos1+displ\n  {}\n".format(
        #         j_bulk_pos) +
        #     "My assumed coordinates in defect cell are:\n  ")

        # j_bulk = bulk.atoms.find_by_position(
        #     j_bulk_pos, err_message=err_message)

        # j_bulk_pos = bulk.atoms.get_positions()[j_bulk]

        if debug:
            defect.embeded.extend([i_def, j_def])
            _add_to_dict_list(defect.interaction_maps, i_def, j_def)
            _add_to_dict_list(defect.interaction_vectors, i_def, displ_vec)
            _add_to_dict_list(defect.interaction_vectors, j_def, -displ_vec)
            _add_to_dict_list(defect.interaction_maps, j_def, i_def)

            bulk.embeded.extend([i_bulk, j_bulk])
            v, _ = bulk.atoms.get_separation_vector(i_bulk, j_bulk)
            _add_to_dict_list(bulk.interaction_maps, i_bulk, j_bulk)
            _add_to_dict_list(bulk.interaction_vectors, i_bulk, v)
            _add_to_dict_list(bulk.interaction_maps, j_bulk, i_bulk)
            _add_to_dict_list(bulk.interaction_vectors, j_bulk, -v)

        m2 = defect.get_atom_pair_part(i_def, j_def)
        bulk.set_atom_pair_part(i_bulk, j_bulk, m2)

    tot_time = time.time() - whole_time_beg
    log.info("* Total pairs iterated {}\n".format(info["total"]) +
             " - too far from defect {}\n".format(info["to_far_from_defect"]) +
             " - crossing cutoff borders {}\n".format(
                 info["in_two_different_images"]) +
             " - embeded {} atom-atom interactions in {:.3f}s".format(
                 info["embeded"], tot_time))

    bulk.defect_positions = defect_positions
    if len(defect_positions) == 2:
        log.info("- distance between unrelaxed defects: {:.6f} A".format(
            np.linalg.norm(
                defect_positions[0] - defect_positions[1])))

    return bulk


def embed_defect_inside_bulk(defect_fc,
                             bulk_dir_or_fc,
                             output_dir=None,
                             dcutoff=None,
                             symmetrize_fc=True,
                             calculate_modes=False,
                             read_if_found=False,
                             postprocess=None):

    if postprocess is None:
        postprocess = lambda x: x

    hdlr = None
    if output_dir is not None:
        ensure_dir(output_dir)
        log_file_path = os.path.join(output_dir, "embeding.log")
        hdlr = logging.FileHandler(log_file_path)
        log.addHandler(hdlr)
        logging.getLogger("defc").addHandler(hdlr)

    log.info("* Starting embeding procedure")

    if isinstance(bulk_dir_or_fc, str):
        bulk_fc = ForceConstants('vasp', hdf5dir=bulk_dir_or_fc)
        log.info('- bulk force constants read from dir: {}'.format(
            bulk_dir_or_fc))
    else:
        bulk_fc = bulk_dir_or_fc

    log.info(
        "- number of atoms in defect supercell: {}\n".format(
            len(defect_fc.atoms)) +
        "- defect cell vectors in Angstrom:\n{}\n".format(
            "  " + str(defect_fc.atoms.get_cell()).replace("\n", "\n  ")) +
        "- number of atoms in bulk supercell: {}\n".format(
            len(bulk_fc.atoms)) +
        "- bulk cell vectors in Angstrom:\n{}".format(
            "  " + str(bulk_fc.atoms.get_cell()).replace("\n", "\n  ")))

    defect_pos, bulk_to_defect_cell_maps = create_defect_inside_bulk(
        bulk_fc, defect_fc)

    bulk_fc = embed_force_constants(
        defect_fc, bulk_fc, defect_pos, dcutoff=dcutoff)

    sbeg = time.time()
    report = bulk_fc.atoms.calculate_symmetry()
    log.info(report)
    log.info("- symmetry calculations finished in: {:.3f}s".format(
        time.time() - sbeg))

    if output_dir is not None:
        xyz_filepath = os.path.join(output_dir, "coordinates.xyz")
        xsf_filepath = os.path.join(output_dir, "coordinates.xsf")

        with open(xyz_filepath, "wt") as f:
            log.info("* Writing atom coordinates in: {}".format(xyz_filepath))
            f.write(bulk_fc.create_xyz_content())

        with open(xsf_filepath, "wt") as f:
            log.info("* Writing atom coordinates in: {}".format(xsf_filepath))
            f.write(bulk_fc.create_xsf_content())

        xyz_defect_embeding_filepath = os.path.join(
            output_dir, "defect_embedin_pattern.xyz")
        xyz_bulk_embeding_filepath = os.path.join(
            output_dir, "bulk_embedin_pattern.xyz")

        with open(xyz_defect_embeding_filepath, "wt") as f:
            log.info("* Writing defect embeding pattern in: {}".format(
                xyz_defect_embeding_filepath))
            f.write(defect_fc.create_xyz_content(
                embeding_pattern=True, interaction_pattern=True))

        with open(xyz_bulk_embeding_filepath, "wt") as f:
            log.info("* Writing bulk embeding pattern in: {}".format(
                xyz_bulk_embeding_filepath))
            f.write(bulk_fc.create_xyz_content(
                embeding_pattern=True, interaction_pattern=True))

        if symmetrize_fc:
            bulk_fc.symmetrize_fc(2)

        log.info("\n* Writting embeded data to: {}".format(output_dir))
        bulk_fc.save_to_hdf5(output_dir)

        if calculate_modes:
            calculate_modes_and_write_results(
                bulk_fc, output_dir, log_fh=hdlr,
                logtillipr=0.15*len(bulk_fc.atoms))

    if hdlr is not None:
        log.removeHandler(hdlr)
        logging.getLogger("defc").removeHandler(hdlr)

    return defect_fc, bulk_fc

##############################
#     EMBEDING VS DIRECT
##############################

if __name__ == "__main__":
    from vasp import Poscar
    from defc import ForceConstants
    import matplotlib.pyplot as plt

    def iter_params(fr=3, dcutofflist=None, cutlist=None):
        if dcutofflist is None:
            dcutofflist = [0.7, 0.9, 1]
        if cutlist is None:
            cutlist = [False, True]

        for fr in range(fr, 5):
            for cut in cutlist:
                for dcutoff in dcutofflist:
                    yield fr, cut, dcutoff

    to = 4
    for fr, cut, dcut in iter_params(3, [0.7, 0.8, 0.9, 1], [True, False]):
        out_dir = "test_data/diamond/embeded/nv{0}x{0}x{0}cut{1}/".format(
            to, dcut)
        directory = "../diamond/vasp/{1}{0}x{0}x{0}/phonopy_phonons"

        defect_fc = ForceConstants(
            'vasp', outdir=directory.format(fr, "nv"))
        bulk_fc = ForceConstants(
            'vasp', outdir=directory.format(to, ""))

        defect_nonrelaxed_atoms = Poscar(
            "../diamond/vasp/nv{0}x{0}x{0}/relax/POSCAR".format(
                fr)).create_atoms()
        defect_fc.atoms = defect_nonrelaxed_atoms

        defect_fc, embeded_fc = embed_defect_inside_bulk(
            defect_fc, bulk_fc, out_dir,
            symmetrize_fc=True,
            dcutoff=defect_fc.max_cutoff()*dcut,
            calculate_modes=True)

        embeded_read_fc = read_force_constants_object_from_dir(
            out_dir, read_modes=True)
        exit(1)

        compare_fc = ForceConstants(
            'vasp', outdir=directory.format(to, "nv"))

        cutoff = defect_fc.max_cutoff()*dcut
        if cut:
            compare_fc.perform_cutoff(cutoff)
            embeded_fc.perform_cutoff(cutoff)

        nonrelaxed_atoms = Poscar(
            "../diamond/vasp/nv{0}x{0}x{0}/relax/POSCAR".format(
                to)).create_atoms()

        compare_fc.atoms = nonrelaxed_atoms
        embeded_fc.atoms = nonrelaxed_atoms

        print("!"*70)
        print("DIRECT")
        print("!"*70)

        compare_fc.symmetrize_fc()
        modes2 = compare_fc.calculate_modes()

        print("!"*70)
        print("EMBEDED")
        print("!"*70)

        embeded_fc.symmetrize_fc()
        modes = embeded_fc.calculate_modes()

        f, axes = plt.subplots(5, 1, figsize=(10, 35))
        modes.compare_dos(axes, modes2, 'embeded', 'direct',
                          plots="freq_displ",
                          integration_points=400)

        name = "nv{0}x{0}x{0}_from_nv{1}x{1}x{1}_dcut{3}{2}".format(
            to, fr, "_cut" if cut else "", dcut)

        plt.suptitle(name)
        plt.tight_layout()
        plt.subplots_adjust(top=0.9)
        plt.savefig(
            "test_data/embeding/{}.pdf".format(name),
            dpi=300)

        plt.clf()

##############################
#     DEFECT VS BULK
##############################

if __name__ == "__main__" and False:
    defect_fc = ForceConstants(
        'vasp', outdir=directory.format(4, "nv"))
    bulk_fc = ForceConstants(
        'vasp', outdir=directory.format(4, ""))

    modes = defect_fc.calculate_modes()
    modes2 = bulk_fc.calculate_modes()

    f, axes = plt.subplots(3, 1, figsize=(10, 15))
    modes.compare_dos(axes, modes2, 'defect', 'bulk',
                      integration_points=400)

    name = "bulkvsnv4x4x4"
    plt.suptitle(name)

    plt.tight_layout()
    plt.subplots_adjust(top=0.95)
    plt.savefig(
        "test_data/embeding/{}.pdf".format(name),
        dpi=300)
    plt.clf()
