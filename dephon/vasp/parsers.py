try:
    from lxml import etree
except:
    import xml.etree.ElementTree as etree
import numpy as np
import os
import re
from scipy.constants import electron_volt, physical_constants, angstrom

Ha_in_ev = physical_constants["Hartree energy in eV"][0]
bohr_in_angstr = physical_constants["Bohr radius"][0]*1e10


def convert_vasp_forces(forces, units="au"):
    if units.lower() in {"n", "newton", "si"}:
        conv_factor = (electron_volt/angstrom)
    elif units.lower() in {"au", "ha/bohr"}:
        conv_factor = (bohr_in_angstr/Ha_in_ev)
    elif units.lower() in {'eV/a', "eV/angst", None, "vasp"}:
        conv_factor = 1
    else:
        raise NotImplementedError("Force units {} unrecognized".format(units))

    return forces*conv_factor


class VaspXML():
    def __init__(self, filepath):
        self.filepath = filepath
        try:
            self.root = etree.parse(self.filepath)
        except:
            raise RuntimeError("Failed to parse {}".format(filepath))

    def get_energy(self):
        e = []
        for calc in self.root.xpath("//calculation"):
            en = calc.xpath("./scstep/energy/i[@name='e_fr_energy']")[-1]
            e.append(float(en.text))
        return e

    def get_forces(self, units="au"):
        forces = self.root.xpath("//varray[@name='forces']")[-1]
        force_set = []
        for i in forces:
            force_set.append([float(j) for j in i.text.split()])

        self.forces = np.array(force_set)
        self.max_force = np.apply_along_axis(
            np.linalg.norm, 0, self.forces).max()

        return convert_vasp_forces(self.forces, units=units)

    def force_max(self):
        return np.apply_along_axis(np.linalg.norm, 0, self.get_forces()).max()

    def relax_info(self):
        e = self.get_energy()
        return e[0], e[-1], e[0] - e[-1]

    def read_eigenvalues(self):
        self.eigenvalues = {"spin 1": None, "spin 2": None}
        self.occupations = self.eigenvalues.copy()

        for spin_ch in self.eigenvalues:
            for calc in self.root.xpath(
                    '//eigenvalues//set[@comment="{}"]'.format(spin_ch)):
                assert len(calc) == 1, "Not implemented for multiple k points"
                vals = []
                occs = []
                for i in calc[0]:
                    val, occ = i.text.split()
                    vals.append(float(val))
                    occs.append(float(occ))

            self.eigenvalues[spin_ch] = np.array(vals)
            self.occupations[spin_ch] = np.array(occs)

        return self.eigenvalues


class Poscar:
    def __init__(self, filepath):
        self.filepath = filepath
        self.workdir = os.path.dirname(filepath)
        self.potcar_path = os.path.join(self.workdir, "POTCAR")

        with open(filepath, 'rt') as f:
            data = [i for i in f.readlines()
                    if not (i.startswith("#") or i.startswith("!"))]

        ########################
        #  Count atomic types
        ########################
        self.atom_types = []

        with open(self.potcar_path, "rt") as f:
            content = f.read()

        for i in re.finditer(r"TITEL.*\n", content):
            self.atom_types.append(i.group().split()[3].split("_")[0])

        self.description = data[0].strip()
        self.ltconst = float(data[1].strip())
        self.ltconst_string = data[1].strip()

        self.cell = []

        self.cell_lines = []
        if self.ltconst > 0.0:
            for i in range(2, 5):
                self.cell_lines.append(data[i])
                self.cell.append([float(k)*self.ltconst
                                  for k in data[i].split()])

            self.cell = np.array(self.cell)
        else:
            raise NotImplementedError("Scaling not implemented")

        ##############################
        #    Read atomic positions
        ##############################

        fr = 5
        # Ignore atoms declaration if it is there
        if ''.join(data[fr].split()).isalpha():
            fr = 6

        self.symbols = []
        self.type_counts = [int(k) for k in data[fr].split()]

        self.nr_of_atoms = 0
        for i, nr in enumerate(self.type_counts):
            self.nr_of_atoms += nr
            for j in range(nr):
                try:
                    self.symbols.append(self.atom_types[i])
                except:
                    raise RuntimeError(
                        "It seems that POTCAR and POSCAR has "
                        "different number of atoms")

        # Advance one line
        fr = fr + 1

        # Check for selective dynamics
        if data[fr].upper()[0] == 'S':
            fr += 1

        # Check for direct coordinates
        self.direct = True
        if data[fr].upper()[0] == 'C':
            self.direct = False
        elif data[fr].upper()[0] == 'D':
            self.dorect = True

        ##############################
        #  Gather atomic positions
        ##############################
        fr += 1

        self.header = "".join(data[:fr])

        self.scaled_positions = []
        for i in range(fr, fr + self.nr_of_atoms):
            pos = data[i].split()
            assert len(pos) in {3, 6}, "Wrong atomic position: {}".format(
                data[i])

            pos = [float(i) for i in pos[:3]]
            self.scaled_positions.append(pos)

        self.scaled_positions = np.array(self.scaled_positions)

        if not self.direct:
            self.scaled_positions = np.array(
                np.dot(self.scaled_positions, np.linalg.inv(self.cell)),
                dtype='double', order='C')

        self.create_atoms()
        self.constrains = []

    def create_atoms(self):
        from deatoms import Atoms

        self.atoms = Atoms(symbols=self.symbols,
                           scaled_positions=self.scaled_positions,
                           cell=self.cell)
        return self.atoms

    def create_input(self):
        content = self.header

        if self.constrains:
            content = content.replace("Direct", "Selective dynamics\nDirect")

        cell_content = "".join(self.cell_lines)
        new_cell_content = ""
        for i in self.atoms.cell:
            new_cell_content += "{:22.14f}  {:22.14f}  {:22.14f}\n".format(
                *i/self.ltconst)

        content = content.replace(cell_content, new_cell_content)

        if self.constrains:
            for pos, const in zip(
                    self.atoms.get_scaled_positions(), self.constrains):
                content += ("{:20.14f}  {:20.14f}  {:20.14f}".format(*pos) +
                            "  {} {} {}\n".format(*const))
        else:
            for pos in self.atoms.get_scaled_positions():
                content += "{:20.14f}  {:20.14f}  {:20.14f}\n".format(
                    pos[0], pos[1], pos[2])

        return content

    def set_atoms(self, atoms):
        self.atoms = atoms


class Outcar():
    def __init__(self, filepath):
        self.filepath = filepath
        self.forces = None

    def read_forces(self):
        with open(self.filepath, "rt") as f:
            content = f.read()

        for i in re.finditer(r"TOTAL-FORCE \(eV/Angst\) *\n *-* *\n(.*?)\n -+",
                             content,
                             flags=re.DOTALL):
            pass

        forces = []
        positions = []

        for i in i.group(1).split("\n"):
            line = i.split()
            forces.append([float(i) for i in line[-3:]])
            positions.append([float(i) for i in line[:3]])

        self.forces = np.array(forces)
        self.max_force = np.apply_along_axis(np.linalg.norm, 0, forces).max()

        return self.forces

    def get_energy(self, units="eV"):
        with open(self.filepath, "rt") as f:
            content = f.read()

        for i in re.finditer(r"free *energy *TOTEN *= *(.*) *eV", content):
            pass

        return float(i.group(1))

    def get_forces(self, units="N"):
        if self.forces is None:
            self.read_forces()

        return convert_vasp_forces(self.forces, units=units)



if __name__ == "__main__":
    # vasp = VaspXML(
    #     "../../diamond/vasp/strains/nv3x3x3hse/normal_strain_0.002/"
    #     "vasprun.xml")
    # vasp.get_eigenvalues()

    print(convert_vasp_forces(1))
