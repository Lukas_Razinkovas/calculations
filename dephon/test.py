import unittest
import dedisp
import numpy as np
from vasp import Poscar


class TestUnitConversion(unittest.TestCase):

    def test_position_units(self):
        atoms1 = Poscar(
            "test_data/simple/two_atoms/POSCAR").create_atoms()

        self.assertEqual(atoms1.symbols, ['C', 'N'])

        pos_in_a = atoms1.get_positions()
        pos_in_bohr = atoms1.get_positions(units="au")
        pos_in_m = atoms1.get_positions(units="si")

        self.assertTrue(
            np.allclose(pos_in_a, np.array([[1, 0, 0], [5, 5, 5]])))
        self.assertTrue(np.isclose(pos_in_bohr[0][0], 1.88973))
        self.assertTrue(np.isclose(pos_in_m[0][0], 1e-10))


class DedispTest(unittest.TestCase):
    """Tests function 'remove_pb_artifacts'."""

    def test_pb_artifacts_removal(self):
        atoms1 = Poscar(
            "test_data/simple/two_atoms/POSCAR").create_atoms()
        atoms2 = Poscar(
            "test_data/simple/two_atoms/POSCAR-DISPLACED").create_atoms()

        self.assertEqual(atoms1.symbols, ['C', 'N'])
        self.assertEqual(atoms2.symbols, ['C', 'N'])

        dedisp.remove_pb_artifacts(atoms1, atoms2)
        self.assertTrue(
            np.allclose(atoms2.get_positions(),
                        np.array([[-1, 0, 0], [5.1, 5.1, 5.1]])))

    def test_mass_center_alignment(self):
        atoms1 = Poscar(
            "test_data/simple/two_atoms/POSCAR").create_atoms()
        atoms2 = Poscar(
            "test_data/simple/two_atoms/POSCAR-SHIFTED").create_atoms()

        dedisp.align_mass_center(atoms1, atoms2)
        self.assertTrue(np.allclose(atoms1.get_positions(),
                                    atoms2.get_positions()))

    def test_displacement_calculations(self):
        atoms1 = Poscar(
            "test_data/simple/two_atoms/POSCAR2").create_atoms()
        atoms2 = Poscar(
            "test_data/simple/two_atoms/POSCAR-DISPLACED2").create_atoms()
        atoms3 = Poscar(
            "test_data/simple/two_atoms/POSCAR-DISPLACED3").create_atoms()

        # Test 1
        self.assertEqual(atoms1.get_symbols(), ["C", "C"])

        for units in {"angstrom", "a", "angstroms"}:
            displ = dedisp.calculate_displacements(
                atoms1, atoms2,
                units=units)
            self.assertTrue(
                np.allclose(displ, [[-1, 0, 0], [1, 0, 0]]))

        for units in {"bohr", "au"}:
            displ = dedisp.calculate_displacements(
                atoms1, atoms2,
                units=units)
            self.assertTrue(
                np.allclose(displ, [[-1.889731, 0, 0], [1.88973, 0, 0]]))

        for units in {"si", "m"}:
            displ = dedisp.calculate_displacements(
                atoms1, atoms2,
                units=units)
            self.assertTrue(
                np.allclose(displ, [[-1e-10, 0, 0], [1e-10, 0, 0]]))

        # Test 2
        dedisp.remove_pb_artifacts(atoms1, atoms3)
        displ = dedisp.calculate_displacements(
            atoms1, atoms3,
            units="a", correct_discrepancies=False)
        self.assertTrue(np.allclose(displ, [[-1, 1, -1], [1, -1, 1]]),
                        displ)


class DedispForcesTest(unittest.TestCase):
    def test_phonopy_displacement(self):
        atoms1 = Poscar("test_data/forces/POSCAR").create_atoms()
        atoms2 = Poscar("test_data/forces/POSCAR2").create_atoms()

        displ = dedisp.calculate_displacements(
            atoms1, atoms2,
            units="a", correct_discrepancies=False)

        displ = np.around(displ, 10)
        indx = np.where(displ != 0)
        self.assertTrue(np.allclose(indx, [[0], [0]]))
        self.assertTrue(np.isclose(displ[indx], 0.01))

    def test_force_differences(self):
        outcar1 = "test_data/forces/OUTCAR"
        outcar2 = "test_data/forces/OUTCAR2"

        # Test cases
        diff1 = np.array([-0.442641, -0.001378, -0.001378]) - np.array(
            [-0.000134, -0.000134, -0.000134])

        diff215 = np.array(
            [0.005805, 0.005713, 0.005713]) - np.array(
                [0.005747, 0.005747, 0.005747])

        # Vasp units
        diff = dedisp.read_force_differences(outcar1, outcar2, units="vasp")

        self.assertTrue(np.allclose(diff[0], diff1))
        self.assertTrue(np.allclose(diff[214], diff215), (diff[214], diff215))

        # vasprun.xml test
        vasprun1 = "test_data/forces/vasprun.xml"
        vasprun2 = "test_data/forces/vasprun2.xml"
        diff2 = dedisp.read_force_differences(vasprun1, vasprun2, units="vasp")
        np.testing.assert_array_almost_equal(diff, diff2)

        # Other Units
        diff = dedisp.read_force_differences(outcar1, outcar2, units="au")
        diff2 = dedisp.read_force_differences(vasprun1, vasprun2, units="au")
        np.testing.assert_array_almost_equal(diff, diff2)
        np.testing.assert_array_almost_equal(diff[0], diff1*0.0367493/1.88973)


if __name__ == '__main__':
    unittest.main()
