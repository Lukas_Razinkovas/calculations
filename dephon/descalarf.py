import numpy as np
import os
try:
    from mayavi import mlab
    import pandas as pd
except:
    raise

import pickle
import pprint
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from scipy.interpolate import griddata
import matplotlib
from matplotlib import colors, rc
from scipy.optimize import curve_fit


rc('font', **{'family': 'serif', 'serif': ['Palatino'], "size": 15})
rc('text', usetex=True)


pp = pprint.PrettyPrinter()


try:
    df_colors = pd.read_csv(
        os.path.join(os.path.abspath(os.path.split(__file__)[0]),
                     "jmolcolors.csv"))

    def get_color(atom):
        """
        Returns RGB color tuple corresponding to atom

        :param str atom: symbol for atom
        """
        r = df_colors[df_colors['atom'] == atom]['R'].values[0] / 255.0
        g = df_colors[df_colors['atom'] == atom]['G'].values[0] / 255.0
        b = df_colors[df_colors['atom'] == atom]['B'].values[0] / 255.0
        return (r, g, b)
except:
    raise


class ScalarField():
    def __init__(self, filepath=None, filetype="xsf"):
        self._validated = False
        self.lengths = None
        self.filepath = None
        self.datagrid = None
        self.source = None
        self.name = None
        self.primvec = None
        self.positions = []
        self.symbols = []
        self.grid_pattern = None
        self.center = np.array([0, 0, 0])

        if filepath is not None:
            self.parse_xsf(filepath)

    def _line_to_floats(self, line):
        return [float(i) for i in line.rstrip().split()]

    def _add_atom(self, line):
        elements = line.split()
        if len(elements) != 4 or len(elements[0]) > 2:
            return False

        self.symbols.append(elements[0])
        self.positions.append(np.array(elements[1:], float))

        return True

    def parse_crystal(self, f):
        assert "PRIMVEC" in next(f).rstrip()
        self.primvec = []
        for i in range(3):
            self.primvec.append(self._line_to_floats(next(f)))

        assert "PRIMCOORD" in next(f).rstrip()
        self.nr_of_atoms, _ = self._line_to_floats(next(f))
        add = True
        nr = 0
        while add:
            line = next(f).rstrip()
            add = self._add_atom(line)
            nr += 1

        print("Total atoms:", nr)

        self.positions = np.array(self.positions)
        return line

    def parse_datagrid(self, f):
        self.source = next(f).rstrip()
        self.name = next(f).rstrip()
        self.grid_pattern = np.array(next(f).rstrip().split(), int)

        print("Gridd pattern: {} ({} points)".format(
            self.grid_pattern,
            self.grid_pattern[0]*self.grid_pattern[1]*self.grid_pattern[2]))

        self.origin = self._line_to_floats(next(f))
        self.cell = []
        for i in range(3):
            self.cell.append(self._line_to_floats(next(f)))

        self.cell = np.array(self.cell)
        print("Cell:\n", self.cell)

        self.index = [0, 0, 0]
        data = []
        line = ""

        while True:
            line = next(f)
            if line.startswith("END_DATAGRID_3D"):
                break
            data += self._line_to_floats(line)

        self.datagrid = np.reshape(data, self.grid_pattern, order="F")
        del data

    def parse_xsf(self, filepath):
        self.filepath = filepath
        nr = 0
        with open(self.filepath, "rt") as f:
            line = None
            while True:
                try:
                    line = next(f)
                except:
                    break

                if line.lstrip().startswith("CRYSTAL"):
                    line = self.parse_crystal(f)

                if line.lstrip().startswith("BEGIN_BLOCK_DATAGRID_3D"):
                    self.parse_datagrid(f)

                nr += 1

    def generate_xyz_grid(self):
        self._validate_cell()
        self.xyz = []

        for i in range(3):
            self.xyz.append(np.linspace(
                0 - self.center[i],
                self.cell[i][i] - self.center[i],
                self.grid_pattern[0]))

        self.xyz = np.array(self.xyz)
        self.xyzgrid = np.array(np.meshgrid(*self.xyz, indexing="ij"))

    def _get_scalar_field(self, log=False):
        self.generate_xyz_grid()
        if log:
            x = self.datagrid * 1e6
            self.datagrid = np.sign(x)*np.log10(np.abs(x) + 1)

        return mlab.pipeline.scalar_field(*self.xyzgrid, self.datagrid)

    def display_isosurface(self):
        src = self._get_scalar_field()
        _min = self.datagrid.min()
        _max = self.datagrid.max()
        mylimit = max(_min, _max)

        mlab.pipeline.iso_surface(
            src, contours=[
                self.datagrid.min()+0.01*abs(self.datagrid.min()),
                self.datagrid.min()+0.1*abs(self.datagrid.min()),
                self.datagrid.min()+0.5*abs(self.datagrid.min()),
                self.datagrid.min()+0.9*abs(self.datagrid.min()),
                self.datagrid.min()+0.95*abs(self.datagrid.min())
            ],
            colormap="RdBu",
            vmin=-mylimit,
            vmax=mylimit,
            opacity=0.3)

    def display_scalar_field(self, name="noname", log=False):
        src = self._get_scalar_field(log=log)

        _min = self.datagrid.min()
        _max = self.datagrid.max()
        mylimit = max(_min, _max)

        a = mlab.pipeline.image_plane_widget(src, plane_orientation="x_axes",
                                             colormap="RdBu",
                                             vmin=-mylimit,
                                             vmax=mylimit)

        mlab.colorbar(a, orientation='vertical')
        mlab.outline(a)
        mlab.axes(a)
        mlab.title(name)

    def display_atoms(self, diagonal=False):
        for nr, symb in enumerate(self.symbols):
            pos = self.positions[nr]

            if diagonal and not np.allclose(pos[0], pos[1]):
                    continue

            mlab.points3d(*pos,
                          scale_factor=0.3,
                          resolution=20,
                          color=get_color(symb))

    def multiply_by(self, nr=3):
        self.datagrid = np.tile(self.datagrid, (nr, nr, nr))
        self.grid_pattern *= nr
        self.cell *= nr

    def _validate_cell(self):
        if self._validated:
            return

        cell = self.cell.copy()
        for i in range(3):
            cell[i][i] = 0

        if cell.sum() != 0:
            raise NotImplementedError(
                "Distance can be calculated only for orthorombic cells.")

        self.lengths = self.cell.sum(axis=0)
        self.validated = True

    def get_distance(self, pos1, pos2, prec=5):
        self._validate_cell()

        displ = pos2 - pos1

        new_pos2 = (pos2 +
                    (abs(displ) > self.lengths/2) *
                    (-np.sign(displ)) *
                    self.lengths)

        displ = new_pos2 - pos1
        dist = np.linalg.norm(displ)

        return displ, round(dist, prec)

    def iter_by_radius(self, center):
        xyz = []

        for m, nr in zip(self.cell, self.grid_pattern):
            xyz.append(np.linspace(0, max(m), nr))

        x, y, z = np.meshgrid(*xyz)

        for index, value in np.ndenumerate(self.datagrid):

            pos = np.array([x[index], y[index], z[index]])
            d, r = self.get_distance(pos, center)
            yield r, d, index

    def zero_arround_defect(self, cutoff=4):
        for r, d, index in self.iter_by_radius(
                self.positions[self.symbols.index("N")]):

            if r < cutoff:
                self.datagrid[index] = 0

    def calculate_charge_distribution(self, center=None):
        if center is None:
            center = self.positions[self.symbols.index("N")]

        self._validate_cell()

        l = self.cell.max()
        delta_r = l/(self.grid_pattern.max())
        vel = delta_r**3

        max_distance = np.sqrt(3*((self.lengths.max()/2)**2))
        # nonoverlaping_max_distance = self.lengths.max()/2
        distances = np.arange(0, max_distance, delta_r)
        charges = {}

        total = [0, 0]

        nr = 0
        for r, _, index in self.iter_by_radius(center):
            if nr % 1000 == 0:
                print(nr, end="\r")
            nr += 1

            c = self.datagrid[index]*vel/(0.52917721067**3)
            total[0] += c
            total[1] += vel

            for d in distances:
                if r <= d:
                    data = charges.get(d, [0, 0])
                    data[0] += c
                    data[1] += vel
                    charges[d] = data

        return charges, total

    def display_center_point(self, nr=1):
        # if nr == 3:
        #     for i in self.cell:
        #         # self.center += i
        #         self.positions += i

        # self.center = self.positions[self.symbols.index("N")]

        # if nr != 1:
        #     self.multiply_by(nr)

        mlab.points3d(0, 0, 0,
                      scale_factor=0.5,
                      resolution=20,
                      color=get_color("N"))

    def recenter(self, point):
        self.center = point
        # self.positions -= point

    # def _get_interpolating_function(self):
    #     self.generate_xyz_grid()
    #     return RegularGridInterpolator(self.xyz, self.datagrid)

    # def get_interpolating_spherical_function(self):
    #     cartfoo = self._get_interpolating_function()

    #     def foo(r, theta, phi):
    #         return cartfoo([
    #             r*np.sin(theta)*np.cos(phi),
    #             r*np.sin(theta)*np.sin(phi),
    #             r*np.cos(theta)])

    #     return foo

    # def integrate_from_center(self, start=0, end=1, grid=3, center=None):
    #     if center is not None:
    #         self.recenter(center)

    #     foo = self.get_interpolating_spherical_function()
    #     x = []
    #     y = []
    #     for r in np.linspace(start, end, grid):
    #         print("integrating r", r)
    #         y.append(integrate.nquad(
    #             lambda r, t, p: foo(r, t, p)*np.sin(p)*r**2,
    #             [[0, r], [0, np.pi], [0, 2*np.pi]]))
    #         x.append((4/3)*np.pi*pow(r, 3))

    #     return x, y


def _extract_data(data):
    v, c, r = [], [], []
    for i in sorted(data.keys()):
        d = data[i]
        r.append(i)
        v.append(d[1])
        c.append(d[0])

    v = np.array(v)

    return (v, r, c, UnivariateSpline(v, c, k=4, s=0))


def fit_linear(v, c, max_cutoff):
    min_val = max_cutoff*0.7
    max_val = max_cutoff*1.6

    v = np.array(v)
    c = np.array(c)

    vfilter = np.where(np.logical_and(min_val < v, v < max_val))[0]

    # def foo(x, a, b):
    #     return a*x + b

    # popt, pcov = curve_fit(foo, v[vfilter], c[vfilter])
    # plt.plot(v[vfilter], c[vfilter], "-")
    # plt.plot(v[vfilter], foo(v[vfilter], *popt), "--")
    param = np.polyfit(v[vfilter], c[vfilter], 1)

    def foo2(x):
        return param[0]*x

    return foo2, v[vfilter], c[vfilter], param


def integrate_charge_from_center(
        paths,
        suptitle,
        axes,
        output_dir="images",
        compare_paths=None,
        center_point=None,
        shorter=True,
        minus_homogenious=False,
        lw=1.5,
        alpha=0.7,
        value="Charge"):

    if center_point is None:
        center_point = [4, 4, 4]

    if compare_paths is not None:
        assert len(compare_paths) == len(paths)
    else:
        compare_paths = []

    ##############################
    #     Save pickle files
    ##############################

    for nm, path in paths + compare_paths:
        pickle_file = os.path.join(output_dir, "charge_{}.p".format(nm))
        if not os.path.exists(pickle_file):
            f = ScalarField(path)
            data = f.calculate_charge_distribution(center_point)
            pickle.dump(data, open(pickle_file, "wb"))

    ##############################
    #           Ploting
    ##############################

    for nr, (nm, path) in enumerate(paths):
        pickle_file = os.path.join(output_dir, "charge_{}.p".format(nm))
        data, total = pickle.load(open(pickle_file, "rb"))
        v, r, c, splv = _extract_data(data)
        f = ScalarField(path)
        max_cutoff_r = f.cell.max()/2
        max_cutoff_v = (4/3)*(np.pi*max_cutoff_r**3)

        print("="*70)
        print(nm)
        print("-"*70)
        print("Target:", total)

        ##############################
        #        Substract
        ##############################
        if compare_paths:
            comp_nm, comp_file = compare_paths[nr]
            pickle_file = os.path.join(output_dir, "charge_{}.p".format(
                comp_nm))
            bdata, btotal = pickle.load(open(pickle_file, "rb"))
            print("Compared:", btotal)
            _v, _r, _c, _splv = _extract_data(bdata)

        ##############################
        #          Volume
        ##############################

        y = np.array(c)
        if compare_paths:
            try:
                y -= _splv(v)
            except:
                print("!!!", nm)
                raise

        color = None
        if minus_homogenious:
            homog_charge, v0, y0, param = fit_linear(v, y, max_cutoff_v)
            line = axes[0].plot(v, y, "-", label=nm, linewidth=lw, alpha=alpha)
            color = line[0].get_color()
            axes[0].plot(v, (lambda x: param[1] + param[0]*x)(v), "--",
                         label="$y = {:.4f} + {:.4f}x$".format(
                             param[1], param[0]),
                         color=color, alpha=1, linewidth=1)
            axes[0].legend(loc=4)
            y = y - homog_charge(v)

        for a in axes[:2]:
            a.set_xlabel("Sphere volume ($\mathrm{\AA}^3$)")
            a.set_ylabel(
                "{}  inside sphere - homogenious charge".format(
                    value))
            a.grid(True)

        axes[0].set_ylabel("{}  inside sphere".format(value))

        if color is not None:
            axes[1].plot(v, y, label=nm, linewidth=lw, alpha=alpha,
                         color=color)
        else:
            axes[1].plot(v, y, label=nm, linewidth=lw, alpha=alpha)

        # ax[1].legend(loc=4)
        axes[1].axvline(max_cutoff_v, color="gray", linestyle="--")
        # ax[1].set_ylim([-0.2, 1])

        ##############################
        #     Charge density
        ##############################

        x = np.linspace(0, max(v), 6000)

        splv = UnivariateSpline(v, y, k=4, s=0)
        y = splv.derivative()(x)
        # y = splv.derivative()(x)
        # if compare_with is not None:
        #     y -= _splv.derivative()(x)

        axes[2].plot(x, y, label=nm, linewidth=lw, alpha=alpha)

        axes[2].set_xlabel(r"Sphere volume ($\mathrm{\AA}^3$)")
        axes[2].set_ylabel("{} density".format(value))
        axes[2].grid(True)
        # ax[2].legend(loc=4)
        axes[2].axvline(max_cutoff_v, color="gray", linestyle="--")
        axes[2].set_xlim([0, 700])

        ##############################
        #     Radial density
        ##############################

        x2 = np.power(x*3/(4*np.pi), float(1)/3)
        y = splv.derivative()(x)
        # if compare_with is not None:
        #     y -= _splv.derivative()(x)

        axes[3].plot(x2, y, label=nm, linewidth=lw, alpha=alpha)
        axes[3].grid(True)
        axes[3].set_xlabel(r"Radius from defect ($\mathrm{\AA}$)")
        axes[3].set_ylabel("{} density".format(value))
        # axes[3].legend(loc=4)
        axes[3].set_xlim([0.9, max(x2)])
        axes[3].axvline(max_cutoff_r, color="gray", linestyle="--")

        if nm in {"nv3x3x3", "nv3x3x3nonerelaxed",
                  "nv03x3x3nonrelaxed"} and shorter:
            axes[0].set_xlim([0, max(v)])
            axes[1].set_xlim([0, max(v)])
            axes[2].set_xlim([0, max(v)])
            axes[3].set_xlim([0, max(r)])

    # plt.tight_layout()
    # plt.subplots_adjust(top=0.95)
    # plt.suptitle(suptitle, size=25)

    # plt.savefig(os.path.join(output_dir, "{}.pdf".format(plotname)),
    #             dpi=300)
    # plt.clf()


def get_slice(scalarf, ax, fig, name, datapoints=1000, center_point=None):
    if center_point is None:
        center_point = np.array([4, 4, 4])
    else:
        center_point = np.array(center_point)

    xx, yy, zz = scalarf.xyzgrid

    index = np.where(xx == yy)
    x = np.sqrt(xx[index]**2 + yy[index]**2)
    y = zz[index]
    data = scalarf.datagrid[index]

    xi = np.linspace(x.min(), x.max(), datapoints)
    yi = np.linspace(y.min(), y.max(), datapoints)
    X, Y = np.meshgrid(xi, yi)
    Z = griddata((x, y), data, (X, Y))

    lim = max([data.min(), data.max()])

    # fig, ax = plt.subplots(1)
    pcm = ax.contourf(
        X, Y, Z,
        norm=colors.SymLogNorm(linthresh=0.1, linscale=10,
                               vmin=-lim, vmax=lim),
        cm=matplotlib.cm.RdBu)

    for nr, symb in enumerate(scalarf.symbols):
        pos = scalarf.positions[nr]

        if not np.allclose(pos[0], pos[1]):
            continue

        ax.scatter(
            [np.linalg.norm(pos[:-1])], [pos[2]],
            color=get_color(symb))

    ax.scatter([np.linalg.norm(center_point[:-1])], [center_point[2]],
               marker="+", color="black")
    ax.contour(X, Y, Z)

    fig.colorbar(pcm, ax=ax, extend='max')
    ax.axis("scaled")
    ax.set_xlim([x.min(), x.max()])
    ax.set_ylim([y.min(), y.max()])
    ax.set_xlabel(r"$\mathrm{\AA}$")
    ax.set_ylabel(r"$\mathrm{\AA}$")
    ax.set_title(name)


def plot_difference_of_scalar_fields(path1, path2, name, matplotlib=True,
                                     ax=None, fig=None, center_point=None):
    scalarf1 = ScalarField(path1)
    scalarf2 = ScalarField(path2)
    scalarf1.datagrid -= scalarf2.datagrid
    scalarf1.multiply_by(2)

    if matplotlib:
        scalarf1.generate_xyz_grid()
        get_slice(scalarf1, ax, fig, name, center_point=center_point)
    else:
        # scalarf1.display_center_point(3)
        scalarf1.display_scalar_field(name, log=True)
        scalarf1.display_atoms(diagonal=True)

        # scalarf1.zero_arround_defect()
        # scalarf1.display_isosurface()
        mlab.show()
