import os
import time
import itertools
import numpy as np
from deatoms import Atoms
from defc import ForceConstants
from deutils import ensure_dir
import logging
from time import gmtime, strftime
import pprint
# from memory_profiler import profile

pp = pprint.PrettyPrinter()

logging.basicConfig(format='%(message)s')
log = logging.getLogger(__name__)


def multiply_atoms_object(atoms, multipl):
    """Creates new Atoms object which is multiplied in every
    unit vector direction (cell must be orthagonal) by 'multipl'.

    Args:
        atoms (Atoms): cell geometry object to be multiplied
        multipl (list of int): multiplication array [x, y, z]

    Returns:
        atoms (Atoms): multiplied cell geometry object
    """

    base_atoms_str = ""

    if atoms.base_atoms is not None:
        for nr, i in enumerate(atoms.base_atoms, 1):
            base_atoms_str += "   {}. {} {} A ({} Crystal)\n".format(
                nr, atoms.get_symbol(i), atoms.get_positions()[i],
                atoms.get_scaled_positions()[i])

            log.info(
                "\n" + "-"*70 +
                "\nCreating new lattice from {} atoms supercell:\n".format(
                    len(atoms)) +
                "-"*70 + "\n" +
                " - base lattice vectors in Angstroms:\n{}\n".format(
                    "   " + str(atoms.get_cell()).replace("\n", "\n   ")) +
                " - base lattice primitive vectors in Angstroms:\n{}\n".format(
                    "   " + str(atoms.symmetry.prim_lattice).replace("\n", "\n   ")) +
                " - base lattice basis atoms:\n{}".format(base_atoms_str)
            )

    positions = atoms.get_positions()
    symbols = atoms.get_symbols()
    cell = atoms.get_cell()
    base_numbers = list(atoms.base_numbers)

    new_positions = []
    new_symbols = []
    new_base_numbers = []

    for nr, m in enumerate(
            itertools.product(*[list(range(i)) for i in multipl]), 1):
        print("Multiplying geometry:", nr, end="\r")
        new_positions += list(np.dot(cell, m) + positions)
        new_symbols += list(symbols)
        new_base_numbers += base_numbers

    new_cell = [i*j for i, j in zip(cell, multipl)]

    print("Creating atoms")
    atoms = Atoms(positions=new_positions, cell=new_cell, symbols=new_symbols,
                  base_numbers=new_base_numbers)
    print("Atoms created")
    atoms._symbols_to_numbers()
    atoms._symbols_to_masses()

    log.info(
        "* New lattice created\n" +
        " - new lattice has {} atoms:\n".format(len(new_positions)) +
        " - new lattice vectors in Angstroms:\n{}".format(
            "   " + str(atoms.get_cell()).replace("\n", "\n   ")))

    return atoms


def _print_pair_info(i, j, fc, name):
    """Just for debuging purpose"""
    spos = fc.atoms.get_scaled_positions()
    pos = fc.atoms.get_positions()
    print("-"*70)
    print(name, len(fc.atoms))
    print("-"*70)
    print("pos1:", pos[i], spos[i])
    print("pos2:", pos[j], spos[j])
    print("diff:", fc.atoms.get_separation_vector(i, j))
    print("FC:\n", fc.get_atom_pair_part(i, j))


def _compare_force_constants(newfc, i, j, donor_fc, i2, j2, prec=0.001):
    """Just for debuging purpose"""
    ij = newfc.get_atom_pair_part(i, j)
    ij2 = donor_fc.get_atom_pair_part(i2, j2)
    diff = ij - ij2
    if diff.max() > prec:
        print("="*70)
        _print_pair_info(i, j, newfc, "old")
        _print_pair_info(i2, j2, donor_fc, "new")
        print("-"*70)
        print(diff)
        return diff.max()

    return None


def create_megasupercell(donor_fc, base_fc, multipl=None, cutoff=None,
                         perform_comparison=None):

    """Creates new force constants object of supercell which is multiple
    of base_fc.atoms object. Force constants are read from donor_fc.
    NO DEFECT CREATED!

    Args:
        donor_fc (ForceConstant): force constant object which will be used
            as donor for force constants
        base_fc (ForceConstant): force constant object which will be used
            for new geometry creation
        multipl (list of int): new cell will be created by multiplying
            base_fc.atoms geometry
        perform_comparison (bool): used for benchmarking. If True
            will emebed force constants into base_fc cell.
    """
    beg = time.time()

    ######################################
    #    Create new larger supercell
    ######################################

    max_cutoff = donor_fc.max_cutoff()
    if cutoff is None:
        cutoff = max_cutoff

    if isinstance(base_fc, ForceConstants):
        atoms = base_fc.atoms
    elif isinstance(base_fc, Atoms):
        atoms = base_fc
    else:
        raise RuntimeError("Unknown type of base_fc {}".format(type(base_fc)))

    if multipl is not None:
        assert perform_comparison is None
        new_atoms = multiply_atoms_object(atoms, multipl)
        newfc = ForceConstants(calc='none', atoms=new_atoms)

    newfc.embeding_info["bulk"]["cutoff"] = cutoff

    ##############################
    #   Assign force constants
    ##############################

    log.info(
        "\n" + "-"*70 +
        "\nCreating mega supercell with " +
        "{:.5f} A interaction cutoff".format(cutoff) + "\n" +
        "-"*70 + "\n" +
        "- FC donor lattice vectors in Angstroms:\n{}\n".format(
            "   " + str(donor_fc.atoms.get_cell()).replace("\n", "\n   ")) +
        "- max cutoff: {:.5f} A".format(max_cutoff) + "\n" +
        "- new cell width: {:.5f} A".format(newfc.atoms.get_cell().max()))

    # Find atom pairs sorted by distance (for performacne purpose)
    # maps = {distance: base_pair: [at1_nr, at2_nr, displ_vec, sym1, sym2]}
    sbeg = time.time()
    maps = donor_fc.atoms.sort_by_distance()
    log.info("- sorting by distance finished in: {:.3f}s".format(
        time.time() - sbeg))

    sbeg = time.time()
    report = donor_fc.atoms.calculate_symmetry()
    log.info(report)
    log.info("- symmetry calculations finished in: {:.3f}s".format(
        time.time() - sbeg))

    # Store info about shells
    sdist = np.array(sorted([float(i) for i in maps.keys()]))
    newfc.embeding_info["bulk"]["donor_shells"] = {}
    for nr, _d in enumerate(sdist):
        newfc.embeding_info["bulk"]["donor_shells"][nr] = _d

    newfc.embeding_info["bulk"][
        "interacting_shells"] = np.where(sdist < cutoff)[0].max()
    log.info("- {} nearest neighbour shells interacting".format(
        newfc.embeding_info["bulk"]["interacting_shells"]))
    log.info("- shells:\n" + "\n".join(
        [" + {}: {:.3f}".format(
            i, newfc.embeding_info["bulk"]["donor_shells"][i]) for i in
         sorted(newfc.embeding_info["bulk"]["donor_shells"].keys())]))

    ##################################################
    #  Converting to dense matrix for fast iteration
    ##################################################

    _l = len(newfc.atoms)
    lfeq = 0
    _i = 0
    for nr, (i, j) in enumerate(newfc.atoms.iter_pairs()):

        if i != _i:
            _i = i
            print("Assigning force constants for large bulk "
                  "{}/{} ({:.2f}\%)".format(nr, _l**2, (100/_l**2)*nr),
                  end="\r")

        _, dist = newfc.atoms.get_separation_vector(i, j)
        if dist > cutoff:
            newfc.set_atom_pair_part(i, j, np.zeros([3, 3]))
            continue

        lfeq += 1

        equiv = donor_fc.atoms.find_equivalent(i, j, newfc.atoms, debug=False)

        ##############################
        #    Further than cutoff
        ##############################

        if perform_comparison:
            _compare_force_constants(newfc, i, j,
                                     donor_fc,
                                     equiv["i"], equiv["j"])

        ij = donor_fc.get_atom_pair_part(equiv["i"], equiv["j"])
        newfc.set_atom_pair_part(i, j, ij)

    newfc.embeding_info["bulk"]["total_pairs"] = nr+1
    newfc.embeding_info["bulk"]["pairs_interacting"] = lfeq

    log.info("- iterated " +
             "{} atom pairs, {} ({:.3f} %) were in cutoff radius".format(
                 nr+1, lfeq, (100/(nr+1))*lfeq))
    log.info("- suppercell with fc created in {:.3f}s\n".format(
        time.time()-beg))

    return newfc


def combine_bulk(donor_fc, base_atoms, multipl, cutoff=None, log_fh=None):
    """Creates combined bulk and writes force constants to directory
    'write_to_dir'

    Args:
        donor_fc_dir (str): directory path to calculational results for
            donor force_constants
        base_fc_dir (str): directory path to supercell file which will be
            used as base geometry block
        displ_dir (str): relative path for phonopy output
        write_to_file (str): if not none writes whole force_constants file
            into pickle file (extension should be '.p')
    """

    base_atoms.identify_different_base_atoms()
    donor_fc.atoms.identify_different_base_atoms(
        base_atom_positions=[base_atoms.get_positions()[i] for i in
                             base_atoms.base_atoms])

    log.info("\n".join(
        ["* Starting combined bulk creation: {}".format(
            strftime("%Y-%m-%d %H:%M:%S", gmtime())),
         " - lattice multiplication pattern: {}".format(multipl)]))

    bigbulk = create_megasupercell(donor_fc, base_atoms, multipl=multipl,
                                   cutoff=cutoff)

    return bigbulk


def calculate_modes_and_write_results(bulk, csv_outdir, log_fh=None,
                                      from_scalapack_output=None,
                                      logtillipr=150):
    if log_fh is not None:
        if log_fh not in log.handlers:
            log.addHandler(log_fh)

    beg = time.time()
    if from_scalapack_output is None:
        modes = bulk.calculate_modes()
    else:
        modes = bulk.read_scalapack_output_and_calculate_modes(
            from_scalapack_output)

    log.info("* Diagonalization finished in: {:.3f}s".format(
        time.time() - beg))

    if log_fh is not None:
        modes.set_log_handler(log_fh)

    modes.analyse(logtillipr=logtillipr)
    modes.write_all_info(csv_outdir)
    modes.remove_log_handler()


def create_megabulk_and_write_results_to_dir(
        donor_fc, base_atoms, output_dir, multipl=None, cutoff=None,
        log_file=True, xyz_file=True, calculate_modes=False):
    """
    Creates crystal bulk. No defect.

    Args:
       donor_fc(defc.ForceConstants): force constants with defect interactions
       base_atoms(deatoms.Atoms): geometry
    """

    ensure_dir(output_dir)

    if log_file:
        log_file_path = os.path.join(output_dir, "embeding.log")
        with open(log_file_path, "wt") as f:
            f.write("")
        hdlr = logging.FileHandler(log_file_path)
        log.addHandler(hdlr)
        logging.getLogger("defc").addHandler(hdlr)

    bulk = combine_bulk(donor_fc, base_atoms, multipl, cutoff)

    xyz_filepath = os.path.join(output_dir, "coordinates.xyz")
    xsf_filepath = os.path.join(output_dir, "coordinates.xsf")

    with open(xyz_filepath, "wt") as f:
        log.info("* Writing atom coordinates in: {}".format(xyz_filepath))
        f.write(bulk.create_xyz_content())

    with open(xsf_filepath, "wt") as f:
        log.info("* Writing atom coordinates in: {}".format(xsf_filepath))
        f.write(bulk.create_xsf_content())

    bulk.symmetrize_fc(2)
    bulk.save_to_hdf5(output_dir)

    if not calculate_modes:
        if log_file is not None:
            log.removeHandler(hdlr)
            logging.getLogger("defc").removeHandler(hdlr)
        return bulk

    calculate_modes_and_write_results(bulk, output_dir, log_fh=hdlr)
    if log_file is not None:
        log.removeHandler(hdlr)
        logging.getLogger("defc").removeHandler(hdlr)
