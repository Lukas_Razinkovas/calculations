import os
import logging
import configparser
import subprocess
from scipy.constants import physical_constants, hbar, c, m_e
import numpy as np
from bisect import bisect_left
from scipy.spatial.distance import cdist


config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), "config.ini"))


log = logging.getLogger(__name__)


def remove_lil_col(lil, j):
    if j < 0:
        j += lil.shape[1]

    if j < 0 or j >= lil.shape[1]:
        raise IndexError('column index out of bounds')

    rows = lil.rows
    data = lil.data
    for i in range(lil.shape[0]):
        pos = bisect_left(rows[i], j)
        if pos == len(rows[i]):
            continue
        elif rows[i][pos] == j:
            rows[i].pop(pos)
            data[i].pop(pos)
            if pos == len(rows[i]):
                continue

        for pos2 in range(pos, len(rows[i])):
            rows[i][pos2] -= 1

    lil._shape = (lil._shape[0], lil._shape[1] - 1)


def remove_lil_row(lil, i):
    if i < 0:
        i += lil.shape[0]

    if i < 0 or i >= lil.shape[0]:
        raise IndexError('row index out of bounds')

    lil.rows = np.delete(lil.rows, i, 0)
    lil.data = np.delete(lil.data, i, 0)
    lil._shape = (lil._shape[0]-1, lil.shape[1])


class Units():
    Bohr = physical_constants["Bohr radius"][0]*1e10
    Ry = physical_constants["Rydberg constant times hc in eV"][0]

    bohr_to_a = physical_constants["Bohr radius"][0]*1e10
    a_to_bohr = 1/bohr_to_a
    a_to_meter = 1e-10
    ha_to_ev = physical_constants["hartree-electron volt relationship"][0]
    ev_to_ha = 1/ha_to_ev
    ha_to_ry = 2
    ry_to_ha = 1/ha_to_ry
    amu_to_me = physical_constants["atomic mass constant"][0]/m_e
    cm_to_mev = 1.23981e-1
    ry_to_ev = ha_to_ev/2

    ha_to_j = physical_constants["Hartree energy"][0]
    hbar_si = hbar
    c_si = c


def ensure_dir(f):
    d = os.path.abspath(f)
    if not os.path.exists(d):
        os.makedirs(d)
        log.debug("Created directory %s", d)


def memory():
    """
    Get node total memory and memory usage
    """
    with open('/proc/meminfo', 'r') as mem:
        ret = {}
        tmp = 0
        for i in mem:
            sline = i.split()
            if str(sline[0]) == 'MemTotal:':
                ret['total'] = int(sline[1])
            elif str(sline[0]) in ('MemFree:', 'Buffers:', 'Cached:'):
                tmp += int(sline[1])
        ret['free'] = tmp
        ret['used'] = int(ret['total']) - int(ret['free'])

        ret['free'] /= 1e3
        ret['used'] /= 1e3

    return ret


def integrate(x, y):
    """Integrating y(x) with trapecies method."""
    x, y = np.array(x), np.array(y)
    differences = x[1:] - x[:-1]
    trapecies = differences*(y[1:] + y[:-1])/2
    return trapecies.sum()


def normalize_result(x, y):
    y = np.array(y)
    y = y/np.sqrt(integrate(x, pow(y, 2)))
    return y


def get_transformation(a, b, direction):
    """Returns transformation function, which translates
    vector 'a' to [0, 0, 0] and then rotates vector 'b-a'
    to 'direction'."""
    a, b = np.array(a), np.array(b)

    trans = a - np.array([0, 0, 0])
    direction = direction/np.linalg.norm(direction)
    vec = (b - a)/np.linalg.norm(b - a)

    v = np.cross(vec, direction)
    s = np.linalg.norm(v)
    c = np.dot(vec, direction)

    vx = np.array([
        [0, -v[2], v[1]],
        [v[2], 0, -v[0]],
        [-v[1], v[0], 0]])

    R = np.eye(3) + vx + np.dot(vx, vx)*((1 - c)/(s**2))

    def foo(vec):
        vec = np.array(vec) - trans
        return R.dot(vec)

    return foo


def rotational_matrix(u=None, phi=2*np.pi/3):
    """Returns matrix which rotates vectors along
    axis u by angle phi."""
    if u is None:
        u = [1, 1, 1]

    u = np.array(u)
    u = u/np.sqrt(np.dot(u, u))

    matrix = (
        np.cos(phi)*np.eye(3) +
        np.sin(phi)*np.array([
            [0, -u[2], u[1]],
            [u[2], 0, -u[0]],
            [-u[1], u[0], 0]]) +
        (1 - np.cos(phi))*np.array([
            [u[0]**2, u[0]*u[1], u[0]*u[2]],
            [u[0]*u[1], u[1]**2, u[1]*u[2]],
            [u[0]*u[2], u[1]*u[2], u[2]**2]]))

    return np.round(np.array(matrix), 9)


def run_pwscf(infile, outfile, workdir, mpi=None):
    ensure_dir(workdir)

    pwx = os.path.join(config["DEFAULT"]["QEBINS"], "pw.x")
    if isinstance(mpi, int):
        pwx = "mpirun -np {}".format(mpi)

    log.info("Executing pw.x for {}".format(os.path.split(infile)[1]))

    subprocess.call(
        "{} -inp {} > {}".format(
            pwx, infile, os.path.join(workdir, outfile)),
        shell=True)


def gaussian_smoothing(x, y, sig=6, n=1000, xlim=None):
    def gaussian(x, mu, sig=sig):
        return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
            2 * np.power(sig, 2.)))

    x = np.array(x)
    y = np.array(y)

    if xlim is None:
        x_new = np.linspace(x.min(), x.max(), n)
    else:
        x_new = np.linspace(xlim[0], xlim[1], n)
    y_new = np.zeros(len(x_new))

    for _x, _y in zip(x, y):
        y_new += _y*gaussian(x_new, _x)

    return x_new, y_new


class gaussian_kde(object):
    """Representation of a kernel-density estimate using Gaussian kernels.

    Kernel density estimation is a way to estimate the probability density
    function (PDF) of a random variable in a non-parametric way.
    `gaussian_kde` works for both uni-variate and multi-variate data.   It
    includes automatic bandwidth determination.  The estimation works best for
    a unimodal distribution; bimodal or multi-modal distributions tend to be
    oversmoothed.

    Parameters
    ----------
    dataset : array_like
        Datapoints to estimate from. In case of univariate data this is a 1-D
        array, otherwise a 2-D array with shape (# of dims, # of data).
    bw_method : str, scalar or callable, optional
        The method used to calculate the estimator bandwidth.  This can be
        'scott', 'silverman', a scalar constant or a callable.  If a scalar,
        this will be used directly as `kde.factor`.  If a callable, it should
        take a `gaussian_kde` instance as only parameter and return a scalar.
        If None (default), 'scott' is used.  See Notes for more details.
    weights : array_like, shape (n, ), optional, default: None
        An array of weights, of the same shape as `x`.  Each value in `x`
        only contributes its associated weight towards the bin count
        (instead of 1).

    Attributes
    ----------
    dataset : ndarray
        The dataset with which `gaussian_kde` was initialized.
    d : int
        Number of dimensions.
    n : int
        Number of datapoints.
    neff : float
        Effective sample size using Kish's approximation.
    factor : float
        The bandwidth factor, obtained from `kde.covariance_factor`, with which
        the covariance matrix is multiplied.
    covariance : ndarray
        The covariance matrix of `dataset`, scaled by the calculated bandwidth
        (`kde.factor`).
    inv_cov : ndarray
        The inverse of `covariance`.

    Methods
    -------
    kde.evaluate(points) : ndarray
        Evaluate the estimated pdf on a provided set of points.
    kde(points) : ndarray
        Same as kde.evaluate(points)
    kde.pdf(points) : ndarray
        Alias for ``kde.evaluate(points)``.
    kde.set_bandwidth(bw_method='scott') : None
        Computes the bandwidth, i.e. the coefficient that multiplies the data
        covariance matrix to obtain the kernel covariance matrix.
        .. versionadded:: 0.11.0
    kde.covariance_factor : float
        Computes the coefficient (`kde.factor`) that multiplies the data
        covariance matrix to obtain the kernel covariance matrix.
        The default is `scotts_factor`.  A subclass can overwrite this method
        to provide a different method, or set it through a call to
        `kde.set_bandwidth`.

    Notes
    -----
    Bandwidth selection strongly influences the estimate obtained from the KDE
    (much more so than the actual shape of the kernel).  Bandwidth selection
    can be done by a "rule of thumb", by cross-validation, by "plug-in
    methods" or by other means; see [3]_, [4]_ for reviews.  `gaussian_kde`
    uses a rule of thumb, the default is Scott's Rule.

    Scott's Rule [1]_, implemented as `scotts_factor`, is::

        n**(-1./(d+4)),

    with ``n`` the number of data points and ``d`` the number of dimensions.
    Silverman's Rule [2]_, implemented as `silverman_factor`, is::

        (n * (d + 2) / 4.)**(-1. / (d + 4)).

    Good general descriptions of kernel density estimation can be found in [1]_
    and [2]_, the mathematics for this multi-dimensional implementation can be
    found in [1]_.

    References
    ----------
    .. [1] D.W. Scott, "Multivariate Density Estimation: Theory, Practice, and
           Visualization", John Wiley & Sons, New York, Chicester, 1992.
    .. [2] B.W. Silverman, "Density Estimation for Statistics and Data
           Analysis", Vol. 26, Monographs on Statistics and Applied Probability,
           Chapman and Hall, London, 1986.
    .. [3] B.A. Turlach, "Bandwidth Selection in Kernel Density Estimation: A
           Review", CORE and Institut de Statistique, Vol. 19, pp. 1-33, 1993.
    .. [4] D.M. Bashtannyk and R.J. Hyndman, "Bandwidth selection for kernel
           conditional density estimation", Computational Statistics & Data
           Analysis, Vol. 36, pp. 279-298, 2001.

    Examples
    --------
    Generate some random two-dimensional data:

    >>> from scipy import stats
    >>> def measure(n):
    >>>     "Measurement model, return two coupled measurements."
    >>>     m1 = np.random.normal(size=n)
    >>>     m2 = np.random.normal(scale=0.5, size=n)
    >>>     return m1+m2, m1-m2

    >>> m1, m2 = measure(2000)
    >>> xmin = m1.min()
    >>> xmax = m1.max()
    >>> ymin = m2.min()
    >>> ymax = m2.max()

    Perform a kernel density estimate on the data:

    >>> X, Y = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
    >>> positions = np.vstack([X.ravel(), Y.ravel()])
    >>> values = np.vstack([m1, m2])
    >>> kernel = stats.gaussian_kde(values)
    >>> Z = np.reshape(kernel(positions).T, X.shape)

    Plot the results:

    >>> import matplotlib.pyplot as plt
    >>> fig = plt.figure()
    >>> ax = fig.add_subplot(111)
    >>> ax.imshow(np.rot90(Z), cmap=plt.cm.gist_earth_r,
    ...           extent=[xmin, xmax, ymin, ymax])
    >>> ax.plot(m1, m2, 'k.', markersize=2)
    >>> ax.set_xlim([xmin, xmax])
    >>> ax.set_ylim([ymin, ymax])
    >>> plt.show()

    """
    def __init__(self, dataset, bw_method=None, weights=None):
        self.dataset = np.atleast_2d(dataset)
        if not self.dataset.size > 1:
            raise ValueError("`dataset` input should have multiple elements.")
        self.d, self.n = self.dataset.shape

        if weights is not None:
            self.weights = weights / np.sum(weights)
        else:
            self.weights = np.ones(self.n) / self.n

        # Compute the effective sample size
        # http://surveyanalysis.org/wiki/Design_Effects_and_Effective_Sample_Size#Kish.27s_approximate_formula_for_computing_effective_sample_size
        self.neff = 1.0 / np.sum(self.weights ** 2)

        self.set_bandwidth(bw_method=bw_method)

    def evaluate(self, points):
        """Evaluate the estimated pdf on a set of points.

        Parameters
        ----------
        points : (# of dimensions, # of points)-array
            Alternatively, a (# of dimensions,) vector can be passed in and
            treated as a single point.

        Returns
        -------
        values : (# of points,)-array
            The values at each point.

        Raises
        ------
        ValueError : if the dimensionality of the input points is different than
                     the dimensionality of the KDE.

        """
        points = np.atleast_2d(points)

        d, m = points.shape
        if d != self.d:
            if d == 1 and m == self.d:
                # points was passed in as a row vector
                points = np.reshape(points, (self.d, 1))
                m = 1
            else:
                msg = "points have dimension %s, dataset has dimension %s" % (d,
                    self.d)
                raise ValueError(msg)

        # compute the normalised residuals
        chi2 = cdist(points.T, self.dataset.T, 'mahalanobis', VI=self.inv_cov) ** 2
        # compute the pdf
        result = np.sum(np.exp(-.5 * chi2) * self.weights, axis=1) / self._norm_factor

        return result

    __call__ = evaluate

    def scotts_factor(self):
        return np.power(self.neff, -1./(self.d+4))

    def silverman_factor(self):
        return np.power(self.neff*(self.d+2.0)/4.0, -1./(self.d+4))

    #  Default method to calculate bandwidth, can be overwritten by subclass
    covariance_factor = scotts_factor

    def set_bandwidth(self, bw_method=None):
        """Compute the estimator bandwidth with given method.

        The new bandwidth calculated after a call to `set_bandwidth` is used
        for subsequent evaluations of the estimated density.

        Parameters
        ----------
        bw_method : str, scalar or callable, optional
            The method used to calculate the estimator bandwidth.  This can be
            'scott', 'silverman', a scalar constant or a callable.  If a
            scalar, this will be used directly as `kde.factor`.  If a callable,
            it should take a `gaussian_kde` instance as only parameter and
            return a scalar.  If None (default), nothing happens; the current
            `kde.covariance_factor` method is kept.

        Notes
        -----
        .. versionadded:: 0.11

        Examples
        --------
        >>> x1 = np.array([-7, -5, 1, 4, 5.])
        >>> kde = stats.gaussian_kde(x1)
        >>> xs = np.linspace(-10, 10, num=50)
        >>> y1 = kde(xs)
        >>> kde.set_bandwidth(bw_method='silverman')
        >>> y2 = kde(xs)
        >>> kde.set_bandwidth(bw_method=kde.factor / 3.)
        >>> y3 = kde(xs)

        >>> fig = plt.figure()
        >>> ax = fig.add_subplot(111)
        >>> ax.plot(x1, np.ones(x1.shape) / (4. * x1.size), 'bo',
        ...         label='Data points (rescaled)')
        >>> ax.plot(xs, y1, label='Scott (default)')
        >>> ax.plot(xs, y2, label='Silverman')
        >>> ax.plot(xs, y3, label='Const (1/3 * Silverman)')
        >>> ax.legend()
        >>> plt.show()

        """
        if bw_method is None:
            pass
        elif bw_method == 'scott':
            self.covariance_factor = self.scotts_factor
        elif bw_method == 'silverman':
            self.covariance_factor = self.silverman_factor
        elif np.isscalar(bw_method):
            self._bw_method = 'use constant'
            self.covariance_factor = lambda: bw_method
        elif callable(bw_method):
            self._bw_method = bw_method
            self.covariance_factor = lambda: self._bw_method(self)
        else:
            msg = "`bw_method` should be 'scott', 'silverman', a scalar " \
                  "or a callable."
            raise ValueError(msg)

        self._compute_covariance()

    def _compute_covariance(self):
        """Computes the covariance matrix for each Gaussian kernel using
        covariance_factor().
        """
        self.factor = self.covariance_factor()
        # Cache covariance and inverse covariance of the data
        if not hasattr(self, '_data_inv_cov'):
            # Compute the mean and residuals
            _mean = np.sum(self.weights * self.dataset, axis=1)
            _residual = (self.dataset - _mean[:, None])
            # Compute the biased covariance
            self._data_covariance = np.atleast_2d(np.dot(_residual * self.weights, _residual.T))
            # Correct for bias (http://en.wikipedia.org/wiki/Weighted_arithmetic_mean#Weighted_sample_covariance)
            self._data_covariance /= (1 - np.sum(self.weights ** 2))
            self._data_inv_cov = np.linalg.inv(self._data_covariance)

        self.covariance = self._data_covariance * self.factor**2
        self.inv_cov = self._data_inv_cov / self.factor**2
        self._norm_factor = np.sqrt(np.linalg.det(2*np.pi*self.covariance)) #* self.n


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    kde = gaussian_kde([1, 1, 2, 2, 2, 2, 3], weights=[0, 0, 1, 1, 1, 0, 0.2])
    x = [0, 1, 2, 3, 4]
    y = kde.evaluate(x)
    plt.plot(x, y)
    plt.savefig("test.pdf", dpi=200)

