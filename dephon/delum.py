"""This module contains routines for:

 * luminescence calculations

"""
import numpy as np
from deutils import gaussian_smoothing
from statsmodels.nonparametric.smoothers_lowess import lowess
from scipy import interpolate

DATA = "../diamond/vasp/experiment/expt-UCSB/8K-data.dat"


def filter_noise(x, y, frac=0.01, delta=0.003):
    filtered = lowess(y, x, is_sorted=True, delta=delta, frac=frac, it=0)
    return filtered[:, 0], filtered[:, 1]


def get_experimental_spectrum(data_filepath=None):
    if data_filepath is None:
        data_filepath = DATA
    freq, intensity = np.loadtxt(data_filepath, delimiter=" ").T

    indx = np.where(freq < 1.87)
    x, y = filter_noise(freq[indx], intensity[indx], frac=0.02)

    indx = np.where((freq < 1.92) & (freq > 1.87))
    x2, y2 = filter_noise(freq[indx], intensity[indx], frac=0.05)

    indx = np.where(freq >= 1.92)
    x3, y3 = filter_noise(freq[indx], intensity[indx], frac=0.003, delta=0)

    freq_smoothed = np.concatenate([x, x2, x3])
    intensity_smoothed = np.concatenate([y, y2, y3])
    indx = freq_smoothed.argsort()
    freq_smoothed = freq_smoothed[indx]
    intensity_smoothed = intensity_smoothed[indx]

    return {
        "direct": {
            "x": freq,
            "y": intensity
        },
        "smoothed": {
            "x": freq_smoothed,
            "y": intensity_smoothed
        }
    }


def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx


def normalised_experimental_spectrum(freq, intensity, cut=1.62):
    data = get_experimental_spectrum()
    efr = data["smoothed"]["x"]
    eint = data["smoothed"]["y"]

    indx = np.where(freq < 1.62)
    tailfreq = freq[indx]
    tailint = intensity[indx]
    factor = eint[find_nearest(efr, 1.62)]/tailint[-1]
    tailint *= factor

    idx = np.where(data["direct"]["x"] > tailfreq[-1])
    efr = np.concatenate((tailfreq, data["direct"]["x"][idx]))
    eint = np.concatenate((tailint, data["direct"]["y"][idx]))
    factor = np.trapz(eint, x=efr)
    return data["direct"]["x"], data["direct"]["y"]/factor


def get_hr_spectrum(filepath, sym=None, sig=4):
    """Read hr spectrum from nvXxXxX_vibrational_couplings.dat file

    Args:
        filepath (str): path of dat file
        sym (str): A1, A2 or Exy symmetry
    """

    dtypes = [
        ('freq', float),
        ('freq_mev', float),
        ('fc_displ', float),
        ('hr_factors', float),
        ('irreps', "U5")]

    data = np.genfromtxt(
        filepath,
        autostrip=True,
        dtype=dtypes,
        delimiter=",")

    if sym is not None:
        indx = np.where(data["irreps"] == sym)[0]
        assert len(indx) > 0, "None vibrations of sym type {} found".format(
            sym)
        freq = data["freq_mev"][indx]
        hr_factors = data["hr_factors"][indx]
    else:
        freq = data["freq_mev"]
        hr_factors = data["hr_factors"]

    if isinstance(sig, list):
        result = []
        for s in sig:
            x, y = gaussian_smoothing(freq, hr_factors, sig=s, n=10000)
            result.append([x, y])
        return result

    x, y = gaussian_smoothing(freq, hr_factors, sig=sig, n=10000)
    return x, y


def fourier_transform(x, y, lim=None, n=10000, inverse=False):
    print("Performing {} fourier transform...".format(
        "inverse" if inverse else "direct"))

    if lim is None:
        lim = [-10, 10]

    omega = np.linspace(lim[0], lim[1], n)
    f = np.zeros(omega.shape, dtype="complex")

    if not inverse:
        prefactor = 1/(2*np.pi)
        sign = 1
    else:
        prefactor = 1
        sign = -1

    for nr, w in enumerate(omega):
        if nr % 1000 == 0:
            print(nr, end="\r")
        val = prefactor*np.trapz(y*np.exp(sign*1j*w*x), x=x)
        f[nr] = val

    return omega, f


def calculate_luminescence_line(vcoupl_file, sym="A1", gamma=0.45, n=16000,
                                jt_data=None):
    print("Reading hr factors...", end=" ")
    [freq, sw], [freq2, sw2] = get_hr_spectrum(
        vcoupl_file, sig=[5, 2], sym=sym)
    print("ok")

    # --------------------------------------- #
    #    Larger smearing for freq < bound
    # --------------------------------------- #

    bound = 53.16
    indx1 = np.where(freq < bound)
    indx2 = np.where(freq2 >= bound)
    freq = np.concatenate((freq[indx1], freq2[indx2]))
    sw = np.concatenate((sw[indx1], sw2[indx2]))

    # --------------------------------------- #
    #         Transform to time domain
    # --------------------------------------- #

    dt = freq[1] - freq[0]
    lim = (np.pi/dt)*0.05
    t, s = fourier_transform(freq, sw, lim=[-lim, lim], n=n, inverse=True)

    # --------------------------------------- #
    #          Generating function
    # --------------------------------------- #

    hr_factor = np.trapz(sw, x=freq)
    G = np.exp(s - hr_factor)

    # ---------------------------------------- #
    #           Spectral Function
    # ---------------------------------------- #

    dt = t[1] - t[0]
    lim = (np.pi/dt)*0.3
    freq, intensity = fourier_transform(
        t, G*np.exp(-gamma*abs(t)), lim=[-lim, lim], n=n)
    intensity = np.flip(intensity, 0).real
    freq = freq/1000 + 1.945

    intensity /= np.trapz(intensity, x=freq)

    # import matplotlib.pyplot as plt
    # f, (ax1, ax2, ax3) = plt.subplots(3, 1)
    # ax2.plot(freq, intensity, label="init")

    if jt_data is not None:
        x = jt_data[0]
        y = jt_data[1]
        f0 = interpolate.interp1d(x, y, fill_value='extrapolate')
        x = np.linspace(min(x), max(x), 1000)
        y = f0(x)

        # ax1.plot(x, y)
        f = interpolate.interp1d(freq, intensity, fill_value='extrapolate')
        new_int = np.zeros(len(freq))
        for fr, stren in zip(x, y):
            new_int += stren*f(freq - fr)

        intensity = new_int

    # ax2.plot(freq, new_int/np.trapz(new_int, x=freq), label="New")
    # ax2.set_ylim(bottom=0)
    # ax2.legend()
    # q = np.convolve(intensity, y)
    # ax3.plot(q)

    # plt.show()

    #     y /= np.trapz(y, x=x)
    #     f = interpolate.interp1d(freq, intensity)
    #     f2 = interpolate.interp1d(x, y, fill_value='extrapolate')
    #     intensity = f(freq) + f2(freq)

    intensity /= np.trapz(intensity, x=freq)

    # ---------------------------------------- #
    #           Luminescence line
    # ---------------------------------------- #

    intensity *= freq**3
    intensity /= np.trapz(intensity, x=freq)

    return freq, intensity


def gaussian_smoothing(x, y, sig=6, n=10000, xlim=None):
    def gaussian(x, mu, sig=sig):
        return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
            2 * np.power(sig, 2.)))

    if xlim is not None:
        x_new = np.linspace(xlim[0], xlim[1], n)
    else:
        x_new = np.linspace(x.min(), x.max(), n)

    y_new = np.zeros(len(x_new))

    for _x, _y in zip(x, y):
        y_new += _y*gaussian(x_new, _x)

    return x_new, y_new


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    jt_x, jt_y = np.loadtxt("../diamond/couplings/lum.dat").T
    jt_x /= 1000

    my_dir = "../diamond/couplings/hse_nv4x4x4/asymmetric/from_displacements/"
    my_dir = "../diamond/couplings/hse_nv4x4x4/asymmetric/from_forces/"

    freq, intensity = calculate_luminescence_line(
        my_dir +
        # "nv4x4x4_vibrational_couplings.dat")
        "nv15x15x15_vibrational_couplings.dat", sym="A1",
        jt_data=[jt_x, jt_y])

    exx, exy = normalised_experimental_spectrum(freq, intensity)

    f, ax = plt.subplots(1, 1, figsize=(15, 10))
    ax2 = ax.twinx()

    ax.plot(exx, exy, label="experiment")
    ax.plot(freq, intensity, label="theory convoluted jt")

    freq, intensity = calculate_luminescence_line(
        my_dir +
        # "nv4x4x4_vibrational_couplings.dat")
        "nv15x15x15_vibrational_couplings.dat", sym="A1")

    ax.plot(freq, intensity, label="theory A1")

    freq, intensity = calculate_luminescence_line(
        my_dir +
        # "nv4x4x4_vibrational_couplings.dat")
        "nv15x15x15_vibrational_couplings.dat", sym=None)

    ax.plot(freq, intensity, label="theory Gali")

    ax.set_xlim([1.5, 2])
    ax.set_ylim([0, 5])

    ax2 = ax.twinx()
    ax2.plot(jt_x + 1.945, jt_y, "--", color="black", lw=3, alpha=0.5)
    ax2.set_ylim(0, 0.015)
    # ax2.set_ylim(bottom=0)
    ax.set_ylim(bottom=0)
    # ax.grid(True)
    ax.grid(True)
    ax.legend()

    plt.savefig("lum4x4x4(displ_jt).pdf")
    plt.clf()
    # plt.show()

