import numpy as np
import pprint
from itertools import product
import time
import logging
import cmath
from scipy.spatial.distance import cdist
from scipy.constants import physical_constants

from deutils import Units
from desym import Symmetry

amu_to_kg = physical_constants["unified atomic mass unit"][0]

log = logging.getLogger(__name__)

pp = pprint.PrettyPrinter()


def _make_key_from_float(val):
    val = "{:.2f}".format(val)
    if val == '-0.00':
        val = "0.00"
    return val


class Atoms():
    """Atoms class which stores all information about suppercell.

    Args:
        symbols (list): chemical symbols of atoms
        positions (list): positions given in Angstroms
        scaled_positions (list): positions given in cell parameters
        number (list): atomic numbers (will be converted to chemical symbols)
        cell (list): two dimensional array with unit vectors of supercell
    """

    def __init__(self,
                 symbols=None,
                 positions=None,
                 scaled_positions=None,
                 numbers=None,
                 masses=None,
                 magmoms=None,
                 base_numbers=None,
                 cell=None,
                 pbc=True,
                 calculate_symmetry=False):

        self._validated = False
        self.lengths = None
        self._positions = None

        # Symbols
        self.symbols = symbols
        if self.symbols is not None:
            self._symbols_to_numbers()

        # Atomic numbers
        if numbers is not None:
            self.numbers = np.array(numbers, dtype='intc')
            self._numbers_to_symbols()

        # Cell
        self.cell = None
        if cell is not None:
            self.set_cell(cell)

        # Positions
        self.scaled_positions = None
        if (scaled_positions is not None):
            self.set_scaled_positions(scaled_positions)
        if (self.cell is not None) and (positions is not None):
            self.set_positions(positions)
        self.sorted_positions = None

        self.forces = None
        self.set_masses(masses)
        self.set_magnetic_moments(magmoms)

        # symbol --> mass
        if self.symbols is not None and (self.masses is None):
            self._symbols_to_masses()

        # Symmetry
        self.symmetry = None
        self.maps = None

        self.cell_lattice_type = None
        if self.cell is not None and calculate_symmetry:
            self.get_cell_lattice_type()
            self.calculate_symmetry()

        self.base_numbers = None
        if base_numbers is not None:
            self.base_numbers = np.array(base_numbers)

        self.base_atoms = None

    def get_mass_center(self):
        positions = self.get_positions()
        masses = self.get_masses().repeat(3).reshape(len(self), 3)
        com = (positions*masses).sum(axis=0)/sum(masses)
        return com

    def _is_image(self, pos, pos2):
        sol = np.linalg.solve(
            self.symmetry.prim_lattice,
            pos - pos2)

        for s in sol:
            if -0.001 < s - round(s) < 0.001:
                return True

        return False

    def _find_new_base_atom(self, found):
        for i in np.sort(self.get_positions()):
            is_image = False
            for j in found:
                if self._is_image(i, j):
                    is_image = True

            if not is_image:
                return i

        raise RuntimeError("Could not find new base atom")

    def identify_different_base_atoms(self, prec=0.001,
                                      base_atom_positions=None):

        if self.symmetry is None:
            self.calculate_symmetry()

        self.base_numbers = np.ones(len(self), int)*-1

        if base_atom_positions is None:
            nr_of_base_atoms = len(self.symmetry.prim_scaled_positions)
            base_atom_positions = []

            while nr_of_base_atoms:
                base_atom_positions.append(
                    self._find_new_base_atom(base_atom_positions))
                nr_of_base_atoms -= 1

        self.base_atoms = []
        for nr, pos in enumerate(base_atom_positions):
            self.base_atoms.append(self.find_by_position(pos))

        for i in range(len(self)):
            print("Identifying base equivalent: {}/{}".format(i, len(self)),
                  end="\r")
            for nr, base in enumerate(self.base_atoms):
                sol = np.linalg.solve(
                    self.symmetry.prim_lattice,
                    self.get_positions()[i] - self.get_positions()[base])

                for s in sol:
                    if -prec < s - round(s) < prec:
                        self.base_numbers[i] = nr

    def get_cell_lattice_type(self):
        cell = self.get_cell()
        for i in range(3):
            cell[i][i] = 0

        if cell.sum() == 0:
            cell_type = "orthorhombic"
        else:
            cell_type = "nonorthorhombic"

        self.cell_lattice_type = cell_type
        return cell_type

    def delete_atoms(self, indexes):
        if isinstance(indexes, int):
            indexes = [indexes]

        self.symbols = np.delete(self.symbols, indexes, 0)
        self.numbers = np.delete(self.numbers, indexes, 0)
        self.masses = np.delete(self.masses, indexes, 0)
        self.scaled_positions = np.delete(self.scaled_positions, indexes, 0)
        self._positions = None

    def add_atom(self, symbol, position):
        self.set_symbols(np.append(self.symbols, symbol))
        self.set_positions(np.concatenate((self.get_positions(), [position])))
        self._symbols_to_numbers()
        self._symbols_to_masses()
        self._positions = None

    def find_by_position(self, pos,
                         err_message="Could not find atom in position ",
                         no_error=False, pbc=True, method=2,
                         prec=0.05):

        given_pos = pos[:]

        ###############################################
        #  Considering periodic boundary conditions
        ###############################################
        if pbc:
            for nr, coord in enumerate(pos[:]):
                # Negative coordinates
                if coord < 0:
                    pos[nr] = self.get_cell()[nr][nr] + coord

                # Outside boundaries
                if pos[nr] > self.get_cell()[nr][nr] or np.allclose(
                        pos[nr], self.get_cell()[nr][nr], 0.001):
                    pos[nr] -= self.get_cell()[nr][nr]

        if method == 1:
            for nr, i in enumerate(self.get_positions()):
                if np.allclose(pos, i, 0.0001, 0.0001):
                    return nr
        elif method == 2:
            # distance matrix
            distm = cdist(self.get_positions(),
                          np.tile([pos], (len(self), 1)),
                          "euclidean")
            indx = np.unravel_index(distm.argmin(), distm.shape)
            # print("???")
            # print(indx)
            # print(distm[indx])
            assert distm[indx] < prec, "Distance is too big:{}\n{}\n{}".format(
                distm[indx], pos, self.get_positions()[indx[0]])
            return int(indx[0])
        elif method == 3:
            try:
                return self.sorted_positions[
                    _make_key_from_float(pos[0])][
                        _make_key_from_float(pos[1])][
                            _make_key_from_float(pos[2])]
            except KeyError:
                print("I was searching for position", pos)
                print("Positions:\n")
                pp.pprint(list(self.get_positions()))
                print("KEYS:")
                pp.pprint(list(self.sorted_positions.keys()))
                print("POSITION:")
                print(round(pos[0], 3), round(pos[1], 3), round(pos[2], 3))
                raise

        if no_error:
            return None

        for i in self.get_positions():
            print(i)

        raise RuntimeError(
            err_message + "{} (periodic {})".format(given_pos, pos))

    ##############################
    #        Symmetries
    ##############################

    def calculate_symmetry(self, symprec=0.1):
        self.symmetry = Symmetry(self, symprec)
        return self.symmetry.report

    def get_rotation_maps(self):
        return self.symmetry.get_atom_transformations()

    def get_rotations(self):
        return self.symmetry.rotations

    ##############################
    #      Manipulations
    ##############################

    def transform(self, tansform_matrix):
        self.scaled_positions = tansform_matrix.dot(self.scaled_positions.T).T

    def _validate_cell(self):
        if self._validated:
            return

        ###########################
        #  Check if orthorhombic
        ###########################

        cell = self.get_cell()
        for i in range(3):
            cell[i][i] = 0

        if cell.sum() != 0:
            raise NotImplementedError(
                "Distance can be calculated only for orthorombic cells.")

        self.lengths = self.cell.sum(axis=0)
        self._validated = True

    def get_separation_vector(self, atom_nr1, atom_nr2, direct=False,
                              scaled=True, debug=False, prec=5,
                              return_position=False):

        """Calculates distance betweem two atoms or positions.
        pos(atom2) - pos(atom1)
        Atoms can be referenced by indexes or positions.
        If direct is True returns direct displacement vector and
        distance without considering periodic boundary conditions.
        Otherwise treats suppercell as periodic structure and searches
        for shortest distance and displacement.

        Returns:
            displt (float): displacement vector in Angstroms
            dist (float): displacement modulus in Agstroms
        """
        self._validate_cell()

        ###################################
        #  Calculate direct displacement
        ###################################

        pos = self.get_positions()

        if isinstance(atom_nr1, int):
            assert isinstance(atom_nr2, int)
            pos1 = pos[atom_nr1]
            pos2 = pos[atom_nr2]
        else:
            pos1 = atom_nr1
            pos2 = atom_nr2

        displ = pos2 - pos1

        if debug:
            print("Direct positions:", pos1, pos2)
            print("Direct displ:", displ)

        if direct:
            return displ, round(np.linalg.norm(displ), prec)

        new_pos2 = (pos2 +
                    (abs(displ) > self.lengths/2) *
                    (-np.sign(displ)) *
                    self.lengths)

        displ = new_pos2 - pos1
        if debug:
            print("New positions:", pos1, new_pos2)
            print("New displ:", displ)

        dist = np.sqrt((displ**2).sum())

        if return_position:
            return displ, round(dist, prec), new_pos2

        return displ, round(dist, prec)

    def iter_pairs(self):
        for i, j in product(range(len(self)), range(len(self))):
            yield i, j

    def sort_positions(self, prec=3):
        self.sorted_positions = {}

        positions = self.get_positions()

        for i in range(len(self)):
            pos = [_make_key_from_float(k) for k in positions[i]]

            x1 = self.sorted_positions.get(pos[0], {})
            x2 = x1.get(pos[1], {})

            x2[pos[2]] = i
            x1[pos[1]] = x2
            self.sorted_positions[pos[0]] = x1

    def sort_by_distance(self, prec=4):
        """Creates maps which will increase embeding procedure
        map_structure::

            maps[dist]["{}{}".format(atom1basenr, atom2basenr)] =
              [atom1indx, atom2indx, displ_vec, sym1, sym2]

        """
        if self.base_numbers is None:
            self.identify_different_base_atoms()

        beg = time.time()

        self.maps = {}
        _l = len(self)

        for nr, (i, j) in enumerate(self.iter_pairs()):

            if logging.getLogger().getEffectiveLevel() < logging.WARN:
                if not nr % 10000:
                    print("Sorting by distance {}/{} ({:.2f}\%)".format(
                        nr, _l**2, (100/_l**2)*nr), end="\r")

            v, dist = self.get_separation_vector(i, j)
            dist = ("{:." + str(prec) + "f}").format(dist)

            bases = self.maps.get(dist, {})
            base_id = (self.base_numbers[i], self.base_numbers[j])
            data = bases.get(base_id, [])
            data.append([i, j, v, self.get_symbol(i), self.get_symbol(j)])
            bases[base_id] = data
            self.maps[dist] = bases

        if logging.getLogger().getEffectiveLevel() < logging.WARN:
            print("Sorting by distance finished in {:.3f} s".format(
                time.time() - beg))

        return self.maps

    def _is_equivalent(self, my_atom1, my_atom2, atom1, atom2, atoms,
                       prec=0.001, debug=False,
                       direct_solution=False):
        """Checks if atom1 position can be reproduced from atom2
        using primitive translations.


        Used for bulk construction procedure.
        """

        if direct_solution:
            if self.symmetry is None:
                self.calculate_symmetry()

            pos = self.get_positions()
            pos2 = atoms.get_positions()

            sol = np.linalg.solve(self.symmetry.prim_lattice,
                                  pos[my_atom1] - pos2[atom1])
            for i in sol:
                if not -prec < i - round(i) < prec:
                    return False
        else:
            if self.base_numbers[my_atom1] != atoms.base_numbers[atom1]:
                return False
            if self.base_numbers[my_atom2] != atoms.base_numbers[atom2]:
                return False

        return True

    def find_equivalent(self, i, j, atoms, debug=False):

        return next(
            self.iter_equivalent(
                i, j, atoms, debug=debug, only_first=True))

    def iter_equivalent(self, i, j, atoms, debug=False,
                        only_first=False):
        """Finds equivalent pair of atoms, given other atoms object and atom
        indexes.

        Algorithm:
           - find separation vector considering periodic boundary conditions
           - iter through pairs with same separation and atomic symbols
           - check if pair in 'atoms' object can be reproduced by lattice
             displacement vectors

        Args:
            i (int): id number of first atom inside given atoms object
            j (int): id number of second atom inside given atoms object
            atoms (Atoms): refered atoms object
        """

        v, dist = atoms.get_separation_vector(i, j)
        # s1, s2 = atoms.get_symbol(i), atoms.get_symbol(j)

        data = {
            "i": None,
            "j": None,
            "dist": dist,
            "sep": v
        }

        if self.maps is None:
            self.sort_by_distance()

        spos = atoms.get_scaled_positions()
        pos = atoms.get_positions()
        dist = "{:.4f}".format(dist)
        base_indx = (atoms.base_numbers[i], atoms.base_numbers[j])

        if dist not in self.maps or base_indx not in self.maps[dist]:
            raise RuntimeError(
                "No equivalent atoms found!\n" +
                "Were looking for atom pair in " +
                "{} cell given {} atoms cell".format(len(self), len(atoms)) +
                "\npos1:{}\t{}\npos2:{}\t{}\n".format(
                    pos[i], spos[i], pos[j], spos[j]) +
                "Max(cell1)/Max(cell2): {}\n".format(
                    self.get_cell().max()/atoms.get_cell().max()) +
                "Dist (sep vector): {} ({})\n".format(dist, v) +
                "\nMap keys:\n {}".format(pp.pformat(list(self.maps.keys()))) +
                ("\nBase keys: \n{}".format(pp.pformat(self.maps[dist])) if
                 dist in self.maps else "")
            )

        if debug:
            print("="*70)
            print(self.get_cell())
            print("Positions:", pos[i], pos[j])
            print("Basis:", atoms.base_numbers[i], atoms.base_numbers[j])
            print("Displacement:", dist, v)

        for map_i, map_j, vec, map_s1, map_s2 in self.maps[dist][base_indx]:
            if debug:
                pos2 = self.get_positions()
                if np.allclose([pos[i], pos[j]], [pos2[map_i], pos2[map_j]]):
                    print("-"*70)
                    print(atoms.get_cell())
                    print("Positions:", pos2[map_i], pos2[map_j])
                    print("Basis:", self.base_numbers[map_i],
                          self.base_numbers[map_j])
                    print("Displacement:", vec)
                    input()

            # # Same symbols
            # if self._is_equivalent(map_i, map_j, i, j, atoms, debug=debug):

            # Same displacement vectors
            if (cmath.isclose(v[0], vec[0], rel_tol=1e-4) and
                    cmath.isclose(v[1], vec[1], rel_tol=1e-4) and
                    cmath.isclose(v[2], vec[2], rel_tol=1e-4)):

                data["i"], data["j"] = map_i, map_j
                yield data
                if only_first:
                    raise StopIteration("!!!{}".format(data))

        print("Atoms:", i, j)
        print("Positions:", pos[i], pos[j])
        print("Scaled positions:", spos[i], spos[j])
        print("Displacement:", v, dist)
        if not debug:
            self.find_equivalent(i, j, atoms, debug=True)
        pp.pprint(self.maps.keys())

    def set_cell(self, cell):
        self.cell = np.array(cell, dtype='double', order='C')
        self._positions = None

    def get_cell(self, units="A"):
        if units.lower() in {"bohr"}:
            return self.cell.copy()/Units.bohr_to_a
        return self.cell.copy()

    def set_positions(self, cart_positions):
        self.scaled_positions = np.array(
            np.dot(cart_positions, np.linalg.inv(self.cell)),
            dtype='double', order='C')
        self._positions = None

    def get_positions(self, units="angstrom"):
        """Positions in angstroms"""
        if self._positions is None:
            self._positions = np.dot(self.scaled_positions, self.cell)

        if units.lower() in {"angstrom", "a"}:
            return self._positions
        elif units.lower() in {"au", "bohr", "bohr radius"}:
            return self._positions*Units.a_to_bohr
        elif units.lower() in {"si", "m"}:
            return self._positions*Units.a_to_meter

        raise NotImplemented("Unrecognized units {}".format(units))

    def set_scaled_positions(self, scaled_positions):
        self._positions = None
        self.scaled_positions = np.array(scaled_positions,
                                         dtype='double', order='C')

    def get_scaled_positions(self):
        return self.scaled_positions.copy()

    def set_masses(self, masses):
        if masses is None:
            self.masses = None
        else:
            self.masses = np.array(masses, dtype='double')

    def get_masses(self, units="amu"):
        if self.masses is None:
            return None

        if units == "amu":
            return self.masses.copy()

        elif units.lower() in {r"me", r"m_e", r"m_{e}", r"au"}:
            return self.masses.copy()*Units.amu_to_me

        elif units == "kg":
            return self.masses.copy()*amu_to_kg

        else:
            raise NotImplementedError("Unknown units:", units)

    def set_magnetic_moments(self, magmoms):
        if magmoms is None:
            self.magmoms = None
        else:
            self.magmoms = np.array(magmoms, dtype='double')

    def get_magnetic_moments(self):
        if self.magmoms is None:
            return None
        else:
            return self.magmoms.copy()

    def set_chemical_symbols(self, symbols):
        self.symbols = symbols
        self._symbols_to_numbers()
        self._symbols_to_masses()

    def get_chemical_symbols(self):
        return self.symbols[:]

    get_symbols = get_chemical_symbols
    set_symbols = set_chemical_symbols

    def get_symbol(self, atom_nr):
        return self.symbols[atom_nr]

    def get_number_of_atoms(self):
        return len(self.scaled_positions)

    def get_atomic_numbers(self):
        self._symbols_to_numbers()
        return self.numbers.copy()

    def get_volume(self):
        return np.linalg.det(self.cell)

    def copy(self):
        return Atoms(cell=self.cell,
                     scaled_positions=self.scaled_positions,
                     masses=self.masses,
                     magmoms=self.magmoms,
                     symbols=self.symbols,
                     pbc=True)

    def set_forces(self, forces, pwscf=False):
        """Sets forces in eV/A.
        If pwscf is True in Ry/Bohr
        """
        self.forces = np.array(forces)
        if pwscf:
            self.forces *= Units.Ry / Units.Bohr

    def get_forces(self, atomic=False):
        if atomic:
            return self.forces * Units.Bohr / Units.Ry

        return self.forces

    def set_energy(self, energy):
        self.energy = energy

    def _numbers_to_symbols(self):
        self.symbols = [atom_data[n][1] for n in self.numbers]

    def _symbols_to_numbers(self):
        self.numbers = np.array([symbol_map[s]
                                 for s in self.symbols], dtype='intc')

    def _symbols_to_masses(self):
        masses = [atom_data[symbol_map[s]][3] for s in self.symbols]
        if None in masses:
            self.masses = None
        else:
            self.masses = np.array(masses, dtype='double')

    ##############################
    #        Manipulation
    ##############################

    def iterate(self, independent=False):
        """Iterates through atoms, yielding dictionary:

        .. code-block:: python

            {
                "symbol": self.symbols[i],
                "mass": self.masses[i],
                "index": i,
                "position": positions[i]
            }

        If independent is True iterates only through unique atoms with
        respect to crystall symmetry.
        """

        positions = self.get_positions()  # Angstrom

        iterator = range(len(self))
        if independent:
            iterator = self.get_independent_atoms()

        for i in iterator:
            yield {
                "symbol": self.symbols[i],
                "mass": self.masses[i],
                "index": i,
                "position": positions[i],
                "scaled_position": self.scaled_positions[i],
                "number": self.numbers[i]
            }

    def __len__(self):
        return len(self.symbols)

    def __iter__(self):
        return iter(range(len(self)))

    def change_position(self, nr, vec, scaled=False):
        if not scaled:
            vec = np.array(
                np.dot(vec, np.linalg.inv(self.cell)),
                dtype='double', order='C')

        self.scaled_positions[nr] = vec

    def __str__(self):
        text = "-"*50
        text += "\nCELL_PARAMETERS angstrom\n"
        for i in self.cell:
            text += "{:6f}  {:6f}  {:6f}\n".format(*i)

        if self.forces is not None:
            text += "\nCRYSTAL_POSITIONS crystal\n"
            for name, i, f in zip(self.symbols, self.get_scaled_positions(),
                                  self.forces):
                text += "{} {:6f}  {:6f}  {:6f}".format(name, *i)
                text += "   [{:6f}  {:6f}  {:6f}]\n".format(*f)
        else:
            text += "\nCRYSTAL_POSITIONS crystal\n"
            for name, i in zip(self.symbols, self.get_scaled_positions()):
                text += "{} {:6f}  {:6f}  {:6f}\n".format(name, *i)

        text += "-"*50
        return text

    def create_xyz_content(self, forces=None):
        nr = 0
        content = ""

        for nr, atom in enumerate(self.iterate()):
            name = atom["symbol"]
            pos = atom["position"]

            atom_line = name + ' '.join('%15.9f' % F for F in pos)

            if forces is not None:
                atom_line += ' '.join('%15.9f' % F for F in forces[nr])

            content += atom_line + "\n"

        head = "	{}\n\"{}, cubic\"\n".format(
            nr + 1, "Unknown")

        return head + content


atom_data = [
    [0, "X", "X", 0],
    [1, "H", "Hydrogen", 1.00794],
    [2, "He", "Helium", 4.002602],
    [3, "Li", "Lithium", 6.941],
    [4, "Be", "Beryllium", 9.012182],
    [5, "B", "Boron", 10.811],
    [6, "C", "Carbon", 12.0107],
    [7, "N", "Nitrogen", 14.0067],
    [8, "O", "Oxygen", 15.9994],
    [9, "F", "Fluorine", 18.9984032],
    [10, "Ne", "Neon", 20.1797],
    [11, "Na", "Sodium", 22.98976928],
    [12, "Mg", "Magnesium", 24.3050],
    [13, "Al", "Aluminium", 26.9815386],
    [14, "Si", "Silicon", 28.0855],
    [15, "P", "Phosphorus", 30.973762],
    [16, "S", "Sulfur", 32.065],
    [17, "Cl", "Chlorine", 35.453],
    [18, "Ar", "Argon", 39.948],
    [19, "K", "Potassium", 39.0983],
    [20, "Ca", "Calcium", 40.078],
    [21, "Sc", "Scandium", 44.955912],
    [22, "Ti", "Titanium", 47.867],
    [23, "V", "Vanadium", 50.9415],
    [24, "Cr", "Chromium", 51.9961],
    [25, "Mn", "Manganese", 54.938045],
    [26, "Fe", "Iron", 55.845],
    [27, "Co", "Cobalt", 58.933195],
    [28, "Ni", "Nickel", 58.6934],
    [29, "Cu", "Copper", 63.546],
    [30, "Zn", "Zinc", 65.38],
    [31, "Ga", "Gallium", 69.723],
    [32, "Ge", "Germanium", 72.64],
    [33, "As", "Arsenic", 74.92160],
    [34, "Se", "Selenium", 78.96],
    [35, "Br", "Bromine", 79.904],
    [36, "Kr", "Krypton", 83.798],
    [37, "Rb", "Rubidium", 85.4678],
    [38, "Sr", "Strontium", 87.62],
    [39, "Y", "Yttrium", 88.90585],
    [40, "Zr", "Zirconium", 91.224],
    [41, "Nb", "Niobium", 92.90638],
    [42, "Mo", "Molybdenum", 95.96],
    [43, "Tc", "Technetium", None],
    [44, "Ru", "Ruthenium", 101.07],
    [45, "Rh", "Rhodium", 102.90550],
    [46, "Pd", "Palladium", 106.42],
    [47, "Ag", "Silver", 107.8682],
    [48, "Cd", "Cadmium", 112.411],
    [49, "In", "Indium", 114.818],
    [50, "Sn", "Tin", 118.710],
    [51, "Sb", "Antimony", 121.760],
    [52, "Te", "Tellurium", 127.60],
    [53, "I", "Iodine", 126.90447],
    [54, "Xe", "Xenon", 131.293],
    [55, "Cs", "Caesium", 132.9054519],
    [56, "Ba", "Barium", 137.327],
    [57, "La", "Lanthanum", 138.90547],
    [58, "Ce", "Cerium", 140.116],
    [59, "Pr", "Praseodymium", 140.90765],
    [60, "Nd", "Neodymium", 144.242],
    [61, "Pm", "Promethium", None],
    [62, "Sm", "Samarium", 150.36],
    [63, "Eu", "Europium", 151.964],
    [64, "Gd", "Gadolinium", 157.25],
    [65, "Tb", "Terbium", 158.92535],
    [66, "Dy", "Dysprosium", 162.500],
    [67, "Ho", "Holmium", 164.93032],
    [68, "Er", "Erbium", 167.259],
    [69, "Tm", "Thulium", 168.93421],
    [70, "Yb", "Ytterbium", 173.054],
    [71, "Lu", "Lutetium", 174.9668],
    [72, "Hf", "Hafnium", 178.49],
    [73, "Ta", "Tantalum", 180.94788],
    [74, "W", "Tungsten", 183.84],
    [75, "Re", "Rhenium", 186.207],
    [76, "Os", "Osmium", 190.23],
    [77, "Ir", "Iridium", 192.217],
    [78, "Pt", "Platinum", 195.084],
    [79, "Au", "Gold", 196.966569],
    [80, "Hg", "Mercury", 200.59],
    [81, "Tl", "Thallium", 204.3833],
    [82, "Pb", "Lead", 207.2],
    [83, "Bi", "Bismuth", 208.98040],
    [84, "Po", "Polonium", None],
    [85, "At", "Astatine", None],
    [86, "Rn", "Radon", None],
    [87, "Fr", "Francium", None],
    [88, "Ra", "Radium", None],
    [89, "Ac", "Actinium", None],
    [90, "Th", "Thorium", 232.03806],
    [91, "Pa", "Protactinium", 231.03588],
    [92, "U", "Uranium", 238.02891],
    [93, "Np", "Neptunium", None],
    [94, "Pu", "Plutonium", None],
    [95, "Am", "Americium", None],
    [96, "Cm", "Curium", None],
    [97, "Bk", "Berkelium", None],
    [98, "Cf", "Californium", None],
    [99, "Es", "Einsteinium", None],
    [100, "Fm", "Fermium", None],
    [101, "Md", "Mendelevium", None],
    [102, "No", "Nobelium", None],
    [103, "Lr", "Lawrencium", None],
    [104, "Rf", "Rutherfordium", None],
    [105, "Db", "Dubnium", None],
    [106, "Sg", "Seaborgium", None],
    [107, "Bh", "Bohrium", None],
    [108, "Hs", "Hassium", None],
    [109, "Mt", "Meitnerium", None],
    [110, "Ds", "Darmstadtium", None],
    [111, "Rg", "Roentgenium", None],
    [112, "Cn", "Copernicium", None],
    [113, "Uut", "Ununtrium", None],
    [114, "Uuq", "Ununquadium", None],
    [115, "Uup", "Ununpentium", None],
    [116, "Uuh", "Ununhexium", None],
    [117, "Uus", "Ununseptium", None],
    [118, "Uuo", "Ununoctium", None],
    [119, "C13", "Carbon13", 13.003355]
]

symbol_map = {
    "X": 0,
    "H": 1,
    "He": 2,
    "Li": 3,
    "Be": 4,
    "B": 5,
    "C": 6,
    "C13": 119,
    "N": 7,
    "O": 8,
    "F": 9,
    "Ne": 10,
    "Na": 11,
    "Mg": 12,
    "Al": 13,
    "Si": 14,
    "P": 15,
    "S": 16,
    "Cl": 17,
    "Ar": 18,
    "K": 19,
    "Ca": 20,
    "Sc": 21,
    "Ti": 22,
    "V": 23,
    "Cr": 24,
    "Mn": 25,
    "Fe": 26,
    "Co": 27,
    "Ni": 28,
    "Cu": 29,
    "Zn": 30,
    "Ga": 31,
    "Ge": 32,
    "As": 33,
    "Se": 34,
    "Br": 35,
    "Kr": 36,
    "Rb": 37,
    "Sr": 38,
    "Y": 39,
    "Zr": 40,
    "Nb": 41,
    "Mo": 42,
    "Tc": 43,
    "Ru": 44,
    "Rh": 45,
    "Pd": 46,
    "Ag": 47,
    "Cd": 48,
    "In": 49,
    "Sn": 50,
    "Sb": 51,
    "Te": 52,
    "I": 53,
    "Xe": 54,
    "Cs": 55,
    "Ba": 56,
    "La": 57,
    "Ce": 58,
    "Pr": 59,
    "Nd": 60,
    "Pm": 61,
    "Sm": 62,
    "Eu": 63,
    "Gd": 64,
    "Tb": 65,
    "Dy": 66,
    "Ho": 67,
    "Er": 68,
    "Tm": 69,
    "Yb": 70,
    "Lu": 71,
    "Hf": 72,
    "Ta": 73,
    "W": 74,
    "Re": 75,
    "Os": 76,
    "Ir": 77,
    "Pt": 78,
    "Au": 79,
    "Hg": 80,
    "Tl": 81,
    "Pb": 82,
    "Bi": 83,
    "Po": 84,
    "At": 85,
    "Rn": 86,
    "Fr": 87,
    "Ra": 88,
    "Ac": 89,
    "Th": 90,
    "Pa": 91,
    "U": 92,
    "Np": 93,
    "Pu": 94,
    "Am": 95,
    "Cm": 96,
    "Bk": 97,
    "Cf": 98,
    "Es": 99,
    "Fm": 100,
    "Md": 101,
    "No": 102,
    "Lr": 103,
    "Rf": 104,
    "Db": 105,
    "Sg": 106,
    "Bh": 107,
    "Hs": 108,
    "Mt": 109,
    "Ds": 110,
    "Rg": 111,
    "Cn": 112,
    "Uut": 113,
    "Uuq": 114,
    "Uup": 115,
    "Uuh": 116,
    "Uus": 117,
    "Uuo": 118,
}


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)
    cell = np.eye(3) * 100
    # cell[0][0] = 2
    atoms = Atoms(symbols=["C"]*8,
                  scaled_positions=[
                      [0.00, 0.00, 0.00],
                      [0.50, 0.50, 0.00],
                      [0.00, 0.50, 0.50],
                      [0.50, 0.00, 0.50],
                      [0.25, 0.25, 0.25],
                      [0.75, 0.75, 0.25],
                      [0.75, 0.25, 0.75],
                      [0.25, 0.75, 0.75]],
                  cell=cell)

    atoms.sort_by_distance()

    # pp.pprint(atoms.symmetry.dataset)
