import numpy as np
import h5py
from scipy import sparse

shape = 5
matrix = np.zeros((shape, shape))

for index, x in np.ndenumerate(matrix):
    matrix[index] = (index[0] + 1) * 10 + index[1] + 1

matrix = sparse.csr_matrix(matrix)
print("Writing matrix to hdf5 file")
print(matrix)

# f = h5py.File("matrix.hdf5", "w")
# dset = f.create_dataset("whessian", (5, 5), dtype='d', data=matrix)
# dset = f.create_dataset("inv_sq_mass_mtx", (5, 5), dtype='d', data=matrix)
# f.close()

# matrix *= 2

# fl = h5py.File('output.hdf5', 'r')
# dataset = fl["eigenvectors"]
# dataset.read_direct(matrix)
# print()
# # print(matrix)
# fl.close()


# evals, evecs = np.linalg.eigh(matrix)
# for i, j in zip(evals, evecs.T):
#     print(i, j)
