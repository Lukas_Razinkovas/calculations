!
! number of processes is assumed to be 4
!

program dataset_by_pattern

    use hdf5 ! this module contains all necessary modules 

    implicit none

    include 'mpif.h'
    character(len=10), parameter :: filename = "sds_pat.h5"  ! file name
    character(len=8), parameter :: dsetname = "intarray" ! dataset name

    integer(hid_t) :: file_id       ! file identifier 
    integer(hid_t) :: dset_id       ! dataset identifier 
    integer(hid_t) :: filespace     ! dataspace identifier in file 
    integer(hid_t) :: memspace      ! dataspace identifier in memory
    integer(hid_t) :: plist_id      ! property list identifier 

    integer(hsize_t), dimension(2) :: dimsf = (/4,8/) ! dataset dimensions
    ! in the file.
    !     integer, dimension(7) :: dimsfi = (/4,8,0,0,0,0,0/) 
    integer(hsize_t), dimension(2) :: dimsfi = (/4,8/)
    integer(hsize_t), dimension(1) :: dimsm = (/8/) ! dataset dimensions
    ! in memory.

    integer(hsize_t), dimension(2) :: count  
    integer(hssize_t), dimension(2) :: offset 
    integer(hsize_t), dimension(2) :: stride
    integer, allocatable :: data (:)  ! data to write
    integer :: rank = 2 ! dataset rank 
    integer :: rank1 = 1 ! memory dataset rank 

    integer :: error, error_n  ! error flags
    !
    ! mpi definitions and calls.
    !
    integer :: mpierror       ! mpi error flag
    integer :: comm, info
    integer :: mpi_size, mpi_rank

    comm = mpi_comm_world
    info = mpi_info_null

    call mpi_init(mpierror)
    call mpi_comm_size(comm, mpi_size, mpierror)
    call mpi_comm_rank(comm, mpi_rank, mpierror) 
    ! quit if mpi_size is not 4 
    if (mpi_size .ne. 4) then
        write(*,*) 'this example is set up to use only 4 processes'
        write(*,*) 'quitting....'
        goto 100
    endif

    !
    ! initialize hdf5 library and fortran interfaces.
    !
    call h5open_f(error) 

    ! 
    ! setup file access property list with parallel i/o access.
    !
    call h5pcreate_f(h5p_file_access_f, plist_id, error)
    call h5pset_fapl_mpio_f(plist_id, comm, info, error)

    !
    ! create the file collectively.
    ! 
    call h5fcreate_f(filename, h5f_acc_trunc_f, file_id, error, access_prp = plist_id)
    call h5pclose_f(plist_id, error)
    
    !
    ! create the data space for the  dataset. 
    !
    call h5screate_simple_f(rank, dimsf, filespace, error)
    call h5screate_simple_f(rank1, dimsm, memspace, error)

    !
    ! create the dataset with default properties.
    !
    call h5dcreate_f(file_id, dsetname, h5t_native_integer, filespace, &
            dset_id, error)
    call h5sclose_f(filespace, error)
    !
    ! each process defines dataset in memory and writes it to the hyperslab
    ! in the file. 
    !
    stride(1) = 2
    stride(2) = 2 
    count(1) =  2 
    count(2) =  4 
    if (mpi_rank .eq. 0) then
        offset(1) = 0
        offset(2) = 0
    endif
    if (mpi_rank .eq. 1) then
        offset(1) = 0
        offset(2) = 1 
    endif
    if (mpi_rank .eq. 2) then
        offset(1) = 1 
        offset(2) = 0
    endif
    if (mpi_rank .eq. 3) then
        offset(1) = 1 
        offset(2) = 1
    endif
    ! 
    ! select hyperslab in the file.
    !
    call h5dget_space_f(dset_id, filespace, error)
    call h5sselect_hyperslab_f (filespace, h5s_select_set_f, offset, count, error, &
            stride)
    ! 
    ! initialize data buffer with trivial data.
    !
    allocate (data(dimsm(1)))
    data = mpi_rank + 1
    !
    ! create property list for collective dataset write
    !
    call h5pcreate_f(h5p_dataset_xfer_f, plist_id, error) 
    call h5pset_dxpl_mpio_f(plist_id, h5fd_mpio_collective_f, error)

    !
    ! write the dataset collectively. 
    !
    call h5dwrite_f(dset_id, h5t_native_integer, data, dimsfi, error, &
            file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
    !
    ! write the dataset independently. 
    !
    !    call h5dwrite_f(dset_id, h5t_native_integer, data, dimsfi, error, &
    !                     file_space_id = filespace, mem_space_id = memspace)
    !
    ! deallocate data buffer.
    !
    deallocate(data)

    !
    ! close dataspaces.
    !
    call h5sclose_f(filespace, error)
    call h5sclose_f(memspace, error)

    !
    ! close the dataset and property list.
    !
    call h5dclose_f(dset_id, error)
    call h5pclose_f(plist_id, error)

    !
    ! close the file.
    !
    call h5fclose_f(file_id, error)

    !
    ! close fortran interfaces and hdf5 library.
    !
    call h5close_f(error)

100 continue
    call mpi_finalize(mpierror)

end program dataset_by_pattern
