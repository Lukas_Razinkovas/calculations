!============================================
!         Subroutines used by eigh
!============================================

module EighUtils
    use mpi
    integer, parameter:: dp=kind(0.d0)    ! double precision

contains
    !--------------------------
    !   Grid initialisation
    !--------------------------

    subroutine Init_Blacs_Grid(context)
        ! Initialises process grid for MPI Blacs/Scalapack parallel environment

        integer, intent(out) :: context

        integer :: mpi_grid_rows
        integer :: mpi_grid_cols
        integer :: nprocs
        integer :: myrank
        integer :: ierr
        integer :: dims(2)

        call MPI_Init(ierr)
        call MPI_Comm_Size(MPI_COMM_WORLD, nprocs, ierr)
        call MPI_Comm_Rank(MPI_COMM_WORLD, myrank, ierr)

        ! Creates optimal 2D cartesian grid for available proceses
        dims = 0 ! Indicates that MPI_Dims_create should fill in a suitable value.
        call MPI_Dims_Create(nprocs, 2, dims, ierr)
        mpi_grid_rows = dims(1)
        mpi_grid_cols = dims(2)
        call Blacs_Get(-1, 0, context)
        call Blacs_Gridinit(context, 'Row-major', mpi_grid_rows, mpi_grid_cols)

        if (myrank == 0) print '(A,I0,A,I0)', '* Initialised MPI Blacs grid ', &
                & mpi_grid_rows, "x", mpi_grid_cols

        return
    end subroutine Init_Blacs_Grid

    !-------------------------------
    !   Matrix reading and writing
    !-------------------------------

    subroutine Read_And_Distribute_Mtx_Matrix(filepath, context, local_matrix, &
            & matrix_descriptor, evect_descriptor, order, block_size)
        ! Reads ``.mtx`` file created with ``scipy.io.mmwrite`` and distributes
        ! it with block cyclic distribution

        character(len=20), intent(in) :: filepath     ! Path to ``.mtx`` file
        integer, intent(in) :: context                ! Path to ``.mtx`` file        
        integer, intent(out) :: matrix_descriptor(9)  ! Diagonalisable matrix descriptor
        integer, intent(out) :: evect_descriptor(9)   ! Results matrix descriptor
        integer, intent(out) :: order                 ! Order of read matrix
        integer, intent(out) :: block_size            ! Matrix division block size
        real(dp), allocatable, intent(out) :: &
                & local_matrix(:,:) ! Local matrix witch is different for each mpi process

        ! Block Cyclic distribution variables
        integer :: local_rows, local_cols ! Dimensions of local matrix
        integer :: leading_dimmension     ! ???

        ! MPI variables
        integer :: myrank, ierr

        ! Global matrix variables
        integer :: col, row       ! Col and row indexes
        real(dp) :: val           ! Val of a matrix
        integer :: nnz            ! Number of nonzero elements matrix
        character(len=90) :: cc   ! For comment reading
        integer :: i, c           ! Iteration variables

        ! Function for local matrix dimensions calculation
        integer :: Numroc 

        block_size = 64
        !------------------------
        !  Count comment lines
        !------------------------

        open(10, file=trim(filepath), status='old')
        cc = '%'
        c = -1
        do while(cc(1:1) == '%')
            c = c + 1
            read(10,'(A)') cc
        enddo
        close(10)

        !---------------------------
        !     Read direct data
        !---------------------------

        open(10, file=trim(filepath), status='old')

        ! Ignore comments
        do i=1,c
            read(10,'(A1)') cc
        enddo

        ! Read matrix specifications
        read(10,*) order, order, nnz

        ! Get MPI process info
        call MPI_Comm_Rank(MPI_COMM_WORLD, myrank, ierr)
        call Blacs_Gridinfo(context, mpi_grid_rows, mpi_grid_cols, &
                & my_grid_row, my_grid_col)

        ! Compute the number of local rows or columns of a distributed matrix
        local_rows = Numroc(order, block_size, my_grid_row, 0, mpi_grid_rows)
        local_cols = Numroc(order, block_size, my_grid_col, 0, mpi_grid_cols)
        
        ! Allocate matrix
        allocate(local_matrix(local_rows, local_cols))
        local_matrix = 0.0d+0 ! Because matrix will be sparse
        
        ! Initialise descriptor which describes block cyclic distributed matrix
        leading_dimmension = local_rows
        call Descinit(matrix_descriptor, order, order, block_size, block_size, &
                & 0, 0, context, leading_dimmension, info)
        call Descinit(evect_descriptor, order, order, block_size, block_size, &
                & 0, 0, context, leading_dimmension, info)
        
        ! Read and distribute values (Pdelset takes care of everything)
        do i = 1, nnz
            read(10,*) row, col, val
            ! call Pdelset(local_matrix, row, col, matrix_descriptor, val)
            call Pdelset(local_matrix, col, row, matrix_descriptor, val)            
        end do
        
        close(10)
        return
    end subroutine Read_And_Distribute_Mtx_Matrix

    !---------------------------
    !   From new scalapack
    !---------------------------

    SUBROUTINE PDLAWRITE(Filenam,M,N,A,Ia,Ja,Desca,Irwrit,Icwrit,Work)
        IMPLICIT NONE
        !*--PDLAWRITE4
        !
        !  -- ScaLAPACK auxiliary routine (version 2.0) --
        !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
        !     and University of California, Berkeley.
        !     August 12, 2001
        !
        !     .. Scalar Arguments ..
        CHARACTER*(*) Filenam
        INTEGER Ia , Icwrit , Irwrit , Ja , M , N
        !     ..
        !     .. Array Arguments ..
        INTEGER Desca(*)
        DOUBLE PRECISION A(*) , Work(*)
        !     ..
        !
        ! Purpose
        ! =======
        !
        ! PDLAWRITE writes to a file named FILNAMa distributed matrix sub( A )
        ! denoting A(IA:IA+M-1,JA:JA+N-1). The local pieces are sent to and
        ! written by the process of coordinates (IRWWRITE, ICWRIT).
        !
        ! WORK must be of size >= MB_ = DESCA( MB_ ).
        !
        ! Further Details
        ! ===============
        !
        ! Contributed by Song Jin, University of Tennessee, 1996.
        !
        ! =====================================================================
        !
        !     .. Parameters ..
        INTEGER CTXT_ , DLEN_ , MB_
        PARAMETER (DLEN_=9,CTXT_=2,MB_=5)
        INTEGER NOUT
        PARAMETER (NOUT=13)
        DOUBLE PRECISION ONE , ZERO
        PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)
        !     ..
        !     .. Local Scalars ..
        LOGICAL isioprocessor
        INTEGER csrc , i , ictxt , iend , isize , istart , j , jend ,     &
                & jsize , jstart , ldd , lwork , mb , mm , mycol , myrow ,  &
                & nb , nn , npcol , nprow , rsrc
        DOUBLE PRECISION alpha , beta
        !     ..
        !     .. Local Arrays ..
        INTEGER descwork(DLEN_)
        !     ..
        !     .. External Subroutines ..
        EXTERNAL BLACS_GRIDINFO , DESCSET , PDGEADD , PXERBLA
        !     ..
        !     .. Intrinsic Functions ..
        INTRINSIC INT , MAX , MIN
        !     ..
        !     .. Executable Statements ..
        lwork = Desca(MB_)
        ictxt = Desca(CTXT_)
        CALL BLACS_GRIDINFO(ictxt,nprow,npcol,myrow,mycol)
        isioprocessor = ((myrow==Irwrit) .AND. (mycol==Icwrit))
        !
        mm = MAX(1,MIN(M,lwork))
        nn = MAX(1,INT(lwork/mm))
        mb = mm
        nb = nn
        rsrc = Irwrit
        csrc = Icwrit
        ldd = MAX(1,mm)
        CALL DESCSET(descwork,mm,nn,mb,nb,rsrc,csrc,ictxt,ldd)
        IF ( isioprocessor ) THEN
            OPEN (NOUT,FILE=Filenam,STATUS='UNKNOWN',FORM='FORMATTED',     &
                    & ACCESS='SEQUENTIAL',ERR=100)
            REWIND (NOUT)
            WRITE (NOUT,FMT=*,ERR=100) M , N
        ENDIF
        DO jstart = Ja , Ja + N - 1 , nn
            jend = MIN(Ja+N-1,jstart+nn-1)
            jsize = jend - jstart + 1
            DO istart = Ia , Ia + M - 1 , mm
                iend = MIN(Ia+M-1,istart+mm-1)
                isize = iend - istart + 1
                alpha = ONE
                beta = ZERO
                CALL PDGEADD('NoTrans',isize,jsize,alpha,A,istart,jstart,   &
                        & Desca,beta,Work,1,1,descwork)
                IF ( isioprocessor ) THEN
                    DO j = 1 , jsize
                        DO i = 1 , isize
                            WRITE (NOUT,FMT=*,ERR=100) Work(i+(j-1)*ldd)
                        ENDDO
                    ENDDO
                ENDIF
            ENDDO
        ENDDO
        IF ( isioprocessor ) CLOSE (NOUT,ERR=100)
        Work(1) = Desca(MB_)
        RETURN
100     CALL PXERBLA(Desca(CTXT_),'PLAWRITE',1)
        Work(1) = Desca(MB_)
        !
        !     End of PDLAWRITE
        !
    END SUBROUTINE PDLAWRITE
    
end module EighUtils


!============================================
!               Main Program
!============================================

program eigh
    use EighUtils
    ! Command line parsing utility
    use M_kracken 
    implicit none

    !---------------------
    !    Declarations
    !---------------------

    integer :: ier, iflen          ! For command line options parsing
    real(dp) :: t1, t2, dt         ! For time measurement    
    character(len=20) :: filepath  ! Path to ``.mtx`` file
    character(len=20) :: evalspath ! Path file where eigenvals will be stored
    character(len=20) :: evecspath ! Path file where eigenvect will be stored
    character(len=20) :: infofile  ! Path file where diagonalisation info will be stored

    ! MPI/Blacs variables
    integer :: myrank, ierr, nprocs
    integer :: context                      ! Index of shared world 
    integer :: mpi_grid_rows, mpi_grid_cols ! dimensions of mpi grid
    integer :: my_grid_row, my_grid_col     ! local coordinates of mpi grid
    integer :: info

    ! Eigensolver variables
    integer :: lwork                                ! Size of workspace
    real(dp) :: size_of_workspace(1)        ! For reading size of ws
    real(dp), allocatable :: workspace(:)   ! Workspace array

    ! Matrix
    real(dp), allocatable :: local_matrix(:,:)  ! local matrix different for each mpi process
    integer :: matrix_descriptor(9)                      ! descriptor used for scalapack
    integer :: evect_descriptor(9)                       ! descriptor used for scalapack
    integer :: order
    integer :: block_size

    ! Output
    real(dp), allocatable :: eigenvals(:)
    real(dp), allocatable :: eigenvectors(:,:)

    ! Dummy variables
    integer :: i

    !--------------------------------
    !  Collect command line options
    !--------------------------------

    ! Default values
    call kracken('cmd','-f none -b 64 -e evals.csv -v evecs.csv -i diag.log')

    ! Get path to matrix file
    call retrev('cmd_f', filepath, iflen, ier)
    if (filepath == 'none') then
        print '(A)', 'Matrix file should be specified with -f option'
        
    end if
    
    !------------------------------
    ! Initialize MPI environment
    !------------------------------

    call Init_Blacs_Grid(context)
    call Blacs_Gridinfo(context, mpi_grid_rows, mpi_grid_cols, &
            & my_grid_row, my_grid_col)
    call MPI_Comm_Rank(MPI_COMM_WORLD, myrank, ierr)
    call MPI_Comm_Size(MPI_COMM_WORLD, nprocs, ierr)

    call Read_And_Distribute_Mtx_Matrix(filepath, context, local_matrix, &
            & matrix_descriptor, evect_descriptor, order, block_size)

    ! Allocate arrays for results
    allocate(eigenvals(order))
    allocate(eigenvectors(order, order))

    ! For benchmarking
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
    t1 = MPI_wtime()

    ! LWORK represents the size of the workspace you are providing to LAPACK
    ! WORK is an array of DOUBLE PRECISION number of size LWORK
    call Pdsyev('V', 'U', order, local_matrix, 1, 1, matrix_descriptor, &
            & eigenvals, eigenvectors, 1, 1, evect_descriptor, size_of_workspace, -1, info)

    print *, 'Proc', myrank, 'Size of workspace', size_of_workspace
    lwork = int(size_of_workspace(1))
    allocate(workspace(lwork))

    call Pdsyev('V', 'U', order, local_matrix, 1, 1, matrix_descriptor, &
            & eigenvals, eigenvectors, 1, 1, evect_descriptor, workspace, lwork, info)

    ! For benchmarking
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
    t2 = MPI_wtime()
    dt = t2 - t1

    !----------------------------
    !   Write results to file
    !----------------------------    

    if (myrank == 0) then
        ! Write evalues
        open(11, file=evalspath)
        open(12, file=evecspath)
        write (11, '(A,I0)') "# Used processes ", nprocs
        write (11, '(A,I0,A,I0)') "# MPI grid ", mpi_grid_rows, 'x', mpi_grid_cols
        write (11, '(A,I0)') "# Block size ", block_size
        write (11, '(A,F12.2,A)') "# Diagonalization time ", dt, " s"

        do i = 1, order
            write (11, "((I6,:,','),(E16.6))") i, eigenvals(i)
            write (12, "(999999(E16.6,:,','))") eigenvectors(:,i)
        enddo
        
        close(11)
        close(12)        

        ! print *, shape(eigenvectors)
        ! open(12, file=evecspath)
        ! do i = 1, order
        !     do j = 1, order
        !         call Pdelget('A', ' ', t2, eigenvectors, i, j, evect_descriptor)
        !     end do
        !     print *, t2
        ! end do
        ! close(12)
        
    end if

    ! call Pdlaprnt(order, order, eigenvectors, 1, 1, evect_descriptor, 0, 0, 'Z', 6, workspace)
    call Pdlawrite('test.mat', order, order, eigenvectors, 1, 1, evect_descriptor, &
            & 0, 0, workspace)

    call Blacs_Gridexit(context)
    call Blacs_Exit(0)
end PROGRAM EIGH
