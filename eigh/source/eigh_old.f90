!============================================
!               Main Program
!============================================

program eigh
    use EighUtils
    use class_CMD_Options
    use class_BLACS_Grid    
    use class_EigHDF5File

    ! use class_BC_Matrix
    ! use EighFiles
    implicit none

    real(dp), parameter :: version = 0.2
    
    !---------------------
    !    Declarations
    !---------------------

    type(blacs_grid)  :: mpigrid
    type(cmd_options) :: cmd_opt
    type(eighdf5file) :: matrix_file
    type(eighdf5file) :: results_file

    real(dp) :: t1              ! For time measurement

    ! MPI/Blacs variables
    integer :: ierr, info

    ! Eigensolver variables
    integer  :: lwork                       ! Size of workspace
    real(dp) :: size_of_workspace(1)        ! For reading size of ws
    real(dp), allocatable :: workspace(:)   ! Workspace array

    ! Matrix
    real(dp), allocatable :: local_matrix(:,:)  ! local process matrix
    real(dp), allocatable :: mass_matrix(:,:)   !
    integer :: matrix_descriptor(9)             ! blacs matrix descriptor

    ! Output
    real(dp), allocatable :: eigenvals(:)
    real(dp), allocatable :: eigenvectors(:,:)
    real(dp) :: factor(1)

    ! Dummy variables
    integer :: i, error
    logical :: i_am_root
    character(len=6) :: fattr = "factor"

    integer :: order
    real(dp) :: alpha
    real(dp) :: beta
    integer :: gdims(2), ldims(2)
    integer(hsize_t) :: bdims(2)

    ! ---------------------------
    !    Initialise BLACS grid
    ! ---------------------------

    call mpigrid%init()
    i_am_root = mpigrid%isroot()
    if (i_am_root) call print_version(version)
    call mpigrid%print_grid_info()

    ! ----------------------------------
    !    Collect command line options
    ! ----------------------------------

    call cmd_opt%collect(mpigrid)
    call MPI_Barrier(MPI_COMM_WORLD, error)
    
    ! -----------------------------
    !       Read HDF5 Matrix
    ! -----------------------------

    t1 = MPI_wtime()
    if (i_am_root) call print_header("READING", 1)

    bdims = cmd_opt%bdims
    call matrix_file%init(mpigrid, bdims, cmd_opt%filepath)


    ! Open HDF5 file and dataset
    call matrix_file%open_file("r")
    call matrix_file%open_dataset(cmd_opt%dataset, gdims)
    ldims = int(matrix_file%ldims)
    order = gdims(1)

    ! Allocating
    call MPI_Barrier(MPI_COMM_WORLD, error)
    if (i_am_root) print '(A)', "Allocating local matrices:"
    call MPI_Barrier(MPI_COMM_WORLD, error)
    call allocate_and_print_info(local_matrix, ldims, &
            " - "//mpigrid%proc_info(), error)
    
    call MPI_Barrier(MPI_COMM_WORLD, error)

    ! Read matrix
    call matrix_file%read_data(local_matrix, &
            matrix_file%ldims, matrix_file%gdims)

    ! Read attributes
    if (matrix_file%attribute_exists('factor')) then
        call matrix_file%read_attribute(fattr, factor)
    else
        factor = 1
    end if

    ! Close HDF5 file
    call matrix_file%close_dataset()
    call matrix_file%close_file()

    if (i_am_root) print *, ""
    if (i_am_root) print '(A,F16.2,A)', "Reading finished in", &
            MPI_wtime() - t1, "s"

    if (i_am_root) call print_header("DIAGONALIZATION", 1)
    
    ! -----------------------------
    !         Allocation
    ! -----------------------------

    if (i_am_root) call print_header("Allocating Arrays For Results")
    call MPI_Barrier(MPI_COMM_WORLD, error)    

    ! Allocate arrays for results
    call allocate_and_print_info(eigenvals, gdims(1), &
            mpigrid%proc_info(), error, 'eigenvectors')
    call allocate_and_print_info(eigenvectors, gdims, &
            mpigrid%proc_info(), error, 'eigenvalues ')
    call MPI_Barrier(MPI_COMM_WORLD, error)

    ! -----------------------------
    !     Workspace Calculation
    ! -----------------------------
    
    ! For benchmarking
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
    if (i_am_root) call print_header(&
            'Calculating Workspace Required to Generate All Results')
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
    t1 = MPI_wtime()

    
    call matrix_file%generate_scalapack_descriptor(matrix_descriptor)

    call Pdsyev(&
            'V', &                  ! Compute eigenvalues and eigenvectors
            'U', &                  ! Store upper triangular part of matrix
            order, &                ! Matrix order (number of rows and cols)
            local_matrix, &         ! Matrix distributed in block_cyclic fashion
            1, &                    ! Begining of submatrix row (1 == all rows)
            1, &                    ! Begining of submatrix col (1 == all cols)
            matrix_descriptor, &    ! SCPK Matrix descriptor (locl and glob info)
            eigenvals, &            ! Eigenvalues bufer  (for output)
            eigenvectors, &         ! Eigenvectors bufer (for output)
            1, &                    ! Begining of submatrix row (1 == all rows)
            1, &                    ! Begining of submatrix col (1 == all cols)
            matrix_descriptor, &    ! SCPK Matrix descriptor (locl and glob info)
            size_of_workspace, &    ! Stores minimal workspace required to
                                    !     generate all the eigenvectors
            -1, &                   ! Perform just workspace query
            info)                   ! Returned information flag (0 == success) 

    lwork = int(size_of_workspace(1))
    call allocate_and_print_info(workspace, lwork, &
            mpigrid%proc_info(), error, 'workspace')

    call MPI_Barrier(MPI_COMM_WORLD, ierr)
    if (i_am_root) print '(A,F16.2,A)', 'Workspaces calculated in ', &
            MPI_wtime() - t1, " s"

    if (i_am_root) call print_header("Diagonalising")
    t1 = MPI_wtime()

    call Pdsyev(&
            'V',               &! Compute eigenvalues and eigenvectors
            'U',               &! Store upper triangular part of matrix
            order,             &! Matrix order (number of rows and cols)
            local_matrix,      &! Matrix distributed in block_cyclic fashion
            1,                 &! Begining of submatrix row (1 == all rows)
            1,                 &! Begining of submatrix col (1 == all cols)
            matrix_descriptor, &! SCPK Matrix descriptor (locl and glob info)
            eigenvals,         &! Eigenvalues bufer  (for output)
            eigenvectors,      &! Eigenvectors bufer (for output)
            1,                 &! Begining of submatrix row (1 == all rows)
            1,                 &! Begining of submatrix col (1 == all cols)
            matrix_descriptor, &! SCPK Matrix descriptor (locl and glob info)
            workspace,         &! Workspace used for diagonalisation
            lwork,             &! Size of workspace
            info)               ! Returned information flag (0 == success) 

    if (i_am_root) print '(A,F16.2,A)', 'Diagonalisation finished in', &
            MPI_wtime() - t1, " s"
    call MPI_Barrier(MPI_COMM_WORLD, ierr)

    call Pdlaprnt(order, order, eigenvectors, 1, 1, matrix_descriptor, &
            0, 0, 'A', 6, workspace)
    
    ! -----------------------
    !     Writing Results
    ! -----------------------
    
    t1 = MPI_wtime()
    if (i_am_root) call print_header('WRITING RESULTS', 1)

    ! ------------------------------
    !    EIGENVALUES POSTPROCESS
    ! ------------------------------

    where (eigenvals < 0)
        eigenvals = -1*eigenvals
    end where    
    eigenvals = sqrt(eigenvals)*factor(1)

    ! ------------------------------
    !    EIGENVECTORS POSTPROCESS
    ! ------------------------------

    call matrix_file%init(mpigrid, bdims, cmd_opt%filepath)
    call matrix_file%open_file("r")
    call matrix_file%open_dataset('inv_sq_mass_mtx', gdims)

    call MPI_Barrier(MPI_COMM_WORLD, error)
    if (i_am_root) print '(A)', "Allocating local matrices:"
    call MPI_Barrier(MPI_COMM_WORLD, error)

    call allocate_and_print_info(mass_matrix, ldims, &
            " - "//mpigrid%proc_info(), error)
    call MPI_Barrier(MPI_COMM_WORLD, error)

    call matrix_file%read_data(local_matrix, &
            matrix_file%ldims, matrix_file%gdims)
    
    call matrix_file%close_dataset()
    call matrix_file%close_file()

    alpha = 0.0d+0
    beta = 1.0d+0
    
    ! call Pdgemm(&
    !         "N",   & !
    !         "N",   & !
    !         order, & !
    !         order, & !
    !         order, & !
    !         alpha, & !
    !         mass_matrix, 1, 1, matrix_descriptor, &
    !         eigenvectors, 1, 1, matrix_descriptor, &
    !         beta, &
    !         local_matrix, 1, 1, matrix_descriptor)

    call results_file%init(mpigrid, matrix_file%bdims, &
            cmd_opt%outputf)
    call results_file%calculate_local_dimensions(matrix_file%gdims, &
            verbose=.false.)
    call results_file%create_file()
    call results_file%write_matrix(&
            eigenvectors, 'eigenvectors')
    call results_file%write_nondistributed_vector(&
            eigenvals, 'eigenvalues')

    call results_file%close_file()

    call MPI_Barrier(MPI_COMM_WORLD, error)
    if (i_am_root) print '(A,F16.2,A)', 'Writing finished in', &
            MPI_wtime() - t1, " s"

    if (i_am_root) print *, ""

    if (i_am_root) then
        if (factor(1) > 1) then
            print '(A)', "Eigenvalues:"
            do i = 1, int(matrix_file%gdims(1))
                print '(I5,F16.8)', i, eigenvals(i)
            end do
        end if
    end if
    
    call mpigrid%finish()

end PROGRAM EIGH
