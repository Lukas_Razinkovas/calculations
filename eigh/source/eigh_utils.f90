module EighUtils
    ! Contains helper subroutines and variables used 
    ! by eigh.f90, eigh_hdf5.f90
    use mpi
    use hdf5
    implicit none

    integer, parameter:: dp=kind(0.d0) ! double precision

    interface allocate_and_print_info
        module procedure allocate_and_print_info1d
        module procedure allocate_and_print_info2d
    end interface allocate_and_print_info

contains

    !-----------------------------------------
    !           Printing routines
    !-----------------------------------------

    subroutine print_version(version)
        real(dp), intent(in) :: version
        integer :: cnt = 19

        print '(A)', repeat('=', 70)
        print '(A,A)',      repeat(' ', cnt), " _____  ______ _       _    _ "
        print '(A,A)',      repeat(' ', cnt), "|  __ \|  ____(_)     | |  | |"
        print '(A,A)',      repeat(' ', cnt), "| |  | | |__   _  __ _| |__| |"
        print '(A,A)',      repeat(' ', cnt), "| |  | |  __| | |/ _` |  __  |"
        print '(A,A)',      repeat(' ', cnt), "| |__| | |____| | (_| | |  | |"
        print '(A,A)',      repeat(' ', cnt), "|_____/|______|_|\__, |_|  |_|"
        print '(A,A)',      repeat(' ', cnt), "                  __/ |       "
        print '(A,A,F5.2)', repeat(' ', cnt), "                 |___/  version", version    
        print *, ""
        print '(A,A)', repeat(' ', 13), "Parallel Symmetric Matrix Eigensolver for HDF5"
        ! print *, ""
        print '(A)', repeat('=', 70)

    end subroutine print_version
    
    subroutine convert_bytes(msize, unit)
        real(dp),         intent(inout) :: msize
        character(len=2), intent(out)   :: unit

        if (msize > 1e9) then
            msize = msize/real(1E+9)
            unit = "Gb"
        else if (msize > 1e6) then
            msize = msize/real(1E+6)
            unit = "Mb"
        else if (msize > 1e3) then
            msize = msize/real(1E+3)
            unit = "Kb"
        else
            unit = "b"
        end if
    end subroutine convert_bytes

    subroutine allocate_and_print_info1d(array, dim, prefix, error, name)
        real(dp), allocatable,   intent(out) :: array(:)
        integer,                 intent(in)  :: dim
        character(len=*),        intent(in)  :: prefix
        integer,                 intent(out) :: error
        character(len=*),        optional    :: name
        character(len=256)                   :: aname = ''
        character(len=256)                   :: errmsg
        character(len=16)                    :: dimstring
        real(dp)                             :: msize
        integer                              :: stat
        character(len=2)                     :: unit

        if (present(name)) aname = name

        allocate(array(dim), stat=stat, errmsg=errmsg)
        msize = sizeof(array)
        write (dimstring, '(A,I0)') 'dims=', dim

        if (stat /= 0) then
            print '(A)', trim(prefix)//' failed to allocate '//dimstring//&
                    trim(name)//' array:'// errmsg
            error = -1
            return
        end if

        call convert_bytes(msize, unit)

        print '(A,F6.2,A)', trim(prefix) // ' allocated ' // &
                trim(name) // ' array of '//dimstring// &
                '(', msize, ' '//unit//')'

    end subroutine allocate_and_print_info1d

    subroutine allocate_and_print_info2d(array, dims, prefix, error, name)
        real(dp), allocatable,   intent(out) :: array(:,:)
        integer,                 intent(in)  :: dims(2)
        character(len=*),        intent(in)  :: prefix
        integer,                 intent(out) :: error
        character(len=*),        optional    :: name
        character(len=256)                   :: aname = ''
        character(len=256)                   :: errmsg
        character(len=16)                    :: dimstring
        real(dp)                             :: msize
        integer                              :: stat
        character(len=2)                     :: unit

        if (present(name)) aname = name

        allocate(array(int(dims(1)), int(dims(2))), stat=stat)
        msize = sizeof(array)
        write (dimstring, '(A,I0,A,I0)') 'dims=', dims(1), "x", dims(2)

        if (stat /= 0) then
            print '(A)', trim(prefix)//' failed to allocate '//dimstring//&
                    trim(name)//' array:'// errmsg
            error = -1
            return
        end if

        call convert_bytes(msize, unit)

        print '(A,F6.2,A)', trim(prefix) // ' allocated ' // &
                name // ' array of '//dimstring// &
                '(', msize, ' '//unit//')'

    end subroutine allocate_and_print_info2d

    
    subroutine print_matrix(rank, wsize, matrix, proc_info)
        integer :: rank, wsize, i, j, error
        real(dp) :: matrix(:,:)
        character(len=*) :: proc_info

        do i = 0, wsize
            if (rank == i) then
                print '(A)', ""
                print '(A)', proc_info
                do j = 1, size(matrix, 2)
                    print '(99(F7.2))', matrix(:, j)
                end do
            end if
            call MPI_Barrier(MPI_COMM_WORLD, error)
        end do

    end subroutine print_matrix
    

    
    subroutine Print_header(header, level)
        ! Just prints header in nice format.
        ! Level 1 is like section.
        ! Level 2 is like subsection.
        ! Level 3 is like subsubsection.

        character(len=*), intent(in) :: header
        character(len=1)  :: separator
        integer, optional :: level
        integer :: lv

        if (present(level)) then
            lv = level
        else
            lv = 3
        endif

        select case (lv)
        case (1)
            separator = "="
        case (2)
            separator = "-"
        case default
            separator = "."
        end select

        lv = len_trim(header)
        lv = (70 - lv)/2
        print '(A)', ""
        print '(A)', repeat(separator, 70)
        print '(A,A)', repeat(' ', lv), trim(header)
        print '(A)', repeat(separator, 70)
        print '(A)', ""
        return
    end subroutine Print_header

end module EighUtils


! -------------------
!     BLACS GRID
! -------------------

module class_BLACS_Grid
    ! Hides all MPI and BLACS initialisation and finalisation
    ! routines
    use mpi
    use EighUtils
    implicit none
    private

    type, public :: blacs_grid
        ! Storess all info about current process and proc grid
        integer, public   :: dims(2) ! MPI Blacs Grid dimensions
        integer, public   :: loc(2)  ! Position in grid
        integer, public   :: rank    ! MPI rank of current process
        integer, public   :: size    ! Number of processes
        integer, public   :: context ! MPI world context
        real(dp), private :: time    ! For time measurement
    contains
        procedure, public :: init
        procedure, public :: print_grid_info
        procedure, public :: terminate
        procedure, public :: finish
        procedure, public :: get_proc_info_str
        procedure, public :: proc_info => get_proc_info_str
        procedure, public :: isroot => i_am_root

    end type blacs_grid

contains

    function i_am_root(self) result(bool)
        class(blacs_grid), intent(in) :: self
        logical :: bool
        bool = (self%loc(1) == 0 .and. self%loc(2) == 0)
    end function i_am_root
    
    function get_proc_info_str(self) result(string)
        ! Returns string describing current process in Blacs grid
        class(blacs_grid) :: self
        character(len=15) :: string

        write(string, "(A,(I0:,', '),I0,A)") 'Proc [', &
                self%loc(1), self%loc(2), ']'
    end function get_proc_info_str

    subroutine init(self)
        class(blacs_grid) :: self
        integer :: ierr

        call MPI_Init(ierr)
        call MPI_Comm_Size(MPI_COMM_WORLD, self%size, ierr)
        call MPI_Comm_Rank(MPI_COMM_WORLD, self%rank, ierr)
        self%dims = 0
        call MPI_Dims_Create(self%size, 2, self%dims, ierr)
        call Blacs_Get(-1, 0, self%context)
        call Blacs_Gridinit(self%context, 'Row-major', self%dims(1), self%dims(2))
        call Blacs_Gridinfo(self%context, self%dims(1), self%dims(2), &
                & self%loc(1), self%loc(2))
        
        self%time = MPI_wtime()

    end subroutine init

    subroutine print_grid_info(self)
        class(blacs_grid) :: self
        integer :: ierr

        if (self%rank == 0) then
            call Print_header('MPI/BLACS Grid Info')
            print '(A,I0)', 'MPI processes:  ', self%size
            print '(A,I0,A,I0)', 'MPI BLACS grid: ', &
                & self%dims(1), "x", self%dims(2)
        end if

        call MPI_Barrier(MPI_COMM_WORLD, ierr)
    end subroutine print_grid_info

    subroutine terminate(self)
        class(blacs_grid) :: self
        integer :: ierr
        ! call Blacs_abort(self%context, ierr)
        call Blacs_Gridexit(self%context)
        call Blacs_Exit(0)
        call BLACS_abort(self%context, ierr)
        stop
    end subroutine terminate

    subroutine finish(self)
        class(blacs_grid) :: self
        
        if (self%rank == 0) then
            print '(A)', "Finishing"
            print '(A,F12.3)', 'Total time spent: ', MPI_wtime() - self%time
        end if
        
        call Blacs_Gridexit(self%context)
        call Blacs_Exit(0)
    end subroutine finish

end module class_BLACS_Grid

! ------------------------
!       CMD options
! ------------------------

module class_CMD_Options
    use M_kracken
    use class_BLACS_Grid
    use EighUtils

    type, public :: cmd_options
        character(len=256), public :: filepath
        character(len=256), public :: dataset
        character(len=256), public :: outputf
        integer, public :: dlen
        integer, public :: flen
        integer, public :: olen
        integer, public :: bdims(2)
        integer, public :: block_size

    contains
        procedure, public :: collect
        
    end type cmd_options

    private

contains

    subroutine collect(self, mpigrid)
        class(cmd_options) :: self
        type(blacs_grid)  :: mpigrid
        integer :: ierr
        logical :: i_am_root

        i_am_root = mpigrid%isroot()

        ! Set default values
        call kracken('cmd','-f none -b 64 -o results.hdf5 -d matrix', ierr)
        ! Get path to input and output files
        call retrev('cmd_f', self%filepath, self%flen, ierr)
        call retrev('cmd_o', self%outputf, self%olen, ierr)
        call retrev('cmd_d', self%dataset, self%dlen, ierr)
        self%block_size = iget('cmd_b')
        self%bdims = [self%block_size, self%block_size]

        if (self%filepath == 'none') then
            if (i_am_root) then
                print '(A)', 'Matrix file should be specified with -f[filepath] option'
                print '(A)', 'Other optional options:'
                print '(A)', '-o [filepath]  output file (default=results.hdf5)'
                print '(A)', '-d [dataset]   hdf5 root group dataset name (default=matrix)'
                print '(A)', '-b [int]       blocking factor used for matrix slicing'
                print '(A)', '                  (default=64)'

            end if
            call mpigrid%terminate()
            stop
        end if

        if (i_am_root) then
            call print_header("Command Line Options")
            print '(A)', "Input Matrix:"
            print '(A)', " - filepath: "//self%filepath(1:self%flen)
            print '(A)', " - group:    root"
            print '(A)', " - dataset:  "//self%dataset(1:self%dlen)
            print '(A)', "Output:"
            print '(A)', " - filepath: "//self%outputf(1:self%olen)
            print '(A)', "Block Cyclic Distribution:"
            print '(A,I0,A,I0,A)', " - block dimensions: ", &
                    self%block_size, "x", self%block_size, ' blocks'
                    
        end if
    end subroutine collect
    
end module class_CMD_Options

