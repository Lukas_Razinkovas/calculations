! Hides Block Cyclic distribution and scaLAPACK routines

module class_BC_Matrix
    use EighUtils
    use class_BLACS_Grid
    implicit none

    ! interface get_dims
    !     module procedure get_dims_hsize_t
    !     module procedure get_dims
    ! end interface get_dims

    type, public :: bcmatrix
        type(blacs_grid)  :: mpigrid       ! BLACS/MPI grid
        integer           :: ldims(2)      ! Local dimensions
        integer           :: gdims(2)      ! Global dimensions
        integer           :: bdims(2)      ! Block dimensions of BC distr
        integer           :: descriptor(9) ! BLACS matrix descriptor
        logical           :: verbose = .false.
        logical           :: root

        ! real(dp), dimension(:,:), pointer :: matrix_ptr

    contains
        private
        procedure          :: inith => init_from_long_integers
        procedure          :: initi => init_from_integers
        procedure          :: gtdimsh => get_dims_long_integers
        procedure          :: gtdimsi => get_dims_integers
        generic,   public  :: init => inith, initi
        generic,   public  :: getdims => gtdimsh, gtdimsi
        procedure, public  :: multiply
        procedure, public  :: diagonalise
        procedure, private :: calculate_local_dimensions
        procedure, private :: create_descriptor

    end type bcmatrix

contains

    ! -------------------------
    !      Init Interfaces
    ! -------------------------

    subroutine init_from_integers(self, gdims, bdims, mpigrid, matrix, name)
        class(bcmatrix)                     :: self
        integer,                intent(in)  :: gdims(2)
        integer,                intent(in)  :: bdims(2)
        type(blacs_grid),       intent(in)  :: mpigrid
        real(dp), allocatable,  intent(out) :: matrix(:,:)
        character(len=*),       intent(in)  :: name
        integer :: error

        self%root = mpigrid%isroot()
        self%mpigrid = mpigrid
        self%gdims = gdims
        self%bdims = bdims        
        call self%calculate_local_dimensions()
        call self%create_descriptor()
        call allocate_and_print_info(&
                matrix, self%ldims, " - "//mpigrid%proc_info(), error, trim(name))
        call MPI_Barrier(MPI_COMM_WORLD, error)
    end subroutine init_from_integers

    subroutine init_from_long_integers(self, gdims, bdims, mpigrid, matrix, name)
        class(bcmatrix)                     :: self
        integer(hsize_t),       intent(in)  :: gdims(2)
        integer(hsize_t),       intent(in)  :: bdims(2)
        type(blacs_grid),       intent(in)  :: mpigrid
        real(dp), allocatable,  intent(out) :: matrix(:,:)
        character(len=*),       intent(in)  :: name
        integer :: error

        self%root = mpigrid%isroot()
        self%mpigrid = mpigrid
        self%gdims = int(gdims)
        self%bdims = int(bdims)
        call self%calculate_local_dimensions()
        call self%create_descriptor()
        call allocate_and_print_info(&
                matrix, self%ldims, " - "//mpigrid%proc_info(), error, trim(name))
        call MPI_Barrier(MPI_COMM_WORLD, error)
    end subroutine init_from_long_integers

    ! -------------------------
    !      Init Functions
    ! -------------------------
    
    subroutine calculate_local_dimensions(self)
        class(bcmatrix)     :: self
        integer             :: error, i
        integer             :: Numroc

        if (self%verbose) then
            call MPI_Barrier(MPI_COMM_WORLD, error)
            if (self%root) print '(A)', &
                    "Calculating dimensions of local matrices:"
            call MPI_Barrier(MPI_COMM_WORLD, error)
        end if

        do i = 1, 2
            self%ldims(i) = Numroc(self%gdims(i), self%bdims(i), &
                    self%mpigrid%loc(i), 0, self%mpigrid%dims(i))
        end do

        if (self%verbose) then
            call MPI_Barrier(MPI_COMM_WORLD, error)
            print '(A,I0,A,I0)', ' - '// trim(self%mpigrid%proc_info()) // &
                    ' ldim = ', self%ldims(1), 'x', self%ldims(2)
            call MPI_Barrier(MPI_COMM_WORLD, error)
        end if
    end subroutine calculate_local_dimensions

    subroutine create_descriptor(self)
        class(bcmatrix)     :: self
        integer             :: info

        call Descinit(                &
                self%descriptor,      &
                self%gdims(1),        & ! global matrix rows
                self%gdims(2),        & ! global matrix cols
                self%bdims(1),        & ! block rows
                self%bdims(2),        & ! block cols
                0,                    & ! proc over which the 1 row is distributed
                0,                    & ! proc over which the 1 col is distributed
                self%mpigrid%context, & ! blacs context handle
                self%ldims(1),        & ! leading dimension of the local matrix
                info)                   ! output flag

        if (info /= 0) then
            if (self%root) then
                print '(A,I0)', &
                        'Blacs descriptor creation failed! Wrong argument:', &
                        -info
            end if
            call self%mpigrid%terminate()
        end if
    end subroutine create_descriptor

    ! ------------------------------
    !      Get dims interface
    ! ------------------------------

    subroutine get_dims_integers(self, ldims, gdims, bdims)
        class(bcmatrix)                      :: self
        integer,                intent(out)  :: ldims(2)
        integer,                intent(out)  :: gdims(2)
        integer,                intent(out)  :: bdims(2)

        ldims = self%ldims
        gdims = self%gdims
        bdims = self%bdims
        
    end subroutine get_dims_integers

    subroutine get_dims_long_integers(self, ldims, gdims, bdims)
        class(bcmatrix)                     :: self
        integer(hsize_t),       intent(out) :: ldims(2)
        integer(hsize_t),       intent(out) :: gdims(2)        
        integer(hsize_t),       intent(out) :: bdims(2)

        ldims = self%ldims
        gdims = self%gdims
        bdims = self%bdims

    end subroutine get_dims_long_integers

    
    ! ----------------------------
    !        Manipulations
    ! ----------------------------

    subroutine multiply(self, self_array, other, other_array, product, &
            product_array)
        class(bcmatrix)                :: self
        class(bcmatrix), intent(in)    :: other
        class(bcmatrix), intent(in)    :: product
        real(dp),        intent(inout) :: self_array(*)
        real(dp),        intent(inout) :: other_array(*)
        real(dp),        intent(out)   :: product_array(*)
        real(dp)                       :: alpha = 1
        real(dp)                       :: beta = 0
        
        call Pdgemm(&
                "N",                   &
                "N",                   &
                self%gdims(1),         &
                other%gdims(1),        &
                product%gdims(1),      &                                
                alpha,                 &
                self_array,  1, 1, self%descriptor,  &
                other_array, 1, 1, other%descriptor, &                
                beta, &
                product_array, 1, 1, product%descriptor)

    end subroutine multiply

    subroutine diagonalise(self, self_array, evecs, evecs_array, evals)
        class(bcmatrix)              :: self
        real(dp),        intent(in)  :: self_array(*)
        class(bcmatrix), intent(in)  :: evecs
        real(dp),        intent(out) :: evecs_array(*)
        real(dp),        intent(out) :: evals(*)
        real(dp)                     :: size_of_workspace(1)
        real(dp),        allocatable :: workspace(:)
        integer                      :: error
        integer                      :: info        
        real(dp)                     :: t1
        integer                      :: lwork

        if (self%verbose) then
            call MPI_Barrier(MPI_COMM_WORLD, error)
            if (self%root) call print_header(&
                    'Calculating Workspace Required to Generate All Results')
            call MPI_Barrier(MPI_COMM_WORLD, error)
            t1 = MPI_wtime()
        end if

        ! --------------------------------
        !        Workspace Query
        ! --------------------------------        
        
        call Pdsyev(&
                'V',               &! Compute eigenvalues and eigenvectors
                'U',               &! Store upper triangular part of matrix
                self%gdims(1),     &! Matrix order (number of rows and cols)
                self_array,        &! Matrix distributed in block_cyclic fashion
                1,                 &! Begining of submatrix row (1 == all rows)
                1,                 &! Begining of submatrix col (1 == all cols)
                self%descriptor,   &! SCPK Matrix descriptor (locl and glob info)
                evals,             &! Eigenvalues bufer  (for output)
                evecs_array,       &! Eigenvectors bufer (for output)
                1,                 &! Begining of submatrix row (1 == all rows)
                1,                 &! Begining of submatrix col (1 == all cols)
                evecs%descriptor,  &! SCPK Matrix descriptor (locl and glob info)
                size_of_workspace, &! (out) minimal required workspace
                -1,                &! Perform just workspace query
                info)               ! Returned information flag (0 == success)

        if (info /= 0) then
            print '(A,I0)', self%mpigrid%proc_info() // &
                    " Workspace query failed: info = ", info
            call self%mpigrid%terminate()
        end if

        lwork = int(size_of_workspace(1))
        call allocate_and_print_info(workspace, lwork, &
                " - "//self%mpigrid%proc_info(), error, 'workspc')

        if (self%verbose) then
            call MPI_Barrier(MPI_COMM_WORLD, error)
            if (self%root) print '(A,F16.2,A)', 'Workspaces calculated in ', &
                    MPI_wtime() - t1, " s"
            if (self%root) call print_header("Diagonalising")
            call MPI_Barrier(MPI_COMM_WORLD, error)
            t1 = MPI_wtime()
        end if

        call Pdsyev(&
                'V',               &! Compute eigenvalues and eigenvectors
                'U',               &! Store upper triangular part of matrix
                self%gdims(1),     &! Matrix order (number of rows and cols)
                self_array,        &! Matrix distributed in block_cyclic fashion
                1,                 &! Begining of submatrix row (1 == all rows)
                1,                 &! Begining of submatrix col (1 == all cols)
                self%descriptor,   &! SCPK Matrix descriptor (locl and glob info)
                evals,             &! Eigenvalues bufer  (for output)
                evecs_array,       &! Eigenvectors bufer (for output)
                1,                 &! Begining of submatrix row (1 == all rows)
                1,                 &! Begining of submatrix col (1 == all cols)
                evecs%descriptor,  &! SCPK Matrix descriptor (locl and glob info)
                workspace,         &! Workspace used for exchanging info
                lwork,             &! Perform just workspace query
                info)               ! Returned information flag (0 == success)

        if (info /= 0) then
            print '(A,I0)', self%mpigrid%proc_info() // &
                    " Diagonalisation failed: info = ", info
            call self%mpigrid%terminate()
        end if

        if (self%verbose) then
            call MPI_Barrier(MPI_COMM_WORLD, error)
            if (self%root) print '(A,F16.2,A)', 'Diagonalisation finished in', &
                    MPI_wtime() - t1, " s"
            call MPI_Barrier(MPI_COMM_WORLD, error)
        end if

        if (allocated(workspace)) deallocate(workspace)
    end subroutine diagonalise
end module class_BC_Matrix




! program test
!     use EighUtils
!     use class_BLACS_Grid
!     use class_BC_Matrix

!     type(blacs_grid) :: mpigrid
!     type(bcmatrix)   :: matrixA
!     type(bcmatrix)   :: matrixB
!     type(bcmatrix)   :: matrixC   
!     real(dp), allocatable :: A(:,:)
!     real(dp), allocatable :: B(:,:)
!     real(dp), allocatable :: C(:,:)
!     real(dp), allocatable :: evals(:)
!     integer :: ldims(2) = 1
!     integer :: gdims(2) = 2
!     integer :: error
    
!     call mpigrid%init()

!     call matrixA%init(gdims, ldims, mpigrid, A, 'A')    
!     call matrixB%init(gdims, ldims, mpigrid, B, 'B')
!     call matrixC%init(gdims, ldims, mpigrid, C, 'C')

    
!     call allocate_and_print_info(evals, matrixA%gdims(1), mpigrid%proc_info(), &
!             error, 'evals')

!     A = mpigrid%rank + 1
!     B = A * 10
    
!     ! call matrixA%multiply(A, matrixB, B, matrixC, C)    
!     ! print '(A,99(F9.1))', mpigrid%proc_info(), C


!     call matrixA%diagonalise(A, matrixB, B, evals)
!     print '(A,99(F12.6))', mpigrid%proc_info(), B
!     print '(99(F12.6))', evals

!     ! call Descinit(&
!     !         descriptor, &
!     !         2, &
!     !         2, &
!     !         1, &
!     !         1, &
!     !         0, &                    ! proc over which the 1 row is distributed
!     !         0, &                    ! proc over which the 1 col is distributed
!     !         mpigrid%context, &      ! blacs context handle
!     !         1, &                    ! leading dimension of the local matrix
!     !         info)                   ! output flag    
    

!     ! matrix1 = mpigrid%rank + 1
!     ! matrix2 = (mpigrid%rank + 1)*10
!     ! resultm = 0

!     ! print '(A,F7.2)', mpigrid%proc_info(), matrix1
!     ! call MPI_Barrier(MPI_COMM_WORLD, error)
!     ! print *, ""
!     ! call MPI_Barrier(MPI_COMM_WORLD, error)        
!     ! print '(A,F7.2)', mpigrid%proc_info(), matrix2
!     ! call MPI_Barrier(MPI_COMM_WORLD, error)
!     ! print *, ""
!     ! call MPI_Barrier(MPI_COMM_WORLD, error)
    


!     ! call MPI_Barrier(MPI_COMM_WORLD, error)
!     ! print '(A,E13.6)', mpigrid%proc_info(), resultm(1,1)
    
!     ! call MPI_Barrier(MPI_COMM_WORLD, error)
!     ! print *, "HM"
    
!     call mpigrid%finish()
    
! end program test
