subroutine Pdlaread(filnam, a, desca, irread, icread, work)
    implicit none

    integer icread , irread
    character*(*) filnam
    integer desca(*)
    double precision a(*) , work(*)
    integer nin
    parameter (nin=11)
    integer csrc_ , ctxt_ , lld_ , mb_ , m_ , nb_ , n_ , rsrc_
    parameter (ctxt_=2,m_=3,n_=4,mb_=5,nb_=6,rsrc_=7,csrc_=8,lld_=9)
    integer h , i , ib , ictxt , icurcol , icurrow , ii , j , jb ,    &
            & jj , k , lda , m , mycol , myrow , n , npcol , nprow
    integer iwork(2)
    external blacs_gridinfo , infog2l , dgerv2d , dgesd2d , igebs2d , &
            & igebr2d
    integer iceil
    external iceil
    intrinsic min , mod
    ictxt = desca(ctxt_)
    call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)
    !
    if ( myrow.eq.irread .and. mycol.eq.icread ) then
        open (nin,file=filnam,status='old')
        read (nin,fmt=*) (iwork(i),i=1,2)
        call igebs2d(ictxt,'all',' ',2,1,iwork,2)
    else
        call igebr2d(ictxt,'all',' ',2,1,iwork,2,irread,icread)
    endif
    m = iwork(1)
    n = iwork(2)
    !
    if ( m.le.0 .or. n.le.0 ) return
    !
    if ( m.gt.desca(m_) .or. n.gt.desca(n_) ) then
        if ( myrow.eq.0 .and. mycol.eq.0 ) then
            write (*,fmt=*) 'pdlaread: matrix too big to fit in'
            write (*,fmt=*) 'abort ...'
        endif
        call blacs_abort(ictxt,0)
    endif
    !
    ii = 1
    jj = 1
    icurrow = desca(rsrc_)
    icurcol = desca(csrc_)
    lda = desca(lld_)
    !
    !     loop over column blocks
    !
    do j = 1 , n , desca(nb_)
        jb = min(desca(nb_),n-j+1)
        do h = 0 , jb - 1
            !
            !           loop over block of rows
            !
            do i = 1 , m , desca(mb_)
                ib = min(desca(mb_),m-i+1)
                if ( icurrow.eq.irread .and. icurcol.eq.icread ) then
                    if ( myrow.eq.irread .and. mycol.eq.icread ) then
                        do k = 0 , ib - 1
                            read (nin,fmt=*) a(ii+k+(jj+h-1)*lda)
                        enddo
                    endif
                elseif ( myrow.eq.icurrow .and. mycol.eq.icurcol ) then
                    call dgerv2d(ictxt,ib,1,a(ii+(jj+h-1)*lda),lda,irread,&
                            & icread)
                elseif ( myrow.eq.irread .and. mycol.eq.icread ) then
                    do k = 1 , ib
                        read (nin,fmt=*) work(k)
                    enddo
                    call dgesd2d(ictxt,ib,1,work,desca(mb_),icurrow,      &
                            & icurcol)
                endif
                if ( myrow.eq.icurrow ) ii = ii + ib
                icurrow = mod(icurrow+1,nprow)
            enddo
            !
            ii = 1
            icurrow = desca(rsrc_)
        enddo
        !
        if ( mycol.eq.icurcol ) jj = jj + jb
        icurcol = mod(icurcol+1,npcol)
        !
    enddo
    !
    if ( myrow.eq.irread .and. mycol.eq.icread ) close (nin)
    !
    !
    !     end of pdlaread
    !
end subroutine pdlaread
