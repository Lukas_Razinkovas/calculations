program testas
    implicit none
    type person
        character(len=10) :: name
        real :: age
        character(len=6) :: eid
    end type person

    type(person) :: you

    you%name = 'John Doe' ! Use (%)
    you%age = 34.2 ! to access
    you%eid = 'jd3456' ! elements
    print *, you
end program testas
