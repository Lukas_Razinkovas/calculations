! ----------------------------------------
!        Block Cyclic Matrix Class
! ----------------------------------------

module class_BC_Matrix
    ! Class for reading NETCDF matrix and distributing it with block-cyclic
    ! manner

    use mpi
    use EighUtils
    use class_BLACS_Grid
    use class_CMD_Options
    use pnetcdf ! parrallel netcdf reading with nonblocking fashion

    implicit none

    type, public :: bc_matrix
        integer(kind=MPI_OFFSET_KIND), public :: gdim(2) ! Global matrix dimensions
        integer, public :: ldim(2)            ! Local matrix dimensions
        integer, public :: bdim(2)            ! Block dimensions
        type(cmd_options), private :: cmd     ! Command line options
        type(blacs_grid), private :: mpigrid  ! Blacs grid
        character(256), private :: filepath 
        integer, private :: flen

    contains
        procedure, public :: init_from_file
        procedure, private :: calculate_local_dimensions
        procedure, private :: get_matrix_description
        procedure, private :: check
        procedure, public :: read_local_part
        procedure, private :: allocate_local_matrix

    end type bc_matrix

    private

contains

    ! -----------------------
    !    Helper routines
    ! -----------------------
    
    subroutine check(self, err, message)
        ! For NETCDF calls checking
        class(bc_matrix) :: self
        integer :: err
        character(len=*) :: message

        ! It is a good idea to check returned value for possible error
        if (err /= NF90_NOERR) then
            write(6, *) trim(message), trim(nf90mpi_strerror(err))
            call MPI_Abort(MPI_COMM_WORLD, -1, err)
        end if

    end subroutine check

    ! -----------------------
    !      Constructors
    ! -----------------------

    subroutine init_from_file(self, cmd, mpigrid)
        ! During initialisation reads matrix file header
        ! and calculates local dimensions of matrix
        class(bc_matrix) :: self
        type(cmd_options) :: cmd
        type(blacs_grid) :: mpigrid

        self%cmd = cmd
        self%mpigrid = mpigrid
        self%filepath = cmd%filepath
        self%flen = cmd%flen
        self%bdim(1) = cmd%block_size
        self%bdim(2) = cmd%block_size

        call self%get_matrix_description()
        call self%calculate_local_dimensions()

    end subroutine init_from_file

    ! --------------------------
    !      NETCDF reading
    ! --------------------------

    subroutine get_matrix_description(self)
        class(bc_matrix) :: self

        integer :: ncid
        integer :: omode
        integer :: dimid
        integer :: i
        character(len=1) :: axis
        logical :: i_am_root

        call self%mpigrid%isroot(i_am_root)
        if (i_am_root) call print_header('NETCDF matrix info')

        ! What kind of file and reading mode
        omode = NF90_NOWRITE + NF90_64BIT_OFFSET
        ! Open file
        call self%check(nf90mpi_open(MPI_COMM_WORLD, self%filepath, omode, &
                & MPI_INFO_NULL, ncid), 'nf90mpi_open: ')

        ! Read axis dimensions
        do i = 1, 2
            select case(i)
            case (1)
                axis = 'x'
            case default
                axis = 'y'
            end select
            
            call self%check(nf90mpi_inq_dimid(ncid, axis, dimid), &
                    'Error in nf90mpi_inq_dimid:')
            call self%check(nf90mpi_inquire_dimension(&
                    ncid, dimid, len=self%gdim(i)), &
                    'Error in nf90mpi_inquire_dimension:')

            if (i_am_root) print '(A,I0)', 'Global matrix '//axis//'dim = ', &
                    self%gdim(i)
        end do

        ! Close file
        call self%check(nf90mpi_close(ncid), "nf90mpi_close:")

        if (self%bdim(1) > self%gdim(1)) then
            if (i_am_root) then
                print '(A,A)', 'Error: block size is biger than global matrix ', &
                        'dimensions'
                print '(A)', "Aborting ..."
                call self%mpigrid%terminate()
            end if
        end if
        
    end subroutine get_matrix_description

    ! ----------------------------------
    !     Block Cyclic distribution
    ! ----------------------------------

    subroutine calculate_local_dimensions(self)
        ! Uses scalapack routine Numroc
        class(bc_matrix) :: self
        integer :: Numroc
        integer :: i

        do i = 1, 2
            self%ldim(i) = Numroc(self%gdim(i), self%bdim(i), &
                    self%mpigrid%loc(i), 0, self%mpigrid%dims(i))
        end do
    end subroutine calculate_local_dimensions


    subroutine allocate_local_matrix(self, local_matrix)
        class(bc_matrix), intent(in) :: self
        real(dp), allocatable, intent(out) :: local_matrix(:,:)
        integer :: stat
        character(len=256) :: errmsg
        character(len=15) :: proc
        character(len=2) :: unit
        real(dp) :: size
        
        allocate(local_matrix(self%ldim(1), self%ldim(2)), stat=stat, &
                errmsg=errmsg)

        call self%mpigrid%get_proc_info_str(proc)
        
        if (stat /= 0) then
            print '(A,A,A)', trim(proc), &
                    ' failed to allocate local array:', errmsg
            call self%mpigrid%terminate()
        else
            size = sizeof(local_matrix)*self%ldim(1)*self%ldim(2)

            if (size > 1e9) then
                size = size/real(1E+9)
                unit = "Gb"
            else if (size > 1e6) then
                size = size/real(1E+6)
                unit = "Mb"
            else if (size > 1e3) then
                size = size/real(1E+3)
                unit = "Kb"
            else
                unit = "b"
            end if
            
            print '(A,A,I0,A,I0,A,F12.3,A)', trim(proc), &
                    ' allocated local array of dim = ',  &
                    self%ldim(1), 'x', self%ldim(2), ' (', &
                    size, ' '//unit//')'
        end if

    end subroutine allocate_local_matrix

    ! subroutine read_local_part(self, local_matrix)
    !     integer :: err
    !     integer :: ncid
    !     integer :: dimid(2)
    !     integer(kind=MPI_OFFSET_KIND) :: global_nx, global_ny
    !     integer(kind=MPI_OFFSET_KIND) start(2), count(2)

    !     err = nf90mpi_open(MPI_COMM_WORLD, self%filepath, NF90_NOWRITE, &
    !             MPI_INFO_NULL, ncid)
    !     call self%check(err, 'In nf90mpi_open: ')

    !     ! the global array is NX * (NY * nprocs) */
    !     err = nf90mpi_inq_dimid(ncid, "x", dimid(1))
    !     call self%check(err, 'In nf90mpi_inq_dimid X: ')
    !     err = nf90mpi_inquire_dimension(ncid, dimid(1), len=global_ny)
    !     call check(err, 'In nf90mpi_inq_dimlen: ')
    !     ! G_NY must == NY * nprocs
    !     ! myNY must == global_ny / nprocs

    !     err = nf90mpi_inq_dimid(ncid, "Y", dimid(2))
    !     call check(err, 'In nf90mpi_inq_dimid Y: ')
    !     err = nf90mpi_inquire_dimension(ncid, dimid(2), len=global_nx)
    !     call check(err, 'In nf90mpi_inq_dimlen: ')
    !     ! global_nx must == NX

    !     err = nf90mpi_inq_varid(ncid, "var", varid)
    !     call check(err, 'In nf90mpi_inq_varid: ')

    !     ! initialize the buffer with -1, so a read error can be pinpointed
    !     buf = -1

    !     ! each proc reads NY columns of the 2D array, block_len controls the
    !     ! the number of contiguous columns at a time */
    !     block_start = 0
    !     block_len   = 2  ! can be 1, 2, 3, ..., NY
    !     if (block_len .GT. NY) block_len = NY

    !     start(1) = rank * block_len + 1
    !     start(2) = 1
    !     count(1) = 1
    !     count(2) = global_nx
    !     num_reqs = 0

    !     do i=1, NY
    !         num_reqs = num_reqs + 1
    !         err = nf90mpi_iget_var(ncid, varid, buf(:,i), &
    !                 reqs(num_reqs), start, count)
    !         call check(err, 'In nf90mpi_iget_vara_int: ')

    !         if (MOD(i, block_len) .EQ. 0) then
    !             stride = NY-i
    !             if (stride .GT. block_len) stride = block_len
    !             block_start = block_start + block_len * nprocs
    !             start(1) = block_start + stride * rank + 1;
    !         else
    !             start(1) = start(1) + 1
    !         endif
    !     enddo

    !     err = nf90mpi_wait_all(ncid, num_reqs, reqs, sts)
    !     call check(err, 'In nf90mpi_wait_all: ')

    !     ! check status of all requests
    !     do i=1, num_reqs
    !         if (sts(i) .NE. NF90_NOERR) then
    !             print*, "Error: nonblocking write fails on request", &
    !                     i, ' ', nf90mpi_strerror(sts(i))
    !         endif
    !     enddo

    !     err = nf90mpi_close(ncid)
    !     call check(err, 'In nf90mpi_close: ')

    subroutine read_local_part(self, local_matrix)
        class(bc_matrix), intent(in) :: self
        real(dp), allocatable, intent(out) :: local_matrix(:,:)
        logical :: i_am_root
        integer :: ierr, ncid, varid, omode
        integer :: start(2)
        integer :: count(2)
        character(len=15) :: proc
        integer :: request
        integer, allocatable :: requests(:)
        integer, allocatable :: states(:)        
        integer :: block_row, block_col
        integer :: block_rows, block_cols
        integer :: blocks(2)
        integer(kind=MPI_OFFSET_KIND) :: step(2)
        integer(kind=MPI_OFFSET_KIND) :: from(2)
        integer(kind=MPI_OFFSET_KIND) :: cnt(2)
        integer(kind=MPI_OFFSET_KIND) :: grow, gcol
        integer :: lstart(2)
        integer :: err
        integer :: req(2)
        integer :: st(2)
        real(dp) :: buf(2,2)
        integer :: info, i
        integer :: to(2)

        call MPI_Barrier(MPI_COMM_WORLD, ierr)
        call self%mpigrid%isroot(i_am_root)
        call self%mpigrid%get_proc_info_str(proc)

        if (i_am_root) call print_header('Reading And Distributing Matrix')
        call MPI_Barrier(MPI_COMM_WORLD, ierr)

        ! Allocate arrays
        call self%allocate_local_matrix(local_matrix)

        ! ! set an MPI-IO hint to disable file offset alignment for
        ! ! fixed-size variables
        ! call MPI_Info_create(info, err)
        ! call MPI_Info_set(info, "nc_var_align_size", "1", err)
        
        ! What kind of data storage and reading mode
        omode = NF90_NOWRITE
        
        ! Open file
        call self%check(nf90mpi_open(MPI_COMM_WORLD, self%filepath, omode, &
                & MPI_INFO_NULL, ncid), 'nf90mpi_open: ')

        ! Get read variable id
        call self%check(nf90mpi_inq_varid(ncid, "matrix", varid), &
                "nf90mpi_inq_varid: ")

        ! ---------------------------------------
        !    Nonblocking Block Cyclic Reading
        ! ---------------------------------------
        
        ! Calculate position of first element read by process
        start = self%mpigrid%loc * self%bdim + 1

        ! Calculate distance between read blocks
        step = self%mpigrid%dims*self%bdim

        ! Calculate number of blocks belonging to process
        blocks = (self%gdim - start)/step + 1

        ! For all requests tracking
        allocate(requests(blocks(1)*blocks(2)))
        allocate(states(blocks(1)*blocks(2)))
        request = 0

        ! Cordinates of local matrix
        lstart = 1

        local_matrix = 0.0d+0
        buf(1, 1) = 1
        buf(2, 1) = 2
        buf(1, 2) = 3
        buf(2, 2) = 4

        ! Synchronous reading block by block
        do gcol = start(2), self%gdim(2), step(2)
            do grow = start(1), self%gdim(1), step(1)
                ! From which row/col start reading
                from(1) = grow
                from(2) = gcol

                ! How many rows/cols to read
                where ((self%gdim - from) < self%bdim)
                    ! If it is last block its dimensions are smaller
                    cnt = self%gdim - from + 1
                elsewhere
                    ! Just size of block
                    cnt = self%bdim
                end where

                ! Calculate local coordinates
                lstart = self%bdim*((from-1)/(self%bdim*self%mpigrid%dims)) + &
                        & mod(from-1, self%bdim) + 1

                request = request + 1

                print *, proc, sizeof(from), sizeof(cnt), buf(1:cnt(1),1:cnt(2))
                call self%check(nf90mpi_iget_var(ncid, varid, &
                        ! local_matrix(lstart(1):lstart(1) + cnt(1) - 1, &
                        ! lstart(2):lstart(2) + cnt(2) -1), &
                        buf, &
                        requests(request), from, cnt), "nf90mpi_iget_var")
                print *, proc, "requested fr", lstart, "to", lstart + cnt - 1
                exit
                ! exit

                ! call self%check(nf90mpi_wait_all(ncid, 1, req, st), &
                !         'In !!! '//proc//':')

                ! print '(A,I0,A,2(I2),A,99(F7.1))', proc//':', requests(request), 'From:', from, 'Matrix:', &
                !         local_matrix(lstart(1):lstart(1) + cnt(1) - 1, &
                !         lstart(2):lstart(2) + cnt(2) -1)

            end do
        end do

        print '(A,I2,A,99(I2:,","))', proc, request, ":", requests
        call self%check(nf90mpi_wait_all(ncid, request, requests, states), &
                'In nf90mpi_wait_all: ')
        print *, proc, "FINISHED"

        do i = 1, request
            if (states(i) .NE. NF90_NOERR) then
                print*, "Error: nonblocking write fails on request", &
                        i, ' ', nf90mpi_strerror(states(i))
            endif
        enddo

        ! Close file
        call self%check(nf90mpi_close(ncid), "nf90mpi_close:")

        print '(A,99(F7.1))', proc, buf

    end subroutine read_local_part
    
end module class_BC_Matrix




! subroutine BCReadNCDFMatrix(filepath)
!     ! Reads netcdf3 matrix with nonblocking fashion
!     ! (for MPI multi process reading) and distributes it with
!     ! block-cyclic distribution

!     character(len=20), intent(in) :: filepath

    
!     integer :: dimid(2)
!     integer :: ncid

!     omode = NF90_NOWRITE + NF90_64BIT_OFFSET
!     call check(nf90mpi_open(MPI_COMM_WORLD, filepath, omode, &
!             MPI_INFO_NULL, ncid), 'nf90mpi_open:')

!     !-------------------------------
!     !  READ dimensions of matrix
!     !------------------------------

!     call check(nf90mpi_inq_dimid(ncid, "x", dimid(1)), &
!             'In nf90mpi_inq_dimid x:')
!     call ckeck(nf90mpi_inquire_dimension(ncid, dimid(1), len=global_ny), &
!             'In nf90mpi_inq_dimlen: x')
!     call check(nf90mpi_inq_dimid(ncid, "y", dimid(1)), &
!             'In nf90mpi_inq_dimid y:')
!     call ckeck(nf90mpi_inquire_dimension(ncid, dimid(1), len=global_ny), &
!             'In nf90mpi_inq_dimlen: y')

!     err = nf90mpi_inq_varid(ncid, "var", varid)
!     call check(err, 'In nf90mpi_inq_varid: ')

    
!           ! G_NY must == NY * nprocs
!           ! myNY must == global_ny / nprocs

!           err = nf90mpi_inq_dimid(ncid, "Y", dimid(2))
!           call check(err, 'In nf90mpi_inq_dimid Y: ')
!           err = nf90mpi_inquire_dimension(ncid, dimid(2), len=global_nx)
!           call check(err, 'In nf90mpi_inq_dimlen: ')
!           ! global_nx must == NX

    
! end subroutine BCReadNCDFMatrix


! end module eigncdf


! program main
!     use eigncdf
!     implicit none

!     ! MPI vars
!     integer :: ierr, rank

!     ! Parameters from matrix file
!     character(len=20) :: filepath = 'test.nc'
!     integer(kind=MPI_OFFSET_KIND) :: rows, cols
!     integer :: ncid

!     call MPI_Init(ierr)
!     call MPI_Comm_rank(MPI_COMM_WORLD, rank, ierr)

!     call read_netcdfinfo(filepath, rows, cols)
!     print *, rows, cols
    
!     call MPI_Finalize(ierr)
! end program main
