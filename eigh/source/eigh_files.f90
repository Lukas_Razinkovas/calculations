!============================================
!   File reading and distribution routines
!============================================

module EighFiles
    use EighUtils

    type block_ptr
        ! Holds various coordinates of block-cyclic distributet matrix.
        ! Used to track location of sparse matrix elements and
        ! blocks during block-cyclic iteration done in DistributeMatrix
        ! subroutine.

        integer :: glob_row = -1  ! Element position in global matrix
        integer :: glob_col = -1

        integer :: loc_row = -1   ! Element position in local matrix
        integer :: loc_col = -1

        ! Block coordinates
        integer :: block_row = -1 ! Which block holds element
        integer :: block_col = -1

        ! Process coordinates
        integer :: proc_row = -1  ! Which process should get this element
        integer :: proc_col = -1

        ! Internal block coordinates
        integer :: loc_bl_row = -1 ! Element position in current block
        integer :: loc_bl_col = -1

        ! Process holding this pointer
        type(proc_descriptor) :: proc

        ! Can hold value (for sparse matrix reading)
        real(dp) :: val = -1
    end type block_ptr

contains

    !----------------------------
    !    Debuging routines
    !----------------------------

    subroutine print_block_ptr_info(bl_ptr)
        ! For debuging puprose. Prints curent state of block_ptr
        type(block_ptr), intent(in) :: bl_ptr
        character(80)               :: string

        write (string, '(A,(I0:,","),I0)') "Element ", &
              & bl_ptr%glob_row, bl_ptr%glob_col
        call print_header(string)
        print '(A,F7.2)', "Value:", bl_ptr%val
        print '(A,(I0:,","),I0)', "BelongsToProcess: ", &
              & bl_ptr%proc_row, bl_ptr%proc_col
        print '(A,(I0:,","),I0)', "BelongsToBlock: ", &
              & bl_ptr%block_row, bl_ptr%block_col
        print '(A,(I0:,","),I0)', "LocalCoordinates: ", &
              & bl_ptr%loc_row, bl_ptr%loc_col
        print '(A,(I0:,","),I0)', "LocalBlockCoordinates: ", &
              & bl_ptr%loc_bl_row, bl_ptr%loc_bl_col        
    end subroutine print_block_ptr_info

    !--------------------------------------------------------------------
    !  Obtaining Coordinates in Block Cyclycally distributed matrix
    !--------------------------------------------------------------------

    subroutine Get_element_coordinates(row, col, matrix_descriptor, el_ptr)
        ! Given global row and column, fills element pointer with various
        ! block cyclic coordinates

        integer, intent(in) :: row ! Row coordinate of global matrix
        integer, intent(in) :: col ! Collumn coordinate of global matrix
        integer, intent(in) :: matrix_descriptor(9) ! Blacs matrix descriptor
        type(block_ptr), intent(inout) &
              & :: el_ptr ! Stores info about relative positions in B-C matrix

        integer :: Numroc
        integer :: rows, cols
        integer :: global_rows, global_cols
        integer :: block_rows, block_cols

        el_ptr%glob_row = row
        el_ptr%glob_col = col

        ! Extract some info about matrix
        global_rows = matrix_descriptor(3)
        global_cols = matrix_descriptor(4)
        block_rows = matrix_descriptor(5)
        block_cols = matrix_descriptor(6)

        ! Computes local matrix coordinates
        call Infog2l(&
            & row, col, &
            & matrix_descriptor, &
            & el_ptr%proc%grid_rows, el_ptr%proc%grid_cols, &
            & el_ptr%proc_row, el_ptr%proc_col, &
            & el_ptr%loc_row, el_ptr%loc_col, &  ! Coordinates in local matrix
            & el_ptr%proc_row, el_ptr%proc_col)  !  owned by this process
        
        ! Calculate number of rows/columns of local matrix
        rows = Numroc(global_rows, block_rows, el_ptr%proc_row, &
              & 0, el_ptr%proc%grid_rows)
        cols = Numroc(global_cols, block_cols, el_ptr%proc_col, &
              & 0, el_ptr%proc%grid_cols)

        ! Calculate in which block this element resides
        el_ptr%block_row = ceiling(row/real(block_rows))
        el_ptr%block_col = ceiling(col/real(block_cols))


        ! Calculate relative block coordinates
        el_ptr%loc_bl_row = row - (el_ptr%block_row - 1)*block_rows
        el_ptr%loc_bl_col = col - (el_ptr%block_col - 1)*block_cols

        return
    end subroutine Get_element_coordinates

    subroutine Read_and_fill_ptr(fl, el_ptr, matrix_descriptor)
        ! Reads next value from fl filehandler and changes coordinates
        ! of el pointer

        integer, intent(in) :: fl                   ! Number of filehandler
        integer, intent(in) :: matrix_descriptor(9) ! Blacs matrix descriptor
        type(block_ptr), intent(inout) :: el_ptr    ! Pointer to be changed

        real(dp) :: val
        integer :: row, col

        read (fl, fmt=*) col, row, val

        el_ptr%val = val

        call Get_element_coordinates(row, col, matrix_descriptor, el_ptr)

        return
    end subroutine Read_and_fill_ptr

    function points_to_my_process(proc, coord) result(bool)
        type(proc_descriptor) :: proc
        type(block_ptr) :: coord
        logical :: bool
        bool = (coord%proc_row == proc%row .and. coord%proc_col == proc%col)
    end function points_to_my_process

    function is_equal(i, j) result(bool)
        integer :: i(2) ! input
        integer :: j(2) ! output
        logical :: bool
        bool = all(abs(i - j) < 1)
    end function is_equal

    function at_the_same_block_col(bl_ptr, el_ptr) result(bool)
        type(block_ptr) :: bl_ptr
        type(block_ptr) :: el_ptr
        logical :: bool

        bool = (bl_ptr%block_row == el_ptr%block_row .and. &
              & bl_ptr%block_col == el_ptr%block_col .and. &
              & bl_ptr%glob_col == el_ptr%glob_col)
    end function at_the_same_block_col

    subroutine GetMatrixInfo(filepath, context, block_dims, reader_coord, &
        & matrix_dims, skiprows, matrix_descriptor, &
        & matrix_format, local_matrix_dim, nnz)

        character(len=255), intent(in) :: filepath        ! Path to matrix file
        integer, intent(in)            :: context         ! MPI world context
        integer, intent(in)            :: block_dims(2)   ! Matrix slicing
        integer, intent(in)            :: reader_coord(2) ! Which process reads
        integer, intent(out)           :: matrix_dims(2)  ! Matrix dimension
        integer, intent(out)           :: skiprows        ! Nr of comment lines

        integer, intent(out) :: matrix_descriptor(9) ! Matrix descriptor
        integer, intent(out) :: local_matrix_dim(2)  ! Local matrix dimensions
        integer, intent(out) :: nnz                  ! Number of nonzero el
        character(len=3), optional :: matrix_format  ! Matrix format

        ! MPI/Blacs variables
        integer :: Numroc          ! Func for local matrix dim calculations
        integer :: info, ierr

        ! Internal variables
        character(len=90) :: cc        ! For comment reading
        integer :: workspace(4)        ! For data sharing between proc
        integer :: i                   ! Iteration variables
        logical :: i_am_reader         ! For readability purpose
        integer :: leading_dimmension  ! Basicaly number of rows

        type(proc_descriptor) :: proc

        ! Set default matrix format
        if (.not. present(matrix_format)) then
            matrix_format = 'dat'
        endif

        ! Get info about current process
        call Get_my_mpi_location(context, proc)

        !------------------------
        !  Count comment lines
        !------------------------

        if (proc%row == reader_coord(1) .and. proc%col == reader_coord(2)) then
            i_am_reader = .true.
        else
            i_am_reader = .false.
        end if

        if (i_am_reader) then
            open(10, file=trim(filepath), status='old')
            cc = '%'
            skiprows = -1
            do while(cc(1:1) == '%')
                skiprows = skiprows + 1
                read(10,'(A)') cc
            enddo
            close(10)
        end if

        !---------------------------
        !       Read header
        !---------------------------

        if (i_am_reader) then
            open(10, file=trim(filepath), status='old')
            ! Ignore comments
            do i = 1, skiprows
                read(10,'(A1)') cc
            enddo

            ! Read matrix specifications
            if (matrix_format == 'dat') read(10,*) matrix_dims
            if (matrix_format == 'mtx') read(10,*) matrix_dims, nnz
            close(10)
        end if

        !-----------------------------------------------------
        !  Distribute matrix dimensions allong all processes
        !-----------------------------------------------------

        if (i_am_reader) then
            workspace(:) = [matrix_dims(1), matrix_dims(2), nnz, skiprows]
            call Igebs2d(context, 'all',' ', 4, 1, workspace, 4)
        else
            call Igebr2d(context, 'all', ' ', 4, 1, workspace, 4, &
                & reader_coord(1), reader_coord(2))

            matrix_dims(1) = workspace(1)
            matrix_dims(2) = workspace(2)
            nnz = workspace(3)
            skiprows = workspace(4)
        end if

        if (i_am_reader) then
            call print_header('Matrix File Info')
            print '(A,I0)', 'Nr of comment lines: ', skiprows
            print '(A,A)', 'Matrix format: ', matrix_format
            print '(A,I0:,"x",I0)', 'Matrix dimensions: ', matrix_dims
            print *, ""
        end if

        !---------------------------------------------------------
        !  Create matrix descriptor used by BLACS MPI processes
        !---------------------------------------------------------

        ! Compute dimensions of local matrix owned by process
        local_matrix_dim(1) = Numroc(matrix_dims(1), block_dims(1), &
            & proc%row, 0, proc%grid_rows)
        local_matrix_dim(2) = Numroc(matrix_dims(2), block_dims(2), &
            & proc%col, 0, proc%grid_cols)

        leading_dimmension = local_matrix_dim(1)
        ! Create descriptor
        call Descinit(matrix_descriptor, matrix_dims(1), matrix_dims(2), &
            & block_dims(1), block_dims(2), &
            & 0, 0, context, leading_dimmension, info)

        if (i_am_reader) print '(A)', 'Initialised matrix descriptors:'

        call MPI_Barrier(MPI_COMM_WORLD, ierr)
        print "(A,999(I0:,', '))", proc_info(proc), matrix_descriptor

        return
    end subroutine GetMatrixInfo


    subroutine DistributeMatrix(filepath, context, block_dims, reader_coord, &
        & local_matrix, matrix_descriptor, matrix_format)

        character(len=255), intent(in) :: filepath        ! Path to matrix file
        integer, intent(in)            :: context         ! MPI world context
        integer, intent(in)            :: reader_coord(2) ! Which process reads
        integer, intent(in)            :: block_dims(2)   ! Matrix slicing

        real(dp), allocatable, intent(out) :: &
            & local_matrix(:,:)                  ! Matrix used by process
        integer, intent(out) :: matrix_descriptor(9) ! Global matrix descriptor
        character(len=3), optional :: matrix_format  ! Matrix format name

        ! MPI/Blacs variables
        integer :: ierr            ! Stores MPI error code
        integer :: matrix_dims(2)  ! Global matrix dimensions

        ! Local variables
        integer  :: local_matrix_dim(2) ! Dimmensions of matrix owned by proc
        integer  :: lda                 ! Leading dimensions of matrix
        logical  :: i_am_reader         ! For readability
        logical  :: this_is_my_part     ! For readability
        integer  :: skiprows            ! Number of comment lines
        integer  :: workspace_size      ! Size of data for send/receive
        character(len=90) :: cc         ! For comment reading
        real(dp) :: t1, t2              ! Time measurement variables

        real(dp), allocatable :: workspace(:)

        !-----------------------------------
        !    Block iteration variables
        !-----------------------------------

        integer :: proc_ptr(2)            ! Points to proc which holds cur block
        integer :: block_col_size         ! Width of current block
        integer :: block_row_size         ! Height of current block

        ! For dense matrices
        integer :: local_mtx_ptr(2) ! Points to last unassigned el in local mat


        ! For debuging purposes
        logical :: debug = .false.
        integer :: i

        ! States
        integer :: block_grid(2)

        type(proc_descriptor) :: proc
        type(block_ptr) :: bl_ptr
        type(block_ptr) :: el_ptr
        integer :: col, ii, jj, nnz, nr
        integer :: loc_row_start, loc_row_end, loc_col


        ! Set default matrix format
        if (.not. present(matrix_format)) then
            matrix_format = 'dat'
        endif

        ! Get process location in process grid
        call Get_my_mpi_location(context, proc)

        if (proc%row == reader_coord(1) .and. proc%col == reader_coord(2)) then
            i_am_reader = .true.
        else
            i_am_reader = .false.
        end if

        ! Workspace is never bigger than nr of rows in the block
        workspace_size = block_dims(1)

        call GetMatrixInfo(filepath, context, block_dims, reader_coord, &
            & matrix_dims, skiprows, matrix_descriptor, matrix_format, &
            local_matrix_dim, nnz)

        block_grid(1) = ceiling(matrix_dims(1)/dble(block_dims(1)))
        block_grid(2) = ceiling(matrix_dims(2)/dble(block_dims(2)))

        ! Print some info
        call MPI_Barrier(MPI_COMM_WORLD, ierr)
        if (i_am_reader) then
            print *, ""
            print '(A)', repeat('-', 70)
            print '(A,A)', repeat(' ', 20), 'Read And Distribute Matrix'
            print '(A)', repeat('-', 70)
            print '(A)', ""
            print '(A,A)', 'Reading matrix from: ', trim(filepath)
            print '(A,(I0:,", "),I0,A)', &
                & 'BLACS process (', reader_coord ,') is reading matrix'
            print '(A,(I0:, ", "),I0,A)', &
                & 'First el of matrix is stored in proc (',reader_coord,')'
            print '(A,(I0:,"x"),I0,A)', 'Matrix will be sliced into ', block_grid, ' blocks'
            print '(A,I0)', 'Allocated workspace of size: ', workspace_size
            print *, ''
        endif

        ! Allocate local matrix
        allocate(local_matrix(local_matrix_dim(1), local_matrix_dim(2)))
        if (i_am_reader) print '(A)', 'Allocated local matrices:'
        call MPI_Barrier(MPI_COMM_WORLD, ierr)
        print "(A,A,I0,A,I0)", proc_info(proc), 'dim ', &
              & local_matrix_dim(1), 'x', local_matrix_dim(2)
        allocate(workspace(workspace_size))
        call MPI_Barrier(MPI_COMM_WORLD, ierr)

        ! Ignore comment lines and matrix description
        if (i_am_reader) then
            print *, ""
            print '(A)', 'Ignoring rows:'
            open(11, file=trim(filepath), status='old')
            do i = 1, skiprows + 1
                read(11,'(A)') cc
                print '(A,A)', '>', cc
            end do
            print *, ""
        end if

        !=====================================
        !-------------------------------------
        !    This is where magic happens
        !-------------------------------------
        !=====================================

        ! left_vals = .false.

        ! We are starting from (0, 0) process
        proc_ptr = [0, 0]
        ! Points to first unassigned element of local matrix
        local_mtx_ptr = [1, 1]
        lda = matrix_descriptor(9)

        !-------------------------------------------------------
        !  Matrix will be read row-wise sending rows of blocks
        !-------------------------------------------------------

        bl_ptr%block_row = 0
        bl_ptr%block_col = 0
        bl_ptr%proc_row = reader_coord(1)
        bl_ptr%proc_col = reader_coord(2)
        bl_ptr%loc_row = 1
        bl_ptr%loc_col = 1
        bl_ptr%proc = proc

        el_ptr%proc = proc

        local_mtx_ptr = [1, 1]
        nr = 1

        if (i_am_reader) then
            print '(A)', 'Reading and distributing matrix...'
        end if
        ! Sync for time measurement
        call MPI_Barrier(MPI_COMM_WORLD, ierr)
        t1 = MPI_wtime()

        !---------------------------------------
        ! Iterate through first cols of blocks
        !---------------------------------------
        do jj = 1, matrix_dims(2), block_dims(2)
            bl_ptr%block_col = bl_ptr%block_col + 1
            block_col_size = min(block_dims(2), matrix_dims(2) - jj + 1)
            do col = jj, jj + block_col_size -1
                bl_ptr%glob_col = col
                !---------------------------------------
                ! Iterate through first rows of blocks
                !---------------------------------------
                do ii = 1, matrix_dims(2), block_dims(2)
                    bl_ptr%block_row = bl_ptr%block_row + 1
                    block_row_size = min(block_dims(1), matrix_dims(1) - ii + 1)
                    this_is_my_part = points_to_my_process(proc, bl_ptr)

                    !------------------------------
                    !   Sparse matrix reading
                    !------------------------------

                    workspace = 0.0d+0 ! Because matrix is sparse

                    if (i_am_reader) then
                        ! Read first element if non was read
                        if (el_ptr%block_row == -1) then
                            call Read_and_fill_ptr(11, el_ptr, matrix_descriptor)
                            if (debug) call print_block_ptr_info(el_ptr)
                            ! Count read elements
                            nr = nr + 1
                        end if

                        ! Read all elements from same block row
                        if (at_the_same_block_col(bl_ptr, el_ptr)) then
                            do
                                ! If element belongs to other process
                                if (.not. at_the_same_block_col(bl_ptr, el_ptr)) exit
                                workspace(el_ptr%loc_bl_row) = el_ptr%val

                                ! Stop if last element was read
                                if (nr > nnz) exit
                                call Read_and_fill_ptr(11, el_ptr, matrix_descriptor)
                                if (debug) call print_block_ptr_info(el_ptr)
                                ! Count read elements
                                nr = nr + 1
                            end do
                        end if
                    end if

                    ! Calculate positions in local matrix
                    loc_row_start = local_mtx_ptr(1)
                    loc_row_end = local_mtx_ptr(1) + block_row_size - 1
                    loc_col = local_mtx_ptr(2)

                    ! Read directly into my local matrix
                    if (this_is_my_part .and. i_am_reader) then
                        ! print '(A)', repeat('=', 70)
                        ! print '(A,A,999(F7.2))', proc_info(proc), 'received', workspace
                        local_matrix(loc_row_start:loc_row_end, loc_col) = &
                              & workspace(1: block_row_size)
                        ! print '(999(F7.2))', local_matrix
                        ! print '(A)', repeat('=', 70)
                        local_mtx_ptr(1) = local_mtx_ptr(1) + block_row_size

                    ! Send to dedicated process
                    else if (i_am_reader) then
                        call dgesd2d(context, block_row_size, 1, workspace, block_row_size, &
                            & bl_ptr%proc_row, bl_ptr%proc_col)

                    ! Receive and write into local matrix
                    else if (this_is_my_part) then
                        call dgerv2d(context, block_row_size, 1, &
                            & workspace, block_row_size, &
                            & reader_coord(1), reader_coord(2))
                        ! print '(A)', repeat('=', 70)
                        ! print '(A,A,999(F7.2))', proc_info(proc), 'received', workspace
                        local_matrix(loc_row_start:loc_row_end, loc_col) = &
                              & workspace(1: block_row_size)
                        ! print '(999(F7.2))', local_matrix
                        ! print '(A)', repeat('=', 70)
                        local_mtx_ptr(1) = local_mtx_ptr(1) + block_row_size
                    end if

                    ! Increase column if last row element for current process was read
                    if (this_is_my_part .and. local_matrix_dim(1) == loc_row_end) then
                        local_mtx_ptr(2) = local_mtx_ptr(2) + 1
                    end if

                    bl_ptr%proc_row = mod(bl_ptr%proc_row + 1, proc%grid_rows)
                end do
                local_mtx_ptr(1) = 1
                bl_ptr%proc_row = reader_coord(1)
                bl_ptr%block_row = 0
            end do
            ! All internal columns iterated
            bl_ptr%proc_col = mod(bl_ptr%proc_col + 1, proc%grid_cols)
        end do

        ! Sync for time measurement
        call MPI_Barrier(MPI_COMM_WORLD, ierr)
        t2 = MPI_wtime()

        if (i_am_reader) then
            print '(A,F17.2,A)', 'Matrix distribution finished in', t2 - t1, ' s'
        end if

        ! Print local matrices
        if (debug) then
            do ii = 0, proc%grid_rows
                do jj = 0, proc%grid_cols
                    call MPI_Barrier(MPI_COMM_WORLD, ierr)
                    if (proc%row == ii .and. proc%col == jj) then
                        print '(A)', repeat("=", 70)
                        print '(A,A)', 'I am proc ', proc_info(proc)
                        print '(A)', 'My Matrix:'
                        print *, ''

                        do i = 1, size(local_matrix, 1)
                            print '(999(F10.2))', local_matrix(i,:)
                        end do
                        print '(A)', repeat('-', 70)
                        ! print '(999(F10.2:,","))', local_matrix
                    end if
                    call MPI_Barrier(MPI_COMM_WORLD, ierr)
                end do
            end do
        end if

        return
    end subroutine DistributeMatrix

    !-----------------------------------
    !   Matrix collection and writing
    !-----------------------------------

    subroutine FillGlobalFromLocal(local_matrix, matrix_descriptor, &
            & lrows, lcols, target_proc,  global_matrix)

        real(dp), intent(in) :: local_matrix(:,:)
        integer, intent(in)  :: matrix_descriptor(9)
        type(proc_descriptor), intent(in) :: target_proc
        integer, intent(in) :: lrows
        integer, intent(in) :: lcols
        real(dp), intent(inout) :: global_matrix(:,:)

        
        integer :: nr, col, row
        integer :: grows, gcols, grow, gcol ! Global matrix parameters
        integer :: rblock_size, cblock_size ! Block size
        integer :: lrow, lcol               ! Row and col of local_matrix
        integer :: Indxl2g                  ! Scalapack function

        grows = matrix_descriptor(3)
        gcols = matrix_descriptor(4)
        rblock_size = matrix_descriptor(5)
        cblock_size = matrix_descriptor(6)

        ! if (target_proc%row == 1 .and. target_proc%col == 0) then
        !     print '(A)', "THIS:"
        !     do nr = 1, size(local_matrix, 1)
        !         print "(99(F7.1))", local_matrix(nr,:)
        !     end do
        ! end if

        do lcol = 1, lcols
            do lrow = 1, lrows
                grow = Indxl2g(lrow, rblock_size, target_proc%row, 0, &
                        & target_proc%grid_rows)
                gcol = Indxl2g(lcol, cblock_size, target_proc%col, 0, &
                        & target_proc%grid_cols)

                !------------------------------------------------------------
                ! Recalculate coordinates in case senders matrix was smaller
                !------------------------------------------------------------                

                nr = (lcol - 1) * lrows + lrow
                col = (nr - 1)/size(local_matrix, 1) + 1
                row = nr - (col - 1)*size(local_matrix, 1)

                ! if (target_proc%row == 1 .and. target_proc%col == 0) then
                !     print '(A,I0,A,I0)', 'LocalCoordinates:', lrow, ',', lcol
                !     print '(A,I0,A,I0)', 'GlobalCoordinates:', grow, ',', gcol
                !     print '(A,I0)', 'NR:', nr
                !     print '(A,I0,A,I0)', 'ReCoord:', row, ',', col
                !     print '(A,F7.1)', 'VAL:', local_matrix(row, col)
                !     print '(A)', repeat("-", 70)
                ! end if

                global_matrix(grow, gcol) = local_matrix(row, col)
            end do
        end do

        return
    end subroutine FillGlobalFromLocal
    
    
    subroutine CollectMatrix(context, local_matrix, matrix_descriptor, &
            & global_matrix)
        ! Collects distributed matrix to blacs process [0, 0]

        integer,  intent(in)  :: context
        real(dp), intent(inout)  :: local_matrix(:,:)
        integer,  intent(in)  :: matrix_descriptor(9)
        real(dp), allocatable, intent(out) :: global_matrix(:,:)

        type(proc_descriptor) :: proc         ! My proc descriptor
        type(proc_descriptor) :: target_proc  ! Target proc descriptor

        integer :: ierr
        integer :: ii, jj
        integer :: grows, gcols
        integer :: lrows, lcols
        integer :: rblock_size, cblock_size
        logical :: i_am_writer
        character(256) :: my_errmsg
        integer :: my_stat
        real(dp) :: t1

        integer :: numroc ! Scalapack function to calculate local rows

        grows = matrix_descriptor(3)
        gcols = matrix_descriptor(4)
        rblock_size = matrix_descriptor(5)
        cblock_size = matrix_descriptor(6)

        call Get_my_mpi_location(context, proc)
        target_proc = proc
        
        i_am_writer = .false.
        if (proc%row == 0 .and. proc%col == 0) then
            i_am_writer = .true.
            t1 = MPI_wtime()
        end if

        !-----------------------------------------------
        !   Allocate global matrix only for writer
        !-----------------------------------------------
        
        if (i_am_writer) then
            allocate(global_matrix(grows, gcols), stat=my_stat, errmsg=my_errmsg)
            global_matrix = 0.0d+0
            if (my_stat /= 0) then
                call AbortMPIFromRoot(context, "Failed to allocate global_array")
            end if
        end if
        !------------------------------------------------------------
        !  Iterate through each process and collect local matrices   
        !------------------------------------------------------------

        do ii = 0, proc%grid_rows - 1            
            do jj = 0, proc%grid_cols - 1
                target_proc%row = ii
                target_proc%col = jj
                
                ! Calculate how many rows in local matrix
                lrows = Numroc(grows, rblock_size, target_proc%row, &
                        & 0, proc%grid_rows)
                lcols = Numroc(gcols, cblock_size, target_proc%col, &
                        & 0, proc%grid_cols)

                if (i_am_writer) then
                    ! Receive local matrix
                    ! local_matrix = 0.0d+0
                    if (.not. same_proc(proc, target_proc)) then
                        call dgerv2d(context, lrows, lcols, local_matrix, &
                                lrows, target_proc%row, target_proc%col)
                    end if
                    print '(A,A,F12.2,A)', 'Received local matrix from ', &
                            & proc_info(target_proc)//' ', &
                            & MPI_wtime() - t1, ' s'
                    call FillGlobalFromLocal(local_matrix, &
                            & matrix_descriptor, &
                            & lrows, lcols, target_proc,  global_matrix)

                    ! if (i_am_writer) then 
                    !     do i = 1, grows
                    !         print '(99(F7.2))', global_matrix(i:)
                    !     end do
                    ! end if
                    
                else if (same_proc(proc, target_proc)) then
                    ! Send local matrix
                    call dgesd2d(context, lrows, lcols, local_matrix, &
                            lrows, 0, 0)
                end if
                call MPI_Barrier(MPI_COMM_WORLD, ierr)
            end do
        end do

        ! if (i_am_writer) then 
        !     do i = 1, grows
        !         print '(99(F7.2))', global_matrix(:,i)
        !     end do
        ! end if
        call MPI_Barrier(MPI_COMM_WORLD, ierr)
        
    end subroutine CollectMatrix
    
    
end module EighFiles
