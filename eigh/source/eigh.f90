!============================================
!               Main Program
!============================================

program eigh
    use EighUtils
    use class_CMD_Options
    use class_BLACS_Grid
    use class_EigHDF5File
    use class_BC_Matrix

    implicit none
    real(dp), parameter :: version = 0.2
    
    !---------------------
    !    Declarations
    !---------------------

    ! Helper Objects
    type(blacs_grid)  :: mpigrid
    type(cmd_options) :: cmd_opt
    type(eighdf5file) :: matrix_file
    type(eighdf5file) :: results_file

    ! MPI/Blacs variables
    integer :: error, info

    ! Matrices
    type(bcmatrix) :: matrixA
    type(bcmatrix) :: matrixB
    type(bcmatrix) :: matrixC
    real(dp), allocatable :: A(:,:) ! Hessian
    real(dp), allocatable :: B(:,:) ! Inverse Square Mass Matrix
    real(dp), allocatable :: C(:,:) ! Eigenvectors Matrix
    
    ! Vectors
    real(dp), allocatable :: evals(:)
    real(dp) :: evalsfactor(1)

    ! Other vars
    logical  :: i_am_root
    real(dp) :: t1

    ! ---------------------------
    !    Initialise BLACS grid
    ! ---------------------------

    call mpigrid%init()
    i_am_root = mpigrid%isroot()
    if (i_am_root) call print_version(version)
    call mpigrid%print_grid_info()

    ! ----------------------------------
    !    Collect command line options
    ! ----------------------------------

    call cmd_opt%collect(mpigrid)
    call MPI_Barrier(MPI_COMM_WORLD, error)

    ! -----------------------------
    !       Read HDF5 Matrix
    ! -----------------------------

    if (i_am_root) call print_header("READING", 1)
    call MPI_Barrier(MPI_COMM_WORLD, error)
    t1 = MPI_wtime()

    call matrix_file%init(mpigrid, filepath=cmd_opt%filepath, bdims=cmd_opt%bdims)

    ! Open HDF5 file and dataset
    call matrix_file%open_file("r")
    call matrix_file%open_dataset('hessian')
    call matrix_file%allocate_and_read(matrixA, A, 'hessian')
    ! Read attributes
    if (matrix_file%attribute_exists('freq_factor')) then
        call matrix_file%read_attribute('freq_factor', evalsfactor)
    else
        evalsfactor = 1
    end if

    call matrix_file%close_dataset()
    call matrix_file%open_dataset('inverse_sqr_mass_matrix')
    call matrix_file%allocate_and_read(matrixB, B, 'inv_mass')
    call matrix_file%close_dataset()
    call matrix_file%close_file()

    if (i_am_root) then
        print *, ""
        print '(A,F12.3,A)', "Reading finished in ", MPI_wtime() - t1, " s"
    end if

    ! -----------------------------
    !   Multiply With Mass Matrix
    ! -----------------------------    

    call MPI_Barrier(MPI_COMM_WORLD, error)
    if (i_am_root) call print_header(&
            "Multiplying Hessian with Inverse Square Mass Matrix ", 1)
    call MPI_Barrier(MPI_COMM_WORLD, error)
    t1 = MPI_wtime()

    call matrixC%init(matrixA%gdims, matrixA%bdims, mpigrid, C, 'results')
    call matrixB%multiply(B, matrixA, A, matrixC, C)
    if (i_am_root) print '(A,F12.3,A)', " - MH = MxH finished in ", &
            MPI_wtime() - t1, " s"
    t1 = MPI_wtime()
    call matrixC%multiply(C, matrixB, B, matrixA, A)
    if (i_am_root) print '(A,F12.3,A)', " - MHM = MHxM finished in ", &
            MPI_wtime() - t1, " s"

    ! -----------------------------
    !         Diagonalise
    ! -----------------------------

    call MPI_Barrier(MPI_COMM_WORLD, error)
    if (i_am_root) call print_header("Diagonalising", 1)
    call MPI_Barrier(MPI_COMM_WORLD, error)
    t1 = MPI_wtime()
    
    call allocate_and_print_info(evals, matrixA%gdims(1), &
            " - "//mpigrid%proc_info(), error, "eigvect")

    call matrixA%diagonalise(A, matrixC, C, evals)

    if (i_am_root) then
        print *, ""
        print '(A,F12.3,A)', "Diagonalisation finished in ", &
                MPI_wtime() - t1, " s"
    end if

    ! -----------------------------
    !       Multiply Results
    ! -----------------------------

    call MPI_Barrier(MPI_COMM_WORLD, error)
    if (i_am_root) call print_header(&
            "Multiplying Eigenvectors with Inverse Square Mass Matrix ", 1)
    call MPI_Barrier(MPI_COMM_WORLD, error)
    t1 = MPI_wtime()

    call matrixB%multiply(B, matrixC, C, matrixA, A)
    if (i_am_root) print '(A,F12.3,A)', " - Multiplication finished in ", &
            MPI_wtime() - t1, " s"

    where (evals < 0)
        evals = -1*evals
    end where    
    evals = sqrt(evals)*evalsfactor(1)

    call MPI_Barrier(MPI_COMM_WORLD, error)
    if (i_am_root) call print_header(&
            "Writing Results", 1)
    call MPI_Barrier(MPI_COMM_WORLD, error)
    t1 = MPI_wtime()

    call results_file%init(mpigrid, matrix=matrixA, filepath=cmd_opt%outputf)
    call results_file%create_file()
    call results_file%write_matrix(A, 'displacements')
    call results_file%write_nondistributed_vector(&
            evals, 'frequencies')
    call results_file%close_file()

    if (i_am_root) print '(A,F12.3,A)', " - Writing finished in ", &
            MPI_wtime() - t1, " s"

    
    if (i_am_root) print *, "" 
    call mpigrid%finish()

end PROGRAM EIGH
