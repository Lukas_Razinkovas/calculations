! HDF5 FILE READER AND WRITER
! ===========================
!
! HDF5 file is a container for storing a variety of scientific data and is
! composed of two primary types of objects: groups and datasets.
!
! - HDF5 group: a grouping structure containing zero or more HDF5 objects,
!   together with supporting metadata
! - HDF5 dataset: a multidimensional array of data elements, together with
!   supporting metadata
!
! Creating HDF5 File
! ------------------
!
! To create an HDF5 file, we need to specify:
!
! * filepath
! * file access mode,
! * file creation property list,
! * file access property list.
!
! Writing Chuncked Data to File
! -----------------------------
!
! The offset, mycount, stride and block parameters of this API define
! the shape and size of the selection
!
! offset: specifies the offset of the starting element of the hyperslab
! mycount: determines how many blocks to select from the dataspace in
!        each dimension
! stride: allows you to sample elements along a dimension.
!         Stride of one will select every element along a dimension,
!         a stride of two will select every other element
! block: determines the size of the element block selected from a
!        dataspace. If the block size is one then the block
!        size is a single element in that dimension.


module class_EigHDF5File
    use hdf5
    use EighUtils
    use class_BLACS_Grid
    implicit none

    integer, parameter :: max_datasets = 10

    type :: eighdf5file

        ! ----------------
        !    Atributes
        ! ----------------
        type(blacs_grid), public :: mpigrid
        character(len=256), public  :: filepath
        logical,            public  :: root
        integer,            public  :: datasets = 0
        integer(hid_t),     private :: access_prp
        integer(hid_t),     private :: file_id
        integer(hsize_t),   private :: bdims(2)
        integer(hid_t),     private :: memspace(max_datasets)
        integer(hid_t),     private :: filespace(max_datasets)
        integer(hid_t),     private :: chunk_prp(max_datasets)
        integer(hid_t),     private :: dset_id(max_datasets)
        integer(hid_t),     private :: plist_id(max_datasets)
        integer(hsize_t),   private :: gdims(max_datasets, 2)
        integer(hsize_t),   private :: ldims(max_datasets, 2)
        logical,            private :: data_written(max_datasets) = .false.

    contains

        ! ----------------
        !     Methods
        ! ----------------

        procedure, public :: init
        procedure, public :: create_file
        procedure, public :: close_file
        procedure, public :: create_chunked_dataset
        procedure, public :: write_chuncked_data
        procedure, public :: write_matrix
        procedure, public :: close_dataset
        procedure, public :: local_dimensions => get_local_dimensions

    end type eighdf5file

contains

    ! CONSTRUCTOR
    subroutine init(self, filepath, mpigrid, bdims)
        class(eighdf5file) :: self
        type(blacs_grid),   intent(in) :: mpigrid
        character(len=256), intent(in) :: filepath
        integer(hsize_t),   intent(in) :: bdims(2)

        self%filepath = filepath
        self%mpigrid = mpigrid
        self%root = mpigrid%isroot()
        self%bdims = bdims

    end subroutine init

    function get_local_dimensions(self, dsetnr) result(ldims)
        class(eighdf5file) :: self
        integer            :: dsetnr
        integer(hsize_t)   :: ldims(2) ! Dimension of local matrix

        ldims = self%ldims(dsetnr,:)
    end function get_local_dimensions

    ! ----------------------------------------
    !    FILE CREATION READING AND CLOSING
    ! ----------------------------------------

    ! CREATE FILE
    subroutine create_file(self)
        class(eighdf5file) :: self
        integer :: error

        if (self%root) print '(A)', 'Creating HDF5 file: '//trim(self%filepath)

        ! initialize hdf5 library and fortran interfaces
        call h5open_f(error)

        ! Create new file access property list access_prp
        call h5pcreate_f(H5P_FILE_ACCESS_F, self%access_prp, error)

        ! Stores MPI IO communicator information to the access_prp
        call h5pset_fapl_mpio_f(self%access_prp, MPI_COMM_WORLD, &
                MPI_INFO_NULL, error)

        ! Create file and store file creation property list file_id
        call h5fcreate_f(self%filepath, H5F_ACC_TRUNC_F, self%file_id, error, &
                access_prp=self%access_prp)

        ! Terminate access to file access property list
        call h5pclose_f(self%access_prp, error)

    end subroutine create_file

    ! CLOSE FILE
    subroutine close_file(self)
        class(eighdf5file) :: self
        integer :: error

        if (self%root) print '(A)', 'Closing file: '//trim(self%filepath)

        ! close the file.
        call h5fclose_f(self%file_id, error)
        ! close fortran interfaces and hdf5 library.
        call h5close_f(error)

    end subroutine close_file

    ! ---------------------------------
    !         DATASET CREATION
    ! ---------------------------------

    ! CREATE DATASET FOR CHUNCKED DATA WRITING
    subroutine create_chunked_dataset(self, name, gdims, bdims, dsetnr)
        ! Creates dataspaces and dataset for chuncked writtig of 2D matrix
        class(eighdf5file) :: self

        character(len=*), intent(in)  :: name       ! Dataset name
        integer(hsize_t), intent(in)  :: gdims(2)   ! Dimension of full matrix
        integer(hsize_t), intent(in)  :: bdims(2)   ! Dimension of block
        integer,          intent(out) :: dsetnr     ! Dataset identifier
        integer(hsize_t)              :: ldims(2)   ! Dimension of local matrix
        integer                       :: Numroc ! Scalapack routine
        integer                       :: error, i

        ! Keep track of all defined datasets
        self%datasets = self%datasets + 1
        dsetnr = self%datasets
        self%gdims(dsetnr,:) = gdims
        self%bdims = bdims

        do i = 1, 2
            ldims(i) = Numroc(gdims(i), bdims(i), &
                    self%mpigrid%loc(i), 0, self%mpigrid%dims(i))
        end do
        self%ldims(dsetnr,:) = ldims

        if (self%root) print '(A,I0)', "Creating '"//trim(name)// &
                "' dataset with id = ", dsetnr

        call MPI_Barrier(MPI_COMM_WORLD, error)
        print '(A,I0,A,I0)', ' -'// trim(self%mpigrid%proc_info())//' ldim = ',&
                ldims(1), 'x', ldims(2)

        call MPI_Barrier(MPI_COMM_WORLD, error)

        ! Create dataspace identifier for whole file
        call h5screate_simple_f(2, gdims, self%filespace(dsetnr), error)

        ! Create dataspace identifier for local part of MPI process
        call h5screate_simple_f(2, ldims, self%memspace(dsetnr), error)

        ! Create new property list for chunck writing
        call h5pcreate_f(H5P_DATASET_CREATE_F, self%chunk_prp(dsetnr), error)

        ! Sets the size of chunk and fill chunck property list
        call h5pset_chunk_f(self%chunk_prp(dsetnr), 2, ldims, error)

        ! Creates a new dataset and link it to a location in the file
        call h5dcreate_f(self%file_id, name, H5T_NATIVE_DOUBLE, &
                self%filespace(dsetnr), self%dset_id(dsetnr), error, &
                self%chunk_prp(dsetnr))

        ! Terminate access to the property list
        call h5sclose_f(self%filespace(dsetnr), error)

        ! print '(A)', "Dataset '"//trim(name)//"' created"

        call MPI_Barrier(MPI_COMM_WORLD, error)

    end subroutine create_chunked_dataset

    ! ---------------------------------
    !             Writing
    ! ---------------------------------

    subroutine write_chuncked_data(self, dsetnr, data, offset, mycount, &
            stride, myblock)
        class(eighdf5file) :: self

        integer,           intent(in) :: dsetnr
        real(dp),          intent(in) :: data(:,:)
        integer(hssize_t), intent(inout) :: offset(2)
        integer(hsize_t),  intent(inout) :: mycount(2)
        integer(hsize_t),  intent(inout) :: stride(2)
        integer(hsize_t),  intent(inout) :: myblock(2)
        

        integer :: error
        integer(hsize_t) :: gdims(2)

        integer(hsize_t) :: start(2)
        integer(hsize_t) :: step(2)
        integer(hsize_t) :: blocks(2)
        integer(hsize_t) :: gcol, grow
        integer :: i
        logical :: first = .true.
        
        gdims = self%gdims(dsetnr, :)
        self%data_written(dsetnr) = .true.

        ! Return an identifier for a copy of the dataspace for a dataset
        call h5dget_space_f(self%dset_id(dsetnr), self%filespace(dsetnr), error)

        ! ---------------------------------------------
        !   Adaptation for Block Cyclic distribution
        ! ---------------------------------------------
        
        ! Calculate position of first element read by process
        start = self%mpigrid%loc * self%bdims + 1

        ! Calculate distance between read blocks
        step = self%mpigrid%dims*self%bdims

        ! Calculate number of blocks belonging to process
        blocks = (gdims - start)/step + 1

        do i = 0, self%mpigrid%size - 1
            if (i == self%mpigrid%rank) then
                do gcol = start(2), gdims(2), step(2)
                    do grow = start(1), gdims(1), step(1)
                        offset(1) = grow
                        offset(2) = gcol
                        where ((gdims - offset) < self%bdims)
                            myblock = gdims - offset + 1
                        elsewhere
                            myblock = self%bdims
                        end where
                        offset = offset - 1
                        print *, "--------------------------"
                        print '(A,2(I0:,", "))', trim(self%mpigrid%proc_info())//' offset:  ', offset
                        print '(A,2(I0:,", "))', trim(self%mpigrid%proc_info())//' myblock:  ', myblock
                        mycount = 1
                        stride = 4
                        if (first) then
                            ! Select subset of dataspace
                            call h5sselect_hyperslab_f(self%filespace(dsetnr), H5S_SELECT_SET_F, &
                                    offset, mycount, error, stride, myblock)
                            first = .false.
                        else
                            call h5sselect_hyperslab_f(self%filespace(dsetnr), H5S_SELECT_XOR_F, &
                                    offset, mycount, error, stride, myblock + 1)
                        end if
                    end do
                end do
            end if
            call MPI_Barrier(MPI_COMM_WORLD, error)
            print *, "==================="
        end do

        ! call h5sget_select_elem_npoints_f(self%filespace(dsetnr), numpoints, error)
        ! print *, self%mpigrid%proc_info(), numpoints

        ! do i = 1, 2
        !     if (loffset(i) /= -1) then
        !         aoffset = offset
        !         aoffset(i) = loffset(i)
        !         acount = mycount
        !         acount(i) = 1
        !         ablock = self%bdims
        !         ablock(i) = ldims(i)

        !         call h5sselect_hyperslab_f(self%filespace(dsetnr), H5S_SELECT_NOTA_F, &
        !                 aoffset, acount, error, stride, ablock)
        !     end if
        ! end do

        ! if (all(loffset /= -1)) then
        !     acount = [1, 1]
        !     call h5sselect_hyperslab_f(self%filespace(dsetnr), H5S_SELECT_NOTA_F, &
        !             loffset, acount, error, stride, ldims)
        ! end if

        print *, self%mpigrid%proc_info()
        call MPI_Barrier(MPI_COMM_WORLD, error)


        ! Create property list for collective dataset write
        call h5pcreate_f(H5P_DATASET_XFER_F, self%plist_id(dsetnr), error)


        call h5pset_dxpl_mpio_f(self%plist_id(dsetnr), H5FD_MPIO_COLLECTIVE_F, &
                error)

        ! write the dataset collectively
        call h5dwrite_f(self%dset_id(dsetnr), H5T_NATIVE_DOUBLE, data, &
                gdims, error, &
                file_space_id=self%filespace(dsetnr), &
                mem_space_id=self%memspace(dsetnr), &
                xfer_prp=self%plist_id(dsetnr))

        print '(I3,I3)', 5, error

    end subroutine write_chuncked_data

    subroutine write_matrix(self, dsetnr, matrix)
        class(eighdf5file)   :: self
        integer,  intent(in) :: dsetnr
        real(dp), intent(in) :: matrix(:,:)
        integer(hsize_t)     :: offset(2)
        integer(hsize_t)     :: mycount(2)
        integer(hsize_t)     :: stride(2)
        integer(hsize_t)     :: bdims(2)
        integer(hsize_t)     :: gdims(2)
        integer(hsize_t)     :: ldims(2)
        integer :: i, er, j

        integer(hsize_t)     :: loffset(2)
        integer(hsize_t)     :: lbdim(2)
        integer(hsize_t)     :: lmycount(2)

        bdims = self%bdims
        gdims = self%gdims(dsetnr, :)
        ldims = self%ldims(dsetnr, :)

        offset = self%mpigrid%loc * bdims
        stride = self%mpigrid%dims*bdims
        mycount = (gdims - offset - 1)/stride + 1

        ! stride = [4, 4]
        ! mycount = [1, 1]

        call self%write_chuncked_data(dsetnr, matrix, offset, mycount, &
                stride, bdims)

    end subroutine write_matrix


    ! --------------------------
    !      Dataset Closing
    ! --------------------------

    subroutine close_dataset(self, dsetnr)
        class(eighdf5file) :: self

        integer, intent(in) :: dsetnr
        integer :: error

        if (self%root) print '(A,I0)', 'Closing dataset: id = ', dsetnr

        ! close dataspaces.
        if (self%data_written(dsetnr)) call h5sclose_f(self%filespace(dsetnr), &
                error)

        call h5sclose_f(self%memspace(dsetnr), error)

        ! close the dataset
        call h5dclose_f(self%dset_id(dsetnr), error)

        ! close the property list.
        call h5pclose_f(self%plist_id(dsetnr), error)

    end subroutine close_dataset

end module class_EigHDF5File



! program dataset_by_chunk
!     use class_EigHDF5File

!     type(eighdf5file) :: myhdf5file

!     integer(hsize_t), dimension(2) :: gdims(2) = [5, 5]
!     integer(hsize_t), dimension(2) :: ldims(2)
!     integer                        :: dsetnr
!     real(dp), allocatable          :: data(:,:)
!     integer(hssize_t)              :: offset(2)
!     integer(hsize_t)               :: mycount(2)
!     integer(hsize_t)               :: stride(2)
!     integer(hsize_t)               :: myblock(2) = [2, 2]
!     character(len=256)             :: filepath = "test.hdf5"

!     ! MPI variables
!     integer :: mpierror
!     integer :: comm, info
!     integer :: mpi_size, mpi_rank

!     ! Set defaut MPI values
!     comm = mpi_comm_world
!     info = mpi_info_null

!     ! Initialise MPI environment
!     call MPI_INIT(mpierror)
!     call MPI_COMM_SIZE(comm, mpi_size, mpierror)
!     call MPI_COMM_RANK(comm, mpi_rank, mpierror)

!     ! quit if mpi_size is not 4
!     if (mpi_size .ne. 4) then
!         write(*,*) 'this example is set up to use only 4 processes'
!         write(*,*) 'quitting....'
!         call MPI_FINALIZE(mpierror)
!         stop
!     end if

!     ! Connecting and creating file, dataset
!     call myhdf5file%init(filepath)
!     call myhdf5file%create_file()
!     call myhdf5file%create_chunked_dataset('matrix', gdims, ldims, dsetnr)

!     stride(1) = 1
!     stride(2) = 1
!     mycount(1) = 1
!     mycount(2) = 1
!     myblock(1) = ldims(1)
!     myblock(2) = ldims(2)
!     if (mpi_rank == 0) then
!         offset(1) = 0
!         offset(2) = 0
!     endif
!     if (mpi_rank == 1) then
!         offset(1) = ldims(1)
!         offset(2) = 0
!     endif
!     if (mpi_rank == 2) then
!         offset(1) = 0
!         offset(2) = ldims(2)
!     endif
!     if (mpi_rank == 3) then
!         offset(1) = ldims(1)
!         offset(2) = ldims(2)
!     endif

!     allocate(data(ldims(1), ldims(2)))
!     data = mpi_rank

!     call myhdf5file%write_chuncked_data(dsetnr, data, offset, mycount, &
!             stride, myblock)


!     call MPI_FINALIZE(mpierror)

! end program dataset_by_chunk
