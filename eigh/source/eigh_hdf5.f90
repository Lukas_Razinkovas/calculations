! HDF5 FILE READER AND WRITER
! ===========================
!
! HDF5 file is a container for storing a variety of scientific data and is
! composed of two primary types of objects: groups and datasets.
!
! - HDF5 group: a grouping structure containing zero or more HDF5 objects,
!   together with supporting metadata
! - HDF5 dataset: a multidimensional array of data elements, together with
!   supporting metadata
!
! Creating HDF5 File
! ------------------
!
! To create an HDF5 file, we need to specify:
!
! * filepath
! * file access mode,
! * file creation property list,
! * file access property list.
!
! Writing Chuncked Data to File
! -----------------------------
!
! The offset, mycount, stride and block parameters of this API define
! the shape and size of the selection
!
! offset: specifies the offset of the starting element of the hyperslab
! mycount: determines how many blocks to select from the dataspace in
!        each dimension
! stride: allows you to sample elements along a dimension.
!         Stride of one will select every element along a dimension,
!         a stride of two will select every other element
! block: determines the size of the element block selected from a
!        dataspace. If the block size is one then the block
!        size is a single element in that dimension.


module class_EigHDF5File
    use EighUtils
    use class_BLACS_Grid
    use class_BC_Matrix
    implicit none

    type :: eighdf5file

        ! ----------------
        !    Atributes
        ! ----------------
        type(blacs_grid),   public  :: mpigrid
        character(len=256), public  :: filepath
        logical,            public  :: root
        integer(hsize_t),   public  :: ldims(2)
        integer(hid_t),     private :: file_id
        integer(hid_t),     private :: dset_id
        integer(hid_t),     private :: plist_id
        integer(hsize_t),   public  :: bdims(2)
        integer(hsize_t),   public  :: gdims(2)
        logical,            private :: data_written = .false.

    contains

        ! ----------------
        !     Methods
        ! ----------------
        
        procedure, public :: init
        procedure, public :: create_file
        procedure, public :: open_file
        procedure, public :: close_file
        procedure, public :: open_dataset
        procedure, public :: create_chunked_dataset
        procedure, public :: select_block_cyclic_hyperslab
        procedure, public :: write_data
        procedure, public :: read_data
        procedure, public :: write_matrix
        procedure, public :: close_dataset
        procedure, public :: attribute_exists
        procedure, public :: read_attribute
        procedure, public :: write_nondistributed_vector
        procedure, public :: allocate_and_read

    end type eighdf5file

contains
    
    ! -------------------------
    !           INIT
    ! -------------------------

    subroutine init(self, mpigrid, matrix, filepath, bdims)
        class(eighdf5file)             :: self
        type(blacs_grid),   intent(in) :: mpigrid
        type(bcmatrix),     optional   :: matrix        
        character(len=256), optional   :: filepath
        integer,            optional   :: bdims(2)

        self%mpigrid = mpigrid
        self%root = mpigrid%isroot()

        if (present(bdims)) then
            self%bdims = bdims
        end if

        if (present(matrix)) then
            call matrix%getdims(self%ldims, self%gdims, self%bdims)
        end if

        if (present(filepath)) then
            self%filepath = filepath
        end if
        
    end subroutine init

    ! ----------------------
    !      CREATE FILE
    ! ----------------------

    subroutine create_file(self)
        class(eighdf5file) :: self
        integer            :: error
        integer(hid_t)     :: access_prp

        if (self%root) write(*, '(A)', advance="no") 'Creating HDF5 file: '&
                //trim(self%filepath)

        ! initialize hdf5 library and fortran interfaces
        call h5open_f(error)

        ! Create new file access property list access_prp
        call h5pcreate_f(H5P_FILE_ACCESS_F, access_prp, error)

        ! Stores MPI IO communicator information to the access_prp
        call h5pset_fapl_mpio_f(access_prp, MPI_COMM_WORLD, &
                MPI_INFO_NULL, error)

        ! Create file and store file creation property list file_id
        call h5fcreate_f(self%filepath, H5F_ACC_TRUNC_F, self%file_id, error, &
                access_prp=access_prp)

        ! Terminate access to file access property list
        call h5pclose_f(access_prp, error)

        if (self%root) print '(A)', " .. OK"
        call MPI_Barrier(MPI_COMM_WORLD, error)

    end subroutine create_file

    ! ------------------------
    !       OPEN FILE
    ! ------------------------
    
    subroutine open_file(self, mode)
        class(eighdf5file) :: self
        character(len=1)   :: mode
        integer            :: error
        integer(hid_t)     :: access_prp
        

        if (self%root) write(*, '(A)', advance="no") 'Opening HDF5 file: '&
                //trim(self%filepath)

        ! initialize hdf5 library and fortran interfaces
        call h5open_f(error)

        ! Create new file access property list access_prp
        call h5pcreate_f(H5P_FILE_ACCESS_F, access_prp, error)

        ! Stores MPI IO communicator information to the access_prp
        call h5pset_fapl_mpio_f(access_prp, MPI_COMM_WORLD, &
                MPI_INFO_NULL, error)

        if (mode == "r") then
            call h5fopen_f(self%filepath, H5F_ACC_RDONLY_F, self%file_id, &
                    error, access_prp=access_prp)
        else
            call h5fopen_f(self%filepath, H5F_ACC_RDWR_F, self%file_id, &
                    error, access_prp=access_prp)
        end if

        if (error /= 0) then
            print '(A)', "ERROR! Could not open file:" // self%filepath
            call self%mpigrid%terminate()
        end if
        

        ! Terminate access to file access property list
        call h5pclose_f(access_prp, error)

        if (self%root) print '(A)', " .. OK"
    end subroutine open_file

    ! ------------------------
    !       CLOSE FILE
    ! ------------------------
    
    subroutine close_file(self)
        class(eighdf5file) :: self
        integer :: error

        if (self%root) write(*, '(A)', advance="no") 'Closing file'

        ! close the file.
        call h5fclose_f(self%file_id, error)
        ! close fortran interfaces and hdf5 library.
        call h5close_f(error)

        if (self%root) print '(A)', " .. OK"
        call MPI_Barrier(MPI_COMM_WORLD, error)
        
    end subroutine close_file

    ! ---------------------------------
    !          OPEN DATASET
    ! ---------------------------------

    subroutine open_dataset(self, dsetname)
        class(eighdf5file)              :: self
        character(len=*), intent(in)    :: dsetname
        integer(hid_t)                  :: dataspace
        integer                         :: error
        integer                         :: dims
        integer(hsize_t)                :: maxdims(2)

        if (self%root) print '(A)', 'Opening dataset "'//trim(dsetname)//'"'

        ! Open dataset
        call h5dopen_f(self%file_id, dsetname, self%dset_id, error)

        ! Get datspace identifier
        call h5dget_space_f(self%dset_id, dataspace, error)

        ! Get dimensionality in dataspace
        call h5sget_simple_extent_ndims_f(dataspace, dims, error)
        if (self%root) print '(A,I0)', ' - number of dimensions = ', dims

        ! Get dimensions 
        call h5sget_simple_extent_dims_f(dataspace, self%gdims, maxdims, error)
        if (self%root) print '(A,(I0:,"x"),I0)', ' - dimensions of matrix = ', self%gdims
        if (self%root) print '(A,(I0:,"x"),I0)', ' - maxdims = ', maxdims
        call h5sclose_f(dataspace, error)

        call MPI_Barrier(MPI_COMM_WORLD, error)
    end subroutine open_dataset

    ! ---------------------------------
    !         CREATE DATASET
    ! ---------------------------------

    subroutine create_chunked_dataset(self, name, chunk_size, gdims)
        ! Creates dataspaces and dataset for chuncked writtig of 2D matrix
        class(eighdf5file) :: self

        character(len=*), intent(in)  :: name          ! Dataset name
        integer(hsize_t), intent(in)  :: chunk_size(2) ! Chunck dimensions
        integer(hsize_t), intent(in)  :: gdims(2)
        integer(hid_t)                :: chunk_prp
        integer(hid_t)                :: filespace
        integer                       :: error
        
        if (self%root) write(*, '(A)', advance='no') &
                "Creating '"//trim(name)//"' dataset"

        ! Create dataspace identifier for whole file
        call h5screate_simple_f(2, gdims, filespace, error)
        ! Create new property list for chunck writing
        call h5pcreate_f(H5P_DATASET_CREATE_F, chunk_prp, error)
        ! Fill chunck property pointer
        call h5pset_chunk_f(chunk_prp, 2, chunk_size, error)
        ! Creates a new dataset and link it to a location in the file
        call h5dcreate_f(&
                self%file_id, &         ! (in)  file pointer
                name, &                 ! (in)  new dataset name
                H5T_NATIVE_DOUBLE, &    ! (in)  saved datatype
                filespace, &            ! (in)  file dataspace
                self%dset_id, &         ! (out) dataset id
                error, &                ! (out) error flag
                chunk_prp)              ! (in)  dataset creation properties

        ! Terminate access to the property lists
        call h5sclose_f(filespace, error)
        call h5pclose_f(chunk_prp, error)

        if (self%root) print *, " .. OK"
        call MPI_Barrier(MPI_COMM_WORLD, error)

    end subroutine create_chunked_dataset

    ! ---------------------------
    !       Close Dataset
    ! ---------------------------

    subroutine close_dataset(self)
        class(eighdf5file) :: self
        integer            :: error
        integer(hid_t)     :: filespace

        call MPI_Barrier(MPI_COMM_WORLD, error)
        if (self%root) write(*, '(A)', advance='no') &
                "Closing dataset"

        ! close dataspaces.
        if (self%data_written) then
            ! Returns an identifier for a copy of the dataspace for a dataset
            call h5dget_space_f(self%dset_id, filespace, error)
            call h5sclose_f(filespace, error)
        end if

        ! close the dataset
        call h5dclose_f(self%dset_id, error)
        if (self%root) print '(A)', ' .. OK'
        call MPI_Barrier(MPI_COMM_WORLD, error)

    end subroutine close_dataset

    ! ---------------------------------
    !      Selecting BC hyperslab
    ! ---------------------------------

    subroutine select_block_cyclic_hyperslab(self, filespace, gdims)
        class(eighdf5file)            :: self
        integer(hid_t), intent(inout) :: filespace
        integer(hsize_t), intent(in)  :: gdims(2)
        integer                       :: error
        integer(hssize_t)             :: offset(2)
        integer(hsize_t)              :: mycount(2)
        integer(hsize_t)              :: stride(2)
        integer(hsize_t)              :: myblock(2)
        integer(hsize_t)              :: start(2)
        integer(hsize_t)              :: step(2)
        integer(hsize_t)              :: blocks(2)
        integer(hsize_t)              :: gcol, grow
        integer                       :: selection_type

        if (self%root) write(*, '(A)', advance='no') &
                " - selecting hyperslab for block cyclic distribution"

        selection_type = H5S_SELECT_SET_F

        if (gdims(1) == 1) then
            start = 1
            step = 1
            blocks = 1

            ! Calculate position of first element read by process
            start(2) = self%mpigrid%rank * self%bdims(2) + 1

            ! Calculate distance between read blocks
            step(2) = self%mpigrid%size*self%bdims(2)

            ! Calculate number of blocks belonging to process
            blocks(2) = (gdims(2) - start(2))/step(2) + 1
        else
            ! Calculate position of first element read by process
            start = self%mpigrid%loc * self%bdims + 1

            ! Calculate distance between read blocks
            step = self%mpigrid%dims*self%bdims

            ! Calculate number of blocks belonging to process
            blocks = (gdims - start)/step + 1
        end if
        
        do gcol = start(2), gdims(2), step(2)
            do grow = start(1), gdims(1), step(1)
                offset(1) = grow - 1
                offset(2) = gcol - 1
                
                where ((gdims - offset) < self%bdims)
                    myblock = gdims - offset
                elsewhere
                    myblock = self%bdims
                end where

                mycount = 1
                stride = 1

                call h5sselect_hyperslab_f(&
                        filespace, &
                        selection_type, &
                        offset, &
                        mycount, &
                        error, &
                        stride, &
                        myblock)
                selection_type = H5S_SELECT_XOR_F
            end do
        end do

        if (self%root) print "(A)", " .. OK"
        
    end subroutine select_block_cyclic_hyperslab

    ! ---------------------------------
    !             Writing
    ! ---------------------------------

    subroutine write_matrix(self, matrix, name)
        class(eighdf5file)   :: self
        real(dp),         intent(in) :: matrix(*)
        character(len=*), intent(in) :: name

        call self%create_chunked_dataset(name, self%bdims, self%gdims)

        call self%write_data(matrix, self%ldims, self%gdims)

        call self%close_dataset()
        
    end subroutine write_matrix


    subroutine write_nondistributed_vector(self, vector, name)
        class(eighdf5file)           :: self
        real(dp),         intent(in) :: vector(:)
        character(len=*), intent(in) :: name
        integer(hsize_t)             :: chunk_size(2)      
        integer(hsize_t)             :: start
        integer(hsize_t)             :: step
        integer(hsize_t)             :: mysize
        integer(hsize_t)             :: blocks
        real(dp),       allocatable  :: mypart(:,:)
        integer                      :: myblock
        integer                      :: offset
        integer                      :: pos
        integer(hsize_t)             :: gcol
        integer(hsize_t)             :: gdims(2)
        integer(hsize_t)             :: ldims(2)


        chunk_size = 1
        chunk_size(2) = self%bdims(2)
        mysize = size(vector)

        ! Calculate position of first element read by process
        start = self%mpigrid%rank * chunk_size(2) + 1

        ! Calculate distance between read blocks
        step = self%mpigrid%size*chunk_size(2)

        ! Calculate number of blocks belonging to process
        blocks = (mysize - start)/step + 1

        ! Collect my part
        allocate(mypart(1, int(chunk_size(2)*blocks)))

        pos = 1
        do gcol = start, mysize, step
            offset = int(gcol) - 1

            if ((mysize - offset) < chunk_size(2)) then
                myblock = int(mysize) - offset
            else
                myblock = int(chunk_size(2))
            end if

            mypart(1, pos: pos + myblock - 1) = vector(gcol: gcol + myblock - 1)
            pos = pos + myblock
        end do
        
        gdims = 1
        gdims(1) = 1
        gdims(2) = mysize
        call self%create_chunked_dataset(name, chunk_size, gdims)

        ldims = 1
        ldims(2) = pos - 1
        ! print '(A,I3,I3)', self%mpigrid%proc_info()//"vector_size:", gdims
        ! print '(A,I3,I3)', self%mpigrid%proc_info()//"local_size:", ldims
        ! print '(A,I3,I3)', self%mpigrid%proc_info()//"blocks:", blocks
        ! print '(A,I3,I3)', self%mpigrid%proc_info()//"start:", start
 
        ! call self%mpigrid%terminate()
        call self%write_data(mypart, ldims, gdims)
        call self%close_dataset()
        
    end subroutine write_nondistributed_vector
    
    subroutine write_data(self, data, ldims, gdims)
        class(eighdf5file) :: self

        real(dp),          intent(in) :: data(*)
        integer(hsize_t),  intent(in) :: ldims(2)
        integer(hsize_t),  intent(in) :: gdims(2)
        integer(hid_t)                :: memspace
        integer(hid_t)                :: filespace
        integer(hid_t)                :: plist_id
        integer                       :: error
        
        if (self%root) print '(A)', "Writing data:"

        self%data_written = .true.

        ! Create dataspace identifier for local part of MPI process
        call h5screate_simple_f(2, ldims, memspace, error)

        ! call h5sget_select_npoints_f(memspace, num_points, error)
        ! call MPI_Barrier(MPI_COMM_WORLD, error)        
        ! print '(A,I0)', self%mpigrid%proc_info()//" selected l:", num_points
        
        ! Return an identifier for a copy of the dataspace for a dataset
        call h5dget_space_f(self%dset_id, filespace, error)
        
        ! Select part of file belonging to process
        call self%select_block_cyclic_hyperslab(filespace, gdims)
        
        ! call h5sget_select_npoints_f(filespace, num_points, error)
        ! print '(A,I0)', self%mpigrid%proc_info()//" selected g:", num_points
        ! call MPI_Barrier(MPI_COMM_WORLD, error)
        
        ! Create property list for collective dataset write
        call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error)

        ! Fill property list with parameters for MPI-IO writing
        call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)

        if (self%root) write(*, '(A)', advance='no') " - writing"
        ! write the dataset collectively
        call h5dwrite_f(&
                self%dset_id, &
                H5T_NATIVE_DOUBLE, &
                data, &
                gdims, &
                error, &
                file_space_id=filespace, &
                mem_space_id=memspace, &
                xfer_prp=plist_id)

        if (error /= 0) then
            print '(A)', "ERROR! Writing failed to file: "//self%filepath
            call self%mpigrid%terminate()
        end if
        
        
        call h5sclose_f(memspace, error)
        call h5pclose_f(plist_id, error)
        if (self%root .and. error == 0) print '(A)', " .. OK"

    end subroutine write_data

    ! --------------------------
    !          READING
    ! --------------------------

    subroutine allocate_and_read(self, matrix, array, name)
        class(eighdf5file)                    :: self
        class(bcmatrix),       intent(inout)  :: matrix
        real(dp), allocatable, intent(out)    :: array(:,:)
        character(len=*),      intent(in)     :: name

        call matrix%init(self%gdims, self%bdims, self%mpigrid, array, name)
        call matrix%getdims(self%ldims, self%gdims, self%bdims)
        call self%read_data(array, self%ldims, self%gdims)
    end subroutine allocate_and_read
    
    subroutine read_data(self, data, ldims, gdims)
        class(eighdf5file)            :: self
        real(dp), intent(out)         :: data(*)
        integer(hsize_t),  intent(in) :: ldims(2)
        integer(hsize_t),  intent(in) :: gdims(2)
        integer(hid_t)                :: memspace
        integer(hid_t)                :: filespace
        integer(hid_t)                :: plist_id
        integer                       :: error

        self%data_written = .true.
        
        ! Create dataspace identifier for local part of MPI process
        call h5screate_simple_f(2, ldims, memspace, error)

        ! Return an identifier for a copy of the dataspace for a dataset
        call h5dget_space_f(self%dset_id, filespace, error)
        
        ! Select part of file belonging to process
        call self%select_block_cyclic_hyperslab(filespace, gdims)
        
        ! Create property list for collective dataset write
        call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error)

        ! Fill property list with parameters for MPI-IO writing
        call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)

        ! read the dataset collectively
        call h5dread_f(&
                self%dset_id, &
                H5T_NATIVE_DOUBLE, &
                data, &
                gdims, &
                error, &
                file_space_id=filespace, &
                mem_space_id=memspace, &
                xfer_prp=plist_id)

        call h5sclose_f(memspace, error)
        call h5pclose_f(plist_id, error)

    end subroutine read_data

    ! ---------------------
    !      Atributes
    ! ---------------------

    function attribute_exists(self, name) result(attr_exists)
        class(eighdf5file)           :: self
        character(len=*), intent(in) :: name
        logical                      :: attr_exists
        integer                      :: error

        call h5aexists_f(self%dset_id, name, attr_exists, error)

    end function attribute_exists

    subroutine read_attribute(self, name, attr)
        class(eighdf5file)            :: self
        character(len=*), intent(in)  :: name
        real(dp),         intent(out) :: attr(1)
        integer                       :: error
        integer(hid_t)                :: attr_id
        integer(hsize_t)              :: dims(1) = 1
        
        call h5aopen_name_f(self%dset_id, name, attr_id, error)
        call h5aread_f(attr_id, H5T_NATIVE_DOUBLE, attr, dims, error)
        call h5aclose_f(attr_id, error) 

    end subroutine read_attribute
       
    
end module class_EigHDF5File

