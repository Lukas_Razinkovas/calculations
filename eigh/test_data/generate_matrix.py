import numpy as np
from netCDF4 import Dataset

write = True
read = True

matrix = np.zeros((5, 5))
for i in range(len(matrix)):
    for j in range(len(matrix)):
        matrix[i, j] = (i + 1) * 10 + j + 1

if write:
    nx = 6
    ny = 12
    ncfile = Dataset('test.nc', 'w', format="NETCDF3_64BIT_OFFSET")
    ncfile.createDimension('x', len(matrix))
    ncfile.createDimension('y', len(matrix))
    data = ncfile.createVariable(
        'matrix', 'd', ('x', 'y'))
    data[:] = matrix
    data.add_offset = -1
    ncfile.close()

if read:
    ncfile = Dataset('test.nc', 'r')
    data = ncfile.variables['matrix'][:]
    nx, ny = data.shape
    ncfile.close()
    print(data)
