import numpy as np
from scipy.io import netcdf

write = True
read = True

matrix = np.zeros((5, 5))
for i in range(len(matrix)):
    for j in range(len(matrix)):
        matrix[i, j] = (i + 1) * 10 + j + 1

if write:
    nx = 6
    ny = 12
    f = netcdf.netcdf_file('test.nc', 'w')
    f.history = 'Created for a test'
    f.createDimension('x', len(matrix))
    f.createDimension('y', len(matrix))
    data = f.createVariable(
        'matrix', np.dtype('float32').char, ('x', 'y'))
    data[:] = matrix
    f.close()

# if read:
#     ncfile = Dataset('test.nc', 'r')
#     data = ncfile.variables['matrix'][:]
#     nx, ny = data.shape
#     ncfile.close()
#     print(data)
