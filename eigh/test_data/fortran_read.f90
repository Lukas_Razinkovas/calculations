! This is part of the netCDF package.
! Copyright 2006 University Corporation for Atmospheric Research/Unidata.
! See COPYRIGHT file for conditions of use.
      
! This is a simple example which reads a small dummy array, from a
! netCDF data file created by the companion program simple_xy_wr.f90.
      
! This is intended to illustrate the use of the netCDF fortran 77
! API. This example program is part of the netCDF tutorial, which can
! be found at:
! http://www.unidata.ucar.edu/software/netcdf/docs/netcdf-tutorial
      
! Full documentation of the netCDF Fortran 90 API can be found at:
! http://www.unidata.ucar.edu/software/netcdf/docs/netcdf-f90

! $Id: simple_xy_rd.f90,v 1.7 2006/12/09 18:44:58 russ Exp $

program simple_xy_rd
  use netcdf
  implicit none

  ! This is the name of the data file we will read. 
  character (len = *), parameter :: FILE_NAME = "simple_xy.nc"

  ! We are reading 2D data, a 6 x 12 grid. 
  integer, parameter :: NX = 6, NY = 12
  integer :: data_in(NY, NX)

  ! This will be the netCDF ID for the file and data variable.
  integer :: ncid, varid

  ! Loop indexes, and error handling.
  integer :: x, y

  ! Open the file. NF90_NOWRITE tells netCDF we want read-only access to
  ! the file.
  call check( nf90_open(FILE_NAME, NF90_NOWRITE, ncid) )

  ! Get the varid of the data variable, based on its name.
  call check( nf90_inq_varid(ncid, "data", varid) )

  ! Read the data.
  call check( nf90_get_var(ncid, varid, data_in) )

  ! Check the data.
  do x = 1, NX
     do y = 1, NY
        if (data_in(y, x) /= (x - 1) * NY + (y - 1)) then
           print *, "data_in(", y, ", ", x, ") = ", data_in(y, x)
           stop "Stopped"
        end if
     end do
  end do

  ! Close the file, freeing all resources.
  call check( nf90_close(ncid) )

  print *,"*** SUCCESS reading example file ", FILE_NAME, "! "

contains
  subroutine check(status)
    integer, intent ( in) :: status
    
    if(status /= nf90_noerr) then 
      print *, trim(nf90_strerror(status))
      stop "Stopped"
    end if
  end subroutine check  
end program simple_xy_rd
!-----------------------------
!       Process Class
!-----------------------------

module class_BLACS_Grid
    use mpi
    implicit none

    type, public :: blacs_grid
        ! Storess all info about current process and proc grid
        integer, public :: dims(2) ! MPI Blacs Grid dimensions
        integer, public :: loc(2)  ! Position in grid
        integer, public :: rank    ! MPI rank of current process
        integer, public :: size    ! Number of processes
        integer, public :: context ! MPI world context
        
    contains
        procedure, public :: init

    end type blacs_grid

    private

contains

    subroutine init(self)
        class(blacs_grid) :: self
        integer :: ierr

        call MPI_Init(ierr)
        call MPI_Comm_Rank(MPI_COMM_WORLD, self%rank, ierr)
        call MPI_Comm_Size(MPI_COMM_WORLD, self%size, ierr)
        call MPI_Dims_Create(self%size, 2, self%dims, ierr)
        call Blacs_Get(-1, 0, context)
        call Blacs_Gridinit(context, 'Row-major', self%dims(1), self%dims(2))

        if (self%rank == 0) then
            call Print_header('Blacs MPI Grid Info')
            print '(A,I0)', 'Total number of processes: ', nprocs
            print '(A,I0,A,I0)', 'Initialised MPI Blacs grid: ', &
                & mpi_grid_rows, "x", mpi_grid_cols
        end if

    end subroutine init

end module EighUtils

!-----------------------------
!      BC MATRIX CLASS
!-----------------------------

module class_BC_Matrix
    use EighUtils
    
    ! Helps navigating block-cyclic matrix
    implicit none

    type, public :: bc_matrix
        integer :: gdim(2)             ! Global matrix dimensions
        integer :: ldim(2)             ! Local matrix dimensions
        integer :: bdims(2)            ! Block dimensions
        type(proc_descriptor) :: proc  ! Location in BLACS MPI grid

    contains
        procedure, public :: set       ! Constructor

    end type bc_matrix
    
    private

contains

    subroutine set(self, proc, gdim, bdims)
        class(bc_matrix) :: self
        type(proc_descriptor) :: proc
        integer :: gdim(2)
        integer :: bdim(2)
        integer :: Numroc

        self%gdim = gdim
        self%proc = proc

        ! Calculate local dimensions
        do i = 1, 2
            self%ldim(1) = Numroc(gdim(i), bdims(i), proc%loc(i), &
                    0, proc%grid(i))
        end do
    end subroutine set

end module class_BC_Matrix


module eigncdf
    use mpi    
    use pnetcdf

    type class_BC_Matrix
        integer :: glob_rows
        integer :: loc_rows
        integer :: row        ! Row in proc grid of current process
        integer :: col        ! Col in proc grid of current process
        integer :: rank       ! MPI rank of current process
    end type matrix_descriptor
    
contains

subroutine check(err, message)
    use mpi
    use pnetcdf
    implicit none
    integer err
    character(len=*) message

    ! It is a good idea to check returned value for possible error
    if (err .NE. NF90_NOERR) then
        write(6,*) trim(message), trim(nf90mpi_strerror(err))
        call MPI_Abort(MPI_COMM_WORLD, -1, err)
    end if
end subroutine check

subroutine read_netcdfinfo(filepath, rows, cols)
    character(len=20), intent(in) :: filepath
    integer(kind=MPI_OFFSET_KIND), intent(out) :: rows
    integer(kind=MPI_OFFSET_KIND), intent(out) :: cols

    integer :: ncid
    integer :: dimid
    integer :: omode

    omode = NF90_NOWRITE + NF90_64BIT_OFFSET
    call check(nf90mpi_open(MPI_COMM_WORLD, trim(filepath), omode, &
            MPI_INFO_NULL, ncid), 'nf90mpi_open: ')
    call check(nf90mpi_inq_dimid(ncid, 'x', dimid), 'nf90mpi_inq_dimid:')
    call check(nf90mpi_inquire_dimension(ncid, dimid, len=rows), &
            'nf90mpi_inquire_dimension:')
    call check(nf90mpi_inq_dimid(ncid, 'y', dimid), 'nf90mpi_inq_dimid:')
    call check(nf90mpi_inquire_dimension(ncid, dimid, len=cols), &
            'nf90mpi_inquire_dimension:')
    call check(nf90mpi_close(ncid), "nf90mpi_close:")
end subroutine read_netcdfinfo


subroutine BCReadNCDFMatrix(filepath)
    ! Reads netcdf3 matrix with nonblocking fashion
    ! (for MPI multi process reading) and distributes it with
    ! block-cyclic distribution

    character(len=20), intent(in) :: filepath

    
    integer :: dimid(2)
    integer :: ncid

    omode = NF90_NOWRITE + NF90_64BIT_OFFSET
    call check(nf90mpi_open(MPI_COMM_WORLD, filepath, omode, &
            MPI_INFO_NULL, ncid), 'nf90mpi_open:')

    !-------------------------------
    !  READ dimensions of matrix
    !------------------------------

    call check(nf90mpi_inq_dimid(ncid, "x", dimid(1)), &
            'In nf90mpi_inq_dimid x:')
    call ckeck(nf90mpi_inquire_dimension(ncid, dimid(1), len=global_ny), &
            'In nf90mpi_inq_dimlen: x')
    call check(nf90mpi_inq_dimid(ncid, "y", dimid(1)), &
            'In nf90mpi_inq_dimid y:')
    call ckeck(nf90mpi_inquire_dimension(ncid, dimid(1), len=global_ny), &
            'In nf90mpi_inq_dimlen: y')

    err = nf90mpi_inq_varid(ncid, "var", varid)
    call check(err, 'In nf90mpi_inq_varid: ')

    
          ! G_NY must == NY * nprocs
          ! myNY must == global_ny / nprocs

          err = nf90mpi_inq_dimid(ncid, "Y", dimid(2))
          call check(err, 'In nf90mpi_inq_dimid Y: ')
          err = nf90mpi_inquire_dimension(ncid, dimid(2), len=global_nx)
          call check(err, 'In nf90mpi_inq_dimlen: ')
          ! global_nx must == NX

    
end subroutine BCReadNCDFMatrix


end module eigncdf


program main
    use eigncdf
    implicit none

    ! MPI vars
    integer :: ierr, rank

    ! Parameters from matrix file
    character(len=20) :: filepath = 'test.nc'
    integer(kind=MPI_OFFSET_KIND) :: rows, cols
    integer :: ncid

    call MPI_Init(ierr)
    call MPI_Comm_rank(MPI_COMM_WORLD, rank, ierr)

    call read_netcdfinfo(filepath, rows, cols)
    print *, rows, cols
    
    call MPI_Finalize(ierr)
end program main
