import sys
import yaml
import os
import numpy as np
sys.path.append("../dephon")
from vasp import VaspXML, Poscar
from espresso import SimplifiedOutputParser, EspressoOutput, EspressoInput
from defc import ForceConstants
from deutils import Units


np.set_printoptions(suppress=True, precision=8)


def _data_line(name, floats):
    content = "{:<20} ".format(name)
    for i in floats:
        if isinstance(i, str):
            content += " {:>15}".format(i)
        else:
            content += " {:>15.6f}".format(i)

    return content + "\n"


def analyse_relaxation(dest, calc, dest2=None):
    if not os.path.exists(dest):
        return "-", "-", "-", "-"

    if calc == "vasp":
        nonrelaxed_atoms = Poscar(
            os.path.join(dest, "POSCAR")).create_atoms()
        relaxed_atoms = Poscar(
            os.path.join(dest, "CONTCAR")).create_atoms()
        vasp = VaspXML(os.path.join(dest, "vasprun.xml"))
        start_e, end_e, diff_e = vasp.relax_info()
        f = vasp.force_max()
    else:
        nonrelaxed_atoms = EspressoInput(dest2).atoms
        try:
            out = EspressoOutput(dest)
        except Exception as e:
            print(e)
            return "-", "-", "-", "-"
        relaxed_atoms = out.atoms

        start_e, end_e, diff_e = out.relax_info()
        f = out.force_max()

    return start_e, end_e, diff_e, f, nonrelaxed_atoms, relaxed_atoms

##############################
#   Displacement analysis
##############################


def plot_displacement_from_nitrogen():
    _, _, _, _, nonrelaxed, relaxed = analyse_relaxation(
        "pwscf/nv{1}{0}x{0}x{0}/nv.relax.out".format(5, ""),
        "pwscf",
        "pwscf/nv{1}{0}x{0}x{0}/nv.relax.in".format(5, ""))

    indxN = relaxed.symbols.index("N")

    m = [0, 0]
    for i in range(len(nonrelaxed)):
        _, d = nonrelaxed.get_separation_vector(indxN, i)
        if d > m[1]:
            m[0] = i
            m[1] = d

    indx = m[0]

    print("="*70)
    print("Nonrelaxed N:", nonrelaxed.get_positions()[indxN],
          nonrelaxed.get_scaled_positions()[indxN])
    print("Relaxed N:", relaxed.get_positions()[indxN],
          relaxed.get_scaled_positions()[indxN])
    print("="*70)

    positions2 = relaxed.get_positions()
    _pos = positions2 - (relaxed.get_positions()[indx] -
                         nonrelaxed.get_positions()[indx])
    relaxed.set_positions(_pos)
    positions = nonrelaxed.get_positions()

    distances = []
    displacements = []
    content = ""

    nr = 0
    for i in range(len(nonrelaxed)):
        name = nonrelaxed.symbols[i]
        pos = positions[i]
        nr += 1

        vec, _ = nonrelaxed.get_separation_vector(
            indx, i)
        vec2, _ = relaxed.get_separation_vector(
            indx, i)

        displ = vec - vec2
        dist = np.linalg.norm(displ)

        if dist > 10:
            vec, _ = nonrelaxed.get_separation_vector(
                indx, i, direct=True)
            vec2, _ = relaxed.get_separation_vector(
                indx, i, direct=True)
            displ = vec - vec2
            dist = np.linalg.norm(displ)

        # if dist > 10:
        #     print("="*70)
        #     print(positions[i], positions2[i])
        #     print("Positions:\n",
        #           nonrelaxed.get_positions()[i],
        #           relaxed.get_positions()[i])
        #     print("Scaled_Positions:\n",
        #           nonrelaxed.get_scaled_positions()[i],
        #           relaxed.get_scaled_positions()[i])
        #     print("Vectors:\n", vec, vec2)
        #     # print("Distances:\n", dist, dist2)
        #     input(d)

        _, dist_fr_N = nonrelaxed.get_separation_vector(indxN, i)
        distances.append(dist_fr_N)
        displ = np.array(displ)
        displacements.append(dist)

        astr = name + ' '.join('%15.9f' % F for F in pos)
        astr += "      "
        astr += ' '.join(['{:15.9f}'.format(k) for k in (displ*5).tolist()])
        content += astr + "\n"

    content = "	{}\n\"{}, cubic\"\n".format(
        nr, "Unknown") + content

    with open("relaxation.xyz", "wt") as f:
        f.write(content)

    displacements = np.array(displacements)
    import matplotlib.pyplot as plt
    plt.scatter(distances, displacements)
    plt.title("1000 NV ground state relaxation")
    plt.xlabel("Distance from N (A)")
    plt.ylabel("Displacement (A)")
    plt.grid(True)
    # print(np.sqrt(3*relaxed.get_cell()[0][0]**2))
    plt.savefig("relaxation_vs_dist_from_n.png", dpi=200)

plot_displacement_from_nitrogen()
exit(0)
content = ""
for cell in [3, 4, 5]:
    exitedv = []
    exitedp = []
    content += "\n"
    content += "="*70 + "\n"
    content += " "*30 + "{0}x{0}x{0}".format(cell) + "\n"
    content += "="*70 + "\n"

    for k, name in enumerate(["", "e"]):
        content += "\n"
        content += " "*1 + "NV{}".format(name) + "\n"
        content += "-"*70 + "\n"

        lines = []
        content += "{:<20}  {:>15} {:>15}".format(
            "", "VASP", "PWSCF") + "\n" + "-"*70 + "\n"

        v0, v1, vd, vf, *_ = analyse_relaxation(
            "vasp/nv{1}{0}x{0}x{0}/relax".format(cell, name),
            "vasp")
        p0, p1, pd, pf, *_ = analyse_relaxation(
            "pwscf/nv{1}{0}x{0}x{0}/nv.relax.out".format(cell, name),
            "pwscf",
            "pwscf/nv{1}{0}x{0}x{0}/nv.relax.in".format(cell, name))

        exitedv.append(v1)
        exitedp.append(p1)
        content += _data_line("Start E(eV)", [v0, p0])
        content += _data_line("End E(eV)", [v1, p1])
        content += _data_line("Relax E(eV)", [vd, pd])
        content += _data_line("Max force (eV/AA)", [vf, pf])
        content += "-"*70 + "\n\n"

    if "-" not in exitedv:
        content += "VASP excitation = {:>2.6f} eV".format(
            exitedv[1] - exitedv[0]) + "\n"

    if "-" not in exitedp:
        content += "PWSCF excitation = {:>2.6f} eV".format(
            exitedp[1] - exitedp[0]) + "\n"

print(content)
with open("nv_summary.log", "wt") as f:
    f.write(content)
