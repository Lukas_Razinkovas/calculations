import sys
import os
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("../dephon")
from vasp import VaspXML, Poscar
from defc import ForceConstants
from deutils import Units
import matplotlib.cm as cm


def draw_simmilarity_matrix(modes, modes2):
    disp = modes.get_normalised_displacements()
    disp2 = modes2.get_normalised_displacements()
    Z = np.zeros((len(disp), len(disp2)))

    X, Y = np.meshgrid(modes.get_frequencies("mev"),
                       modes2.get_frequencies("mev"))

    for nr, d in enumerate(disp):
        Z[nr] = np.apply_along_axis(d.dot, 1, disp2)

    f, ax = plt.subplots(1, figsize=(30, 30))
    CS = ax.contourf(X, Y, abs(Z), 20, cmap=plt.get_cmap("gray_r"),
                     interpolation="bilinear")
    ax.set_xticks(np.arange(0, 170, 10))
    ax.set_yticks(np.arange(0, 170, 10))
    ax.grid(b=True, which='both', color='red', linestyle='--')
    f.colorbar(CS, shrink=0.8, extend='both')
    ax.set_xlabel("meV")
    ax.set_ylabel("meV")
    plt.savefig("modes_mixing.pdf", dpi=300)
    plt.show()

if __name__ == "__main__" and False:
    fc = ForceConstants("vasp", outdir="vasp/nv4x4x4/phonopy_phonons")
    fce = ForceConstants("vasp", outdir="vasp/nve4x4x4/phonopy_phonons")

    ground = fc.calculate_modes()
    excited = fce.calculate_modes()

    ground.analyse()
    excited.analyse()

if True:
    draw_simmilarity_matrix(ground, excited)
