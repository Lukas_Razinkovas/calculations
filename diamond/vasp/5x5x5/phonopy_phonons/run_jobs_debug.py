import subprocess
import time

jobs = [
    "phonons1.job",
    "phonons2.job",
]

while jobs:
    proc = subprocess.Popen(['sqs', '-w'], stdout=subprocess.PIPE)
    lines = proc.communicate()[0].split()
    print(lines)
    time.sleep(2)