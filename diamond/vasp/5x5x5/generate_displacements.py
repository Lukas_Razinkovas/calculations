import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

create_displacements("vasp", dim=5)
generate_job_files("vasp", "cori", "DiaPh2", 3, 96, plan="regular",
                   jobs=2, time="6:00:00",
                   binary="vasp.541_p3.cori_gamma",
                   qos=None)
