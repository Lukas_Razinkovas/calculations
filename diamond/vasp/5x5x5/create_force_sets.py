"""When abinitio calculations finished call this for postprocessing"""

import sys
import matplotlib.pyplot as plt
sys.path.append("../../../dephon")

from dephonopy import create_force_sets, calculate_fcmatrix, create_plot
from defc import ForceConstants

create_force_sets("vasp", workdir="phonopy_phonons")
calculate_fcmatrix("vasp", workdir="phonopy_phonons", dim=1)

f, ax = plt.subplots(1, figsize=(15, 10))

fc = ForceConstants("vasp", phonopydir="phonopy_phonons")
modes = fc.calculate_modes()
print("-"*70)
print("vasp")
print(fc.get_frequencies("cm^-1").max(), "cm")
print(fc.get_frequencies("thz").max(), "thz")
print(fc.matrix.max())
# fc.calculate_bulk_band_structure(ax=ax, color="red", label="vasp")


# modes.fc.plot_experimental_data(
#     '/home/lukas/super/calculations/diamond/experimental/',
#     ax,
#     ignore=["xk_square.csv", "gammax_round.csv"])

# exit(0)
# fc = ForceConstants("pwscf", outdir="pwscf")
# modes = fc.calculate_modes()
# print("-"*70)
# print("pwscf")
# print(fc.get_frequencies("cm^-1").max(), "cm")
# print(fc.get_frequencies("thz").max(), "thz")
# print(fc.atoms.get_cell())
# print(fc.matrix.max())
# fc.calculate_bulk_band_structure(ax=ax, color="blue", label="pwscf")

# plt.legend()
# plt.savefig("dispersion_lines.pdf", dpi=300)
# plt.show()
