import sys
import numpy as np
sys.path.append("../../../dephon")
from vasp import Poscar


def remove_pb_artifacts(atoms1, atoms2):
    positions1 = atoms1.get_positions()
    positions2 = atoms2.get_positions()

    new_positions2 = []
    for nr, (pos1, pos2) in enumerate(zip(positions1, positions2)):
        v, d = atoms1.get_separation_vector(pos1, pos2)
        v_, d_ = atoms1.get_separation_vector(pos1, pos2, direct=True)
        if d != d_:
            new_positions2.append(pos1-v)
        else:
            new_positions2.append(pos2)

    atoms2.set_positions(new_positions2)


def read_displacements(poscar1, poscar2):
    if isinstance(poscar1, str):
        atoms1 = Poscar(poscar1).create_atoms()
        atoms2 = Poscar(poscar2).create_atoms()
    else:
        atoms1 = poscar1
        atoms2 = poscar2

    remove_pb_artifacts(atoms1, atoms2)

    mass_center1 = atoms1.get_mass_center()
    mass_center2 = atoms2.get_mass_center()

    print("Cell 1 mass center:", mass_center1)
    print("Cell 2 mass center:", mass_center2)

    atoms2.set_positions(
        atoms2.get_positions() - (mass_center2 - mass_center1))

    mass_center1 = atoms1.get_mass_center()
    mass_center2 = atoms2.get_mass_center()

    positions1 = atoms1.get_positions()
    positions2 = atoms2.get_positions()

    displ = np.zeros(positions1.shape, dtype=float)
    for nr, (pos1, pos2) in enumerate(zip(positions1, positions2)):
        v, d = atoms1.get_separation_vector(pos2, pos1)
        displ[nr] = v

    distances = np.apply_along_axis(np.linalg.norm, 1, displ)

    return displ, distances


def read_most_displaced(poscar1, poscar2, min_displ=0.014):
    atoms1 = Poscar(poscar1).create_atoms()
    atoms2 = Poscar(poscar2).create_atoms()
    displacements, distances = read_displacements(atoms1, atoms2)
    indx = np.where(distances > min_displ)[0]

    lattice_constant = atoms1.get_cell()[0, 0]

    rel_displacements = []
    numbers = []
    for nr in indx:
        rel_displacements.append(displacements[nr]/lattice_constant)
        numbers.append(nr)

    rel_displacements = np.array(rel_displacements)
    numbers = np.array([[i] for i in numbers])

    data = np.hstack((numbers, rel_displacements))

    print(data[0], numbers[0], rel_displacements[0])

    return data


if __name__ == "__main__":
    data = read_most_displaced("../nve4x4x4/relax/CONTCAR", "./relax/POSCAR")
    np.savetxt("relative_displacements.dat", data)
