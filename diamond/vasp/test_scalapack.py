import sys
import os
import h5py
import numpy as np
sys.path.append("../../dephon")
from defc import ForceConstants


for i in range(3, 5):
    directory = "embeded/nv{0}x{0}x{0}direct".format(i)
    fc = ForceConstants('vasp', phonopydir=os.path.join(
        "nv{0}x{0}x{0}".format(i), "phonopy_phonons"))
    fc.calculate_modes()
    displ1 = fc.get_displacements("bohr")
    freq1 = fc.get_frequencies("mev")

    f = h5py.File(os.path.join(directory, "modes.hdf5"), "r")
    displ2 = f['displacements'][:]
    freq2 = f['frequencies'][:].flatten()
    f.close()

    print("="*70)
    print("DIRECT")
    print("="*70)
    print(displ1[:5, :5])

    print("="*70)
    print("SCALA")
    print("="*70)
    print(displ2[:5, :5])
    input()

    different = False
    for i, j in zip(freq1, freq2):
        if not np.allclose(abs(i), abs(j)):
            different = True
            print("!!!DIFERENT")

        print(i, j)

    if different:
        print("FOUND DIFFERENCES")
        input()

