import sys
import os
sys.path.append("../../dephon")
from defc import ForceConstants
from demegabulk import create_megabulk_and_write_results_to_dir
from deutils import ensure_dir
from vasp import Poscar
import h5py


def create_job_file(directory, name, nodes=1):
    source = (
        r"#!/bin/bash",
        r"#PBS -l select={}".format(nodes),
        r"#PBS -q apc14j",
        r"#PBS -N {}".format(name),
        r"cd $PBS_O_WORKDIR",
        r"date >&2 ;hostname >&2",
        "\n",
        r"source /opt/intel/composer_xe_2011_sp1.9.293/mkl/10.2.3.029/tools/" +
        r"environment/mklvarsem64t.sh",
        r"source /opt/intel/impi/4.0.3.008/intel64/bin/mpivars.sh",
        r"source /opt/intel/composer_xe_2011_sp1.9.293/bin/" +
        r"iccvars.sh intel64",
        r"source /opt/intel/composer_xe_2011_sp1.9.293/bin/" +
        r"ifortvars.sh intel64",
        "\n",
        r"mpirun -np {} ".format(nodes*12) +
        r"../../../../eigh/bin/eigh " +
        r"-f hessian.hdf5 -o modes.hdf5 -b 64 " +
        r"> diagonalisation.log",
        "\n"
        r"date >&2"
    )

    source = "\n".join(source)
    with open(os.path.join(directory, "hpcs.job"), "wt") as f:
        f.write(source)

##########################################
#   Copy Direct Results For Comparison
##########################################

# for i in range(3, 5):
#     directory = "embeded/nv{0}x{0}x{0}direct".format(i)
#     ensure_dir(directory)
#     fc = ForceConstants('vasp', phonopydir=os.path.join(
#         "nv{0}x{0}x{0}".format(i), "phonopy_phonons"))
#     fc.save_to_hdf5(directory)
#     create_job_file(directory)10

# exit(0)
##################################################
#          Create New Combined Bulks
##################################################


if __name__ == "__main__":
    donor = ForceConstants('vasp', phonopydir="4x4x4/phonopy_phonons")
    base_atoms = Poscar("2x2x2/POSCAR").create_atoms()
    base_atoms.identify_different_base_atoms()

    for i in range(4, 16):
        directory = "embeded/{0}x{0}x{0}/".format(i)
        name = "{0}x{0}x{0}d".format(i)
        # ensure_dir(directory)
        create_megabulk_and_write_results_to_dir(
            donor, base_atoms, directory, multipl=[i, i, i],
            calculate_modes=False)
        create_job_file(directory, name, nodes=1 if i <= 11 else 2)
