import sys
import os
import copy
sys.path.append("../../dephon")
from defc import ForceConstants
from demegabulk import create_megabulk_and_write_results_to_dir
from deutils import ensure_dir
from vasp import Poscar
import h5py
import pprint
from collections import Counter

pp = pprint.PrettyPrinter()


def create_job_file(directory, name, nodes=1):
    source = (
        r"#!/bin/bash",
        r"#PBS -l select={}".format(nodes),
        r"#PBS -q apc14j",
        r"#PBS -N {}".format(name),
        r"cd $PBS_O_WORKDIR",
        r"date >&2 ;hostname >&2",
        "\n",
        r"source /opt/intel/composer_xe_2011_sp1.9.293/mkl/10.2.3.029/tools/" +
        r"environment/mklvarsem64t.sh",
        r"source /opt/intel/impi/4.0.3.008/intel64/bin/mpivars.sh",
        r"source /opt/intel/composer_xe_2011_sp1.9.293/bin/" +
        r"iccvars.sh intel64",
        r"source /opt/intel/composer_xe_2011_sp1.9.293/bin/" +
        r"ifortvars.sh intel64",
        "\n",
        r"mpirun -np {} ".format(nodes*12) +
        r"../../../eigh/bin/eigh " +
        r"-f hessian.hdf5 -o modes.hdf5 -b 64 " +
        r"> diagonalisation.log",
        "\n"
        r"date >&2"
    )

    source = "\n".join(source)
    with open(os.path.join(directory, "hpcs.job"), "wt") as f:
        f.write(source)


def create_superlattice(size):
    from_megabulk = "embeded/{0}x{0}x{0}/".format(size)
    outdir = "../embeded/{0}x{0}x{0}superlat2".format(size)
    bulk_outdir = "../embeded/{0}x{0}x{0}".format(size)

    # ----------------------------- #
    #     Read force constants
    # ----------------------------- #

    bulk = ForceConstants('vasp', hdf5dir=from_megabulk)
    if bulk_outdir is not None:
        bulk.save_to_hdf5(bulk_outdir)
        create_job_file(bulk_outdir, "bulk{0}x{0}x{0}diag".format(size),
                        nodes=1 if size < 8 else 2)

    bulk.masses = None
    bulk.whessian = None
    bulk.inv_square_masses = None

    # ----------------------------- #
    #      Generate layers
    # ----------------------------- #

    pos = bulk.atoms.get_positions()
    layers = list(set(pos[:, 0]))
    layers.sort()
    slice_dim = 2
    assert len(layers) % slice_dim == 0

    carbon13 = False
    c13layers = []
    for i in range(0, len(layers), slice_dim):
        if carbon13:
            for j in range(i, i + slice_dim, 1):
                print(j, carbon13)
            carbon13 = False
        else:
            for j in range(i, i + slice_dim, 1):
                c13layers.append("{:.5f}".format(layers[j]))
            carbon13 = True

    # ----------------------------- #
    #   Set different atomic types
    # ----------------------------- #

    symbols = bulk.atoms.get_symbols()

    for i in bulk.atoms:
        if "{:.5f}".format(pos[i][0]) in c13layers:
            symbols[i] = "C13"

    bulk.atoms.set_symbols(symbols)

    # ----------------------------- #
    #       Write new data
    # ----------------------------- #

    bulk.save_to_hdf5(outdir)
    create_job_file(outdir, "sprlt2{0}dg".format(size),
                    nodes=1 if size < 8 else 2)
    bulk.atoms.set_symbols(["X" if i == "C13" else i for i in symbols])

    with open(os.path.join(outdir, "coordinates.xyz"), "wt") as f:
        f.write(bulk.atoms.create_xyz_content())


def create_superlattice_with_defect(size, slice_dim=2, shift=0):
    from_megabulk = "embeded/nv{0}x{0}x{0}/".format(size)
    outdirc13 = "../embeded/{0}x{0}x{0}c13".format(size)
    outdir1 = "../embeded/nv{0}x{0}x{0}superlat{1}".format(
        size, slice_dim)
    outdir2 = "../embeded/nv{0}x{0}x{0}superlat{1}inv".format(
        size, slice_dim)

    if shift != 0:
        outdir1 += "shift{}".format(shift)
        outdir2 += "shift{}".format(shift)

    # ----------------------------- #
    #     Read force constants
    # ----------------------------- #

    bulk = ForceConstants('vasp', hdf5dir=from_megabulk)
    bulk.masses = None
    bulk.whessian = None
    bulk.inv_square_masses = None

    # ----------------------------- #
    #       Generate layers
    # ----------------------------- #

    pos = bulk.atoms.get_positions()
    layers = list(set(pos[:, 0]))
    layers.sort()
    assert len(layers) % slice_dim == 0

    carbon13 = False
    c13layers = []
    for i in range(shift, len(layers), slice_dim):
        if carbon13:
            carbon13 = False
        else:
            for j in range(i, min(i + slice_dim, len(layers)), 1):
                c13layers.append("{:.5f}".format(layers[j]))
            carbon13 = True

    # ----------------------------- #
    #   Set different atomic types
    # ----------------------------- #

    symbolsc13 = ["C13" if i == "C" else i for i in bulk.atoms.get_symbols()]
    symbols1 = list(bulk.atoms.get_symbols())
    symbols2 = list(copy.deepcopy(symbols1))

    for i in bulk.atoms:
        if symbols1[i] == "N":
            continue
        if "{:.5f}".format(pos[i][1]) in c13layers:
            print("TAIP")
            symbols1[i] = "C13"
        else:
            symbols2[i] = "C13"

    print(Counter(symbols1), Counter(symbols2), Counter(symbolsc13))

    cnt = 1
    for outdir, symbols in [
            (outdir1, symbols1), (outdir2, symbols2), (outdirc13, symbolsc13)]:

        if slice_dim != 2 and cnt == 3:
            continue

        bulk.atoms.set_symbols(symbols)
        bulk.save_to_hdf5(outdir)
        create_job_file(outdir, "diag{}_{}".format(size, cnt),
                        nodes=1 if size < 8 else 2)

        bulk.atoms.set_symbols(
            ["X" if i == "C13" else i for i in bulk.atoms.get_symbols()])
        print(Counter(bulk.atoms.get_symbols()))
        with open(os.path.join(outdir, "coordinates.xyz"), "wt") as f:
            f.write(bulk.atoms.create_xyz_content())

        bulk.masses = None
        bulk.whessian = None
        bulk.inv_square_masses = None
        cnt += 1


if __name__ == "__main__":
    for i in [4, 6, 8, 10, 12, 14][-3:]:
        # create_superlattice_with_defect(i, 2)
        create_superlattice_with_defect(i, 4, shift=-1)
