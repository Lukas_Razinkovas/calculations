import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

create_displacements("vasp", 1)
generate_job_files("vasp", "cori", "DiaPh2", 1, 32, plan="debug",
                   jobs=2, binary="vasp.541_p3.cori")
