"""When abinitio calculations finished call this for postprocessing"""

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
sys.path.append("../../../dephon")

from dephonopy import create_force_sets, calculate_fcmatrix, create_plot
from defc import ForceConstants

if False:
    create_force_sets("vasp", workdir="phonopy_phonons")
    calculate_fcmatrix("vasp", workdir="phonopy_phonons", dim=1)


f, ax = plt.subplots(1, figsize=(8, 4))
fc = ForceConstants("vasp", phonopydir="phonopy_phonons")
modes = fc.calculate_modes()
# print("-"*70)
# print("vasp")
# print(fc.get_frequencies("cm^-1").max(), "cm")
# print(fc.get_frequencies("thz").max(), "thz")
# print(fc.matrix.max())

freq, val = fc.calculate_pw_dos()
freq *= 0.12398
val /= np.trapz(val, x=freq)
ax.plot(freq, val, label="Plane wave approach")

data = np.genfromtxt('../embeded/nv15x15x15/freq.dat',
                     dtype=None, skip_header=1)
freq2 = data['f1']
kernel = gaussian_kde(freq2)
kernel.set_bandwidth(0.055)
ax.plot(
    freq, kernel(freq),
    label=r"$\Gamma$ point basis approach for embeded 15x15x15 cell (27000 sites)")
ax.set_xlabel("Frequency (meV)")
ax.set_ylabel("DOS")
plt.tight_layout()
plt.legend()
plt.savefig("DOS.pdf")
plt.show()



# fc.calculate_bulk_band_structure(ax=ax, color="darkcyan", label="vasp",
#                                  just_line=True)

# modes.fc.plot_experimental_data(
#     '../../experiment/bulk/',
#     ax,
#     ignore=["xk_square.csv", "gammax_round.csv"])


# # exit(0)
# # fc = ForceConstants("pwscf", phonopydir="pwscf")
# # modes = fc.calculate_modes()
# # print("-"*70)
# # print("pwscf")
# # print(fc.get_frequencies("cm^-1").max(), "cm")
# # print(fc.get_frequencies("thz").max(), "thz")
# # print(fc.atoms.get_cell())
# # print(fc.matrix.max())
# # fc.calculate_bulk_band_structure(ax=ax, color="orange",
# #                                  label="pwscf",
# #                                  alpha=0.4)

# # plt.legend()
# ax.set_ylabel("meV")
# ax.set_xlabel("Wave vector")
# plt.tight_layout()
# plt.savefig("4x4x4dispersion_lines.pdf", dpi=300)
# plt.clf()

