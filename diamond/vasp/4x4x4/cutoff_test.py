import sys
sys.path.append("../../../dephon")
import matplotlib.pyplot as plt
from defc import ForceConstants
from deutils import ensure_dir

ensure_dir("cutoff_graphs")

fc = ForceConstants('vasp', phonopydir="phonopy_phonons")
modes = fc.calculate_modes()

diffs = [6, 6, 5.5, 4, 4, 4]

distances = set()
for i, j in fc.atoms.iter_pairs():
    pos = fc.atoms.get_positions()
    vec, d = fc.atoms.get_separation_vector(i, j)
    distances.add(d)
    if 0.1 < d < 1.5:
        print(pos[i])
        print(pos[j])
        print(vec, d)
        input()


distances = list(distances)
distances.sort()
print(list(enumerate(distances)))


for nn, cut in [[6, 4.5], [3, 3], [4, 3.6], [5, 4], [6, 4.5], [8, 5.1], [9, 5.3]][:1]:
    f, ax = plt.subplots(1)

    fc2 = ForceConstants('vasp', phonopydir="phonopy_phonons")
    fc2.perform_cutoff(cut)
    fc2.symmetrize_fc(2)
    modes2 = fc2.calculate_modes()
    modes.compare_phonon_dispersion(modes2, ax, differences=None)
    ax.legend(loc=4)
    ax.set_title("{} nearest neighbours ({:.1f} A)".format(nn, cut))
    plt.savefig("cutoff_graphs/{}nn_{}A.png".format(nn, cut), dpi=200)
    plt.clf()

    f, ax = plt.subplots(1, figsize=(8, 4))

    modes.compare_phonon_dispersion(modes2, ax, differences=1)
    ax.legend(loc=4)
    ax.set_title("{} nearest neighbours ({:.1f} A)".format(nn, cut))
    plt.tight_layout()
    plt.savefig("cutoff_graphs/{}nn_{}A_diffs.pdf".format(nn, cut), dpi=300)
    plt.clf()
