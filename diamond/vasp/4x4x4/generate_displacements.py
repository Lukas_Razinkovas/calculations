import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

create_displacements("vasp", dim=4)
generate_job_files("vasp", "cori", "DiaPh2", 2, 64, plan="regular",
                   jobs=1, time="2:00:00",
                   binary="vasp.541_p3.cori_gamma")
