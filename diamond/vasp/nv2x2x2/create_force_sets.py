"""When abinitio calculations finished call this for postprocessing"""

import sys
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("../../../dephon")

from dephonopy import create_force_sets, calculate_fcmatrix, create_plot
from defc import ForceConstants
from vasp import Poscar

create_force_sets("vasp", workdir="phonopy_phonons")
calculate_fcmatrix("vasp", workdir="phonopy_phonons", dim=1)

fc = ForceConstants("vasp", outdir="phonopy_phonons")
modes = fc.calculate_modes()

print(fc.get_frequencies("thz").max(), "THz")
print(fc.get_frequencies("cm").max(), "cm^-1")
print(fc.matrix.max())
print(fc.atoms.get_cell())


modes.analyse()
indx = np.where(modes.iprs < 40)[0]
with open("localized_modes.csv", "wt") as f:
    header = modes._mode_header()
    f.write(header + "\n")
    for i in indx:
        f.write(modes._mode_info(i) + "\n")

matrix = fc.get_mass_weighted_force_constants()
with open("matrix.csv", "wt") as f:
    f.write("{}\n".format(len(matrix)))
    for line in matrix:
        f.write(" ".join(["{:2.8f}".format(k) for k in line]) + "\n")

with open("evals.csv", "wt") as f:
    for i, j in zip(fc._evals[3:], fc._evects[3:]):
        f.write("{:2.8e}\n".format(i))
        f.write(" ".join(["{:2.8e}".format(k) for k in j]) + "\n")
