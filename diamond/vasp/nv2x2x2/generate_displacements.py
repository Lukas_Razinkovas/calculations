import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

create_displacements("vasp", 1, tolerance=0.02)
generate_job_files("vasp", "cori", "NV2x2x2vsp", 8, 256, plan="debug",
                   jobs=26, time="30:00", binary="vasp.541_p3.cori")
