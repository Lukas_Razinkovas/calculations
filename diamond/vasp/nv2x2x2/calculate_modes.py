import sys
import os
import numpy as np
sys.path.append("../../../dephon")
from defc import ForceConstants
from dedisp import calculate_couplings
from deutils import ensure_dir
from vasp import Poscar
import h5py

if True:
    fc = ForceConstants('vasp', phonopydir='phonopy_phonons')
    fc.symmetrize_fc(2)
    modes = fc.calculate_modes()
    modes.analyse()

    f = h5py.File(os.path.join("./modes", "modes.hdf5"), "w")
    f["frequencies"] = np.array([fc.get_frequencies("mev")]).T
    f["displacements"] = fc.get_displacements("A")
    f.close()

    ensure_dir("./modes")
    fc.save_to_hdf5("./modes")
    modes.save_to_hdf5("./modes")

poscar = "./relax/POSCAR"

vasprun1 = "../../vasp_hybrid_new/nv4x4x4/excited_asym_geometry/vasprun.xml"
vasprun2 = "../../vasp_hybrid_new/nve4x4x4asym/relaxed/vasprun.xml"

# vasprun1 = "../../vasp_hybrid_new/nv4x4x4/excited_sym_geometry/vasprun.xml"
# vasprun2 = "../../vasp_hybrid_new/nve4x4x4sym/relaxed/vasprun.xml"

bulkg_poscar = "../../vasp_hybrid_new/nv4x4x4/bulk_geometry/POSCAR"

data = calculate_couplings(
    vasprun1, vasprun2, from_forces=True,
    bulkg_poscar=bulkg_poscar, hdf5dir="modes",
    eatoms=Poscar(poscar).create_atoms())
