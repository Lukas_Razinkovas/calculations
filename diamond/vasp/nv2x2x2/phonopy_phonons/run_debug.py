import subprocess
import time

jobs = [
    "phonons1.job",
    "phonons2.job",
    "phonons3.job",
    "phonons4.job",
    "phonons5.job",
    "phonons6.job",
    "phonons7.job",
    "phonons8.job",
    "phonons9.job",
    "phonons10.job",
    "phonons11.job",
    "phonons12.job",
    "phonons13.job",
    "phonons14.job",
    "phonons15.job",
    "phonons16.job",
    "phonons17.job",
    "phonons18.job",
    "phonons19.job",
    "phonons20.job",
    "phonons21.job",
    "phonons22.job",
    "phonons23.job",
    "phonons24.job",
    "phonons25.job",
    "phonons26.job",
]


while jobs:
    proc = subprocess.Popen(['sqs', '-w'], stdout=subprocess.PIPE)
    lines = proc.communicate()[0].split()
    cnt = lines.count("debug")
    print("running {}".format(cnt))
    if lines.count("debug") < 3:
        jb = jobs.pop(0)
        print("submiting job {}".format(jb))
        subprocess.call("sbatch {}".format(jb), shell=True)

    time.sleep(10)
