import sys
import os
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("../../../dephon")
from defc import ForceConstants
from dedisp import (calculate_couplings,
                    calculate_nondimensional_vibronic_couplings)
from deutils import ensure_dir, gaussian_smoothing
from vasp import Poscar
import h5py
import numpy as np
import os
from scipy.optimize import curve_fit
import logging
logging.basicConfig(level=logging.WARNING)

os.environ["TF_CPP_MIN_LOG_LEVEL"]="3"



create = True
if create:
    fc = ForceConstants('vasp', phonopydir='phonopy_phonons')
    fc.symmetrize_fc(2)
    modes = fc.calculate_modes()
    modes.analyse()

    mydir = "../embeded/nv3x3x3direct"
    ensure_dir("modes")
    f = h5py.File(os.path.join(mydir, "modes.hdf5"), "w")
    f["frequencies"] = np.array([fc.get_frequencies("mev")]).T
    f["displacements"] = fc.get_displacements("A")
    f.close()

    bulk_atoms = Poscar("relax/POSCAR-0").create_atoms()
    fc.atoms = bulk_atoms
    fc.save_to_hdf5(mydir)
    modes.save_to_hdf5(mydir)

poscar = "./relax/POSCAR-0"

calcs = [
    dict(
        vasprun1="../../vasp_hybrid_new/nv3x3x3/relax/CONTCAR",
        vasprun2="../../vasp_hybrid_new/nve3x3x3asym/relax/CONTCAR",
        bulkg_poscar="../../vasp_hybrid_new/nv3x3x3/bulk_geometry/POSCAR",
        hdf5dir="modes",
        from_forces=False,
        eatoms=Poscar(poscar).create_atoms()),
    dict(
        vasprun1="../../vasp_hybrid_new/nv4x4x4/excited_asym_geometry/vasprun.xml",
        vasprun2="../../vasp_hybrid_new/nve4x4x4asym/relaxed/vasprun.xml",
        bulkg_poscar="../../vasp_hybrid_new/nv4x4x4/bulk_geometry/POSCAR",
        from_forces=True,
        hdf5dir="../embeded/nv4x4x4/",
        eatoms=None),
    dict(
        vasprun1="../../vasp_hybrid_new/nv4x4x4/excited_asym_geometry/vasprun.xml",
        vasprun2="../../vasp_hybrid_new/nve4x4x4asym/relaxed/vasprun.xml",
        bulkg_poscar="../../vasp_hybrid_new/nv4x4x4/bulk_geometry/POSCAR",
        from_forces=True,
        hdf5dir="../embeded/nv6x6x6/",
        eatoms=None)]


# vasprun1 = "../../vasp_hybrid_new/nv4x4x4/excited_sym_geometry/vasprun.xml"
# vasprun2 = "../../vasp_hybrid_new/nve4x4x4sym/relaxed/vasprun.xml"

colors = ["red", "blue", "yellow"]
names = ["nv6x6x6", "nv4x4x4", "nv3x3x3"]


def gaussian(x, mu, sig):
    return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
        2 * np.power(sig, 2.)))


def func(x, *popt):
    mylen = int((len(popt) - 1)/2)
    ks = np.array(popt[:mylen])
    ms = np.array(popt[mylen:-1])
    sig = popt[-1]

    val = 0
    for k, m in zip(ks, ms):
        val += k*gaussian(x, m, sig)

    return val

f, ax = plt.subplots(1, 1, figsize=(15, 10))

for params in [calcs[1]]:
    vasprun1 = params["vasprun1"]
    vasprun2 = params["vasprun2"]
    del params["vasprun1"]
    del params["vasprun2"]

    # params["from_forces"] = False

    color = colors.pop()
    data, e_data = calculate_couplings(
        vasprun1, vasprun2, **params)

    e_data["k"] = e_data["k"]**2

    # ax.scatter(e_data["freq_mev"], e_data["k"], color=color, s=0.5)

    l = 10
    bndsd = tuple([0]*l + [10]*l + [0])
    bndsu = tuple([10]*l + [180]*l + [20])

    p0 = np.concatenate((np.ones(l), np.linspace(20, 175, l), [1]))

    x, y = gaussian_smoothing(e_data["freq_mev"], e_data["k"], sig=6)
    popt, pcov = curve_fit(func, x, y, p0=p0,
                           maxfev=10000, bounds=(bndsd, bndsu))

    ks = np.zeros(l)
    fs = np.zeros(l)
    ks, fs, sig = popt[0:l], popt[l:2*l], popt[-1]

    ax.plot(x, func(x, *popt), color="red")
    ax2 = ax.twinx()
    _keff = np.sqrt(sum(ks))
    _feff = sum((ks)*fs)/sum(ks)
    ax2.stem(
        fs, ks, label=r"$k_{{eff}}={:.2f}, \omega_{{eff}}={:.2f}, \sigma={:.2f}$".format(
            _keff, _feff, sig
        ))
    # print(pcov)
    # input(popt)

    ax.plot(x, y, color=color,
            label=r"{} $k_{{eff}} = {:.2f}, \omega_{{eff}} = {:.2f}$".format(
                names.pop(), e_data["keff"], e_data["effreq_mev"]))

ax.legend(loc=1)
ax2.legend(loc=2)
plt.savefig("comparison_k_sq.pdf")
# plt.show()


# # vibr_data = calculate_nondimensional_vibronic_couplings(data=data)

# data = np.array([e_data["freq_mev"], e_data["freq"], e_data["k"]]).T

# np.savetxt("/home/lukas/programming/dejt/test_data/nv3x3x3.dat", data)
# print("!!!!", e_data["keff"], e_data["effreq_mev"])

