import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

create_displacements("vasp", 1)
generate_job_files("vasp", "cori", "NV3Phvsp", 4, 128, plan="debug",
                   jobs=125,
                   qos=None,
                   binary="vasp.541_p3.cori_gamma")
