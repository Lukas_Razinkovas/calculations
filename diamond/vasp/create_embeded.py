import sys
import os
import pprint
sys.path.append("../../dephon")
from defc import ForceConstants
from dembeding import (embed_defect_inside_bulk,
                       calculate_modes_and_write_results)
from vasp import Poscar
from create_megabulks import create_job_file

pp = pprint.PrettyPrinter()

if __name__ == "__main__":
    for i in range(15, 16):
        out_dir = "embeded/nv{0}x{0}x{0}/".format(i)
        bulk_dir = "embeded/{0}x{0}x{0}/".format(i)

        defect_fc = ForceConstants('vasp',
                                   phonopydir="nv4x4x4/phonopy_phonons")
        bulk_nonrelaxed_atoms = Poscar("nv4x4x4/relax/POSCAR-1").create_atoms()
        defect_fc.atoms = bulk_nonrelaxed_atoms

        embed_defect_inside_bulk(
            defect_fc, bulk_dir, out_dir,
            symmetrize_fc=True,
            calculate_modes=False)

        name = "nv{0}x{0}x{0}d".format(i)
        create_job_file(out_dir, name)
