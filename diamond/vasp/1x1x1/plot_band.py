import re


def read_outcar_and_plot_band(filepath="OUTCAR"):
    with open(filepath, "rt") as f:
        content = f.read()
        kpoints = re.search(
            r"k-points in reciprocal lattice and weights:(.*)" +
            r"position of ions in fractional coordinates", content,
            flags=re.DOTALL)
        print(kpoints.group())

read_outcar_and_plot_band()
