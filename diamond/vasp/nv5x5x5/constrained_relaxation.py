import sys
import os
import numpy as np
sys.path.append("../../../dephon")
from vasp import Poscar
from deutils import ensure_dir


poscar = Poscar(os.path.join("relax", "POSCAR"))
atoms = poscar.atoms

indxN = atoms.symbols.index("N")

xyzcontent = ""
for i in range(len(atoms)):
    vec, dist = atoms.get_separation_vector(i, indxN)
    pos = "  ".join(
        ["{:2.6f}".format(k) for k in atoms.get_positions()[i].tolist()])
    if dist > 5:
        xyzcontent += "Fe  " + pos + "\n"
        poscar.constrains.append(["F", "F", "F"])
    else:
        xyzcontent += "C  " + pos + "\n"
        poscar.constrains.append(["T", "T", "T"])

DIR = "relax_constrained"
ensure_dir(DIR)
with open(os.path.join(DIR, "POSCAR"), "wt") as f:
    f.write(poscar.create_input())

xyzcontent = "	{}\n\"{}, cubic\"\n".format(
    len(atoms), "Unknown") + xyzcontent
with open(os.path.join(DIR, "relaxation.xyz"), "wt") as f:
    f.write(xyzcontent)
