import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

# create_displacements("vasp", 1)
generate_job_files("vasp", "cori", "NVe4x4x4vsp", 6, 192, plan="regular",
                   jobs=8, time="16:00:00", binary="vasp.541_p3.cori",
                   workdir="phonopy_phonons2", qos=None)
