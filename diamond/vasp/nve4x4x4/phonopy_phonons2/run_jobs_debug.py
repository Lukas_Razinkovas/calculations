import subprocess
import time

jobs = [
    "phonons1.job",
    "phonons2.job",
    "phonons3.job",
    "phonons4.job",
    "phonons5.job",
    "phonons6.job",
    "phonons7.job",
    "phonons8.job",
]

while jobs:
    proc = subprocess.Popen(['sqs', '-w'], stdout=subprocess.PIPE)
    lines = proc.communicate()[0].split()
    print(lines)
    time.sleep(2)