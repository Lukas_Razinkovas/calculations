import os
import shutil

files = os.listdir("phonopy_phonons")
missing = []
infiles = list(filter(lambda x: x.startswith("disp-"), files))


for i in infiles:
    out_file = os.path.join(os.path.join("phonopy_phonons", i), "OUTCAR")
    if not os.path.exists(out_file):
        shutil.copytree(
            os.path.join("phonopy_phonons", i),
            os.path.join("phonopy_phonons2", i))
        continue

    with open(out_file, "rt") as f:
        content = f.read()

    if "General timing and accounting informations" not in content:
        shutil.copytree(
            os.path.join("phonopy_phonons", i),
            os.path.join("phonopy_phonons2", i))
        continue
