import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

# create_displacements("vasp", 1, tolerance=0.02)
generate_job_files("vasp", "cori", "NV4x4x4vsp", 12, 384, plan="regular",
                   jobs=2, time="14:30:00", binary="vasp.541_p3.cori")
