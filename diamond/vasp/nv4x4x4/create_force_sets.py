"""When abinitio calculations finished call this for postprocessing"""

import sys
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("../../../dephon")

from dephonopy import create_force_sets, calculate_fcmatrix, create_plot
from defc import ForceConstants
from vasp import Poscar

# create_force_sets("vasp", workdir="phonopy_phonons")
# calculate_fcmatrix("vasp", workdir="phonopy_phonons", dim=1)

fc = ForceConstants("vasp", outdir="phonopy_phonons")

# atoms = Poscar("relax/POSCAR").create_atoms()
# fc.atoms = atoms
modes = fc.calculate_modes()
print(fc.get_frequencies("thz").max(), "THz")
print(fc.get_frequencies("cm").max(), "cm^-1")
print(fc.matrix.max())
print(fc.atoms.get_cell())
modes.analyse()


indx = np.where(modes.iprs < 100)[0]
with open("localized_modes.csv", "wt") as f:
    header = modes._mode_header()
    print(header)
    f.write(header + "\n")
    for i in indx:
        print(modes._mode_info(i))
        f.write(modes._mode_info(i) + "\n")

matrix = fc.get_mass_weighted_force_constants()
with open("matrix.csv", "wt") as f:
    f.write("{}\n".format(len(matrix)))
    for line in matrix:
        f.write(" ".join(["{:16.6f}".format(k) for k in line]) + "\n")

print()
print("#"*70)
print(" "*20, "FORTRAN")
print("#"*70)
print()

_evals = np.genfromtxt("evals_fortran.csv", skip_header=5, names=True,
                       dtype=float, delimiter=',',
                       comments="#")["evals"]
_evects = np.loadtxt(open("evects_fortran.csv", "rb"), delimiter=",")

modes = fc.calculate_modes(evals=_evals, evects=_evects.T)
modes.analyse()
print(fc.get_frequencies("thz").max(), "THz")
print(fc.get_frequencies("cm").max(), "cm^-1")
print(fc.matrix.max())
print(fc.atoms.get_cell())

indx = np.where(modes.iprs < 100)[0]
with open("localized_modes_fortran.csv", "wt") as f:
    header = modes._mode_header()
    print(header)
    f.write(header + "\n")
    for i in indx:
        print(modes._mode_info(i))
        f.write(modes._mode_info(i) + "\n")


# fc._evects = np.genfromtxt("evects_fortran.csv",
#                            dtype=float, delimiter=',', names=True,
#                            comment="#")
