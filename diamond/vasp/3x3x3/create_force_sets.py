"""When abinitio calculations finished call this for postprocessing"""

import sys
import matplotlib.pyplot as plt
sys.path.append("../../../dephon")

from dephonopy import create_force_sets, calculate_fcmatrix, create_plot
from defc import ForceConstants

create_force_sets("vasp", workdir="phonopy_phonons")
calculate_fcmatrix("vasp", workdir="phonopy_phonons", dim=1)

fc = ForceConstants("vasp", outdir="phonopy_phonons")
modes = fc.calculate_modes()
print(fc.get_frequencies("cm^-1").max(), "cm")
print(fc.get_frequencies("thz").max(), "thz")
print(fc.matrix.max())
