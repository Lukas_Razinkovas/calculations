import h5py

f = h5py.File("vibrations_e.hdf5", "r")
positions = f["init_positions"][:]
symbols = f["species"][:]
displacements = f["displacements"][:]
iprs = f["iprs"][:]
freq = f["frequencies"][:]
f.close()

indx = iprs.argsort()
iprs = iprs[indx]
displacements = displacements[indx]
freq = freq[indx]

for nr, displ in enumerate(displacements):

    text = "{}\n\n".format(len(symbols))
    for sym, pos, displ in zip(symbols, positions, displ):
        text += ("{} {:12.6f} {:12.6f} {:12.6f}" +
                 " {:12.6f} {:12.6f} {:12.6f}\n").format(
                     sym.decode(), *pos, *(10*displ))

    with open("xyz/mode_e{:0=2d}_ipr{}_freq{:.1f}.xyz".format(
            nr + 1, int(iprs[nr]), freq[nr]), "wt") as f:
        f.write(text)
