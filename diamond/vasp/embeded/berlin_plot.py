import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

##############################
#        Arguments
##############################
cell_size = 15
bound = 53.16


def gaussian_smoothing(x, y, sig=6, n=1000):
    def gaussian(x, mu, sig=sig):
        return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
            2 * np.power(sig, 2.)))

    x_new = np.linspace(0, 200, n)
    y_new = np.zeros(len(x_new))

    for _x, _y in zip(x, y):
        y_new += _y*gaussian(x_new, _x)

    return x_new, y_new


def get_hr_spectrum(mydir="hr_factors/HSEexcited-ground/", cell_size=7,
                    n=1000, sig=4, bulk=False):

    filename = "nv{0}x{0}x{0}_hr_spectrum.dat".format(cell_size)
    if bulk:
        filename = "{0}x{0}x{0}_hr_spectrum.dat".format(cell_size)
    data_filepath = os.path.join(mydir, filename)

    print("Reading HR factors from:\n - {}".format(data_filepath))
    data = np.loadtxt(data_filepath).T
    frequencies = data[0]
    hr_factors = data[1]
    x, y = gaussian_smoothing(frequencies, hr_factors, sig=sig, n=10000)
    hr_factor = hr_factors.sum()
    assert abs(np.trapz(y, x=x) - hr_factor) < 0.001
    return x, y, hr_factors.sum(), data


##############################
#         Routine
##############################

f, ax = plt.subplots(1, 1, sharex=True, figsize=(9, 3.8))

for_differece = []
lns = None

for bulk in [False, True][:1]:
    ##############################
    #    Huang-Rhys factors
    ##############################

    freq, sw, hr_factor, data = get_hr_spectrum(
        cell_size=cell_size, sig=5, n=1500, bulk=bulk)
    # axes[0].plot(freq, sw)
    freq2, sw2, hr_factor2, data = get_hr_spectrum(
        cell_size=cell_size, sig=2, n=1500, bulk=bulk)
    # axes[0].plot(freq2, sw2)

    indx1 = np.where(freq < bound)
    indx2 = np.where(freq2 >= bound)

    if not bulk:
        freq = np.concatenate((freq[indx1], freq2[indx2]))
        sw = np.concatenate((sw[indx1], sw2[indx2]))

    ln = ax.plot(
        freq, sw,
        label=(r"$S_{{\mathrm{{{}}}}}(\hbar\omega)\ " +
               r"(S_{{\mathrm{{total}}}}={:.2f})$").format(
                   "bulk\ phonons" if bulk else "defect\ phonons",
                   np.trapz(sw, x=freq)))
    if lns is None:
        lns = ln
    else:
        lns += ln

    continue
    ##############################
    #       Localization
    ##############################

    nm = "" if bulk else "nv"

    f = h5py.File(
        os.path.join(nm + "{0}x{0}x{0}".format(cell_size),
                     "mode_analysis.hdf5"), "r")
    freq = f["frequencies"][:]

    ##############################
    #    Filter A1 modes
    ##############################

    if not bulk:
        irreps = f['irreps'][:]
        indx = np.where(irreps == b'A1')
    else:
        print(len(data[1]), len(irreps))
        indx = np.where(data[1] > 0.01)

    indx = np.where(freq > 0)

    beta = 8*(cell_size**3)/f["iprs"][:]
    f.close()

    # axes[1].scatter(freq, beta, alpha=0.5, color="red")
    # fr, bt = gaussian_smoothing(freq, beta, sig=3)
    # axes[2].plot(fr, bt, color="red")

    # axes[1].scatter(freq[indx], beta[indx], alpha=0.5, color="blue")
    # fr, bt = gaussian_smoothing(freq[indx], beta[indx], sig=3)
    # axes[2].plot(fr, bt)
    # for_differece.append([fr, bt])

##############################
#       Beta parameter
##############################

new_x = np.linspace(0, 180, 1000)
cell_size = 4

f = h5py.File(
    os.path.join("nv{0}x{0}x{0}".format(cell_size),
                 "mode_analysis.hdf5"), "r")
freq = f["frequencies"][:]
beta = 8*(cell_size**3)/f["iprs"][:]
irreps = f['irreps'][:]
indx = np.where(irreps == b'A1')
freq = freq[indx]
beta = beta[indx]

ax2 = ax.twinx()
indx2 = np.where(beta > 2.4)
markerline, stemlines, baseline = ax2.stem(
    freq[indx2], beta[indx2],
    alpha=0.5, color="red")
color = "lightsteelblue"
color = "lightslategray"

plt.setp(stemlines, 'linestyle', 'dotted')
plt.setp(stemlines, 'color', color, "alpha", 1)
plt.setp(markerline, 'color', color, "markersize", 4)
plt.setp(baseline, "alpha", 0.001)

# x, y = gaussian_smoothing(freq[indx2], beta[indx2], sig=3)
# new_y1 = np.interp(new_x, x, y)
# f.close()

# f = h5py.File(
#     os.path.join("{0}x{0}x{0}".format(4),
#                  "mode_analysis.hdf5"), "r")
# freq = f["frequencies"][:]
# beta = 8*(cell_size**3)/f["iprs"][:]
# print("MAX BETA:", max(beta))
# indx = np.where(data[1] > 0.004)
# x, y = gaussian_smoothing(freq[indx], beta[indx], sig=1)
# new_y2 = np.interp(new_x, x, y)
# f.close()

# ln = ax2.plot(x, y, color="black",
#               label="DOS of localized modes")
# lns += ln

ax.set_xlabel("Phonon energy (meV)")
ax.set_ylabel("$S(\hbar\omega)$")
ax2.set_ylabel(r"$\mathrm{localization\ ratio}\ " +
               r"\left(\mathrm{nr\ of\ atoms}/\mathrm{IPR}\right)$")
# for_differece = np.array(for_differece)
# x = np.linspace(0, 180, 100)
# y = np.interp(x, for_differece[0][0], for_differece[0][1])
# y2 = np.interp(x, for_differece[1][0], for_differece[1][1])

# print(for_differece.shape)
# ax = axes[0].twinx()
# ax.plot(x, y-y2, color="gray")

# for ax in axes:
#     ax.grid()
#     ax.legend()
ax.set_ylim(bottom=0, top=0.1)
ax2.set_ylim(bottom=0)
ax.plot([0, -0.02], "o", markersize=3, alpha=0.8, color=color,
        label="highly localised modes")
ax.legend()
plt.tight_layout()
ax.grid(True, color="gray", lw=0.5)
plt.savefig("vibrational_structure2.pdf", dpi=300)
# plt.show()
