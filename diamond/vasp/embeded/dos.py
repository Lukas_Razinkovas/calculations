import h5py
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
from statsmodels.nonparametric.kde import KDEUnivariate
from sklearn.neighbors import KernelDensity
# f = h5py.File("nv13x13x13/modes.hdf5", "r")
# modes = f["frequencies"][:]
# f.close()
# np.savetxt("modes.csv", modes)

modes = np.loadtxt("modes.csv")


def kde_statsmodels_u(x, x_grid, bandwidth=0.2, **kwargs):
    """Univariate Kernel Density Estimation with Statsmodels"""
    kde = KDEUnivariate(x)
    kde.fit(bw=bandwidth, **kwargs)
    return kde.evaluate(x_grid)


def kde_sklearn(x, x_grid, bandwidth=2.3, **kwargs):
    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)


x = np.linspace(0, modes.max() + 20, 2000)
y = kde_sklearn(modes, x)

plt.plot(x, y)
plt.show()

# kde = gaussian_kde(modes[:1000])

# # kde.set_bandwidth(0.055)
# x = np.linspace(0, max(modes), 1000)
# kde = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(
#     modes[:, np.newaxis])
# log_dens = kde.score_samples(x[:, np.newaxis])

# # kde_skl.fit(modes)
# # log_pdf = kde_skl.score_samples(x)



# # kde = KDEUnivariate(modes)
# # kde.fit(bw=0.055)
# # y = kde.evaluate(x)
# # y = kde(x)
# # np.savetxt("dos.csv", np.array([x, y]))
# plt.plot(x, np.exp(log_dens))
# plt.savefig("dos.pdf", dpi=200)
