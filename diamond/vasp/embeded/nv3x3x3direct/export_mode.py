import h5py

f = h5py.File("vibrations.hdf5", "r")
nr = 29
displ = f["displacements"][nr]
positions = f["init_positions"]
symbols = f["species"]

text = "{}\n\n".format(len(symbols))
for sym, pos, displ in zip(symbols, positions, displ):
    text += "{} {:12.6f} {:12.6f} {:12.6f} {:12.6f} {:12.6f} {:12.6f}\n".format(
        sym.decode(), *pos, *(10*displ)
    )

with open("mode.xyz", "wt") as f:
    f.write(text)
