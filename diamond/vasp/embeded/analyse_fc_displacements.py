import h5py
import os
import sys
import re
import numpy as np
import pickle
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
from scipy.constants import physical_constants, electron_volt, hbar
from scipy.optimize import minimize

sys.path.append("../../../dephon")
from vasp import Poscar, Outcar, VaspXML
from deutils import ensure_dir

np.set_printoptions(precision=3, suppress=True)

hbar_ev = physical_constants["Planck constant in eV s"][0]/(2*np.pi)


def read_embeded_bulk_positions(filepath):
    with open(filepath, "rb") as f:
        atoms = pickle.load(f)

    return atoms


def read_force_differences(ground_vasprun, excited_vasprun, geometry_poscar):
    print("Reading forces from:")
    print(" -", ground_vasprun)
    print(" -", excited_vasprun)

    if "OUTCAR" in ground_vasprun:
        forces1 = Outcar(ground_vasprun).get_forces(units="N")
    else:
        forces1 = VaspXML(ground_vasprun).get_forces(units="N")

    if "OUTCAR" in excited_vasprun:
        forces2 = Outcar(excited_vasprun).get_forces(units="N")
    else:
        forces2 = VaspXML(excited_vasprun).get_forces(units="N")

    forces = forces2 - forces1
    atoms = Poscar(geometry_poscar).create_atoms()

    return atoms, forces


def read_displacements(ground_contcar, excited_contcar, geometry_poscar,
                       minimization=False):
    print("Reading displacements from:")
    print(" - ground:", ground_contcar)
    print(" - excited:", excited_contcar)
    print("Reading bulk geometry from:")
    print(" - base:", geometry_poscar)

    gatoms = Poscar(ground_contcar).create_atoms()
    eatoms = Poscar(excited_contcar).create_atoms()
    base_atoms = Poscar(geometry_poscar).create_atoms()

    assert np.allclose(gatoms.get_cell(), eatoms.get_cell()), (
        "Mismatch in cell sizes\n{}\n{}".format(gatoms.get_cell(),
                                                eatoms.get_cell()))

    base_atoms.set_cell(eatoms.get_cell())

    ##################################
    #  Ignore translational motion
    ##################################

    # --------------------------------
    #    Align with bulk geometry
    # --------------------------------

    for atoms in [gatoms, eatoms]:
        new_positions = []
        for pos, bpos in zip(
                atoms.get_positions(), base_atoms.get_positions()):
            v, d = base_atoms.get_separation_vector(bpos, pos)
            new_positions.append(bpos + v)

        new_positions = np.array(new_positions)
        atoms.set_positions(new_positions)

    # ----------------------------------------
    #   Calculate mass centers for all cells
    # ----------------------------------------

    bcenter = base_atoms.get_mass_center()
    gcenter = gatoms.get_mass_center()
    ecenter = eatoms.get_mass_center()

    print("Center of mass:")
    print(" - ground", gcenter)
    print(" - excited", ecenter)
    print(" - base", bcenter)

    # ------------------------------------------
    #   Allign mass centers with bulk geometry
    # ------------------------------------------

    gr_positions = gatoms.get_positions()
    ex_positions = eatoms.get_positions()
    ex_positions += (gcenter - ecenter)
    eatoms.set_positions(ex_positions)

    print("New excited center:", eatoms.get_mass_center())

    gr = gr_positions.copy()
    ex = ex_positions.copy()

    # ------------------------------------------
    #         Calculate displacements
    # ------------------------------------------

    displ = np.zeros(ex_positions.shape, dtype=float)
    for nr, (ep, gp) in enumerate(zip(eatoms.get_positions(), gr_positions)):
        v, d = eatoms.get_separation_vector(gp, ep)
        displ[nr] = v

    distances = np.apply_along_axis(np.linalg.norm, 1, displ)
    for nr, i in enumerate(distances):
        if i > 0.1:
            print("{:3}: {:.3f} {} {}".format(nr, i, ex[nr], gr[nr]))
            print(" "*9 + " ", ex_positions[nr], gr_positions[nr])
            print(" "*9 + " ", displ[nr])
            print("-"*70)

    print("Max displacement:", distances.max())

    return base_atoms, displ


def restructure(atoms, ref_poscar):
    indx = list(atoms.symbols).index("N")
    pos = atoms.get_positions()[indx]
    return pos


def iter_modes(hdf5file, mass_matrix):
    # Yields freq in (2 pi Hz) and displ in dimensionless units
    f = h5py.File(hdf5file, "r")
    for nr, freq in enumerate(f["frequencies"]):
        if nr < 3:
            continue
        displ = f["displacements"][nr]*mass_matrix
        # frequencie is read in meV
        yield (freq[0]*1e-3)/hbar_ev, displ/np.linalg.norm(displ)


def gaussian_smoothing(x, y, sig=6, n=1000, x_new=None):
    def gaussian(x, mu, sig=sig):
        return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
            2 * np.power(sig, 2.)))

    if x_new is None:
        x_new = np.linspace(0, 200, n)

    y_new = np.zeros(len(x_new))

    for _x, _y in zip(x, y):
        y_new += _y*gaussian(x_new, _x)

    return x_new, y_new


def calculate_hr_factors_from_forces(ground_output, excited_output,
                                     bulk_geometry_poscar, hdf5dir,
                                     defect_center=None, units="mev",
                                     cell_multipl=4):
    print("="*70)
    print("Calculating Huang-Rhys spectrum from force differences")
    print("="*70)

    print("Reading forces from:")
    print(" - ground:", ground_output)
    print(" - excited:", excited_output)
    print("Reading bulk geometry from:")
    print(" - base:", bulk_geometry_poscar, "(bulk geometry)")
    print("Reading vibrational spectrum from:", hdf5dir)

    # -----------------------------
    #  Read forces and positions
    # -----------------------------

    small_atoms, small_forces = read_force_differences(
        ground_output, excited_output, bulk_geometry_poscar)
    embeded_atoms = read_embeded_bulk_positions(
        os.path.join(hdf5dir, "atoms.p"))

    # -----------------------------
    #    Ensure same cell sizes
    # -----------------------------

    conventional_cell = small_atoms.get_cell()/cell_multipl
    embeded_dims = round(
        embeded_atoms.get_cell()[0][0]/conventional_cell[0][0])
    new_cell = embeded_dims*conventional_cell
    embeded_atoms.set_cell(new_cell)

    # -----------------------------
    #     Set defect center
    # -----------------------------

    if defect_center is None:
        defect_center = small_atoms.get_positions()[
            small_atoms.symbols.index("N")]

    # ---------------------------------------
    #   Embed forces into larger supercell
    # ---------------------------------------

    embeded_atoms.sort_positions()
    force_positions = []
    forces = np.zeros(len(embeded_atoms)*3).reshape(len(embeded_atoms), 3)

    # Iterate small supercell atoms and forces
    for nr, (pos, force) in enumerate(zip(
            small_atoms.get_positions(), small_forces)):

        vec, displ = small_atoms.get_separation_vector(defect_center, pos)

        # ignore forces further than 0.95*0.5*lattice_constant
        if displ > (embeded_atoms.get_cell()[0][0]/2)*0.95:
            continue

        indx = embeded_atoms.find_by_position(defect_center + vec, pbc=True,
                                              method=3)

        force_positions.append(embeded_atoms.get_positions()[indx])
        forces[indx] = np.array(force)

    tmp = np.apply_along_axis(np.linalg.norm, 1, forces)
    tmp = max(tmp)

    with open("test.xyz", "wt") as f:
        f.write('{}\n"bla bla"\n'.format(len(embeded_atoms)))
        positions = embeded_atoms.get_positions()
        symbols = embeded_atoms.get_symbols()

        for indx in range(len(embeded_atoms)):
            f.write(symbols[indx] + " ".join(
                ["{:12.6f}".format(i) for i in positions[indx]]))
            f.write(" ".join(
                ["{:12.6f}".format(i/tmp) for i in forces[indx]]) + "\n")

    # ---------------------------------------
    #            Weight forces
    # ---------------------------------------

    mass_array = embeded_atoms.get_masses(units="kg").repeat(3)
    print("MASSES:", set(mass_array), "kg")
    inv_square_masses = 1/np.sqrt(mass_array)
    weighted_forces = forces.flatten()*inv_square_masses

    # ---------------------------------------
    #         Calculate HR factors
    # ---------------------------------------

    HR_factors = []
    frequencies = []
    print("Reading Modes From:", os.path.join(hdf5dir, "modes.hdf5"))
    for nr, (freq, displ) in enumerate(iter_modes(
            os.path.join(hdf5dir, "modes.hdf5"), inv_square_masses)):
        print(nr, end="\r")

        if units.lower() == "mev":
            units = "meV"
            frequencies.append(freq*hbar_ev*1e3)
        else:
            units = "THz"
            frequencies.append(freq*1e-12/(2 * np.pi))

        fc_displ = (1/freq**2)*(weighted_forces.dot(displ))
        hr_factor = freq*(fc_displ**2)/(2*hbar)
        HR_factors.append(hr_factor)

    print("S =", sum(HR_factors))
    return {
        "HR_factors": np.array(HR_factors),
        "frequencies": np.array(frequencies),
        "S": sum(HR_factors)
    }


def calculate_hr_factors_from_displacements(
        ground_contcar, excited_contcar, bulk_geometry_poscar, hdf5dir,
        defect_center, units, cell_multipl=4):

    # -----------------------------
    #     Read displacements
    # -----------------------------

    small_atoms, small_displ = read_displacements(
        ground_contcar, excited_contcar, bulk_geometry_poscar)
    embeded_atoms = read_embeded_bulk_positions(
        os.path.join(hdf5dir, "atoms.p"))

    # -----------------------------
    #    Ensure same cell sizes
    # -----------------------------

    conventional_cell = small_atoms.get_cell()/cell_multipl
    embeded_dims = round(
        embeded_atoms.get_cell()[0][0]/conventional_cell[0][0])
    new_cell = embeded_dims*conventional_cell
    embeded_atoms.set_cell(new_cell)

    # -----------------------------
    #     Set defect center
    # -----------------------------

    if defect_center is None:
        defect_center = small_atoms.get_positions()[
            small_atoms.symbols.index("N")]

    # ---------------------------------------------
    #   Embed displacements into larger supercell
    # ---------------------------------------------

    embeded_atoms.sort_positions()
    displacements = np.zeros(
        len(embeded_atoms)*3).reshape(len(embeded_atoms), 3)

    for nr, (pos, d) in enumerate(zip(
            small_atoms.get_positions(), small_displ)):

        vec, _ = small_atoms.get_separation_vector(defect_center, pos)
        indx = embeded_atoms.find_by_position(defect_center + vec, pbc=True,
                                              method=3)
        displacements[indx] = np.array(d)

    print("NORM:", np.apply_along_axis(np.linalg.norm, 1, displacements).sum())

    # ---------------------------------------
    #         Weight displacements
    # ---------------------------------------

    mass_array = embeded_atoms.get_masses(units="kg").repeat(3)
    square_masses = np.sqrt(mass_array)

    displacements *= 1e-10
    weighted_displ = displacements.flatten()*square_masses
    print("Weighted_displ:", max(weighted_displ))

    # ---------------------------------------
    #         Calculate HR factors
    # ---------------------------------------

    HR_factors = []
    frequencies = []

    for nr, (freq, displ) in enumerate(iter_modes(
            os.path.join(hdf5dir, "modes.hdf5"), 1/square_masses)):
        print(nr, end="\r")

        if units.lower() == "mev":
            units = "meV"
            frequencies.append(freq*hbar_ev*1e3)
        else:
            units = "THz"
            frequencies.append(freq*1e-12/(2 * np.pi))

        fc_displ = weighted_displ.dot(displ)
        hr_factor = freq*(fc_displ**2)/(2*hbar)
        HR_factors.append(hr_factor)

    print("S = ", sum(HR_factors))

    return {
        "HR_factors": np.array(HR_factors),
        "frequencies": np.array(frequencies),
        "S": sum(HR_factors),
        "xyzfile_content": embeded_atoms.create_xyz_content(displacements)
    }


def calculate_hr_factors(ground_output, excited_output,
                         bulk_geometry_poscar, hdf5dir,
                         name, outdir, from_forces=True,
                         defect_center=None, units="mev", ax=None, ax2=None,
                         marker="O"):
    print("#"*70)
    print(name)
    print("#"*70)
    ensure_dir(outdir)

    if from_forces:
        data = calculate_hr_factors_from_forces(ground_output, excited_output,
                                                bulk_geometry_poscar, hdf5dir,
                                                defect_center, units)
    else:
        data = calculate_hr_factors_from_displacements(
            ground_output, excited_output,
            bulk_geometry_poscar, hdf5dir,
            defect_center, units)

    save = False
    if ax is None:
        save = True
        f, ax = plt.subplots(1, figsize=(11.69, 8.27))

    x, y = gaussian_smoothing(data["frequencies"], data["HR_factors"])

    a = ax.plot(x, y, label="{} $S={:.3}$".format(name, data["S"]))
    color = a[-1].get_color()

    ax.set_xlim([0, 180])
    ax.set_xlabel("Phonon Energy (meV)")
    ax.set_ylabel("$S(\hbar\omega)$")

    if ax2 is None:
        ax2 = ax.twinx()
        ax2.set_ylabel("$S_k$")
        ax2.set_xlim([0, 180])

    indx = np.where(data["HR_factors"] > 0.001)

    markerline, stemlines, baseline = ax2.stem(
        data["frequencies"][indx], data["HR_factors"][indx], "-",
        markerfmt=marker)

    plt.setp(
        markerline, 'color', color, 'markerfacecolor', color, 'alpha', 0.7)
    plt.setp(stemlines, 'color', color, 'markerfacecolor', color, 'alpha', 0.7)
    plt.setp(baseline, 'color', color, 'linewidth', 2, 'alpha', 0.7)

    # ax.legend()
    ax.grid(color="gray", lw=0.1, alpha=0.5)

    if save:
        ax2.legend()
        plt.savefig(os.path.join(outdir, "{}_hr_spectrum.pdf".format(name)),
                    dpi=200)
        plt.clf()

    return data, ax2


if __name__ == "__main__":

    OUTPUTS = {
        # "HSE:ground->excited": dict(
        #     ground="../../vasp_hybrid/nv4x4x4/relaxed/OUTCAR",
        #     excited="../../vasp_hybrid/nve4x4x4/ground_geometry/OUTCAR",
        #     geometry="../../vasp_hybrid/nv4x4x4/bulk_geometry/POSCAR",
        #     forces=True,
        #     marker="o"),
        "HSE:excited->ground": dict(
            ground="../../vasp_hybrid/nv4x4x4/excited_geometry/vasprun.xml",
            excited="../../vasp_hybrid/nve4x4x4/relaxed/vasprun.xml",
            geometry="../../vasp_hybrid/nv4x4x4/bulk_geometry/POSCAR",
            marker="o",
            forces=True),
        "HSE:displacements": dict(
            ground="../../vasp_hybrid/nv4x4x4/relaxed/POSCAR",
            excited="../../vasp_hybrid/nve4x4x4/relaxed/POSCAR",
            geometry="../../vasp_hybrid/nv4x4x4/bulk_geometry/POSCAR",
            forces=False,
            marker="o"),
        # "PBE4x4x4:displacements": dict(
        #     dim=4,
        #     ground="../nv4x4x4/relax/CONTCAR",
        #     excited="../nve4x4x4/relax/CONTCAR",
        #     geometry="../nv4x4x4/relax/POSCAR",
        #     forces=False,
        #     marker="o"),
        # "PBE5x5x5:displacements": dict(
        #     dim=5,
        #     ground="../nv5x5x5/relax/CONTCAR",
        #     excited="../nve5x5x5/relax/CONTCAR",
        #     geometry="../nv5x5x5/relax/POSCAR-0",
        #     forces=False,
        #     marker="o"),
        # "PBE4x4x4:excited->ground": dict(
        #     dim=4,
        #     ground="../nv4x4x4/excited_geometry/vasprun.xml",
        #     excited="../nve4x4x4/relax/vasprun.xml",
        #     geometry="../nv4x4x4/relax/POSCAR",
        #     forces=True,
        #     marker="D"),
        # "PBE:ground->excited": dict(
        #     ground="../nv4x4x4/relax/vasprun.xml",
        #     excited="../nve4x4x4/ground_geometry/vasprun.xml",
        #     geometry="../nv4x4x4/relax/POSCAR",
        #     forces=True,
        #     marker="s")
        }

    # Audriaus
    # ground = "audriaus/OUTCAR_g"
    # excited = "audriaus/OUTCAR_e"
    # geometry = "audriaus/POSCAR"

    output_dir = "hr_factors"

    for i in range(6, 7, 1):
        embeded_dir = "nv{0}x{0}x{0}/".format(i)
        print("!"*70)
        print("!"*70)
        print(embeded_dir)
        print("!"*70)
        print("!"*70)

        for name in sorted(OUTPUTS.keys()):
            f, ax = plt.subplots(1, figsize=(11.69, 8.27))
            ax2 = None

            if OUTPUTS[name].get("dim", 4) > i:
                continue
            ground = OUTPUTS[name]["ground"]
            excited = OUTPUTS[name]["excited"]
            geometry = OUTPUTS[name]["geometry"]
            marker = OUTPUTS[name]["marker"]
            forces = OUTPUTS[name]["forces"]
            data, ax2 = calculate_hr_factors(
                ground, excited,
                geometry, embeded_dir,
                embeded_dir[:-1] + name, output_dir,
                from_forces=forces,
                defect_center=[4, 4, 4],
                ax=ax, ax2=ax2,
                marker=marker)

            # ------------------------------------
            #             Write data
            # ------------------------------------

            path = os.path.join(
                output_dir, name.replace("->", "-").replace(":", ""),
                "{}_hr_spectrum.dat".format(
                    embeded_dir[:-1]))

            ensure_dir(os.path.dirname(path))

            np.savetxt(
                path, np.array([data['frequencies'], data['HR_factors']]).T,
                header='frequencies, HR_factors', fmt=['%16.12f', '%18.12f'])

            ax.legend()
            ax2.set_ylim([0, ax2.get_ylim()[1]])
            ax.set_ylim([0, ax.get_ylim()[1]])

            # ax2.legend()
            plt.savefig(
                os.path.join(
                    output_dir, name.replace("->", "-").replace(":", ""),
                    "{}_hr_spectrum.pdf".format(
                        embeded_dir[:-1])),
                dpi=200)
            plt.clf()

