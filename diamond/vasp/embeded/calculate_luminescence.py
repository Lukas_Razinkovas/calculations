import os
import numpy as np
from analyse_fc_displacements import gaussian_smoothing
import matplotlib.pyplot as plt
from scipy.constants import physical_constants
from experimental import get_experimental_spectrum
from scipy.optimize import minimize
import h5py

h = physical_constants["Planck constant in eV s"][0]
hbar = h/(2*np.pi)


def find_two_nearest(array, value):
    idx = (np.abs(array - value)).argsort()
    return idx[:2]


def get_hr_spectrum(mydir="hr_factors/HSEexcited-ground/", cell_size=7,
                    n=1000, sig=4,
                    jt_dir="../../vasp_hybrid/nve4x4x4jt/jt_data/",
                    sym=None):

    data_filepath = os.path.join(
        mydir, "nv{0}x{0}x{0}_hr_spectrum.dat".format(cell_size))

    print("Reading HR factors from:\n - {}".format(data_filepath))
    data = np.loadtxt(data_filepath).T

    frequencies = data[0]
    hr_factors = data[1]
    if sym is not None:
        data_file = os.path.join(mydir, "nv{0}x{0}x{0}_couplings.dat".format(
            cell_size))

        dtypes = [
            ('freq', float),
            ('freq_mev', float),
            ('fc_displ', float),
            ('k', float),
            ('irreps', "U5")]

        if "forces" in mydir:
            dtypes.append(('forces', float))

        data = np.genfromtxt(
            data_file,
            autostrip=True,
            dtype=dtypes,
            delimiter=",")

        if isinstance(sym, list):
            indx = np.where(
                (data["irreps"] == sym[0]) | (data["irreps"] == sym[1]))
        else:
            indx = np.where(data["irreps"] == sym)
        frequencies = frequencies[indx]
        hr_factors = hr_factors[indx]

    x, y = gaussian_smoothing(frequencies, hr_factors, sig=sig, n=10000)
    hr_factor = hr_factors.sum()
    assert abs(np.trapz(y, x=x) - hr_factor) < 0.001
    return x, y, hr_factors.sum()


def fourier_transform(x, y, lim=None, n=10000, inverse=False):
    print("Performing {} fourier transform...".format(
        "inverse" if inverse else "direct"))

    if lim is None:
        lim = [-10, 10]

    omega = np.linspace(lim[0], lim[1], n)
    f = np.zeros(omega.shape, dtype="complex")

    if not inverse:
        prefactor = 1/(2*np.pi)
        sign = 1
    else:
        prefactor = 1
        sign = -1

    for nr, w in enumerate(omega):
        if nr % 1000 == 0:
            print(nr, end="\r")
        val = prefactor*np.trapz(y*np.exp(sign*1j*w*x), x=x)
        f[nr] = val

    return omega, f


def test_fourier_transform():
    f, axes = plt.subplots(2, 1, figsize=(8, 8))

    def gaussian(x, mu, sig):
        return 1/(sig*np.sqrt(2*np.pi))*np.exp(
            -np.power(x - mu, 2.)/(2*np.power(sig, 2.)))

    x = np.linspace(-10, 10, 1000)
    y = gaussian(x, 0, 2)

    w, f = fourier_transform(x, y)
    x2, y2 = fourier_transform(w, f, inverse=True)

    axes[0].plot(x, y, label="function")
    axes[1].plot(w, f, label="transform")
    axes[0].plot(x2, y2, label="funnction_from_transform")

    axes[0].legend()
    axes[1].legend()
    plt.savefig("test_ft.pdf")
    plt.clf()


# test_fourier_transform()
def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx


def calculate_luminescence_line(
        mydir="hr_factors/HSEexcited-ground/",
        cell_size=7, n=12000, gamma=1, sig=6,
        figpath="test.pdf", title=None,
        jt_dir="../../vasp_hybrid/nve4x4x4jt/jt_data/",
        sym=None, jt_spectrum=None):

    ##############################
    #      HR spectral line
    ##############################

    f, axes = plt.subplots(5, 1, figsize=(6, 13))
    freq, sw, hr_factor = get_hr_spectrum(
        mydir=mydir, cell_size=cell_size, sig=5, n=1500, jt_dir=jt_dir,
        sym=sym)
    freq2, sw2, hr_factor2 = get_hr_spectrum(
        mydir=mydir, cell_size=cell_size, sig=2, n=1500, jt_dir=jt_dir,
        sym=sym)
    omega_ev = freq.copy()
    axes[0].plot(freq, sw, label="$S={}$".format(hr_factor))
    axes[0].plot(freq2, sw2, label="$S={}$".format(hr_factor))

    bound = 55
    bound = 53.16

    # Transform to 2pi*Hz
    indx1 = np.where(freq < bound)
    indx2 = np.where(freq2 >= bound)
    freq = np.concatenate((freq[indx1], freq2[indx2]))
    sw = np.concatenate((sw[indx1], sw2[indx2]))
    omega = freq/1e3/hbar
    axes[1].plot(omega, sw, label="$S={}$".format(hr_factor))

    for nr, units in zip(range(2), ["meV", r"$2\pi$ Hz"]):
        axes[nr].set_xlabel(r"$\omega$ ({})".format(units))
        axes[nr].set_ylabel(r"$S(\omega)$")
        axes[nr].grid(lw=0.5, color="grey")
        axes[nr].legend()

    ##############################
    #  Transform to time domain
    ##############################

    dt = omega[1] - omega[0]
    dt = freq[1] - freq[0]
    lim = (np.pi/dt)*0.05
    t, s = fourier_transform(freq, sw, lim=[-lim, lim], n=n, inverse=True)

    axes[2].plot(t, s.real,
                 label=r"$\mathrm{Re}\,[S(t)=\mathcal{F}[S(\omega)]]$")
    axes[2].plot(t, s.imag,
                 label=r"$\mathrm{Im}\,[S(t)=\mathcal{F}[S(\omega)]]$")
    axes[2].set_xlim(-lim*0.1, lim*0.1)
    axes[2].set_xlabel(r"$t$ (s)")
    axes[2].set_ylabel(r"$S(t)$")
    axes[2].set_xlim(-0.7, 0.7)

    ##############################
    #           Test
    ##############################

    omega2, sw2 = fourier_transform(t, s, n=n, lim=[0, freq.max()])
    axes[0].plot(omega2, sw2, lw=5, alpha=0.5,
                 label=r"$\mathcal{F}^{-1}[S(t)]$")

    ##############################
    #    Generating function
    ##############################

    print(s.max())
    G = np.exp(s - hr_factor)
    print(G.max())
    axes[3].plot(t, G.real, label=r"$\mathrm{Re}\,G(t)$")
    axes[3].plot(t, G.imag, label=r"$\mathrm{Im}\,G(t)$")
    axes[3].set_xlim(-1, 1)

    ###################
    #   Luminescence
    ###################

    # ax = axes[4].twinx()
    dt = t[1] - t[0]
    lim = (np.pi/dt)*0.3
    freq, intensity = fourier_transform(
        t, G*np.exp(-gamma*abs(t)), lim=[-lim, lim], n=n)
    intensity = np.flip(intensity, 0).real

    freq = freq/1000 + 1.945

    if jt_spectrum is not None:
        jt_data = np.loadtxt("lum.dat").T
        # jt_freq = jt_data[0]/1000 + 1.945
        # jt_intensity = jt_data[1]
        # _, add = gaussian_smoothing(
        #     jt_freq, jt_intensity, x_new=freq, sig=0.01)
        # intensity += add

    intensity *= freq**3
    intensity = intensity.real/np.trapz(intensity, x=freq)

    ##############################
    #   Experimental spectrum
    ##############################

    data = get_experimental_spectrum()
    efr = data["smoothed"]["x"]
    eint = data["smoothed"]["y"]

    indx = np.where(freq < 1.62)
    tailfreq = freq[indx]
    tailint = intensity[indx]

    factor = eint[find_nearest(efr, 1.62)]/tailint[-1]
    tailint *= factor

    # axes[4].plot(
    #     data["direct"]["x"],
    #     data["direct"]["y"],
    #     label="experimental")
    axes[4].set_ylim(0, 5)
    idx = np.where(data["direct"]["x"] > tailfreq[-1])
    efr = np.concatenate((tailfreq, data["direct"]["x"][idx]))
    eint = np.concatenate((tailint, data["direct"]["y"][idx]))

    axes[4].plot(efr, eint/np.trapz(eint, x=efr))
    axes[4].plot(
        freq,
        intensity,
        label="theoretical",
        color="red")

    indx = np.where((freq > data["direct"]["x"].min()) &
                    (freq < data["direct"]["x"].max()))
    ax = axes[4].twinx()
    omega_ev = 1.945 - omega_ev/1e3
    indx = omega_ev.argsort()
    ax.plot(omega_ev[indx], sw[indx], "--", lw=0.5, color="gray")
    ax.set_ylim(0, ax.get_ylim()[1])

    axes[4].grid(True, lw=0.8, color="gray")
    axes[4].legend()
    axes[4].set_xlim(1.62, 2)
    axes[4].set_ylim(0, 6)

    if title is not None:
        plt.title(title)

    plt.tight_layout()
    plt.savefig(figpath, dpi=300)

    plt.figure()
    plt.clf()

    ##############################
    #       Luminescence
    ##############################

    _, ax = plt.subplots(1, 1, figsize=(10, 8))

    ax2 = ax.twinx()
    indx = omega_ev.argsort()
    data2 = {}

    # ln3 = ax2.plot(omega_ev[indx], sw[indx], "--", lw=1.5,
    #                color="gray",
    #                label=(r"spectral function " +
    #                       r"$S(\hbar\omega)={:.2f}$".format(hr_factor)))

    data2["S"] = {
        "x": omega_ev[indx],
        "y": sw[indx]
    }

    ax2.set_ylim(0, ax2.get_ylim()[1])

    input(np.trapz(eint, x=efr))
    ln1 = ax.plot(
        data["direct"]["x"],
        data["direct"]["y"]/np.trapz(eint, x=efr),
        lw=2.5, label="experiment")

    data2["expr"] = {"x": data["direct"]["x"],
                     "y": data["direct"]["y"]/np.trapz(eint, x=efr)}

    ln2 = ax.plot(
        freq,
        intensity.real/np.trapz(intensity, x=freq),
        lw=2.5,
        label="theory")

    data2["thr"] = {
        "x": freq,
        "y": intensity.real/np.trapz(intensity, x=freq)
    }

    data2["Ssum"] = hr_factor

    if jt_spectrum:
        data2["jt_data"] = {
            "x": jt_data[0],
            "y": jt_data[1]
        }

    # np.savetxt(
    #     "lum.dat", np.array([
    #         freq,
    #         intensity.real/np.trapz(intensity, x=freq)
    #     ]).T)

    ax.set_ylim(0, 5)
    ax.set_xlim(1.5, 2)

    # ax2.set_ylabel(r"$S(\hbar\omega)$ (meV$^{-1}$)")

    # ax2.set_xlim(1.5, 2)
    ax.grid(True, lw=0.5, color="gray")
    ax.set_xlabel("energy (eV)")
    ax.set_ylabel("Intensity")

    lns = ln1 + ln2
    # lns = ln2
    ax.legend(lns, [l.get_label() for l in lns])
    ax.get_yaxis().set_tick_params(direction='in')
    ax.get_xaxis().set_tick_params(direction='in')

    plt.tight_layout()
    plt.savefig("luminescence.ps", dpi=300, orientation='landscape')
    plt.savefig("luminescence2.pdf", dpi=300)

    return data2

if __name__ == "__main__":
    cell_size = 15
    hr_dir = "../../couplings/"

    final_plot = []

    tests = [
        ["asymmetric_forces_ground_state", "A1"],
        ["symmetric_forces_ground_state", "A1"],
        ["asymmetric_forces_ground_state", None]][:1]

    for name, sym in tests:

        mydir = os.path.join(hr_dir, name)

        dat = calculate_luminescence_line(
            mydir=mydir, cell_size=cell_size, gamma=0.45, sig=5,
            figpath="displacements.pdf",
            title="Luminescence", sym=sym, jt_spectrum=True)

        if sym is None:
            name += "(all_modes)"
        else:
            name += "({}_modes)".format(sym)

        final_plot.append([dat, name])

if True:
    plt.clf()
    f, ax = plt.subplots(1, 1, figsize=(10, 8))

    ax.plot(dat["expr"]["x"], dat["expr"]["y"], lw=2.5, label="expr")
    for dat, nm in final_plot:
        ax.plot(dat["thr"]["x"], dat["thr"]["y"], lw=2.5,
                label=r"{} $S={:.2f}$".format(nm, dat["Ssum"]))

    ax2 = ax.twinx()
    ax2.stem((dat["jt_data"]["x"] + 1945)/1000, dat["jt_data"]["y"],
             label="jt. eff. mode trans. str.")
    ax2.set_ylim(bottom=0)
    ax2.legend()

    ax.set_ylim(0, 5)
    ax.set_xlim(1.5, 2)

    # ax2.set_xlim(1.5, 2)
    ax.grid(True, lw=0.5, color="gray")
    ax.set_xlabel("energy (eV)")
    ax.set_ylabel("Intensity")

    # ax2 = ax.twinx()
    # ax2.stem((dat["jt_data"]["x"] + 1945)/1000, dat["jt_data"]["y"])

    ax.legend()
    plt.tight_layout()
    plt.savefig("luminescence_all.pdf", dpi=300, orientation='landscape')


# mydir = "hr_factors/HSEexcited-ground/"
# calculate_luminescence_line(mydir=mydir, cell_size=14, gamma=0.6, sig=2,
#                             figpath="forces.pdf",
#                             title=mydir.replace(
#                                 "hr_factors/", "").replace("/", ""))


##############################
#       Luminescence
##############################

# ----------------------------
#    Find right parameters
# ----------------------------



# def calculate_luminescence(t, G, gamma, multiplier):
    # dt = t[1] - t[0]
    # lim = (np.pi/dt)*0.3

    # freq, intensity = fourier_transform(
    #     t, G*np.exp(-gamma*abs(t)), lim=[-lim, lim], n=n)

    # intensity = np.flip(intensity, 0)*multiplier
    # freq /= 1e3
    # freq = freq*1e3*hbar*1e12 + 1.945

#     return freq, intensity


# def minimization_function(listas):
#     gamma = listas[0]
#     multiplier = listas[1]
#     interval = np.linspace(1.5, 1.95, 100)

#     freq, intensity = calculate_luminescence(t, G, gamma, multiplier)

#     th_dat = np.interp(interval, freq, intensity)
#     exp_dat = np.interp(interval, exp_x, exp_y)

#     # axes[3].scatter(interval, th_dat, alpha=0.3)
#     # axes[3].scatter(interval, exp_dat, alpha=0.3)

#     return (th_dat - exp_dat).sum().real

# # res = minimize(minimization_function, [0.3, 1e7])
# # print("MINIMIZATION FINISHED:", res.x)
# # print(minimization_function())
# freq, intensity = calculate_luminescence(t, G, 2, 5*1e2)

# axes[3].plot(freq, intensity, label="theoretical")
# axes[3].set_xlim(1.5, 1.98)

# axes[3].plot(data["smoothed"]["x"], data["smoothed"]["y"],
#              label="experimental")
# axes[3].set_ylim(0, 5)

# ax = axes[3].twinx()

# omega_ev = 1.945 - omega_ev
# indx = omega_ev.argsort()
# ax.set_ylim(0, 0.07)
# axes[3].set_xlim(1.5, 1.98)


# for ax in axes:
#     ax.legend()

# plt.tight_layout()
# plt.savefig("test.pdf")
# plt.clf()


# # freq /= h
# # freq *= 2*np.pi
# # sw = sw*(h/(2*np.pi))

# # axes[0].plot(freq, sw)
# # # axes[0].set_xlim(0, 2*np.pi)


# # def get_fourier_coeficient(t, s, omega):
# #     return np.trapz(s*np.exp(-1j*omega*t), x=omega)

# # t = []
# # st = []
# # for nr, i in enumerate(np.linspace(-1e7, 1e7, 500000)):
# #     if nr % 1000 == 0:
# #         print(nr, end="\r")

# #     t.append(i)
# #     st.append(get_fourier_coeficient(i, sw, freq))

# # t = np.array(t)
# # st = np.array(st)
# # a = gaussian_smoothing(t, st.real)
# # b = gaussian_smoothing(t, st.imag)


# # axes[1].plot(*a, label="real")
# # axes[1].plot(*b, label="imag")

# # # t = np.array(t)
# # # G = np.exp(st - get_fourier_coeficient(0, sw, freq))

# # # A = []
# # # for w in freq:
# # #     A.append(np.trapz(G*np.exp(1j*w*t), x=t))


# # # st = np.array(st)
# # # plt.xlim([0, 0.1])



# print("OK")

# # print(y)


# # a = np.fft.ifft(y)
# # print(a[0])
# # plt.plot(a)
