import numpy as np
import h5py


f = h5py.File("nv11x11x11/modes.hdf5", "r")
for i in range(3):
    freq = f["frequencies"][0]
    displ = f['displacements'][0]
    displ /= np.linalg.norm(displ)
f.close()
