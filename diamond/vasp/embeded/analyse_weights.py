import h5py
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from collections import Counter

sys.path.append("../../../dephon")
from deutils import gaussian_kde
from demodes import Modes
colors = cm.rainbow(np.linspace(0, 1, 3))
fig, axes = plt.subplots(5, 4, figsize=(26, 26), sharex=True)


peaks = np.array([1743.502824858757,
                  1757.6271186440679,
                  1781.3559322033898,
                  1791.5254237288136,
                  1806.7796610169491,
                  1819.774011299435,
                  1880.7909604519773])

zpl = 1945.1977401129943

peaks = zpl - peaks
localized = []

ir = {}

for i, m, l in zip(["A1", "A2", "E"], ["o", "s", "v"], ["-", '--', ':']):
    ir[i] = {"indx": [],
             "avg": [],
             "marker": m,
             "linestyle": l}

for iii, symtype in enumerate(["", b"A1", b"A2", b"Exy"]):
    cnr = 0
    for i in [11, 13, 15]:
        directory = "nv{0}x{0}x{0}".format(i)
        if i == 3:
            directory = "nv{0}x{0}x{0}direct".format(4)
            i = 4

        print(directory)
        fl2 = os.path.join(directory, "mode_analysis.hdf5")

        nr = 8*i*i*i - 1
        f = h5py.File(fl2, "r")
        freq = f["frequencies"][:]
        iprs = f["iprs"][:]
        weights = f["weights"][:]
        irreps = f['irreps'][:]

        if symtype != "":
            indx = np.where(irreps == symtype)
            if symtype == b"Exy":
                indx = np.where((irreps == symtype) | (irreps == b"Ex") |
                                (irreps == b"Ey"))
            freq = freq[indx]
            iprs = iprs[indx]
            weights = weights[indx]

        sphere_volume = 4/3 * np.pi * (4**3)
        lattice_volume = (i*3.5620940727273465)**3
        part = sphere_volume/lattice_volume
        part *= 5
        print("PART:", part)

        indx = np.where(weights > part)

        s = axes[0][iii].scatter(freq, nr/iprs, color=colors[cnr],
                                 label=directory + r" $\beta$", alpha=0.5)
        s.set_rasterized(True)

        if symtype == "":
            axes[0][iii].set_title("All modes")
        else:
            axes[0][iii].set_title(symtype.decode())

        s = axes[1][iii].scatter(freq[indx], weights[indx], color=colors[cnr],
                                 label=directory +
                                 " weights", alpha=0.5)
        s.set_rasterized(True)

        bw = 0.055
        kde = gaussian_kde(freq, bw)
        x = np.linspace(0, freq.max() + 10, 1000)
        y = kde(x)
        axes[2][iii].plot(x, y, color=colors[cnr],
                          label=directory + " full DOS")

        # bw = 0.04
        kde = gaussian_kde(freq, bw, weights=weights)
        axes[3][iii].plot(x, kde(x), color=colors[cnr],
                          label=directory + " weighted DOS")

        kde = gaussian_kde(freq, bw, weights=weights*(weights > part))
        axes[4][iii].plot(x, kde(x), color=colors[cnr],
                          label=directory + " weighted DOS (localised)")

        cnr += 1

    # axes[1].scatter(peaks, peaks*0,
    #                 label="ph sideband peaks")
    for nr, ax in enumerate(axes[:, iii]):
        ax.legend(loc=2)
        ax.set_xlim([0, freq.max() + 10])
        ax.grid(True)

        if nr == 0:
            ax.set_ylabel("atoms/ipr")
        elif nr == 1:
            ax.set_ylabel("projection into 4A radius sphere")
        elif nr == 2:
            ax.set_ylabel("DOS")
        elif nr == 3:
            ax.set_ylabel("Projected DOS")
        elif nr == 4:
            ax.set_ylabel(
                r"Projected DOS " +
                r"$\left(\mathrm{weight} = 0\ \mathrm{if}\ "
                r"\mathrm{weight} < 4\frac{V_{sphere}}{V_{supercell}}\right)$")

        ax.scatter(peaks, peaks*0,
                   label="ph sideband peaks", color="black")

        ax.set_xticks(np.arange(0, 190, 10))


    # axes[1].legend(loc=2)
    # axes[0].set_ylabel("sites/ipr")
    # axes[1].set_xlabel("meV")
    # axes[1].set_ylabel("DOS of localized")
    # axes[0].set_xticks(np.arange(0, 190, 10))

    # axes[1].grid(True)

plt.tight_layout()
plt.savefig("weights11_13_15.pdf", dpi=300)
# plt.show()
plt.clf()
