import numpy as np
import matplotlib.pyplot as plt

freq = []
iprs = []
with open("freq.dat", "rt") as f:
    lines = f.readlines()

for l in lines[1:]:
    freq.append(float(l.split()[1]))
    iprs.append(float(l.split()[2]))

iprs = np.array(iprs)
freq = np.array(freq)
plt.scatter(freq, 17576/iprs)
plt.show()
