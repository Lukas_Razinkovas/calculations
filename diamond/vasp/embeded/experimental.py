import numpy as np
from statsmodels.nonparametric.smoothers_lowess import lowess
import matplotlib.pyplot as plt

DATA = "../experiment/expt-UCSB/8K-data.dat"


def filter_noise(x, y, frac=0.01, delta=0.003):
    filtered = lowess(y, x, is_sorted=True, delta=delta, frac=frac, it=0)
    return filtered[:, 0], filtered[:, 1]


def get_experimental_spectrum(data_filepath=None):
    if data_filepath is None:
        data_filepath = DATA
    freq, intensity = np.loadtxt(data_filepath, delimiter=" ").T

    indx = np.where(freq < 1.87)
    x, y = filter_noise(freq[indx], intensity[indx], frac=0.02)

    indx = np.where((freq < 1.92) & (freq > 1.87))
    x2, y2 = filter_noise(freq[indx], intensity[indx], frac=0.05)

    indx = np.where(freq >= 1.92)
    x3, y3 = filter_noise(freq[indx], intensity[indx], frac=0.003, delta=0)

    freq_smoothed = np.concatenate([x, x2, x3])
    intensity_smoothed = np.concatenate([y, y2, y3])
    indx = freq_smoothed.argsort()
    freq_smoothed = freq_smoothed[indx]
    intensity_smoothed = intensity_smoothed[indx]

    return {
        "direct": {
            "x": freq,
            "y": intensity
        },
        "smoothed": {
            "x": freq_smoothed,
            "y": intensity_smoothed
        }
    }


if __name__ == "__main__":
    f, axes = plt.subplots(3, 1, figsize=(8, 12))
    data = get_experimental_spectrum()

    for nm in sorted(data.keys()):
        lw = 1
        alpha = 0.5
        if nm != "direct":
            lw = 2
            alpha = 0.3

        for nr, rng in enumerate([[1.5, 1.8], [1.8, 1.92], [1.92, 1.96]]):
            axes[nr].plot(data[nm]["x"], data[nm]["y"], label=nm,
                          lw=lw, alpha=alpha)
            axes[nr].set_xlim(rng)
            indx = np.where(
                (rng[0] < data[nm]["x"]) & (rng[1] > data[nm]["x"]))
            dt = data[nm]["y"][indx]
            axes[nr].set_ylim([0, dt.max()*1.1])

    plt.tight_layout()
    plt.legend()
    plt.savefig("test.pdf")
