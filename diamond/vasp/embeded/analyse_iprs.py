import h5py
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy.stats import gaussian_kde
from collections import Counter

sys.path.append("../../../dephon")
from demodes import Modes
colors = cm.rainbow(np.linspace(0, 1, 4))
fig, axes = plt.subplots(2, 1, figsize=(16, 16), sharex=True)


peaks = np.array([1743.502824858757,
                  1757.6271186440679,
                  1781.3559322033898,
                  1791.5254237288136,
                  1806.7796610169491,
                  1819.774011299435,
                  1880.7909604519773])

zpl = 1945.1977401129943

peaks = zpl - peaks
localized = []

ir = {}

for i, m, l in zip(["A1", "A2", "E"], ["o", "s", "v"], ["-", '--', ':']):
    ir[i] = {"indx": [],
             "avg": [],
             "marker": m,
             "linestyle": l}

cnr = 0
for i in range(3, 4):
    directory = "nv{0}x{0}x{0}direct".format(i)
    # if i in {11, 12}:
    #     continue

    print(directory)
    fl = os.path.join(directory, "modes.hdf5")
    fl2 = os.path.join(directory, "mode_analysis.hdf5")

    if not os.path.exists(fl):
        continue

    if not os.path.exists(fl2):
        print("ANALYSING")
        modes = Modes(hdf5dir=directory)
        fl3 = os.path.join(directory, "freq.dat")
        modes.analyse_mode_by_mode(write_to_file=fl3)
        modes.save_to_hdf5(directory)

    nr = 8*i*i*i - 1
    f = h5py.File(fl2, "r")
    iprs = f["iprs"][:]
    freq = f["frequencies"][:]
    irreps = f['irreps'][:]

    ir['A1']["indx"] = np.where((iprs < nr*0.20) & (irreps == b'A1'))
    ir['A1']["avg"] += list(freq[ir['A1']["indx"]])

    ir['A2']["indx"] = np.where((iprs < nr*0.20) & (irreps == b'A2'))
    ir['A2']["avg"] += list(freq[ir['A2']["indx"]])

    ir['E']["indx"] = np.where(
        (iprs < nr*0.30) & (irreps == b'Exy'))
    ir['E']["avg"] += list(freq[ir['E']["indx"]])

    cnt = Counter(irreps)
    for j in cnt.most_common():
        print(j)

    for name in ["A2"]:
        indx = ir[name]["indx"]
        axes[0].scatter(freq[indx], nr/iprs[indx],
                        color=colors[cnr], alpha=1,
                        label=directory + " {}".format(name),
                        marker=ir[name]['marker'])

        if len(indx[0]) > 1:
            x = np.linspace(0, 170, 1000)
            kde = gaussian_kde(freq[indx])
            kde.set_bandwidth(0.03)
            axes[1].plot(x, kde(x),
                         color=colors[cnr],
                         label=directory + " {}".format(name),
                         lw=2)

    cnr += 1

axes[1].scatter(peaks, peaks*0,
                label="ph sideband peaks")
axes[0].legend(loc=2)
axes[1].legend(loc=2)
axes[0].set_ylabel("sites/ipr")
axes[1].set_xlabel("meV")
axes[1].set_ylabel("DOS of localized")
axes[0].set_xticks(np.arange(0, 190, 10))
axes[0].set_xlim([0, freq.max() + 10])
axes[0].grid(True)
axes[1].grid(True)
plt.savefig("iprs_A2.pdf", dpi=300)
# plt.show()
plt.clf()
