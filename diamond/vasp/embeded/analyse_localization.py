
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from collections import Counter
from analyse_fc_displacements import gaussian_smoothing, calculate_hr_factors
from calculate_luminescence import get_.hr_spectrum
from sklearn.neighbors import KernelDensity

sys.path.append("../../../dephon")
from deutils import gaussian_kde, Units
from demodes import Modes
from defc import ForceConstants


def plot_graph(sizes, symtype, plottype, bw=0.055, ax=None, color="b",
               label=None):
    # colors = cm.rainbow(np.linspace(0, 1, len(sizes)))
    # colors = plt.get_cmap("Dark2").colors
    # colors.pop(2)
    separate_plot = False
    if ax is None:
        colors = list(plt.get_cmap("Vega10").colors)
        separate_plot = True
        _, ax = plt.subplots(1, 1)
    else:
        colors = [color]

    ir = {}

    for i, m, l in zip(["A1", "A2", "E"], ["o", "s", "v"], ["-", '--', ':']):
        ir[i] = {
            "indx": [],
            "avg": [],
            "marker": m,
            "linestyle": l}

    cnr = 0
    for i in sizes:
        directory = "nv{0}x{0}x{0}".format(i)
        if i == 3:
            directory = "nv{0}x{0}x{0}direct".format(4)
            i = 4

        if label is None:
            label = directory

        fl = os.path.join(directory, "mode_analysis.hdf5")

        nr = 8*i*i*i - 1
        f = h5py.File(fl, "r")
        freq = f["frequencies"][:]
        iprs = f["iprs"][:]
        weights = f["weights"][:]
        irreps = f['irreps'][:]

        if symtype != "":
            indx = np.where(irreps == symtype)
            if symtype == b"Exy":
                indx = np.where((irreps == symtype) | (irreps == b"Ex") |
                                (irreps == b"Ey"))
            freq = freq[indx]
            iprs = iprs[indx]
            weights = weights[indx]

        sphere_volume = 4/3 * np.pi * (4**3)
        lattice_volume = (i*3.5620940727273465)**3
        part = sphere_volume/lattice_volume

        part *= 4
        print("PART:", part)
        indx = np.where(weights > part)

        if plottype == "ipr":
            s = ax.stems(freq, iprs, color=colors[cnr],
                         alpha=0.6, label=label)
            s.set_rasterized(True)

        elif plottype == "beta":
            markerline, stemlines, baseline = ax.stem(
                freq, nr/iprs, rasterized=True, markersize=1, label=label)
            plt.setp(markerline, color="orangered", markersize=1,
                     linewidth=0.5)
            # plt.setp(markerline, 'size', 2)
            # plt.setp(stemlines, 'color', color[cnr], 'linewidth', 1)
            # color=colors[cnr],
            # label=label, alpha=0.8)
            # s.set_rasterized(True)
            return ax

        elif plottype == "beta_smooth":
            threshold = 2.5
            indx = nr/iprs > threshold
            x, y = gaussian_smoothing(freq[indx], nr/iprs[indx], sig=1.3)

            if False:
                y = (nr/iprs)
                x = np.linspace(0, freq.max() + 10, 2000)
                kde = gaussian_kde(freq, bw, weights=weights*(weights > part))
                y = kde(x)

            # ax2 = ax.twinx()
            # ax2.scatter(freq[indx], nr/iprs[indx], alpha=0.8,
            #             color="orangered",
            #             label=r"$\beta = \mathrm{nr\ of\ atoms}/\mathrm{IPR}$")

            ax.plot(freq[indx], nr/iprs[indx], "o",
                    alpha=0.8, markersize=2,
                    color="orangered",
                    label=r"$\beta=\mathrm{nr\ of\ atoms}/\mathrm{IPR} >" +
                    " {}$".format(threshold))

            ax.plot(x, y, color=colors[cnr], lw=1.5,
                    label=r"smoothed $\beta$".format(
                        threshold))

            # h1, l1 = ax.get_legend_handles_labels()
            # h2, l2 = ax2.get_legend_handles_labels()

            # ax.legend(h1 + h2, l1 + l2)
            ax.legend()

        # elif plottype == "dos difference":
        #     x = np.linspace(0, freq.max() + 10, 2000)
        #     kde = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(X)
        #     y = kde(x)

        elif plottype == "weights":
            s = ax.scatter(
                freq[indx],
                weights[indx],
                color=colors[cnr],
                label=directory +
                " weights",
                alpha=0.5)
            s.set_rasterized(True)

        elif plottype.startswith("projected"):
            power = 1
            if plottype.find("square") != -1:
                power = 2

            if plottype.find("kde") != -1:
                x = np.linspace(0, freq.max() + 10, 2000)
                kde = gaussian_kde(
                    freq[indx], bw,
                    weights=(weights[indx]**power))
                y = kde(x)
            else:
                x, y = gaussian_smoothing(
                    freq[indx], weights[indx]**power, sig=1.3)

            ax.plot(x, y/np.trapz(y, x=x), color=colors[cnr],
                    label=label)

            if i == sizes[-1]:
                ax.set_ylabel("Projected DOS")

        cnr += 1

    # ax.set_xticks(np.arange(0, 190, 10))
    # ax.legend(loc=2)
    # ax.grid(True)
    ax.set_xlim([0, 180])
    ax.set_ylim(bottom=0)
    ax.set_xlabel("Phonon energy (meV)")
    ax.get_yaxis().set_tick_params(direction='in')
    ax.get_xaxis().set_tick_params(direction='in')
    plt.tight_layout(rect=[0, 0.03, 1, 0.95])
    try:
        l = symtype.decode()
        if l == "Exy":
            l = "E"
        l = l + " Modes"
    except:
        l = "All Modes"

    ax.set_title(l)

    if separate_plot:
        plt.savefig("projected_dos_{}_bw{}.png".format(l, bw), dpi=300)
        plt.clf()

    return


def plot_dos_difference(size=13):
    bulk_directory = "{0}x{0}x{0}".format(size)
    directory = "nv{0}x{0}x{0}".format(size)

    bulk = h5py.File(bulk_directory + "/modes.hdf5", "r")
    defect = h5py.File(directory + "/modes.hdf5", "r")

    freq = bulk["frequencies"][:]
    freq2 = defect["frequencies"][:]

    y = []
    x = np.linspace(0, freq.max() + 10, 2000)
    for i in [freq, freq2]:
        kde = KernelDensity(kernel='gaussian', bandwidth=2).fit(i)
        y.append(np.exp(kde.score_samples(x[:, np.newaxis])))
        print("ok")

    plt.fill(x, y[1]-y[0], fc='#AAAAFF')
    plt.plot(x, y[1]-y[0])
    plt.savefig("dos_difference.pdf")
    plt.xlabel("Phonon energy (meV)")
    plt.ylabel("DOS$_{\mathrm{defect}}$ $-$ DOS$_{\mathrm{bulk}}$")

    plt.show()
    exit(1)

# plot_dos_difference()

# labels = {
#     "projected": "projected DOS",
#     "beta_smooth": r"localization factor $\mathrm{IPR}^{-1}$",
#     "beta": r"$\mathrm{localization\ factor} = " +
#     r"\mathrm{nr\ of\ atoms} \cdot \mathrm{IPR}^{-1}$"
# }

# for color, mytype in zip(["sandybrown"],
#                          ["beta"]):
#     for s in [b"A1"]:
#         for i in [0.001, 0.03, 0.04, 0.05, 0.08][:1]:
#             plot_graph([15], s, mytype, i, ax, color=color,
#                        label=labels[mytype])

# OUTPUTS = {
#     "HSE:displacements": dict(
#         ground="../../vasp_hybrid/nv4x4x4/relaxed/POSCAR",
#         excited="../../vasp_hybrid/nve4x4x4/relaxed/POSCAR",
#         geometry="../../vasp_hybrid/nv4x4x4/bulk_geometry/POSCAR",
#         forces=False,
#         marker="o")
# }


fig, ax = plt.subplots(1, 1, figsize=(9, 3.8))

##############################
#   Huang Rhys Spectrum
##############################

SIZE = 13
directory = "nv{0}x{0}x{0}/".format(10)
freq0, sw, hr_factor = get_hr_spectrum(cell_size=SIZE,
                                       sig=2, n=1500)

ax.set_xlabel("Phonon energy (meV)")
ax.set_ylabel(r"$S(\hbar\omega)$")

f = h5py.File(os.path.join(directory, "mode_analysis.hdf5"), "r")
iprs = f["iprs"][:]
freq = f["frequencies"][:]
f.close()
nr = 8*(SIZE**3)
ax.set_ylim(bottom=0)

##############################
#           BETA
##############################

print("PLOTING BETA")
ax2 = ax.twinx()
ax2.set_ylabel(r"$\mathrm{localization\ ratio}\ " +
               r"\left(\mathrm{nr\ of\ atoms}/\mathrm{IPR}\right)$")

# ax2.scatter(
#     freq, nr/iprs,
#     rasterized=True)

f = h5py.File(os.path.join("13x13x13", "modes.hdf5"), "r")
iprs = f["iprs"][:]
freq = f["frequencies"][:]
f.close()

ax2.scatter(
    freq, nr/iprs,
    rasterized=True)

ax.plot(freq0, sw/np.trapz(sw, x=freq0), lw=2.5,
        label=r"$S(\hbar\omega)$".format(hr_factor),
        color="dodgerblue")

ax.set_ylim(0, 0.02)
ax.set_xlim(0, 175)
ax2.set_ylim(0, ax2.get_ylim()[1]*1.3)
ax2.set_ylim(2, 270)
plt.setp(markerline, color="orangered", markersize=3.5, alpha=0.5)
plt.setp(stemlines, color="orangered", alpha=0.5, linewidth=1.5)
plt.tight_layout()
ax.grid(True, lw=0.5, color="gray")
plt.savefig("vibrational_structure.png", dpi=300)
ax2.set_yscale('symlog')
plt.savefig("vibrational_structure2.png", dpi=300)
plt.show()
plt.clf()


if False:
    fc = ForceConstants("vasp", phonopydir="../4x4x4/phonopy_phonons")
    dos = fc.calculate_pw_dos()
    fig, ax = plt.subplots(1, 1)
    ax.plot(dos[0]*Units.cm_to_mev, dos[1],
            color=list(plt.get_cmap("Vega10").colors[0]))
    ax.set_xlabel("Phonon energy (meV)")
    ax.set_ylabel("DOS")
    ax.get_yaxis().set_tick_params(direction='in')
    ax.get_xaxis().set_tick_params(direction='in')
    ax.grid(True)
    ax.set_xlim([0, 180])
    ax.set_ylim(bottom=0)
    plt.tight_layout(rect=[0, 0.03, 1, 0.4])
    plt.savefig("dos.png", dpi=300)


# ax.savefig("weights11_13_15.pdf", dpi=300)
# # ax.show()
# ax.clf()
