import h5py
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
sys.path.append("../../../dephon")
from demodes import Modes
from vasp import Poscar

mev_to_th = 0.24180
mev_to_cm = 8.06554
init_geometry = "../nv{0}x{0}x{0}/relax/POSCAR-0".format(3)
init_atoms = Poscar(init_geometry).create_atoms()
init_positions = init_atoms.get_positions()

if False:
    cell = 3
    directory = "nv{0}x{0}x{0}direct".format(cell)
else:
    cell = 15
    directory = "nv{0}x{0}x{0}".format(cell)
    init_positions = None


displacements = []
modes = Modes(hdf5dir=directory)
freq_mev = modes.get_frequencies("mev")
positions = modes.atoms.get_positions()

# indexes = np.where(modes.sym_types == b"Exy")
indexes = np.where((modes.sym_types == b"Exy") & (modes.iprs < 8000))
# indexes = np.where((modes.sym_types == b"A1") & (modes.iprs < 8000))
# indexes = (np.arange(len(modes.sym_types)),)

print("Nr of atoms:", len(modes.atoms))
print("Nr of modes:", len(indexes[0]))
print(modes.iprs[indexes])

positionst = positions.T
prefered_size = init_atoms.cell[0][0]
where = positionst[0] < prefered_size
for i in [1, 2]:
    other = positionst[i] <= prefered_size
    where = where & other
pos_indexes = np.where(where)
positions = positions[pos_indexes]
if init_positions is None:
    init_positions = positions


content = ""
cnt = 0
modes.open_file()
for nr in indexes[0]:
    displ = modes.f["displacements"][nr]
    displ /= np.linalg.norm(displ)
    displ = displ.reshape(int(len(displ)/3), 3)[pos_indexes]
    displ /= np.linalg.norm(displ.flatten())

    freq = freq_mev[nr]
    ipr = modes.iprs[nr]
    irrep = modes.sym_types[nr]

    cnt += 1
    print(cnt, "/", len(indexes[0]), end="\r")

    st = "{:5} f  = {:11.6f} THz {:12.6f} 2PiTHz {:11.6f} cm-1 {:12.6f} meV"

    st = st.format(
        nr + 1, freq*mev_to_th, freq*mev_to_th*2*np.pi, freq*mev_to_cm, freq)

    st += "\n      ipr = {:11.2f}".format(ipr)
    st += "\n      irrep = {}".format(irrep.decode())
    st += ("\n    {:>11} {:>11} {:>11}   {:>11} {:>11} {:>11}   " +
           "{:>11} {:>11} {:>11}").format(
               "Xinit", "Yinit", "Zinit",
               "Xrel", "Yrel", "Zrel",
               "dx", "dy", "dz")

    displacements.append(displ)

    for init_pos, pos, d in zip(init_positions, positions, displ):
        line = "\n    {:11.6f} {:11.6f} {:11.6f}".format(*init_pos)
        line += "   {:11.6f} {:11.6f} {:11.6f}".format(*pos)
        line += "   {:11.6f} {:11.6f} {:11.6f}".format(*d)

        st += line

    content += st + "\n\n"


with open(os.path.join(directory, "e_modes.dat"), "wt") as f:
    f.write(content)

hdf5f = h5py.File(os.path.join(directory, "vibrations_e.hdf5"), "w")
hdf5f["displacements"] = np.array(displacements)
hdf5f["lattice_constant"] = modes.atoms.get_cell()[0][0]/3
hdf5f["base_vectors"] = modes.atoms.get_scaled_positions()[pos_indexes]
hdf5f["species"] = np.array(modes.atoms.symbols, dtype="|S3")[pos_indexes]
hdf5f["init_positions"] = init_positions
hdf5f["relaxed_positions"] = positions
hdf5f["frequencies"] = freq_mev[indexes]
hdf5f["symmetries"] = modes.sym_types[indexes]
hdf5f["iprs"] = modes.iprs[indexes]
hdf5f.close()


# # # with open(os.path.join(directory, "xyz.xyz"), "wt") as f:
# # #     f.write(content)
# # # fl = os.path.join(directory, "modes.hdf5")
# # np.tile
