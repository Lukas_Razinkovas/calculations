import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KernelDensity
sys.path.append("../../")

def gaussian_smoothing(x, y, sig=6, n=1000):
    def gaussian(x, mu, sig=sig):
        return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
            2 * np.power(sig, 2.)))

    x_new = np.linspace(0, 200, n)
    y_new = np.zeros(len(x_new))

    for _x, _y in zip(x, y):
        y_new += _y*gaussian(x_new, _x)

    return x_new, y_new

f, ax = plt.subplots(1, 1, figsize=(10, 8))

SIG = 2
data = np.loadtxt("nv13x13x13_hr_spectrum.dat")
x, y = data[:, 0], data[:, 1]


kde = KernelDensity(kernel='gaussian', bandwidth=3).fit(x[:, np.newaxis])
x2 = np.linspace(0, 200, 1000)
y2 = np.exp(kde.score_samples(x2[:, np.newaxis]))
ax2 = ax.twinx()
ax2.plot(x2, y2, color="gray", label="bulk DOS")
ax2.legend(loc=2)
ax2.set_ylim(bottom=0)

ax.plot(x, y, label=(
    r"$S_{k}^{\mathrm{defect}}$" + "$S={:.2f}$".format(sum(y))),
         color="blue", alpha=0.4)
x, y = gaussian_smoothing(x, y, sig=SIG)
ax.plot(x, y, label=r"$S^{\mathrm{defect}}(\hbar\omega)$", color="blue")

data = np.loadtxt("13x13x13_hr_spectrum.dat")
x, y = data[:, 0], data[:, 1]
ax.plot(x, y, label=(
    r"$S_{k}^{\mathrm{bulk}}$" + "$S={:.2f}$".format(sum(y))),
         color="red", alpha=0.8, lw=2)
x, y = gaussian_smoothing(x, y, sig=SIG)
ax.plot(x, y, label=r"$S^{\mathrm{bulk}}(\hbar\omega)$", color="red")
ax.set_ylim(bottom=0)

ax.legend(loc=1)
ax.grid()
ax.set_xlim(0, 175)
plt.savefig("13bulk_vs_defect.pdf", dpi=300)
plt.show()
