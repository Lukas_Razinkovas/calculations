import sys
import os
import shutil
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("../../../dephon")
from plot_band import read_doscar_and_plot, read_and_plot_band
from declusters import Cluster
from destrain import create_strain_inputs
import pprint

pp = pprint.PrettyPrinter()

strains = [-0.02, -0.01, 0, 0.01, 0.02]

# indir = "unpolarised/"
# outdir = "unpolarised/strains"

indir = "noncolinear/scf/"
outdir = "noncolinear/strains"

MESH_CALCULATIONS = False
CREATE_DOS_AND_PREPARE_FOR_PATH = False
SAVE_AND_PLOT_BANDS = True

if MESH_CALCULATIONS:
    for strain_type in ["hydrostatic"]:
        dirs = create_strain_inputs(indir, strains, outdir,
                                    strain=strain_type)
        jobparams = {"cpus": 16, "name": strain_type + "strain2pbe",
                     "time": "4:00:00", "mem": "16Gb"}
        jobs = [
            {
                "cwd": i,
                "exec": "vasp_std"
            } for i in dirs
        ]

        clust = Cluster("raijin", "vasp")
        content = clust.generate_job_file(jobparams, jobs)
        with open(os.path.join(outdir, strain_type + ".job"), "wt") as f:
            f.write(content)

if CREATE_DOS_AND_PREPARE_FOR_PATH:
    f, ax = plt.subplots(1, figsize=(11.69, 8.27))
    NEW_INCAR = os.path.join(indir, "INCAR-PATH")
    NEW_KPOINTS = os.path.join(indir, "KPOINTS-PATH")
    for strain_type in ["hydrostatic"]:
        for st in strains:
            name = "{}_strain_{:.3f}".format(strain_type, st)
            _dir = os.path.join(outdir, name)
            for fl in ["OUTCAR", "INCAR", "KPOINTS", "vasprun.xml", "DOSCAR"]:
                DOSCAR = os.path.join(_dir, fl)
                if fl.find("vasprun") != -1:
                    shutil.copy(DOSCAR, DOSCAR.replace(
                        "vasprun", "vasprun-MESH"))
                else:
                    shutil.copy(DOSCAR, os.path.join(_dir, fl + "-MESH"))

            shutil.copy(NEW_INCAR, os.path.join(_dir, "INCAR"))
            shutil.copy(NEW_KPOINTS, os.path.join(_dir, "KPOINTS"))
            read_doscar_and_plot(DOSCAR, label=name, ax=ax,
                                 savedata=os.path.join(_dir, "dos.dat"))

        plt.legend()
        plt.savefig(strain_type + "DOS.pdf", dpi=200)
        plt.clf()


if SAVE_AND_PLOT_BANDS:
    energies = []
    for strain_type in ["hydrostatic"]:
        for st in strains:
            name = "{}_strain_{:.3f}".format(strain_type, st)
            _dir = os.path.join(outdir, name)
            vasprun = os.path.join(_dir, "vasprun.xml")
            data = read_and_plot_band(
                vasprun,
                labels=[
                    "G", "X", "W", "K", "G", "L",
                    "U", "W", "L", "K", "U", "X"],
                title="PBE diamond SOC {} strain $=$ {:.2f}".format(
                    strain_type, st),

                saveplot=os.path.join(_dir, "bands.pdf"),
                savedata=os.path.join(_dir, "bands.dat"))

            energies.append(data["energy"])

    outfile = os.path.join(outdir, "hydrostatic_strain_SOC_energies.dat")
    np.savetxt(outfile, np.array([strains, energies]).T, fmt="%.6f",
               header="strain    e(eV)")

    # plt.plot(strains, energies)
    # plt.show()
