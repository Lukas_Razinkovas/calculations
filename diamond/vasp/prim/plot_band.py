import os
import pprint
from lxml import etree
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
import matplotlib.gridspec as gridspec


pp = pprint.PrettyPrinter()


def get_kpoints_data(filepath=None, root=None):
    if filepath is not None:
        root = etree.parse(filepath).getroot()

    ##############################
    #      Collect kpoints
    ##############################

    kpoints = root.find("kpoints")
    kmesh_type = kpoints.find("generation").attrib["param"]
    if kmesh_type == "listgenerated":
        division = int(kpoints.find('./generation/i[@name="divisions"]').text)
    else:
        division = kpoints.find('./generation/v[@name="divisions"]').text
        division = [int(i) for i in division.split()]

    kpoints_data = []
    for i in kpoints.find("varray[@name='kpointlist']"):
        kpoints_data.append([float(k) for k in i.text.split()])
    kpoints = np.array(kpoints)

    ##################################
    #  Collect Evals And Occupations
    ##################################

    occupations = []
    evals = []
    for i in root.find(".//set[@comment='spin 1']"):
        _evals = []
        _occ = []
        for k in i:
            e, o = k.text.split()
            _evals.append(float(e))
            _occ.append(float(o))

        evals.append(_evals)
        occupations.append(_occ)

    # for i in root.findall(".//energy"):
    #     print(i.getparent().tag)

    return {
        "evals": np.array(evals).T,
        "occ": np.array(occupations).T,
        "type": kmesh_type,
        "kpoints": np.array(kpoints_data),
        "division": division,
        "energy": float(root.find(".//calculation/energy")[0].text)
    }


def get_listgenerated_ticks_and_vals(data, labels=None):
    """Generates ticks and labels for line mode in KPOINTS"""
    division = data["division"]

    xvals = []
    xticks = []
    xticks_vals = []

    breaks = []
    last_point = None
    for nr, i in enumerate(data["kpoints"], 1):
        if last_point is None:
            last_point = i
            val = 0
            xticks_vals.append(i)
            xticks.append(val)
            xvals.append(val)
            continue

        diff = np.linalg.norm(last_point - i)
        val = val + diff

        if nr % division == 0:
            xticks.append(val)
            xticks_vals.append(i)

        if nr % division == 1 and not np.allclose(xticks_vals[-1], i):
            val = val - diff
            xticks.append(val)
            xticks_vals.append(i)
            breaks.append(nr)

        xvals.append(val)
        last_point = i

    ##############################
    #   Form ticks and labels
    ##############################

    if labels is None:
        labels = ["$[{:.3f}, {:.3f}, {:.3f}]$".format(*i) for i in xticks_vals]

    maps = {"G": r"\Gamma"}
    assert len(xticks) == len(labels), "ticks and labels are inconsistent"

    new_xticks = []
    new_labels = []
    for nr, (v, l) in enumerate(zip(xticks, labels)):
        if nr != 0 and xticks[nr - 1] == v:
            new_labels[-1] += "|{}".format(maps.get(l, l))
        else:
            new_labels.append(maps.get(l, l))
            new_xticks.append(v)

    return {
        "xvals": np.array(xvals),
        "xticks": new_xticks,
        "xlabels": [r"$\mathrm{{{}}}$".format(i) for i in new_labels],
        "breaks": breaks
    }


def read_doscar_and_plot(filepath="unpolarised/DOSCAR", ax=None,
                         saveplot=None, savedata=None, label="DOS"):

    print("Reading DOS from:", filepath)

    if ax is None:
        f, ax = plt.subplots(1)

    energy = []
    dos = []
    integrated_dos = []

    with open(filepath, "rt") as f:
        for i in range(6):
            next(f)

        l = next(f).split()
        while len(l) == 3:
            energy.append(float(l[0]))
            dos.append(float(l[1]))
            integrated_dos.append(float(l[2]))
            try:
                l = next(f).split()
            except StopIteration:
                break

    ax.plot(energy, dos, label=label)
    ax.set_ylim((0, ax.get_ylim()[1]))
    ax.set_xlim((min(energy), max(energy)))
    ax.set_xlabel("Energy (eV)")
    ax.set_ylabel("DOS")

    if savedata is not None:
        data = np.hstack([[[i] for i in energy], [[i] for i in dos]])
        np.savetxt(savedata, data, fmt=("%9.3f", "%.4e"))


def read_and_plot_band(filepath="vasprun.xml", labels=None, title="",
                       saveplot=None, ax=None, savedata=None):

    if ax is None:
        _dir = os.path.split(filepath)[0]
        if "dos.dat" in os.listdir(_dir):
            dos = os.path.join(_dir, "dos.dat")
            plt.figure(figsize=(11.69, 8.27))
            gs = gridspec.GridSpec(1, 4)
            ax = plt.subplot(gs[0, 0:3])
            dosax = plt.subplot(gs[0, 3], sharey=ax)
        else:
            dos = None
            _, ax = plt.subplots(1, figsize=(11.69, 8.27))

    data = get_kpoints_data(filepath)
    if data["type"] == "listgenerated":
        xdata = get_listgenerated_ticks_and_vals(data, labels)
    else:
        raise Exception("!!! Wrong kpoint mesh type: {}".format(data["type"]))
        return

    ################################################
    #           Plot Dispersion Lines
    ################################################

    zoom = True
    if zoom:
        ax2 = plt.axes([0.105, 0.72, .15, .2])

    fr = 0
    for b in xdata["breaks"]:
        for i, occ in zip(data["evals"], data["occ"]):
            if (occ == 1).all():
                ax.plot(xdata["xvals"][fr:b], i[fr:b], c="red")
                if zoom:
                    ax2.plot(xdata["xvals"][fr:b], i[fr:b], c="red")
            elif (occ == 0).all():
                ax.plot(xdata["xvals"][fr:b], i[fr:b], c="blue")
                if zoom:
                    ax2.plot(xdata["xvals"][fr:b], i[fr:b], c="blue")
            else:
                ax.plot(xdata["xvals"][fr:b], i[fr:b], c="black")
                if zoom:
                    ax2.plot(xdata["xvals"][fr:b], i[fr:b], c="blue")

        fr = b - 1

    for i, occ in zip(data["evals"], data["occ"]):
        if (occ == 1).all():
            ax.plot(xdata["xvals"][fr:], i[fr:], c="red")
        elif (occ == 0).all():
            ax.plot(xdata["xvals"][fr:], i[fr:], c="blue")
        else:
            ax.plot(xdata["xvals"][fr:], i[fr:], c="black")

    for val, label in zip(xdata["xticks"], xdata["xlabels"]):
        if label.find("|") == -1:
            ax.axvline(val, color='gray', linestyle='--', lw=0.5)
        else:
            ax.axvline(val, color='gray', linestyle='-', lw=0.8)

    ##############################
    #      Finding BandGap
    ##############################

    max_occupied = np.where(data["occ"].T[0] == 1)[0].max()
    min_unoccupied = np.where(data["occ"].T[0] == 0)[0].min()

    _max_indx = data["evals"][max_occupied].argmax()
    _min_indx = data["evals"][min_unoccupied].argmin()
    gam_indexes = []
    for nr, i in enumerate(data["kpoints"]):
        if np.allclose(i, [0, 0, 0]):
            gam_indexes.append(nr)
            print(data["kpoints"][nr])

    for max_indx, min_indx in [(gam_indexes[1], gam_indexes[1]),
                               (_max_indx, _min_indx)]:
        ax.scatter(
            [xdata["xvals"][max_indx],
             xdata["xvals"][min_indx]],
            [data["evals"][max_occupied][max_indx],
             data["evals"][min_unoccupied][min_indx]])

        ax.plot(
            [xdata["xvals"][max_indx],
             xdata["xvals"][min_indx]],
            [data["evals"][max_occupied][max_indx],
             data["evals"][min_unoccupied][min_indx]],
            linestyle="--", lw=2, alpha=0.5, c="darkcyan")

        ax.annotate(
            '$E_G = {:.3f}$ eV'.format(
                data["evals"][min_unoccupied][min_indx] -
                data["evals"][max_occupied][max_indx]),
            xy=(0, 0),
            xytext=(
                (xdata["xvals"][max_indx] + xdata["xvals"][max_indx])/2 + 0.2,
                (data["evals"][max_occupied][max_indx] +
                 data["evals"][min_unoccupied][min_indx])/2 - 1), fontsize=12)

    if dos is not None:
        dat = np.loadtxt(dos).T
        dosax.plot(dat[1], dat[0])
        # dosax.yaxis.set_ticklabels([])
        dosax.set_xlim((0, dosax.get_xlim()[1]))
        dosax.set_xlabel("DOS")

    ax.set_xlim([min(xdata["xvals"]), max(xdata["xvals"])])
    ax.set_xticks(xdata["xticks"])
    ax.set_xticklabels(xdata["xlabels"])
    ax.set_ylabel("Energy (eV)")
    ax.set_xlabel("Wavevector")

    indx = xdata["xlabels"][1:].index('$\\mathrm{\\Gamma}$') + 1
    gamma_pt = xdata["xticks"][indx]
    ax2.set_xticks(xdata["xticks"])
    ax2.set_xticklabels(xdata["xlabels"])
    ax2.grid()
    ax2.set_xlim((gamma_pt - 0.05, gamma_pt + 0.05))
    _m = data["evals"][max_occupied][max_indx]
    ax2.set_ylim((_m - 0.02, _m + 0.01))

    if saveplot is not None:
        ax.set_title(title)
        plt.tight_layout()
        # plt.show()
        plt.savefig(saveplot)
        plt.clf()

    if savedata is not None:
        evals = data["evals"].T
        occupations = data["occ"].T[0]
        nr = np.array([[i] for i in range(len(evals))])
        kpoints = np.array(
            [["{:.6f}, {:.6f} {:.6}".format(*i)] for i in data["kpoints"]])
        kpoints = data["kpoints"]
        dist = np.array([[i] for i in xdata["xvals"]])

        dt = np.hstack((nr, kpoints, dist, evals))
        fmt = ['%18.12f']*len(dt[0])
        fmt[0] = '%4.0f'

        header = ["Dist", "Kx", "Ky", "Kz"] + [
            "band{}({:.0f})".format(n, i) for n, i
            in enumerate(occupations, 1)]

        header = ["{:18}".format(i) for i in header]
        header = " ".join([" Nr   "] + header)

        np.savetxt(savedata, dt,
                   header=header, fmt=tuple(fmt))

    return data
    # plt.show()

if __name__ == "__main__":
    kpoints = read_and_plot_band(
        "unpolarised/vasprun.xml",
        labels=[
            "G", "X", "W", "K", "G", "L", "U", "W", "L", "K", "U", "X"],
        title="PBE diamond nonpolarised strain $=$ 0",
        saveplot="PBEbandsS0.pdf", savedata="PBEbandsS0.dat")

    read_doscar_and_plot()
