#!/bin/bash -l
#SBATCH -p regular
#SBATCH -N 2
#SBATCH -C haswell   
#SBATCH -L SCRATCH,project
#SBATCH -J my_job
export OMP_NUM_THREADS=1
module load espresso
srun -n 64 -c 1 pw.x -inp supercell.in > supercell.out