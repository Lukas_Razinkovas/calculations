import sys
import pprint
import os
import numpy as np
sys.path.append("../../dephon")
from vasp import Outcar, VaspXML
from tabulate import tabulate

pp = pprint.PrettyPrinter()

groups = {
    "hybrid_nv4x4x4": {
        "ground": {
            "outdir": "nv4x4x4/relaxed/",
        },
        "excited_symmetric": {
            "outdir": "nve4x4x4sym/relaxed/",
        },
        "excited_asymmetric": {
            "outdir": "nve4x4x4asym/relaxed/",
        },
        "excited_asymmetric_geometry_in_ground_state": {
            "outdir": "nv4x4x4/excited_asym_geometry/",
        },
        "excited_symmetric_geometry_in_ground_state": {
            "outdir": "nv4x4x4/excited_sym_geometry/",
        },
        "ground_geometry_in_exited_state": {
            "outdir": "nve4x4x4sym/ground_geometry/",
        }},
    "hybrid_nv3x3x3": {
        "ground": {
            "outdir": "nv3x3x3/relax/",
        },
        "excited_symmetric": {
            "outdir": "nve3x3x3sym/relaxed/",
        },
        "excited_asymmetric": {
            "outdir": "nve3x3x3asym/relaxed/",
        },
        "excited_asymmetric_geometry_in_ground_state": {
            "outdir": "nv3x3x3/excited_asym_geometry/",
        },
        "excited_symmetric_geometry_in_ground_state": {
            "outdir": "nv3x3x3/excited_sym_geometry/",
        },
        "ground_geometry_in_exited_state": {
            "outdir": "nve3x3x3sym/ground_geometry/",
        }
    }
}


print()
print("="*70)
print(" "*30 + "SUMMARY")
print("="*70)
tables = []


for group in groups:
    table = []
    calculations = groups[group]
    for nm in calculations:
        outdir = calculations[nm]["outdir"]
        outcar = Outcar(os.path.join(outdir, "OUTCAR"))
        try:
            calculations[nm]["energy"] = outcar.get_energy()
        except:
            continue
        outcar.read_forces()
        calculations[nm]["max_force"] = outcar.max_force
        table.append(
            [nm.replace("ground_state", "grnd_st").replace("geometry", "geom"),
             calculations[nm]["energy"],
             calculations[nm]["max_force"]])

    ##############################
    #           ZPLS
    ##############################

    szpl = (calculations["excited_symmetric"]["energy"] -
            calculations["ground"]["energy"])
    table.append(["Symmetric ZPL", szpl])
    szpl = (calculations["excited_asymmetric"]["energy"] -
            calculations["ground"]["energy"])
    table.append(["Asymmetric ZPL", szpl])

    ##############################
    #         Relaxation
    ##############################
    try:
        rel = (calculations["ground_geometry_in_exited_state"]["energy"] -
               calculations["excited_symmetric"]["energy"])
        table.append(["Symmetric E rel", rel])
    except:
        pass

    table_str = tabulate(
        table, headers=["Calculation", "Energy (eV)", "Max Force (eV/A)"])

    print()
    print("-"*70)
    print(" "*25 + group)
    print("-"*70)
    tables.append(table_str)
    print(table_str)



# with open("summary.txt", "wt") as f:
#     f.write(table_str)
