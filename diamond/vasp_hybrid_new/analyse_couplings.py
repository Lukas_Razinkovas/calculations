import sys
import os
import re
import numpy as np
import pprint
from yaml import load, dump
sys.path.append("../../dephon")
from deutils import ensure_dir
from dedisp import calculate_fc_displacements, calculate_couplings
import logging
from collections import OrderedDict

logger = logging.getLogger()
logger.setLevel(logging.INFO)

pp = pprint.PrettyPrinter()

couplings_dir = "../couplings/"
embeded_dir = "../vasp/embeded/"
hse_bulks = {
    "hse_nv3x3x3": {
        "bulk_geometry": "./nv3x3x3/bulk_geometry/POSCAR",
        "phonopy_dir": "../vasp/nv3x3x3/phonopy_phonons",
        "phonopy_bulk_poscar": "../vasp/nv3x3x3/relax/POSCAR-0",
        "calculations": [
            {
                "poscar1": "./nv3x3x3/relax/CONTCAR",
                "poscar2": "./nve3x3x3asym/relax/CONTCAR",
                "vasprun1": "./nve3x3x3asym/relaxed/vasprun.xml",
                "vasprun2": "./nv3x3x3/excited_asym_geometry/vasprun.xml",
                "name": "asymmetric"
            },
            {
                "poscar1":  "./nv3x3x3/relax/CONTCAR",
                "poscar2":  "./nve3x3x3sym/relax/CONTCAR",
                "vasprun1": "./nve3x3x3sym/relaxed/vasprun.xml",
                "vasprun2": "./nv3x3x3/excited_sym_geometry/vasprun.xml",
                "name": "symmetric"
            }
        ]
    },
    "hse_nv4x4x4": {
        "bulk_geometry": "./nv4x4x4/bulk_geometry/POSCAR",
        "phonopy_dir": "../vasp/nv4x4x4/phonopy_phonons",
        "phonopy_bulk_poscar": "../vasp/nv4x4x4/relax/POSCAR-1",
        "calculations": [
            {
                "poscar1": "./nv4x4x4/relaxed/CONTCAR",
                "poscar2": "./nve4x4x4asym/relax/CONTCAR",
                "vasprun1": "./nve4x4x4asym/relaxed/vasprun.xml",
                "vasprun2": "./nv4x4x4/excited_asym_geometry/vasprun.xml",
                "name": "asymmetric"
            },
            {
                "poscar1": "./nv4x4x4/relaxed/CONTCAR",
                "poscar2": "./nve4x4x4sym/relaxed/CONTCAR",
                "vasprun1": "./nve4x4x4sym/relaxed/vasprun.xml",
                "vasprun2": "./nv4x4x4/excited_sym_geometry/vasprun.xml",
                "name": "symmetric"
            }
        ]
    }
}


def iter_calculations():
    for hse_bulk in hse_bulks:
        for calc_data in hse_bulks[hse_bulk]["calculations"]:
            calc_data["hse_bulk"] = hse_bulk
            calc_data["bulk_geometry"] = hse_bulks[hse_bulk]["bulk_geometry"]
            calc_data["phonopy_dir"] = hse_bulks[hse_bulk]["phonopy_dir"]
            calc_data["phonopy_bulk_poscar"] = hse_bulks[hse_bulk][
                "phonopy_bulk_poscar"]
            yield calc_data


def save_vibrational_data(data, outdir, name):
    ensure_dir(outdir)
    outfile = os.path.join(outdir, name + "_vibrational_couplings.dat")

    dtypes = [
        ('freq', float),
        ('freq_mev', float),
        ('fc_displ', float),
        ('hr_factors', float),
        ('irreps', 'U6')]

    fmt = ", ".join(4*['%13.9f']) + ', %6s'
    couplings_data = np.zeros(
        data["freq"].size,
        dtype=dtypes)

    couplings_data["freq"] = data["freq"]
    couplings_data["freq_mev"] = data["freq_mev"]
    couplings_data["fc_displ"] = data["fc_displ"]
    couplings_data["hr_factors"] = data["hr_factors"]
    couplings_data["irreps"] = data["irreps"]
    header = " ".join(5*['{:13}']).format(
        "freq (au),",
        "freq (meV),",
        "fc_displ (au),",
        "hr_factors,",
        "irrep")

    np.savetxt(
        outfile,
        couplings_data,
        header=header,
        fmt=fmt)

    info_data = {}
    info_data["Total modes"] = len(couplings_data["freq"])
    info_data["A1 modes"] = len(
        np.where(couplings_data["irreps"] == "A1")[0])
    info_data["A2 modes"] = len(
        np.where(couplings_data["irreps"] == "A2")[0])
    info_data["Exy modes"] = len(
        np.where(couplings_data["irreps"] == "Exy")[0])
    info_data["S_tot"] = float(data["hr_tot"])
    yaml_file = os.path.join(outdir, name + "_vibrational_info.yaml")

    with open(yaml_file, "wt") as f:
        f.write(dump(info_data, default_flow_style=False))


def save_vibronic_data(data, outdir, name):
    ensure_dir(outdir)
    outfile = os.path.join(outdir, name + "_vibronic_couplings.dat")

    dtypes = [
        ('freq', float),
        ('freq_mev', float),
        ('k', float),
        ('ejt', float),
        ('v', float)]

    fmt = ", ".join(5*['%13.9f'])
    couplings_data = np.zeros(
        data["freq"].size,
        dtype=dtypes)

    couplings_data["freq"] = data["freq"]
    couplings_data["freq_mev"] = data["freq_mev"]
    couplings_data["k"] = data["k"]
    couplings_data["ejt"] = data["ejt"]
    couplings_data["v"] = data["v"]
    header = "  ".join(5*['{:13}']).format(
        "freq (au),",
        "freq (meV),",
        "k,",
        "E_JT (Hartree),",
        "v (au)")

    np.savetxt(
        outfile,
        couplings_data,
        header=header,
        fmt=fmt)

    info_data = {}
    info_data["Total modes"] = len(couplings_data["freq"])
    info_data["O'Brien effective frequency"] = "{} meV".format(
        float(data["effreq_mev"]))
    info_data["O'Brien keff"] = float(data["keff"])
    yaml_file = os.path.join(outdir, name + "_vibronic_info.yaml")

    with open(yaml_file, "wt") as f:
        f.write(dump(info_data, default_flow_style=False))


for calc_data in iter_calculations():
    outdir = os.path.join(couplings_dir, calc_data["hse_bulk"],
                          calc_data["name"])

    for cell_size in ["direct", 4, 6, 8, 10, 11, 13, 15][4:]:
        if cell_size == "direct":
            cell_size = re.search("x(.*?)x", os.path.abspath(
                calc_data["phonopy_dir"])).group(1)

            displ_data, displ_e_data = calculate_couplings(
                calc_data["poscar1"], calc_data["poscar2"],
                calc_data["bulk_geometry"],
                phonopy_dir=calc_data["phonopy_dir"],
                phonopy_bulk_poscar=calc_data["phonopy_bulk_poscar"],
                from_forces=False,
                units="au")

            forces_data, forces_e_data = calculate_couplings(
                calc_data["poscar1"], calc_data["poscar2"],
                calc_data["bulk_geometry"],
                phonopy_dir=calc_data["phonopy_dir"],
                phonopy_bulk_poscar=calc_data["phonopy_bulk_poscar"],
                forces_output1=calc_data["vasprun1"],
                forces_output2=calc_data["vasprun2"],
                from_forces=True,
                units="au")
            name = "nv{0}x{0}x{0}direct".format(cell_size)
        else:
            hdf5dir = os.path.join(
                embeded_dir, "nv{0}x{0}x{0}".format(cell_size))

            displ_data, displ_e_data = calculate_couplings(
                calc_data["poscar1"], calc_data["poscar2"],
                calc_data["bulk_geometry"],
                hdf5dir=hdf5dir,
                from_forces=False,
                units="au")

            forces_data, forces_e_data = calculate_couplings(
                calc_data["poscar1"], calc_data["poscar2"],
                calc_data["bulk_geometry"],
                hdf5dir=hdf5dir,
                forces_output1=calc_data["vasprun1"],
                forces_output2=calc_data["vasprun2"],
                from_forces=True,
                units="au")
            name = "nv{0}x{0}x{0}".format(cell_size)

        save_vibrational_data(
            displ_data, os.path.join(outdir, "from_displacements"), name)
        save_vibrational_data(
            forces_data, os.path.join(outdir, "from_forces"), name)
        save_vibronic_data(
            displ_e_data, os.path.join(outdir, "from_displacements"), name)
        save_vibronic_data(
            forces_e_data, os.path.join(outdir, "from_forces"), name)
