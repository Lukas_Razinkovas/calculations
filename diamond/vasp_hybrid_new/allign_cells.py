import sys
import os
import shutil
import numpy as np
sys.path.append("../../dephon")
from vasp import Poscar
from dedisp import remove_pb_artifacts, align_mass_center
from deutils import ensure_dir

base_poscar = "nv3x3x3/bulk_geometry/POSCAR"
to_be_aligned = [
    ["nv3x3x3", "relax/CONTCAR"],
    ["nve3x3x3sym", "relax/CONTCAR"],
    ["nve3x3x3asym", "relax/CONTCAR"],
]

base_atoms = Poscar(base_poscar).create_atoms()

for my_dir, path in to_be_aligned:
    old_poscar = os.path.join(my_dir, path)
    poscar = Poscar(old_poscar)
    atoms = poscar.create_atoms()
    assert np.allclose(base_atoms.get_cell(), atoms.get_cell())

    remove_pb_artifacts(base_atoms, atoms, verbose=True)
    align_mass_center(base_atoms, atoms, verbose=True)
    poscar.set_atoms(atoms)

    new_dir = os.path.join(my_dir, "relaxed")
    ensure_dir(new_dir)

    old_dir = os.path.dirname(old_poscar)
    for i in ["POTCAR", "INCAR", "KPOINTS", "hpss.job"]:
        shutil.copy(os.path.join(old_dir, i),
                    os.path.join(new_dir, i))

    with open(os.path.join(new_dir, "POSCAR"), "wt") as f:
        f.write(poscar.create_input())
