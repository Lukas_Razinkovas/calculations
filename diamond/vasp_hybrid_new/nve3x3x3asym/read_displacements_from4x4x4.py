import sys
import numpy as np
sys.path.append("../../../dephon")
from dedisp import read_displacements
from vasp import Poscar

poscar1 = "../nve4x4x4sym/relax/CONTCAR"
poscar2 = "../nve4x4x4asym/relax/CONTCAR"

to_be_changed = Poscar("../nve3x3x3sym/relax/CONTCAR")
atoms = to_be_changed.create_atoms()

displ, atoms1, atoms2 = read_displacements(poscar1, poscar2, units="angstrom")
bulk_atoms4 = Poscar("../nv4x4x4/bulk_geometry/POSCAR").create_atoms()
bulk_atoms3 = Poscar("../nv3x3x3/bulk_geometry/POSCAR").create_atoms()

bulk_atoms3.set_cell((bulk_atoms4.get_cell()/4)*3)


positions3 = bulk_atoms3.get_positions()
positions4 = bulk_atoms4.get_positions()
n_pos = positions3[bulk_atoms3.symbols.index("N")]

old_positions = atoms.get_positions()
new_positions = []
for nr, pos in enumerate(bulk_atoms3.get_positions()):
    _, dist = bulk_atoms3.get_separation_vector(n_pos, pos)
    if dist > 5:
        new_positions.append(old_positions[nr])
        continue

    indx = bulk_atoms4.find_by_position(pos)
    d = displ[indx]

    new_positions.append(old_positions[nr] + displ[indx])

new_positions = np.array(new_positions)

diff = old_positions - new_positions
print(diff)
print(diff.max())

print(to_be_changed.create_input())
atoms.set_positions(new_positions)
to_be_changed.set_atoms(atoms)

with open("./relax/POSCAR", "wt") as f:
    f.write(to_be_changed.create_input())
