import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import physical_constants

sys.path.append("../../dephon")
sys.path.append("../../dejt")
from dedisp import calculate_nondimensional_vibronic_couplings
from vibronic import vibronic_spectrum
from visual import plot_luminescence
from deutils import gaussian_smoothing


output_dir = ("./couplings/asymmetric_forces_ground_state/")
data = calculate_nondimensional_vibronic_couplings(
    output_dir, 4)
print("keff = ", data["keff"])
print("weff = ", data["effreq"], "au",
      data["effreq"]*data["freq_conversion"], "meV")
print("Ejt = ", sum(data["ejt"])*27.211396132)
print("Ejteff = ", (data["keff"]**2)*data["effreq"])

f, axes = plt.subplots(1, 2, figsize=(15, 10))
ax = axes[1]
# plot_luminescence(
#     evals, evecs, h0, ax, freq_conversion=data["freq_conversion"])
# plt.show()

NMODES = -0
MAXN = 2
k = data["k"]
freq = data["freq"]
input(len(data["freq"]))

ax0 = axes[0].twinx()
axes[0].scatter(data["freq_mev"], k)
axes[0].set_xlabel("E doublet freq (meV)")
axes[0].set_ylabel("k (dimensionless vibron. const.)")

x, y = gaussian_smoothing(data["freq_mev"], k, sig=5)
y /= np.trapz(x=x, y=y)
ax0.plot(x, y, color="red")
axes[0].set_ylim(bottom=0)
ax0.set_ylim(bottom=0)


indx = k.argsort()[-NMODES:]
keff = np.sqrt(sum(k[indx]**2))
efffreqsq = sum(
    (k[indx]**2) * (freq[indx]**2) /
    (keff**2))
effreq = np.sqrt(efffreqsq)

# effreq *= data["freq_conversion"]

##############################
#    Effective frequency
##############################

Ha_in_ev = physical_constants["Hartree energy in eV"][0]

print("keff =", keff)
print("effreq =", effreq)
gali_freq = 77.6/data["freq_conversion"]
print("Gali freq =", gali_freq)

# evals, evecs, h0 = vibronic_spectrum(
#     40, 1, [effreq], dvc=[keff], j=0.5, sparse_diag=False)
evals, evecs, h0 = vibronic_spectrum(
    40, 1, [gali_freq],
    dvc=[0.7071071004895408], j=0.5, sparse_diag=False)

evals *= data["freq_conversion"]
h0 *= data["freq_conversion"]

mymin = -600
mymax = 50
alpha = 0.4

plot_luminescence(
    evals, evecs, h0, ax, color="purple", alpha=alpha)
ax2 = ax.twinx()
label = (r"Effective mode" +
         r" $k_{{eff}} = {:.2f},\ \omega_{{eff}}={:.2f}$ meV".format(
             keff, effreq*data["freq_conversion"]))

_, feq, strenghts = plot_luminescence(
    evals, evecs, h0, ax2, smooth=10,
    color="purple", label=label, lw=3, alpha=alpha, xlim=[mymin, mymax])

np.savetxt(os.path.join(output_dir, "jt_lum.dat"),
           np.array([feq.T[0], strenghts]).T)
plt.show()

##############################
#
##############################

full = False
if full:
    evals, evecs, h0 = vibronic_spectrum(
        MAXN, doublets=NMODES,
        freq=freq[indx], dvc=k[indx])
    evals *= data["freq_conversion"]
    h0 *= data["freq_conversion"]

    plot_luminescence(
        evals, evecs, h0, ax, color="darkcyan")
    plot_luminescence(
        evals, evecs, h0, ax2, smooth=10,
        color="darkcyan", label="NrOfModes = {}".format(NMODES),
        xlim=[mymin, mymax])

ax.set_xlim(mymin, mymax)
ax.set_ylim(bottom=0)
ax2.set_ylim(bottom=0)
ax2.legend()
ax.set_xlabel("freq (meV)")
ax.set_ylabel("Oscillator strenght (normalized)")

plt.tight_layout()
plt.savefig("effective_mode.png")
# plt.savefig("effective_vs_full{}_{}.pdf".format(NMODES, MAXN))
plt.clf()
