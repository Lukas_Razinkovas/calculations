import sys
import pprint
import os
import numpy as np
sys.path.append("../../dephon")
from vasp import Outcar, VaspXML
from tabulate import tabulate
import matplotlib.pyplot as plt


def draw_occupation_diagram(vaspxml, figname="occup.pdf", title="",
                            ignore=[[0, 0], [0, 0]]):
    import matplotlib.pyplot as plt
    vasp = VaspXML(vaspxml)
    vasp.read_eigenvalues()
    vals = vasp.eigenvalues
    occs = vasp.occupations

    levels = [[], []]
    occupations = [[], []]

    for nr, i in enumerate(vals.keys()):
        o = occs[i]
        e = vals[i]
        indx = np.where(o == 0)[0]

        for j in range(indx[0] - 6, indx[0] + 6):
            levels[nr].append(e[j])
            occupations[nr].append(o[j])

    print("LEVELS")
    pp.pprint(levels)
    print("OCCUPATIONS")
    pp.pprint(occupations)

    f, axes = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
    if title:
        f.suptitle(title + " ({:.4f} eV)".format(vasp.get_energy()[-1]))

    for chn_nr, lv in enumerate(levels):
        last_val = 0
        last_left = False
        colors = ["blue", "red", "orange"]
        colors_used = [False]*3

        for nr, e in enumerate(lv):
            if np.isclose(occupations[chn_nr][nr], 0):
                color = colors[0]
                label = "unoccupied"
                if colors_used[0]:
                    label = ""
                colors_used[0] = True
            elif np.isclose(occupations[chn_nr][nr], 1):
                color = colors[1]
                label = "occupied"
                if colors_used[1]:
                    label = ""
                colors_used[1] = True
            else:
                label = "half occupied"
                if colors_used[2]:
                    label = ""
                color = colors[2]
                colors_used[2] = True

            print(colors_used)
            print(label)

            lw = 3

            if nr < ignore[chn_nr][0] or len(lv) - (nr + 1) < ignore[chn_nr][1]:
                last_val = e
                continue

            if (nr + 1 != len(lv)) and (
                    np.isclose(e, lv[nr + 1], rtol=0, atol=0.1)):
                axes[chn_nr].hlines(e, -0.9, -0.1, color=color, lw=lw,
                                    label=label)
                axes[chn_nr].text(-1.9, e - 0.05, "${:.4f}$ eV".format(e))
                last_left = True
            elif last_left:
                axes[chn_nr].hlines(e, 0.1, 0.9, color=color, lw=lw,
                                    label=label)
                axes[chn_nr].text(1, e - 0.05, "${:.4f}$ eV".format(e))
                last_left = False
            else:
                axes[chn_nr].hlines(e, -.3, 0.3, color=color, lw=lw,
                                    label=label)
                axes[chn_nr].text(0.5, e - 0.05, "${:.4f}$ eV".format(e))
                last_left = False

            last_val = e

    axes[0].set_xlim(-2, 2)
    for ax in axes:
        ax.get_xaxis().set_visible(False)
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=3, fancybox=True, shadow=True)

    axes[0].set_ylim(top=15)

    plt.tight_layout()
    plt.savefig(figname)
    plt.clf()


def draw_parabolas(deltax, deltay, omega, ax, labels):
    coordinates = [[0, 0]]
    d = 0.9
    x = np.linspace(-d, d, 10000)
    y1 = 0.5*(omega**2)*(x**2)
    ax.plot(x, y1, lw=2, label=labels[0])
    coordinates.append([deltax, 0.5*(omega**2)*(deltax**2)])

    coordinates.append([deltax, deltay])
    x = np.linspace(-d + deltax, d + deltax, 10000)
    y2 = 0.5*(omega**2)*((x - deltax)**2) + deltay
    coordinates.append([0, 0.5*(omega**2)*((0 - deltax)**2) + deltay])
    ax.plot(x, y2, lw=2, label=labels[1])
    ax.set_xlim(-1, 2)
    ax.set_ylim(-0.1, 2.5)

    return np.array(coordinates)


def draw_fc_graphs(ground, groundeg, excited, excitedgg, filename,
                   labels=("", "")):
    f, ax = plt.subplots(1, figsize=(8, 6))
    deltax = 0.3
    deltay = 1.5
    coord = draw_parabolas(deltax, deltay, 2, ax, labels)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    eg = VaspXML(ground).get_energy()[-1]
    eeg = VaspXML(groundeg).get_energy()[-1]
    ee = VaspXML(excited).get_energy()[-1]
    if excitedgg is not None:
        egg = VaspXML(excitedgg).get_energy()[-1]
    else:
        egg = None

    ax.scatter(*coord[:2].T)
    ax.scatter(*coord[2:].T)

    # ZPL
    ax.hlines(0, -deltax, deltax * 2, linestyles="dashed", color="gray")
    ax.hlines(deltay, -deltax, deltax*4, linestyles="dashed", color="gray")
    ax.annotate(
        '', xy=(-0.8*deltax, deltay), xycoords='data',
        xytext=(-0.8*deltax, 0), textcoords='data',
        arrowprops={'arrowstyle': '<->'})
    plt.annotate(
        '$E_{{ZPL}} = {:.4f}$ eV'.format(ee - eg),
        xy=(-0.8*deltax, deltay/2), xycoords='data',
        xytext=(5, 0), textcoords='offset points')

    # Ground relaxation
    ax.hlines(coord[1][1], deltax, deltax*2,
              linestyles="dashed", color="gray")
    ax.annotate(
        '', xy=(0.8*deltax + deltax, coord[1][1]), xycoords='data',
        xytext=(0.8*deltax + deltax, 0), textcoords='data',
        arrowprops={'arrowstyle': '<->'})
    plt.annotate(
        'Ground relaxation = {:.4f} eV'.format(eeg - eg),
        xy=(0.8*deltax + deltax, coord[1][1]/2), xycoords='data',
        xytext=(5, 0), textcoords='offset points')

    if egg is not None:
        # Excited relaxation
        ax.hlines(coord[3][1], 0, deltax * 4,
                  linestyles="dashed", color="gray")
        ax.annotate(
            '', xy=(0.5*deltax + 3*deltax, coord[3][1]), xycoords='data',
            xytext=(0.5*deltax + 3*deltax, deltay), textcoords='data',
            arrowprops={'arrowstyle': '<->'})
        plt.annotate(
            'Excited relaxation = {:.4f} eV'.format(egg - ee),
            xy=(0.5*deltax + 3*deltax, deltay + (coord[3][1] - deltay)/2),
            xycoords='data',
            xytext=(5, 0), textcoords='offset points')

    ax.vlines(0, 0, coord[3][1], color="gray", lw=0.2)
    ax.vlines(deltax, coord[1][1], deltay, color="gray", lw=0.2)
    plt.tight_layout()
    ax.legend()
    plt.savefig(filename)
    plt.clf()


draw_fc_graphs(
    "nv4x4x4/relaxed/vasprun.xml",
    "nv4x4x4/excited_sym_geometry/vasprun.xml",
    "nve4x4x4sym/relaxed/vasprun.xml",
    "nve4x4x4sym/ground_geometry/vasprun.xml",
    "nv4x4x4sym_fc.pdf", labels=["nv4x4x4", "nve4x4x4sym"])

draw_fc_graphs(
    "nv4x4x4/relaxed/vasprun.xml",
    "nv4x4x4/excited_sym_geometry/vasprun.xml",
    "nve4x4x4asym/relaxed/vasprun.xml",
    None,
    "nv4x4x4asym_fc.pdf", labels=["nv4x4x4", "nve4x4x4asym"])


DRAW_OCCUPATION_DIAGRAMS = False

if DRAW_OCCUPATION_DIAGRAMS:
    draw_occupation_diagram("nve4x4x4sym/relaxed/vasprun.xml",
                            figname="nve4x4x4sym_occ.pdf",
                            title="nve4x4x4sym",
                            ignore=[[4, 0], [1, 0]])

    draw_occupation_diagram("nve4x4x4asym/relaxed/vasprun.xml",
                            figname="nve4x4x4asym_occ.pdf",
                            title="nve4x4x4asym",
                            ignore=[[4, 0], [1, 0]])

    draw_occupation_diagram("nv4x4x4/relaxed/vasprun.xml",
                            figname="nv4x4x4_occ.pdf",
                            title="nv4x4x4",
                            ignore=[[3, 0], [1, 0]])

