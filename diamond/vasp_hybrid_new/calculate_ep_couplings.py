import sys
import os
import re
import numpy as np
import time
sys.path.append("../../dephon")
from deutils import ensure_dir
from dedisp import calculate_couplings

out_dir = "./couplings/"
bulk_geometry_poscar = "./nv4x4x4/bulk_geometry/POSCAR"
ground_poscar = "./nv4x4x4/relax/CONTCAR"
hdf5dir = "../vasp/embeded/nv{0}x{0}x{0}"


calculations = [
    {
        "poscar1": ground_poscar,
        "poscar2": "./nve4x4x4sym/relaxed/CONTCAR",
        "from_forces": False,
        "name": "symmetric_displacements"
    },
    {
        "poscar1": ground_poscar,
        "poscar2": "./nve4x4x4asym/relaxed/CONTCAR",
        "from_forces": False,
        "name": "asymmetric_displacements"
    },
    {
        "vasprun1": "./nv4x4x4/excited_sym_geometry/vasprun.xml",
        "vasprun2": "./nve4x4x4sym/relaxed/vasprun.xml",
        "from_forces": True,
        "name": "symmetric_forces_ground_state"
    },
    {
        "vasprun1": "./nv4x4x4/excited_asym_geometry/vasprun.xml",
        "vasprun2": "./nve4x4x4asym/relaxed/vasprun.xml",
        "from_forces": True,
        "name": "asymmetric_forces_ground_state"
    },
    {
        "vasprun1": "./nve4x4x4sym/ground_geometry/vasprun.xml",
        "vasprun2": "./nve4x4x4sym/relaxed/vasprun.xml",
        "from_forces": True,
        "name": "excited_forces_ground_state"
    }
]

if __name__ == "__main__":
    poscar1 = "./nv3x3x3/relax/CONTCAR"
    poscar2 = "./nve3x3x3asym/relax/CONTCAR"
    bulkg_poscar = "./nv3x3x3/bulk_geometry/POSCAR"
    data = calculate_couplings(
        poscar1, poscar2,
        bulkg_poscar=bulkg_poscar,
        from_forces=False,
        hdf5dir=hdf5dir.format(3) + "direct")
    print(data)

if __name__ == "__main__" and False:
    for cell_size in [4, 6, 15]:
        for calc_data in calculations:
            print("-"*70)
            print("cell size =", cell_size, calc_data["name"])
            print("-"*70)
            beg = time.time()

            from_forces = calc_data["from_forces"]

            if not from_forces:
                input1 = calc_data["poscar1"]
                input2 = calc_data["poscar2"]
            else:
                input1 = calc_data["vasprun1"]
                input2 = calc_data["vasprun2"]

            cell_name = re.search(r"(nv.*?)/", input2).group(1)
            output_dir = os.path.join(out_dir, calc_data["name"])
            ensure_dir(output_dir)

            data = calculate_couplings(
                input1, input2,
                bulkg_poscar=bulk_geometry_poscar,
                from_forces=from_forces,
                hdf5dir=hdf5dir.format(cell_size))

            print("Couplings calculated in: {:.2f} s".format(
                time.time() - beg))
            print("S = {:.2f}".format(data["hr_tot"]))

            np.savetxt(
                os.path.join(
                    output_dir, "nv{0}x{0}x{0}_hr_spectrum.dat".foramat(
                        cell_size)),
                np.array([data["freq_mev"], data["hr_factors"]]).T,
                header='frequencies (meV), HR_factors',
                fmt=['%16.12f', '%20.14f'])

            dtypes = [
                ('freq', float),
                ('freq_mev', float),
                ('fc_displ', float),
                ('k', float),
                ('irreps', 'U6')]

            if from_forces:
                dtypes.append(('forces', float))

            couplings_data = np.zeros(
                data["freq"].size,
                dtype=dtypes
            )

            couplings_data["freq"] = data["freq"]
            couplings_data["freq_mev"] = data["freq_mev"]
            couplings_data["fc_displ"] = data["fc_displ"]
            couplings_data["k"] = data["k"]
            couplings_data["irreps"] = data["irreps"]
            fmt = '%12.9f, %12.9f, %13.9f, %12.9f, %6s'
            header = '{:11} {:14} {:14} {:15} {:6}'.format(
                "freq (au)",
                "freq (meV)",
                "fc_displ (au)",
                "k", "irrep")

            if from_forces:
                fmt = '%12.9f, %12.9f, %13.9f, %12.9f, %6s, %12.9f'
                couplings_data["forces"] = data["forces"]
                header += " {:15}".format("forces (au)")

            np.savetxt(
                os.path.join(output_dir, "nv{0}x{0}x{0}_couplings.dat".format(
                    cell_size)),
                couplings_data,
                header=header,
                fmt=fmt)
