import sys
import os
import matplotlib.pyplot as plt
sys.path.append("../../dephon")

try:
    from descalarf import (
        plot_difference_of_scalar_fields,
        integrate_charge_from_center)
except:
    raise


##############################
#    Plot charge slice
##############################

if False:
    for nr in range(2, 6):
        output_dir = "images/"
        dir1 = "{0}x{0}x{0}/plots/".format(nr)
        dir2 = "nv{0}x{0}x{0}/plots/".format(nr)

        center_point = [4.4683, 4.4683, 4.4683]
        f, ax = plt.subplots(1, figsize=(14, 10))
        plot_difference_of_scalar_fields(
            os.path.join(dir2 + "bulk_el_psch_den.xsf"),
            os.path.join(dir1 + "el_psch_den.xsf"),
            "charge(nv{0}x{0}x{0}-bulk{0}x{0}x{0})".format(nr),
            ax=ax, fig=f, center_point=center_point)

        plt.savefig("images/charge_nv{0}x{0}x{0}.pdf".format(nr), dpi=300)
        plt.clf()

################################
#  Plot spherical integration
################################
fr, till = 2, 6

paths = [
    ["bulk_nv{0}x{0}x{0}".format(nr),
     "nv{0}x{0}x{0}/plots/bulk_el_psch_den.xsf".format(nr)]
    for nr in range(fr, till)]
compare_paths = [
    ["bulk{0}x{0}x{0}".format(nr),
     "{0}x{0}x{0}/plots/el_psch_den.xsf".format(nr)]
    for nr in range(fr, till)]
name = "Charge(NV$^{-1}$)"
center_point = [4.4683, 4.4683, 4.4683]


fig = plt.figure(figsize=(20, 20))
ax1 = plt.subplot2grid((4, 4), (0, 0), colspan=2)
ax2 = plt.subplot2grid((4, 4), (1, 0), colspan=2)
ax3 = plt.subplot2grid((4, 4), (2, 0), colspan=2)
ax4 = plt.subplot2grid((4, 4), (3, 0), colspan=2)
ax5 = plt.subplot2grid((4, 4), (0, 2), colspan=2, rowspan=2)
ax6 = plt.subplot2grid((4, 4), (2, 2), colspan=2, rowspan=2)


integrate_charge_from_center(
    paths,
    name,
    [ax1, ax2, ax3, ax4],
    compare_paths=compare_paths,
    center_point=center_point,
    shorter=True)

plt.show()
# plot_difference_of_scalar_fields(
#     d + "nv{0}x{0}x{0}nonrelaxed{1}.xsf".format(4, val),
#     d + "nv0{0}x{0}x{0}nonrelaxed{1}.xsf".format(4, val),
#     # d + "bulk4x4x4el_psch_den.xsf",
#     # "potencial(nv4x4x4nonrelaxed) - potencial(nv04x4x4nonrelaxed)",
#     "Charge(NV$^{-1}$) - Charge(NV$^0$)",
#     matplotlib=True,
#     ax=ax5, fig=fig, center_point=center_point)

# val = "tot_pot"
# plot_difference_of_scalar_fields(
#     d + "nv{0}x{0}x{0}nonrelaxed{1}.xsf".format(4, val),
#     d + "nv0{0}x{0}x{0}nonrelaxed{1}.xsf".format(4, val),
#     # d + "bulk4x4x4el_psch_den.xsf",
#     # "potencial(nv4x4x4nonrelaxed) - potencial(nv04x4x4nonrelaxed)",
#     "TotPot(NV$^{-1}$) - TotPot(NV$^0$)",
#     matplotlib=True,
#     ax=ax6, fig=fig, center_point=center_point)

# plt.tight_layout()
# plt.savefig(os.path.join(output_dir, "new.pdf"), dpi=300)
# # plt.show()
# plt.clf()

#     # plt.show()
