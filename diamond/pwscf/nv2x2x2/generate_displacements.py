import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

create_displacements("pwscf", 1, filepath="nv.scf.in")
generate_job_files("pwscf", "cori", "nv2x2x2pw", 1, 32, plan="regular", jobs=5,
                   time="2:00:00")
