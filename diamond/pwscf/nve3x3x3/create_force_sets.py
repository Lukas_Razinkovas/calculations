"""When abinitio calculations finished call this for postprocessing"""

import sys
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("../../../dephon")

from dephonopy import create_force_sets, calculate_fcmatrix, create_plot
from defc import ForceConstants

create_force_sets("pwscf", workdir="phonopy_phonons")
calculate_fcmatrix("pwscf", workdir="phonopy_phonons", dim=1,
                   conv_file_name="nv.scf.in")

pwscf = ForceConstants("pwscf", outdir="phonopy_phonons")
modes = pwscf.calculate_modes()
print(pwscf.get_frequencies("thz").max(), "THz")
print(pwscf.get_frequencies("cm").max(), "cm^-1")
print(pwscf.matrix.max())
print(pwscf.atoms.get_cell())

modes.analyse()
indx = np.where(modes.iprs < 40)[0]
with open("localized_modes.csv", "wt") as f:
    header = modes._mode_header()
    f.write(header + "\n")
    for i in indx:
        f.write(modes._mode_info(i) + "\n")
