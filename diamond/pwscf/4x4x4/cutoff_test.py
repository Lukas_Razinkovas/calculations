import sys
import os
sys.path.append("../../../dephon")
import matplotlib.pyplot as plt
from defc import ForceConstants
from deutils import ensure_dir

fc = ForceConstants('pwscf', phonopydir="phonopy_phonons")

for mydir in ["cutoff_graphs/my_corr"]:
    ensure_dir(mydir)

    for cut in fc.get_shells()[1:18]:
        fc = ForceConstants('pwscf', phonopydir="phonopy_phonons")
        modes = fc.calculate_modes()
        diff = 4.5
        if cut > 3:
            diff = 1.5
        if cut > 3.6:
            diff = 1
        if cut > 4.5:
            diff = 0.5
        if cut > 6.5:
            diff = 0.2

        cut = cut + 0.001
        f, ax = plt.subplots(1)

        fc2 = ForceConstants('pwscf', phonopydir="phonopy_phonons")
        nn = fc2.perform_cutoff(cut)
        if mydir.endswith("phonopy"):
            fc2.symmetrize_fc(2, phonopy=True)
        elif mydir.endswith("my_corr"):
            fc2.symmetrize_fc(2)

        modes2 = fc2.calculate_modes()
        modes.compare_phonon_dispersion(modes2, ax, differences=None)
        ax.legend(loc=4)
        ax.set_title("{} nearest neighbours ({:.1f} A)".format(nn, cut))
        plt.savefig(
            os.path.join(mydir, "{}nn_{:.2f}A.png".format(nn, cut)),
            dpi=300)
        plt.clf()

        f, ax = plt.subplots(1)
        modes.compare_phonon_dispersion(modes2, ax, differences=diff)
        ax.legend(loc=4)
        ax.set_title("{} nearest neighbours ({:.1f} A)".format(nn, cut))
        plt.savefig(
            os.path.join(mydir, "{}nn_{:.2f}A_diffs.png".format(nn, cut)),
            dpi=300)
        plt.clf()
