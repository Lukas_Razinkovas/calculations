import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

create_displacements("pwscf", 4, filepath="bulk.scf.in")
generate_job_files("pwscf", "cori", "DiaPh4", 2, 64, plan="regular", jobs=1,
                   time="2:30:00")
