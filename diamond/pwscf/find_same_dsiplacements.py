import yaml
import sys
import pprint
import os
import numpy as np
sys.path.append("../../dephon")

from dephonopy import dispyaml_to_atom
from espresso import EspressoInput

pp = pprint.PrettyPrinter()

displaced = []
files = {}

for mult in [3, 2, 4, 5]:
    DIR = "./nv{0}x{0}x{0}".format(mult)
    filepath = os.path.join(DIR, "phonopy_phonons/disp.yaml")
    with open(filepath, "rt") as f:
        content = f.read()

    data = yaml.load(content)
    displ = data["displacements"]
    atoms = dispyaml_to_atom(filepath, "pwscf")
    indxN = atoms.symbols.index("N")
    bulkatoms = atoms

    ##############################
    #  Collect reference points
    ##############################
    q = set()
    displacements = {
        0,
        2.7837900000000002,
        4.5819200000000002,
        5.1795099999999996,
        5.7204699999999997
    }

    files[DIR] = {}
    if not displaced:
        for nr, disp in enumerate(displ):
            indx = disp["atom"] - 1
            vec, d = bulkatoms.get_separation_vector(indx, indxN)
            q.add(d)
            direct = disp["direction"]
            for j in displacements.copy():
                if np.allclose(j, d, 0.1, 0):
                    displacements.remove(j)
                    pos = bulkatoms.get_positions()[indx]
                    displaced.append([pos, direct, vec])
                    files[DIR][d] = nr + 1
                    break
        # pp.pprint(q)
        # input(displacements)
    else:
        displaced2 = displaced[:]
        for nr, disp in enumerate(displ):
            indx = disp["atom"] - 1
            direct = disp["direction"]
            vec, d = bulkatoms.get_separation_vector(indx, indxN)
            pos = bulkatoms.get_positions()[indx]
            for nr2, (_pos, _dir, _vec) in enumerate(displaced2[:]):
                if np.allclose(pos, _pos, 0.1, 0) and np.allclose(
                        direct, _dir):
                    assert np.allclose(
                        vec, _vec, 0.1, 0), "{} -> {}".format(vec, _vec)
                    files[DIR][d] = nr + 1
                    displaced2.pop(nr2)
        print("="*70)
        print(DIR)
        pp.pprint(displaced)

pp.pprint(files)

