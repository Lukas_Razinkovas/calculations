import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

create_displacements("pwscf", 3, filepath="bulk.scf.in")
generate_job_files("pwscf", "cori", "DiaPh3", 1, 32, plan="debug", jobs=2,
                   time="30:00")
