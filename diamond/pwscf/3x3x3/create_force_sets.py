"""When abinitio calculations finished call this for postprocessing"""

import sys
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("../../../dephon")

from dephonopy import create_force_sets, calculate_fcmatrix, create_plot
from defc import ForceConstants

create_force_sets("pwscf", workdir="phonopy_phonons")
calculate_fcmatrix("pwscf", workdir="phonopy_phonons", dim=3,
                   conv_file_name="bulk.scf.in")

f, ax = plt.subplots(1, figsize=(15, 10))

fc = ForceConstants("pwscf", outdir="phonopy_phonons")
modes = fc.calculate_modes()
print(fc.get_frequencies("thz").max(), "THz")
print(fc.get_frequencies("cm").max(), "cm^-1")
print(fc.matrix.max())

f, ax = plt.subplots(1, figsize=(15, 10))
fc.calculate_bulk_band_structure(ax=ax, color="red", label="new")

plt.legend()
plt.savefig("dispersion_lines.pdf", dpi=300)
# plt.show()
