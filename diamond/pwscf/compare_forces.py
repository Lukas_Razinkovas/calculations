import os
import sys
import numpy as np
import pprint
sys.path.append("../../dephon")
from espresso import EspressoInput, SimplifiedOutputParser

pp = pprint.PrettyPrinter()


def iterate_displaced(outdir, find_pos=None):

    relaxed = EspressoInput(
        "{}/phonopy_phonons/supercell.in".format(
            outdir)).atoms.get_positions()
    bulk_atoms = EspressoInput(
        "{}/nv.relax.in".format(outdir)).atoms

    bulk = bulk_atoms.get_positions()

    npos = bulk[bulk_atoms.symbols.index("N")]

    _dir = "{}/phonopy_phonons".format(outdir)

    atoms_info = {}

    for fl in os.listdir(_dir):
        if find_pos is None:
            if fl.endswith(".out") and fl.startswith("supercell"):
                displ = EspressoInput(
                    os.path.join(_dir, os.path.splitext(fl)[0] + ".in"))
                pos = displ.atoms.get_positions()

                a = np.linalg.norm(pos - relaxed, axis=1)
                i = a.argmax()

                dist_fr_n = np.linalg.norm(bulk[i] - npos)

                forces = SimplifiedOutputParser(
                    os.path.join(_dir, fl)).get_forces()[i]

                info = {
                    "relaxed_pos": relaxed[i],
                    "bulk_pos:": bulk[i],
                    "displaced_pos": pos[i],
                    "forces": forces
                }

                atoms_info[dist_fr_n] = info

    return atoms_info


out = iterate_displaced("nv5x5x5")
pp.pprint(out)
