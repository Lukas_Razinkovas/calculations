import os
import shutil

files = os.listdir("phonopy_phonons")
missing = []
infiles = list(
    filter(lambda x: x.startswith("supercell") and x.endswith(".in"), files))

for i in infiles:
    out_file = os.path.splitext(i)[0] + ".out"
    if out_file not in files:
        shutil.copy(os.path.join("phonopy_phonons", i), "phonopy_phonons2")
        continue

    with open(os.path.join("phonopy_phonons", out_file), "rt") as f:
        content = f.read()

    if "JOB DONE." not in content:
        shutil.copy(os.path.join("phonopy_phonons", i), "phonopy_phonons2")
        continue
