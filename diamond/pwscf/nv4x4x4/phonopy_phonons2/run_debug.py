import subprocess
import time

jobs = [
    "phonons0.job",
    "phonons1.job",
]


while jobs:
    proc = subprocess.Popen(['sqs', '-w'], stdout=subprocess.PIPE)
    lines = proc.communicate()[0].split()
    cnt = lines.count("debug")
    print("running {}".format(cnt))
    if lines.count("debug") < 3:
        jb = jobs.pop(0)
        print("submiting job {}".format(jb))
        subprocess.call("sbatch {}".format(jb), shell=True)

    time.sleep(10)
