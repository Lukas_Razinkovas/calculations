import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

# create_displacements("pwscf", 1, filepath="nv.scf.in")
# generate_job_files("pwscf", "cori", "nv4x4x4ph", 3, 96, plan="regular",
#                    jobs=25,
#                    time="10:00:00")
generate_job_files("pwscf", "cori", "nv4x4x4phdb", 8, 256, plan="debug",
                   jobs=132,
                   time="30:00", workdir="phonopy_phonons2")
