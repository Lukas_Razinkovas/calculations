import sys
sys.path.append("../../../dephon")
from dephonopy import create_displacements, generate_job_files

# create_displacements("pwscf", 1, filepath="nv.scf.in")
generate_job_files("pwscf", "cori", "nv4x4x4ph", 6, 192, plan="regular",
                   jobs=2,
                   qos=None,
                   time="16:00:00")

