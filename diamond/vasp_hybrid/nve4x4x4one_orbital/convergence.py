import numpy as np
import matplotlib.pyplot as plt

energies = []
with open("OSZICAR", "rt") as f:
    for i in f:
        if not i.startswith("DAV"):
            continue
        energies.append(float(i.split()[3]))

plt.plot(energies[40:])
plt.show()
