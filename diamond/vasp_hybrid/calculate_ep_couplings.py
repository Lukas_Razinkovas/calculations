import sys
import os
import re
import numpy as np
sys.path.append("../../dephon")
from deutils import ensure_dir
from dedisp import calculate_couplings

parent_output_dir = "./direct_couplings/"
output_dir_relax = os.path.join(parent_output_dir, "from_relaxation/")
output_dir_forces = os.path.join(parent_output_dir, "from_forces/")
bulk_geometry_poscar = "./nv4x4x4/bulk_geometry/POSCAR"
ground_poscar = "./nv4x4x4/relax/CONTCAR"
hdf5dir = "../vasp/embeded/nv{0}x{0}x{0}"


calculations = [
    {
        "poscar1": ground_poscar,
        "poscar2": "./nve4x4x4/relax/CONTCAR",
        "output_dir": output_dir_relax,
        "from_forces": False
    },
    {
        "poscar1": ground_poscar,
        "poscar2": "./nve4x4x4jt/relax/CONTCAR",
        "output_dir": output_dir_relax,
        "from_forces": False
    },
    {
        "vasprun1": "./nv4x4x4/excited_geometry/vasprun.xml",
        "vasprun2": "./nve4x4x4/relax/vasprun.xml",
        "output_dir": output_dir_forces,
        "from_forces": True
    },
    {
        "vasprun1": "./nv4x4x4/excited_jt_geometry/vasprun.xml",
        "vasprun2": "./nve4x4x4jt/relax/vasprun.xml",
        "output_dir": output_dir_forces,
        "from_forces": True
    }
]


for cell_size in range(4, 5, 1):
    for calc_data in calculations:
        from_forces = calc_data["from_forces"]

        if not from_forces:
            input1 = calc_data["poscar1"]
            input2 = calc_data["poscar2"]
        else:
            input1 = calc_data["vasprun1"]
            input2 = calc_data["vasprun2"]

        dirname = re.search(r"(nv.*?)/", input2).group(1)
        output_dir = os.path.join(calc_data["output_dir"], dirname)
        ensure_dir(output_dir)

        data = calculate_couplings(
            input1, input2,
            bulkg_poscar=bulk_geometry_poscar,
            from_forces=from_forces,
            hdf5dir=hdf5dir.format(cell_size))

        print("{} {} S = {}".format(
            dirname, "forces" if from_forces else "relaxation",
            data["hr_tot"]))

        np.savetxt(
            os.path.join(output_dir, "nv{0}x{0}x{0}_hr_spectrum.dat".format(
                cell_size)),
            np.array([data["freq_mev"], data["hr_factors"]]).T,
            header='frequencies (meV), HR_factors',
            fmt=['%16.12f', '%20.14f'])

        couplings_data = np.zeros(
            data["freq"].size,
            dtype=[
                ('freq', float),
                ('freq_mev', float),
                ('fc_displ', float),
                ('k', float),
                ('irreps', 'U6')]
        )
