rewriting = True
with open("CHGCAR", "wt") as f:
    with open("CHGCAR.bak", "rt") as f2:
        for nr, i in enumerate(f2):
            if nr % 10000 == 0:
                print(nr, end="\r")

            if "augmentation occupancies" in i:
                rewriting = False

            elements = i.split()
            if len(elements) == 5 and rewriting:
                f.write(
                    " ".join(elements[:3] + [elements[4]]) +
                    "\n")
            else:
                f.write(i)
