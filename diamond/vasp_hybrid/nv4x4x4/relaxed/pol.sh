#!/usr/bin/env bash 
filename=`echo $1 | sed 's/PARCHG./CHGCAR-/' | sed 's/.ALLK//'` 
rastgele=$RANDOM 
natoms=`awk "NR==6" $1 | awk '{print ($1+$2+$3+$4+$5+$6+$7+$8+$9)}'`
ngrid=`let ngrid=$natoms+9 ; echo $ngrid`

grid=`awk "NR==$ngrid" $1` 

nupd_b=`echo $ngrid | awk '{print ($1+1)}'` 
nupd_e=`grep -n "$grid" $1 | awk "NR==2" | awk -F ":" '{print $1}'` 

awk "NR<$nupd_b" $1 > title.$rastgele 

cat title.$rastgele > $filename-u-d.$rastgele ; awk "NR>$nupd_e" $1 >> $filename-u-d.$rastgele 
awk "NR>=$nupd_b" $filename-u-d.$rastgele > u-d1.$rastgele ; awk "NF==10" u-d1.$rastgele > u-d.$rastgele 
mv $filename-u-d.$rastgele $filename-u-d 

rm -rf *.$rastgele
