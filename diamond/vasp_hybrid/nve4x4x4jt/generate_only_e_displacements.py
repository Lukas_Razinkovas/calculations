import sys
import os
import numpy as np
import matplotlib.pyplot as plt

sys.path.append("../../../dephon")
sys.path.append("../../../dejt")
from dedisp import (calculate_fc_displacements, read_and_allign_embeded_cell,
                    iter_modes, read_displacements, bohr_in_angstr,
                    get_mapping_between_geometries)
from vasp import Poscar
from deutils import ensure_dir

cell_size = 4
only_irrep = b"Exy"

poscar1 = "../nve4x4x4/relax/CONTCAR"
poscar2 = "./relax/CONTCAR"
bulkg_poscar = "../nv4x4x4/bulk_geometry/POSCAR"
hdf5dir = "../../vasp/embeded/nv{0}x{0}x{0}".format(cell_size)

init_displ, _, atoms1, atoms2 = read_displacements(poscar1, poscar2)

eatoms, bulk_atoms = read_and_allign_embeded_cell(bulkg_poscar, hdf5dir)
freq, fc_displ, irreps = calculate_fc_displacements(
    poscar1, poscar2, bulkg_poscar, hdf5dir, units="au")

new_displ = np.zeros(len(eatoms)*3)
for (f, mode_displ, irrep), fc_d in zip(
        iter_modes(hdf5dir, eatoms, weighted=True), fc_displ):
    if irrep == only_irrep:
        new_displ += -fc_d*mode_displ

indx = np.where(irreps == "Exy")[0]
plt.scatter(freq[indx], fc_displ[indx], label="init")

masses = eatoms.get_masses(units="au").repeat(3)
new_displ /= np.sqrt(masses)
new_displ *= bohr_in_angstr
new_displ = new_displ.reshape(init_displ.shape)

bulk_atoms.sort_positions()
mymap = get_mapping_between_geometries(eatoms, bulk_atoms)
final_displ = np.zeros(new_displ.shape)
for nr, i in enumerate(new_displ):
    final_displ[mymap[nr]] = i

print(max(final_displ.flatten() - init_displ.flatten()))

poscar_obj = Poscar(poscar1)
atoms = poscar_obj.create_atoms()

old_positions = atoms.get_positions()
new_positions = np.zeros(shape=old_positions.shape)

for nr, (pos, d) in enumerate(zip(old_positions, final_displ)):
    new_positions[nr] = pos - d

atoms.set_positions(new_positions)
poscar_obj.set_atoms(atoms)

mydir = "relax_from_e_displ"
ensure_dir(mydir)
new_poscar = os.path.join(mydir, "POSCAR")

with open(new_poscar, "wt") as f:
    f.write(poscar_obj.create_input())

freq, fc_displ, irreps = calculate_fc_displacements(
    poscar1, new_poscar, bulkg_poscar, hdf5dir, units="au")

init_displ, _, atoms1, atoms2 = read_displacements(poscar1, new_poscar)
print(atoms1.get_mass_center(), atoms2.get_mass_center())

plt.scatter(freq, fc_displ, label="new", alpha=0.4)
plt.legend()
plt.show()

# indx = np.where(irreps != "???")[0]
# plt.scatter(freq[indx], fc_displ[indx])


# plt.show()
