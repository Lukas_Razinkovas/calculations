import sys
import numpy as np
sys.path.append("../../../dephon")
from vasp import Poscar


dat = np.loadtxt("relative_displacements.dat")
poscar = Poscar("../nve4x4x4/relax/CONTCAR")
atoms1 = poscar.create_atoms()
lc = atoms1.get_cell()[0, 0]

with open("relax/POSCAR-SYMMETRIC", "wt") as f:
    f.write(poscar.create_input())

pos = atoms1.get_positions()
for i in dat:
    nr = int(i[0])
    displ = i[1:]
    pos[nr] = pos[nr] + 2.3*displ*lc

atoms1.set_positions(pos)
poscar.set_atoms(atoms1)
with open("relax/POSCAR-SYMMETRIC-DISPLACED", "wt") as f:
    f.write(poscar.create_input())
