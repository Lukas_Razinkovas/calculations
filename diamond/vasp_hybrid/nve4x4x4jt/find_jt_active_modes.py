import sys
import os
import pickle
import re
import numpy as np
import h5py
from scipy.constants import physical_constants, electron_volt, hbar
sys.path.append("../../../dephon")
from dedisp import read_displacements, remove_pb_artifacts
from vasp import Poscar


hbar_ev = physical_constants["Planck constant in eV s"][0]/(2*np.pi)

##############################
#        HR Factors
##############################


def read_embeded_bulk_positions(filepath):
    with open(filepath, "rb") as f:
        atoms = pickle.load(f)

    return atoms


def iter_modes(hdf5dir, mass_matrix, sym="Exy"):
    # Yields freq in (2 pi Hz) and displ in dimensionless units
    hdf5file = os.path.join(hdf5dir, "modes.hdf5")
    hdf5file2 = os.path.join(hdf5dir, "mode_analysis.hdf5")
    f = h5py.File(hdf5file, "r")
    f2 = h5py.File(hdf5file2, "r")
    indx = np.where(b'Exy' == f2["irreps"][:])

    for cnt, nr in enumerate(indx[0]):
    # for nr, freq in enumerate(f["frequencies"]):
        # cnt = nr
        if cnt % 100 == 0:
            print("{}/{}".format(cnt, len(indx[0])), end="\r")
        freq = f["frequencies"][nr]
        if nr < 3:
            continue
        displ = f["displacements"][nr]*mass_matrix
        # frequencie is read in meV
        yield (freq[0]*1e-3)/hbar_ev, displ/np.linalg.norm(displ)


def calculate_hr_factors(poscar1, poscar2, bulk_poscar, hdf5dir,
                         defect_atom="N", name="displacements.xyz",
                         units="mev"):

    displ, distances, atoms1, atoms2 = read_displacements(poscar1, poscar2)
    bulk_atoms = Poscar(bulk_poscar).create_atoms()
    eatoms = read_embeded_bulk_positions(os.path.join(hdf5dir, "atoms.p"))

    indx = distances.argsort()[-6:]
    print("Max displacements:", distances[indx])

    #####################################################
    #  Equalise lattice constants for bulk and embeded
    #####################################################

    embeded_size = int(re.search("x(.*?)x", hdf5dir).group(1))
    bulk_size = int(re.search("x(.*?)x", bulk_poscar).group(1))

    elc = eatoms.get_cell()[0, 0]/embeded_size
    blc = bulk_atoms.get_cell()[0, 0]/bulk_size
    eatoms.set_cell(eatoms.get_cell()*blc/elc)

    ##############################
    #       Error checking
    ##############################

    tmp = bulk_atoms.copy()
    tmp.set_cell(atoms1.get_cell())
    remove_pb_artifacts(bulk_atoms, atoms1)
    remove_pb_artifacts(bulk_atoms, atoms2)

    for indx, (pos1, pos2) in enumerate(
            zip(tmp.get_positions(), atoms1.get_positions())):
        assert np.allclose(pos1, pos2, atol=0.2, rtol=0), (
            "Positions strongly differ from bulk geometry:",
            indx, pos1, pos2)

    with open(name + "_displacements.xyz", "wt") as f:
        f.write(bulk_atoms.create_xyz_content(displ))

    # --------------------------- #
    #          Embeding           #
    # --------------------------- #

    displacements = np.zeros(len(eatoms)*3).reshape(len(eatoms), 3)
    for nr, (pos, d) in enumerate(zip(
            bulk_atoms.get_positions(), displ)):

        indx = eatoms.find_by_position(pos, pbc=True, method=3)
        displacements[indx] = np.array(d)

    print("NORM:", np.apply_along_axis(np.linalg.norm, 1, displacements).sum())

    mass_array = eatoms.get_masses(units="kg").repeat(3)
    square_masses = np.sqrt(mass_array)
    displacements *= 1e-10
    weighted_displ = displacements.flatten()*square_masses

    # --------------------------------------#
    #         Calculate HR factors          #
    # --------------------------------------#

    HR_factors = []
    Ejt = []
    frequencies = []

    for nr, (freq, displ) in enumerate(iter_modes(
            hdf5dir, 1/square_masses, sym="Exy")):
        # print(nr, end="\r")

        if units.lower() == "mev":
            units = "meV"
            frequencies.append(freq*hbar_ev*1e3)
        else:
            units = "THz"
            frequencies.append(freq*1e-12/(2 * np.pi))

        fc_displ = weighted_displ.dot(displ)
        hr_factor = freq*(fc_displ**2)/(2*hbar)
        HR_factors.append(hr_factor)
        Ejt.append(freq*hbar_ev*hr_factor)

    print("S = ", sum(HR_factors))
    return {
        "HR_factors": np.array(HR_factors),
        "frequencies": np.array(frequencies),
        "Ejt": np.array(Ejt),
        "S": sum(HR_factors)
    }


def gaussian_smoothing(x, y, sig=6, n=1000):
    def gaussian(x, mu, sig=sig):
        return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
            2 * np.power(sig, 2.)))

    x_new = np.linspace(0, 200, n)
    y_new = np.zeros(len(x_new))

    for _x, _y in zip(x, y):
        y_new += _y*gaussian(x_new, _x)

    return x_new, y_new


if __name__ == "__main__":
    print()
    import matplotlib
    matplotlib.use('agg')
    import matplotlib.pyplot as plt

    for embeded_size in range(15, 16):
    # embeded_size = 6

        hse_poscars = dict(
            bulk="../../vasp/nve4x4x4/relax/POSCAR",
            symmetric="../nve4x4x4/relax/CONTCAR",
            jt="./relax_vasp544/CONTCAR",
            hdf5dir="../../vasp/embeded/nv{0}x{0}x{0}".format(embeded_size),
            name="hse")

        pbe_poscars = dict(
            bulk="../../vasp/nve4x4x4/relax/POSCAR",
            symmetric="../../vasp/nve4x4x4/relax/CONTCAR",
            jt="../../vasp/nve4x4x4jt/relax/CONTCAR",
            hdf5dir="../../vasp/embeded/nv{0}x{0}x{0}".format(embeded_size),
            name="pbe")

        for name, poscars in [
                ["hse", hse_poscars], ["pbe", pbe_poscars]]:

            f, axes = plt.subplots(2, 1, figsize=(10, 12), sharex=True)
            print("="*70)
            print(" "*20, name)
            print("="*70)

            data = calculate_hr_factors(
                poscars["symmetric"], poscars["jt"], poscars["bulk"],
                poscars["hdf5dir"], name=poscars["name"])

            hr_factors = data["HR_factors"]
            indx = np.where(hr_factors < np.inf)
            freq = data["frequencies"]
            vc = data["Ejt"][indx]

            freq = freq[indx]
            hr_factors = hr_factors[indx]

            axes[0].scatter(freq, hr_factors)
            x, y = gaussian_smoothing(freq, hr_factors)
            ax0 = axes[0].twinx()
            ax0.plot(x, y, label="S={:.4f}".format(
                sum(data["HR_factors"])))
            ax0.set_title("HR-Factors " + "{0} {1}x{1}x{1}".format(
                name, embeded_size))
            axes[0].set_xlim(0, 180)
            axes[0].set_ylim(bottom=0)
            ax0.set_ylim(bottom=0)
            axes[0].set_ylabel("HR factors")

            axes[1].scatter(freq, vc)
            axes[1].set_xlabel("Freq (meV)")
            axes[1].set_ylabel(r"$E_{JT}$")
            axes[1].set_ylim(bottom=0, top=max(vc)*1.1)
            # axes[1].set_ylim(bottom=0)
            x, y = gaussian_smoothing(freq, vc)
            ax1 = axes[1].twinx()
            ax1.plot(x, y)
            ax1.set_ylim(bottom=0)
            ax0.legend()
            axes[1].set_title("$E_{JT}$")

            plt.tight_layout()
            plt.savefig("jt_data/vibronic_constants_{}_{}.pdf".format(
                name, embeded_size))
            plt.clf()

            tosave = np.array([freq, hr_factors]).T
            np.savetxt("jt_data/hr_factors_{}_{}.dat".format(
                name, embeded_size), tosave)
