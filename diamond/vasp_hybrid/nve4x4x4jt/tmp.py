import sys
import os
import numpy as np
import matplotlib.pyplot as plt

sys.path.append("../../../dephon")
sys.path.append("../../../dejt")
from dedisp import (calculate_fc_displacements, read_and_allign_embeded_cell,
                    iter_modes, read_displacements, bohr_in_angstr,
                    get_mapping_between_geometries)
from vasp import Poscar
from deutils import ensure_dir

cell_size = 4
only_irrep = "A2"

poscar1 = "../nv4x4x4/relax/CONTCAR"
poscar2 = "./relax/CONTCAR"
poscar3 = "./relax_from_e_displ/CONTCAR"
vasprun1 = "../nv4x4x4/excited_jt_geometry/vasprun.xml"
vasprun2 = "./relax/vasprun.xml"
bulkg_poscar = "../nv4x4x4/bulk_geometry/POSCAR"
hdf5dir = "../../vasp/embeded/nv{0}x{0}x{0}".format(cell_size)


freq, fc_displ, irreps = calculate_fc_displacements(
    poscar1, poscar2, bulkg_poscar, hdf5dir, units="au")
indx = np.where(irreps == only_irrep)[0]
plt.scatter(freq[indx], fc_displ[indx], label="displ", alpha=0.5,
            color="blue", marker="X")

freq, fc_displ, irreps = calculate_fc_displacements(
    poscar1, poscar2, bulkg_poscar, hdf5dir, units="au")
indx = np.where(irreps == only_irrep)[0]
plt.scatter(freq[indx], fc_displ[indx], label="displ", alpha=0.5,
            color="blue", marker="X")

freq, fc_displ, irreps = calculate_fc_displacements(
    poscar1, poscar3, bulkg_poscar, hdf5dir, units="au")
indx = np.where(irreps == only_irrep)[0]
plt.scatter(freq[indx], fc_displ[indx], label="displ_new", alpha=0.5,
            color="green", marker="D")

freq, fc_displ, irreps = calculate_fc_displacements(
    vasprun1, vasprun2, bulkg_poscar, hdf5dir, units="au",
    from_forces=True)
indx = np.where(irreps == only_irrep)[0]
plt.scatter(freq[indx], fc_displ[indx], label="forces", alpha=0.5,
            color="red")

plt.xlim(0, 0.008)
plt.legend()
plt.show()
