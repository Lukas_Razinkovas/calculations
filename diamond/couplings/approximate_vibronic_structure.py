import sys
import os
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
sys.path.append("../../dephon")
sys.path.append("../../dejt")
from deutils import gaussian_smoothing
from vibronic import calculate_vibronic_levels
from visual import plot_luminescence
import copy


# au to meV
conv = 27211.389804354127


def load_vibronic_data(filepath):
    data = np.genfromtxt(filepath, delimiter=",", autostrip=True)
    freq, freq_mev, k, ejt, v = data.T
    keff = np.sqrt(sum(k**2))
    freq_eff = sum(freq*(k**2))/(keff**2)
    freq_mev_eff = sum(freq_mev*(k**2))/(keff**2)

    data = dict(freq=freq, freq_mev=freq_mev, k=k, ejt=ejt, v=v, keff=keff,
                freq_eff=freq_eff, freq_mev_eff=freq_mev_eff)

    print("-"*70)
    print(filepath)
    print("-"*70)
    print("nr_of_doubl:\t", len(data["freq"]))
    print("keff:   \t {:.2f}".format(data["keff"]))
    print("eff_freq: \t {:.2f} meV".format(data["freq_mev_eff"]))

    return data


def load_calculated_results():
    evals = np.loadtxt("full_vibronic_spectrum/vals.dat")*conv
    evecs = np.loadtxt("full_vibronic_spectrum/vecs.dat")*conv
    h0 = np.loadtxt("full_vibronic_spectrum/h0.dat")*conv
    if np.isscalar(evals):
        evals = np.array([evals])
        evecs = np.array([evecs])

    return evals, evecs, h0


def gaussian(x, mu, sig):
    return 1/(sig*np.sqrt(2*np.pi))*np.exp(-np.power(x - mu, 2.) / (
        2 * np.power(sig, 2.)))


def func(x, *popt):
    mylen = int((len(popt) - 1)/2)
    ks = np.array(popt[:mylen])
    ms = np.array(popt[mylen:-1])
    sig = popt[-1]

    val = 0
    for k, m in zip(ks, ms):
        val += k*gaussian(x, m, sig)

    return val


def approximate_ks(x, y, peaks=3):
    bndsd = tuple([0]*peaks + [10]*peaks + [0])
    bndsu = tuple([10]*peaks + [180]*peaks + [20])

    p0 = np.concatenate((np.ones(peaks), np.linspace(20, 175, peaks), [1]))

    popt, pcov = curve_fit(func, x, y, p0=p0, bounds=(bndsd, bndsu))

    ks = np.zeros(peaks)
    fs = np.zeros(peaks)
    ks, fs, sig = popt[0:peaks], popt[peaks:2*peaks], popt[-1]

    args = list([i for i in popt])

    def foo(x):
        return func(x, *args)

    return fs, ks, sig, foo


if __name__ == "__main__" and True:
    filepath = ("hse_nv4x4x4/asymmetric/from_forces/"
                "nv15x15x15_vibronic_couplings.dat")
    filepath = ("hse_nv4x4x4/asymmetric/from_displacements/"
                "nv15x15x15_vibronic_couplings.dat")

    data = load_vibronic_data(filepath)
    freq_mev = data["freq_mev"]
    freq = data["freq"]
    k = data["k"]
    keff = data["keff"]
    freq_eff = data["freq_eff"]

    ##############################
    #       Approximation
    ##############################

    sig = 3
    x, y = gaussian_smoothing(freq_mev, k**2, sig=sig, xlim=[0, 300])
    fs, ks, sig, foo = approximate_ks(x, y, peaks=20)

    fig, (ax, ax3) = plt.subplots(2, 1, figsize=(15, 15))
    ax.plot(x, y, label="direct", lw=4, alpha=0.5)
    ax.plot(x, foo(x), label="approximate")
    ax2 = ax.twinx()
    ax2.stem(fs, ks)
    ax.set_ylim(bottom=0)
    ax2.set_ylim(bottom=0)

    ##############################
    #     Vibronic spectrum
    ##############################

    fs /= conv
    ks = np.sqrt(ks)
    evals, evecs, h00 = calculate_vibronic_levels(3, len(fs), fs, dvc=ks)
    evals *= conv
    h00 *= conv

    ax31 = ax3.twinx()
    # _, freq, stren = plot_luminescence(
    #     evals, evecs, h00, ax31, xlim=[-500, 50])
    print("Smoothing")
    _, freq, stren = plot_luminescence(
        evals, evecs, h00, ax3, xlim=[-500, 50])

    # print("Smoothing")
    sig = 4
    x, y = gaussian_smoothing(freq[1:], stren[1:], n=10000, sig=sig,
                              xlim=[-500, -5])
    print("Smoothing 2")
    x0, y0 = gaussian_smoothing(freq[:1], stren[:1], sig=0.5, xlim=[-5, 5])
    print("Finished")

    # x, y = gaussian_smoothing(freq, stren, sig=3, xlim=[-500, 20], n=10000)

    x = np.concatenate((x, x0))
    y = np.concatenate((y, y0))

    ax3.set_ylim(bottom=0)
    ax31.plot(x, y)
    ax31.set_ylim([0, 0.01])
    # ax31.plot(x0, y0)

    # ax31.set_ylim(bottom=0)
    # freq = freq.T
    y /= np.trapz(y, x=x)

    np.savetxt("lum.dat", np.array([x, y]).T)

    ax.legend()
    plt.show()


if __name__ == "__main__" and False:

    sig = 2
    xlim = [-400, 0]

    ##############################
    #     Vibronic coulings
    ##############################

    filepath = ("hse_nv3x3x3/asymmetric/from_forces/"
                "nv3x3x3direct_vibronic_couplings.dat")
    data = load_vibronic_data(filepath)
    freq_mev = data["freq_mev"]
    freq = data["freq"]
    k = data["k"]
    keff = data["keff"]
    freq_eff = data["freq_eff"]

    fig, axes = plt.subplots(2, 6, figsize=(50, 10))
    print("Loading")
    eigenvalues, eigenvectors, h0 = load_calculated_results()
    x, y = gaussian_smoothing(freq_mev, k**2, sig=sig)

    print("Luminescence")
    _, _f, _s = plot_luminescence(
        eigenvalues, eigenvectors, h0, None,
        color="blue", label="full", xlim=xlim)

    sig = 4.5
    print("Smoothing")
    x, y = gaussian_smoothing(_f[1:], _s[1:], sig=sig, xlim=[-500, -5])
    print("Smoothing2")
    x0, y0 = gaussian_smoothing(_f[:1], _s[:1], sig=0.5, xlim=[-5, 5])
    print("ok")

    x = np.concatenate((x, x0))
    y = np.concatenate((y, y0))

    print("saving")
    np.savetxt("lum.dat", np.array([x, y/np.trapz(y, x=x)]).T)

    exit()
    # np.savetxt("lum.dat", np.array([x, y]).T)

    for nr, l in enumerate(["effective", 1, 2, 3, 5, 13]):
        axes[0][nr].plot(x, y, color="blue", label="full")
        axes[0][nr].set_xlabel("Freq (meV)")
        axes[0][nr].set_ylabel(r"$k^2$")

        _, _f, _s = plot_luminescence(
            eigenvalues, eigenvectors, h0, axes[1][nr], smooth=sig,
            color="blue", label="full", xlim=xlim)

        if l == "effective":
            np.savetxt("lum.dat", np.array([_f, _s]).T)
            print("OK!!!!")
            evals, evecs, h00 = calculate_vibronic_levels(
                25, 1, freq=[freq_eff], dvc=[keff])
            aks = np.array([keff**2])
            afreq = np.array([freq_eff])
            def foo(a):
                return a*0
        else:

            if l < 3:
                max_particles = 10
            else:
                max_particles = 6

            if l > 5:
                max_particles = 3

            afreq, aks, asig, foo = approximate_ks(x, y, l=l)
            afreq /= conv
            evals, evecs, h00 = calculate_vibronic_levels(
                max_particles, len(aks), freq=afreq, dvc=np.sqrt(aks))

        # else:
        #     afreq, aks, asig, foo = approximate_ks(x, y, l=l)
        #     evals, evecs, h00 = calculate_vibronic_levels(
        #         10, len(aks), freq=afreq, dvc=np.sqrt(aks))
        # afreq *= conv

        axes[0][nr].plot(x, foo(x), color="red")
        ax2 = axes[0][nr].twinx()
        markerline, stemlines, baseline = ax2.stem(afreq*conv, aks)
        plt.setp(baseline, 'linewidth', 0)
        ax2.set_ylim(bottom=0)
        axes[0][nr].set_ylim(bottom=0)

        
        # axes[0][nr].plot(foo(x))
        # aks = np.sqrt(aks)

        evals *= conv
        h00 *= conv
        plot_luminescence(
            evals, evecs, h00, axes[1][nr], smooth=sig,
            color="red", label="approximate", xlim=xlim)

        # if nr == 2:
        #     break

        
        

        # plot_luminescence(
        #     eigenvalues, eigenvectors, h0, axes[2][nr], smooth=sig,
        #     color="blue", label="all 3x3x3 doublets", xlim=xlim)

            


        


    # ax.plot(x, y, )
    # ax.plot(x, foo(x), color="red")

    

    # afreq /= conv
    # aks = np.sqrt(aks)

    # print("Calculating vinronic levels")
    # eigenvalues, eigenvectors, h0 = calculate_vibronic_levels(
    #     10, len(aks), freq=afreq, dvc=aks)
    # print("done")

    # eigenvalues *= conv
    # h0 *= conv

    # _, freq, stren = plot_luminescence(
    #     eigenvalues, eigenvectors, h0, ax2, smooth=sig,
    #     color="red", label="???", xlim=xlim)

    plt.tight_layout()
    plt.savefig("figs/approximations2.pdf")
    plt.clf()

    # np.savetxt("lum.dat", np.array([freq.T[0], stren]).T)
    # print("Plotted 2")

    # ax.legend()
    # ax.set_ylim(bottom=0)
    # ax2.legend()
    # ax2.set_ylim(bottom=0)
    # ax2.set_xlabel("freq (meV)")
    # ax.set_ylabel("Vibronic luminescence")
    # ax2.set_ylabel(r"Vibronic luminescence ($\sigma = {:.2f}$ meV)".format(
    #     sig
    # ))
    # plt.tight_layout()
    # plt.show()


# Local Variables:
# compile-command: "mpirun -np 6 python approximate_vibronic_structure.py"
# End:
