.import sys
import os
import matplotlib.pyplot as plt
import numpy as np
sys.path.append("../../dephon")
sys.path.append("../../dejt")
from deutils import gaussian_smoothing, ensure_dir


def load_vibronic_data(filepath):
    data = np.genfromtxt(filepath, delimiter=",", autostrip=True)
    freq, freq_mev, k, ejt, v = data.T
    keff = np.sqrt(sum(k**2))
    freq_eff = sum(freq*(k**2))/(keff**2)
    freq_mev_eff = sum(freq_mev*(k**2))/(keff**2)

    return dict(freq=freq, freq_mev=freq_mev, k=k, ejt=ejt, v=v, keff=keff,
                freq_eff=freq_eff, freq_mev_eff=freq_mev_eff)


sig = 6
fig, (ax, ax2) = plt.subplots(2, 1, figsize=(20, 20), sharex=True)
axtw = ax.twinx()
axtw2 = ax2.twinx()


def calc_iterator():
    for relax_name in ["hse_nv3x3x3", "hse_nv4x4x4"][:1]:
        for calc_type in ["from_displacements", "from_forces"][1:]:
            vibronic_dir = os.path.join(
                relax_name, "asymmetric", calc_type)
            for file_name in os.listdir(vibronic_dir):
                if "vibronic_couplings.dat" not in file_name:
                    continue
                filepath = os.path.join(vibronic_dir, file_name)
                name = "{} {} {}".format(
                    file_name.replace("_vibronic_couplings.dat", ""),
                    "(" + relax_name.replace("_", ":") + ")",
                    calc_type.replace("displacements", "displ"))

                yield filepath, name

ensure_dir("figs")

if __name__ == "__main__":
    for filepath, name in calc_iterator():
        # print(filepath)
        if "direct" in name:
            continue
        # if "direct" in name or "4" in name or "6" in name:
        #     continue
        data = load_vibronic_data(filepath)

        print("-"*70)
        print(name)
        print("-"*70)
        print("nr_of_doubl:\t", len(data["freq"]))
        print("keff:   \t {:.2f}".format(data["keff"]))
        print("eff_freq: \t {:.2f} meV".format(data["freq_mev_eff"]))

        # ax.scatter(data["freq_mev"], data["k"], label=name)
        # ax2.scatter(data["freq_mev"], data["k"]**2, label=name)
        x, y = gaussian_smoothing(data["freq_mev"], data["k"], sig=sig)
        axtw.plot(
            x, y, "--",
            label=name + r" $\omega_{{eff}}={:.2f}, k_{{eff}}={:.2f}$".format(
                data["freq_mev_eff"], data["keff"]),
            lw=8 if "displ" in name else 4,
            alpha=0.3 if "displ" in name else 1)
        x, y = gaussian_smoothing(data["freq_mev"], data["k"]**2, sig=sig)
        axtw2.plot(
            x, y, "--",
            label=name + r" $\omega_{{eff}}={:.2f}, k_{{eff}}={:2f}$".format(
                data["freq_mev_eff"], data["keff"]),
            lw=8 if "displ" in name else 4,
            alpha=0.3 if "displ" in name else 1)

    for a in [axtw, axtw2]:
        a.set_ylim(bottom=0)
        a.legend()

    for a in [ax, ax2]:
        a.set_ylim(bottom=0)

    ax.set_title("NV vibronic coupling (smoothing $6$ meV)")
    ax.set_ylabel("Dimensionless Vibronic Constant ($k$)")
    ax2.set_ylabel("$k^2$")
    ax2.set_xlabel("Frequency (meV)")
    plt.tight_layout()
    plt.savefig("figs/comparison_of_cell_sizes3x3x3.pdf")
