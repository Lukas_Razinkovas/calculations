import sys
import os
import matplotlib.pyplot as plt
import numpy as np
sys.path.append("../../dephon")
sys.path.append("../../dejt")
from deutils import gaussian_smoothing
from vibronic_mpi import calculate_vibronic_levels, rank
from visual import plot_luminescence


def load_vibronic_data(filepath):
    data = np.genfromtxt(filepath, delimiter=",", autostrip=True)
    freq, freq_mev, k, ejt, v = data.T
    keff = np.sqrt(sum(k**2))
    freq_eff = sum(freq*(k**2))/(keff**2)
    freq_mev_eff = sum(freq_mev*(k**2))/(keff**2)

    return dict(freq=freq, freq_mev=freq_mev, k=k, ejt=ejt, v=v, keff=keff,
                freq_eff=freq_eff, freq_mev_eff=freq_mev_eff)


sig = 10
fig, (ax, ax2) = plt.subplots(2, 1, figsize=(20, 20), sharex=True)
axtw = ax.twinx()
axtw2 = ax2.twinx()

filepath = ("hse_nv3x3x3/asymmetric/from_forces/"
            "nv3x3x3direct_vibronic_couplings.dat")

if __name__ == "__main__":
    data = load_vibronic_data(filepath)

    if rank == 0:
        print("-"*70)
        print(filepath)
        print("-"*70)
        print("nr_of_doubl:\t", len(data["freq"]))
        print("keff:   \t {:.2f}".format(data["keff"]))
        print("eff_freq: \t {:.2f} meV".format(data["freq_mev_eff"]))

    freq_mev = data["freq_mev"]
    freq = data["freq"]
    k = data["k"]
    conv = freq_mev[10]/freq[10]
    eigenvalues, eigenvectors, h0, errors = calculate_vibronic_levels(
        2, len(freq), freq, dvc=k, j=0.5, nvals=1)

    keff = np.sqrt(sum(k**2))
    freq_eff = sum(k**2 * freq)/(keff**2)

    eff_eigenvalues, eff_eigenvectors, eff_h0, _ = calculate_vibronic_levels(
        40, 1, [freq_eff], dvc=[keff], j=0.5, nvals=1)
    sig = 6
    if rank == 0:
        np.savetxt("vecs.dat", eigenvectors)
        np.savetxt("vals.dat", eigenvalues)
        np.savetxt("h0.dat", h0)
        eigenvalues *= conv
        h0 *= conv

        plot_luminescence(eigenvalues, eigenvectors, h0, ax,
                          color="blue", label="all", xlim=[-500, 0])
        plot_luminescence(eigenvalues, eigenvectors, h0, ax2, smooth=sig,
                          color="blue", label="all", xlim=[-500, 0])

        eff_eigenvalues *= conv
        eff_h0 *= conv

        xlim = [-500, 0]
        plot_luminescence(eff_eigenvalues, eff_eigenvectors, eff_h0, ax,
                          color="red", label="keff={:.4f}".format(keff),
                          xlim=xlim)
        plot_luminescence(eff_eigenvalues, eff_eigenvectors, eff_h0, ax2,
                          smooth=sig,
                          color="red", label="keff={:.4f}".format(keff),
                          xlim=xlim)

        ax.legend()
        ax.set_ylim(bottom=0)
        ax2.legend()
        ax2.set_ylim(bottom=0)
        ax2.set_xlabel("freq (meV)")
        ax.set_ylabel("Vibronic luminescence")
        ax2.set_ylabel(r"Vibronic luminescence ($\sigma = {:.2f}$ meV)".format(
            sig
        ))
        plt.tight_layout()
        plt.savefig("test.pdf")



# Local Variables:
# compile-command: "mpirun -np 6 python approximate_vibronic_structure.py"
# End:
