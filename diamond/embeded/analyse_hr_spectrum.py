import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from yaml import dump
sys.path.append("../../dephon")
from dedisp import calculate_couplings
from deutils import ensure_dir, gaussian_smoothing
from scipy.stats import gaussian_kde


poscar1 = "../vasp_hybrid_new/nv4x4x4/relaxed/CONTCAR"
poscar2 = "../vasp_hybrid_new/nve4x4x4sym/relax/CONTCAR"
vasprun1 = "../vasp_hybrid_new/nve4x4x4sym/relaxed/vasprun.xml"
vasprun2 = "../vasp_hybrid_new/nv4x4x4/excited_sym_geometry/vasprun.xml"
bulkg_poscar = "../vasp_hybrid_new/nv4x4x4/bulk_geometry/POSCAR"

dirs = [
    # "../vasp/embeded/nv10x10x10",
    # "nv10x10x10superlat2",
    # "nv10x10x10superlat2inv",
    # "nv10x10x10superlat4",
    # "nv10x10x10superlat4inv",
    # "10x10x10c13",
    "nv10x10x10superlat4shift-1",
    "nv10x10x10superlat4invshift-1"
]


def save_vibrational_data(data, outdir):
    ensure_dir(outdir)
    outfile = os.path.join(outdir, "vibrational_couplings.dat")

    dtypes = [
        ('freq', float),
        ('freq_mev', float),
        ('fc_displ', float),
        ('hr_factors', float),
        ('irreps', 'U6')]

    fmt = ", ".join(4*['%13.9f']) + ', %6s'
    couplings_data = np.zeros(
        data["freq"].size,
        dtype=dtypes)

    couplings_data["freq"] = data["freq"]
    couplings_data["freq_mev"] = data["freq_mev"]
    couplings_data["fc_displ"] = data["fc_displ"]
    couplings_data["hr_factors"] = data["hr_factors"]
    couplings_data["irreps"] = data["irreps"]
    header = " ".join(5*['{:13}']).format(
        "freq (au),",
        "freq (meV),",
        "fc_displ (au),",
        "hr_factors,",
        "irrep")

    np.savetxt(
        outfile,
        couplings_data,
        header=header,
        fmt=fmt)

    info_data = {}
    info_data["Total modes"] = len(couplings_data["freq"])
    info_data["A1 modes"] = len(
        np.where(couplings_data["irreps"] == "A1")[0])
    info_data["A2 modes"] = len(
        np.where(couplings_data["irreps"] == "A2")[0])
    info_data["Exy modes"] = len(
        np.where(couplings_data["irreps"] == "Exy")[0])
    info_data["S_tot"] = float(data["hr_tot"])
    yaml_file = os.path.join(outdir, "vibrational_info.yaml")

    with open(yaml_file, "wt") as f:
        f.write(dump(info_data, default_flow_style=False))


def load_vibrational_data(outdir):
    filepath = os.path.join(outdir, "vibrational_couplings.dat")
    data = np.genfromtxt(filepath, delimiter=",", autostrip=True).T
    return {
        "freq": data[0],
        "freq_mev": data[1],
        "fc_displ": data[2],
        "hr_factors": data[3]
    }

    # freq, freq_mev, k, ejt, v = data.T
    # keff = np.sqrt(sum(k**2))
    # freq_eff = sum(freq*(k**2))/(keff**2)
    # freq_mev_eff = sum(freq_mev*(k**2))/(keff**2)

    # return dict(freq=freq, freq_mev=freq_mev, k=k, ejt=ejt, v=v, keff=keff,
    #             freq_eff=freq_eff, freq_mev_eff=freq_mev_eff)


if False:
    for hdf5dir in dirs[1:]:
        data = calculate_couplings(
            poscar1, poscar2, bulkg_poscar, hdf5dir=hdf5dir,
            forces_output1=vasprun1,
            forces_output2=vasprun2,
            from_forces=True, vibronic=False)

        outdir = os.path.join("couplings", os.path.split(hdf5dir)[-1])
        save_vibrational_data(data, outdir)


dirs = [
    "../vasp/embeded/nv10x10x10",
    "nv10x10x10superlat2",
    "nv10x10x10superlat2inv",
    "nv10x10x10superlat4",
    "nv10x10x10superlat4inv",
    "nv10x10x10superlat4shift-1",
    "nv10x10x10superlat4invshift-1",
    "10x10x10c13",
]

names = {
    "nv10x10x10": r"10x10x10($^{12}C$)",
    "nv10x10x10superlat2": "10x10x10_2lrsprlt",
    "nv10x10x10superlat2inv": "10x10x10_2lrsprlt2",
    "nv10x10x10superlat4": "10x10x10_4lrsprlt",
    "nv10x10x10superlat4inv": "10x10x10_4lrsprlt2",
    "10x10x10c13": r"10x10x10($^{13}C$)",
    "nv10x10x10superlat4shift-1": "10x10x10_4lrsprlts-1",
    "nv10x10x10superlat4invshift-1": "10x10x10_4lrsprlt2s-1"
}

f, axes = plt.subplots(2, 3, figsize=(20, 10))
for hdf5dir in dirs:
    outdir = os.path.join("couplings", os.path.split(hdf5dir)[-1])
    data = load_vibrational_data(outdir)

    freq = data["freq_mev"]
    hr = data["hr_factors"]

    nm = names[os.path.split(hdf5dir)[-1]]
    if nm in {r"10x10x10($^{12}C$)", r"10x10x10($^{13}C$)"}:
        my_axes = [0, 1, 2]
    elif "4lrspr" in nm:
        my_axes = [1]
    elif "2lrspr" in nm:
        my_axes = [0]
    if "s-1" in nm:
        my_axes = [2]

    x, y = gaussian_smoothing(freq, hr, sig=4, xlim=[0, 200])
    for i in my_axes:
        axes[0][i].plot(x, y, label=nm + " $S={:.2f}$".format(sum(hr)))

    kde = gaussian_kde(freq)
    kde.set_bandwidth(0.055)
    x = np.linspace(0, 200, 2000)
    for i in my_axes:
        axes[1][i].plot(x, kde(x), label=nm)

for ax in axes.flatten():
    ax.legend()
    ax.set_xlabel("Freq (meV)")
    ax.set_ylim(bottom=0)

for ax in axes[0]:
    ax.set_ylabel("S")

for ax in axes[1]:
    ax.set_ylabel("DOS")


plt.tight_layout()
ensure_dir("figs")
plt.savefig("figs/10x10x10superlattices.pdf", dpi=200)
plt.clf()





