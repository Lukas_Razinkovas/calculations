import os
import h5py
from scipy.stats import gaussian_kde
import numpy as np
import matplotlib.pyplot as plt


def read_dos(filepath):
    assert os.path.exists(filepath), filepath
    f = h5py.File(filepath, "r")

    freq = f["frequencies"][:].T[0]
    f.close()
    kde = gaussian_kde(freq)
<<<<<<< HEAD
    kde.set_bandwidth(0.05)
    x = np.linspace(0, 200, 1000)
=======
    kde.set_bandwidth(0.055)
    x = np.linspace(0, 200, 2000)
>>>>>>> 2b1077c786800d4da32f5179e61931ef7853779a
    return x, kde(x)


if __name__ == "__main__":
<<<<<<< HEAD
    x, y = read_dos("../vasp/embeded/10x10x10/modes.hdf5")
    plt.plot(x, y, label="2 layers superlattice")
    # x, y = read_dos("..//6x6x6/modes.hdf5")
    # plt.plot(x, y, label="diamond C12")
=======
    dim = 15
    f, (ax, ax2) = plt.subplots(2, 1)
    # x, y = read_dos("{0}x{0}x{0}superlat2/modes.hdf5".format(dim))
    x, y = read_dos("../vasp/embeded/15x15x15/modes.hdf5".format(dim))
    ax.plot(x, y, label="2 layers superlattice")
    # x2, y2 = read_dos("{0}x{0}x{0}/modes.hdf5".format(dim))
    # ax.plot(x2, y2, label="diamond C12")
    # ax2.plot(x, y - y2)
    ax.set_xlim([0, 20])
    ax.set_ylim(bottom=0)
    ax.legend()

    plt.ylim(bottom=0)
>>>>>>> 2b1077c786800d4da32f5179e61931ef7853779a
    plt.legend()
    plt.show()
