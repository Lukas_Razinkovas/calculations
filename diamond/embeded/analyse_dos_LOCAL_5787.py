import os
import h5py
from scipy.stats import gaussian_kde
import numpy as np
import matplotlib.pyplot as plt


def read_dos(filepath):
    assert os.path.exists(filepath), filepath
    f = h5py.File(filepath, "r")

    freq = f["frequencies"][:].T[0]
    f.close()
    kde = gaussian_kde(freq)
    kde.set_bandwidth(0.05)
    x = np.linspace(0, 200, 1000)
    return x, kde(x)


if __name__ == "__main__":
    x, y = read_dos("../vasp/embeded/10x10x10/modes.hdf5")
    plt.plot(x, y, label="2 layers superlattice")
    # x, y = read_dos("..//6x6x6/modes.hdf5")
    # plt.plot(x, y, label="diamond C12")
    plt.legend()
    plt.show()
