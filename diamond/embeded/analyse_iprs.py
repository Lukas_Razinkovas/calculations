import sys
import os
sys.path.append("../../dephon")
import logging
from demodes import Modes
logging.basicConfig(level=logging.DEBUG)


directories = [
    "nv10x10x10superlat2",
    "nv10x10x10superlat2inv",
    "nv10x10x10superlat4",
    "nv10x10x10superlat4inv",
    "10x10x10c13"
]

for directory in directories:
    with open("log.log", "at") as f:
        f.write(directory + "\n")

    print(directory)
    fl = os.path.join(directory, "freq.dat")
    print("Reading modes")
    modes = Modes(hdf5dir=directory)
    print("Modes read")
    modes.analyse_mode_by_mode(write_to_file=fl)
    modes.save_to_hdf5(directory)
    modes.analyse_weights_and_save_to_hdf5(directory, logfile="log2.log")
