import sys
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pprint
sys.path.append("../dephon")
from defc import ForceConstants

cm_to_mev = 1.23981e-1
calc = 'pwscf'
nr = 5
fc_dir = "../diamond/{1}/{0}x{0}x{0}/phonopy_phonons/".format(nr, calc)


def plot_difference(modes, modes2, dos, freq, dos2, freq2):
    ax = plt.subplot2grid((3, 6), (0, 0), colspan=3, rowspan=3)
    ax1 = plt.subplot2grid((3, 6), (0, 4), colspan=2, rowspan=1)
    ax2 = plt.subplot2grid((3, 6), (1, 4), colspan=2, rowspan=2)
    ax3 = plt.subplot2grid((3, 6), (0, 3), colspan=1, rowspan=3,
                           sharey=ax)
    ax1.yaxis.tick_right()
    ax2.yaxis.tick_right()
    ax1.yaxis.set_label_position("right")
    ax2.yaxis.set_label_position("right")

    ax3.set_xlabel("DOS", labelpad=20)
    ax3.grid(True, alpha=0.5, color="gray")
    ax3.yaxis.set_ticks_position('both')
    plt.setp(ax3.get_xticklabels(), visible=False)
    plt.setp(ax3.get_yticklabels(), visible=False)

    modes.compare_phonon_dispersion(modes2, ax, lw=0.7)
    modes.compare_phonon_dispersion(modes2, ax1, lw=0.7)
    intervals = modes.compare_phonon_dispersion(modes2, ax2, lw=0.7)
    ax1.xaxis.label.set_visible(False)

    colors = ["darkmagenta", "darkcyan"]

    ax3.plot(dos, freq*cm_to_mev,
             color=colors[0],
             linewidth=0.7)
    ax3.plot(dos2, freq2*cm_to_mev,
             color=colors[1],
             linewidth=0.7)
    ax.legend(loc=4)

    ##############################
    #  PATCH 1
    ##############################

    y = 151
    dy = 12

    x = intervals[2][0]
    dx = intervals[3][1] - x

    ax.add_patch(
        patches.Rectangle(
            (x, y),
            dx,
            dy,
            fill=False,
            linestyle="--",
            color="gray",
        )
    )

    ax1.set_xlim([x, x+dx])
    ax1.set_ylim([y, y+dy])

    ##############################
    #  PATCH 2
    ##############################

    x = intervals[0][0]
    dx = intervals[0][1] - x

    y = 0
    dy = 80

    ax.add_patch(
        patches.Rectangle(
            (x, y),
            dx,
            dy,
            fill=False,
            linestyle="--",
            color="gray"
        )
    )

    ax2.set_xlim([x, x+dx])
    ax2.set_ylim([y, y+dy])

bulk = ForceConstants(calc, fc_dir)
distances = sorted(
    [float(i) for i in bulk.atoms.sort_by_distance().keys()])[2:]

f = plt.figure(figsize=(12, 5))
for cut in distances:
    cut += 0.01
    bulk = ForceConstants(calc, fc_dir)
    fc = ForceConstants(calc, fc_dir)
    bulk.atoms.calculate_symmetry()
    fc.atoms.calculate_symmetry()
    modes = bulk.calculate_modes()

    fc.perform_cutoff(cut)
    fc.symmetrize_fc()
    modes2 = fc.calculate_modes()

    freq, dos = bulk.calculate_pw_dos()
    freq2, dos2 = fc.calculate_pw_dos()
    plot_difference(modes, modes2, dos, freq, dos2, freq2)
    plt.tight_layout()
    plt.savefig("cutoff{}.pdf".format(cut), dpi=100)
    plt.clf()
