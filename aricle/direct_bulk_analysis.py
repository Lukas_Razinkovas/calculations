import sys
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pprint
sys.path.append("../dephon")
from defc import ForceConstants
cm_to_mev = 1.23981e-1
pp = pprint.PrettyPrinter()

f = plt.figure(figsize=(12, 5))
ax = plt.subplot2grid((3, 6), (0, 0), colspan=3, rowspan=3)

ax1 = plt.subplot2grid((3, 6), (0, 4), colspan=2, rowspan=1)
ax2 = plt.subplot2grid((3, 6), (1, 4), colspan=2, rowspan=2)
ax1.yaxis.tick_right()
ax2.yaxis.tick_right()
ax1.yaxis.set_label_position("right")
ax2.yaxis.set_label_position("right")

ax3 = plt.subplot2grid((3, 6), (0, 3), colspan=1, rowspan=3,
                       sharey=ax)


def _add_cm_axis(ax):
    ax.set_ylabel("meV")
    # mn, mx = ax.get_ylim()
    # cmax = ax.twinx()
    # cmax.set_ylim(mn/cm_to_mev, mx/cm_to_mev)
    # cmax.set_ylabel("cm$^{-1}$")
    # lim = cmax.get_xlim()
    # cmax.set_xticks(np.arange(*lim, 100))


markers = [None, None, None, None]
markerevery = [None, None, None, None]
linestyles = ["-", "-", "-", "-"]
colors = ['red', "green", "blue", "dimgray"]
alphas = [0.8, 0.8, 0.8, 0.9]
dashesl = [(None, None), (None, None), (None, None), (5, 5)]


def plot(ax, color, linestyle, marker, dashes, markevery, alpha):
    fc.calculate_bulk_band_structure(ax=ax, color=color,
                                     label="{} atoms".format(atoms),
                                     linestyle=linestyle,
                                     marker=marker,
                                     markersize=3.5,
                                     dashes=dashes,
                                     markevery=markevery,
                                     linewidth=0.7,
                                     alpha=alpha)

color = colors.pop()
linestyle = linestyles.pop()
marker = markers.pop()
dashes = dashesl.pop()
markevery = markerevery.pop()
alpha = alphas.pop()
calc = 'pwscf'

for i in range(3, 6):
    fc_dir = "../diamond/{1}/{0}x{0}x{0}/phonopy_phonons/".format(i, calc)
    fc = ForceConstants(calc, fc_dir)
    atoms = i*i*i*8
    color = colors.pop()
    linestyle = linestyles.pop()
    marker = markers.pop()
    dashes = dashesl.pop()
    markevery = markerevery.pop()
    alpha = alphas.pop()
    plot(ax, color, linestyle, marker, dashes, markevery, alpha)

    frequency, dos = fc.calculate_pw_dos()

    if markevery is not None:
        markevery *= 5

    plot(ax1, color, linestyle, marker, dashes, markevery, alpha)
    plot(ax2, color, linestyle, marker, dashes, markevery, alpha)

    if markevery is not None:
        markevery *= 0.5

    ax3.plot(dos, frequency*cm_to_mev,
             color=color,
             linewidth=0.7,
             marker=marker, linestyle=linestyle, dashes=dashes,
             markevery=markevery, alpha=alpha, markersize=3.5)

ax3.set_xlabel("DOS", labelpad=20)
ax3.grid(True, alpha=0.5, color="gray")
ax3.yaxis.set_ticks_position('both')
plt.setp(ax3.get_xticklabels(), visible=False)
plt.setp(ax3.get_yticklabels(), visible=False)
# _add_cm_axis(ax3)


##############################
#   Gamma region optical
##############################

y = 151
dy = 12


x = 0.27
dx = 0.175

ax.add_patch(
    patches.Rectangle(
        (x, y),
        dx,
        dy,
        fill=False,
        linestyle="--",
        color="gray",
    )
)

ax1.set_xlim([x, x+dx])
ax1.set_ylim([y, y+dy])


x = 0.32
dx = 0.075

y = 0
dy = 2*dy

ax.add_patch(
    patches.Rectangle(
        (x, y),
        dx,
        dy,
        fill=False,
        linestyle="--",
        color="gray"
    )
)

ax2.set_xlim([x, x+dx])
ax2.set_ylim([y, y+dy])


_add_cm_axis(ax)
_add_cm_axis(ax1)
_add_cm_axis(ax2)
ax.set_xlabel("Wave vector")
ax2.set_xlabel("Wave vector")
ax.legend(loc=4)

plt.tight_layout()
plt.savefig("direct_bulk_comparison_pwscf.pdf", dpi=300)
